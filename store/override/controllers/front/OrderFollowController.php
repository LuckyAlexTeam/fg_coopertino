<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class OrderFollowController extends OrderFollowControllerCore
{

    /**
     * Assign template vars related to page content
     * @see FrontController::initContent()
     */
    public function initContent()
    {
        parent::initContent();

        $ordersReturn = OrderReturn::getOrdersReturn($this->context->customer->id);
        if (Tools::isSubmit('errorQuantity')) {
            $this->context->smarty->assign('errorQuantity', true);
        } elseif (Tools::isSubmit('errorMsg')) {
            $this->context->smarty->assign(
                array(
                    'errorMsg' => true,
                    'ids_order_detail' => Tools::getValue('ids_order_detail', array()),
                    'order_qte_input' => Tools::getValue('order_qte_input', array()),
                    'id_order' => (int)Tools::getValue('id_order'),
                )
            );
        } elseif (Tools::isSubmit('errorDetail1')) {
            $this->context->smarty->assign('errorDetail1', true);
        } elseif (Tools::isSubmit('errorDetail2')) {
            $this->context->smarty->assign('errorDetail2', true);
        } elseif (Tools::isSubmit('errorNotReturnable')) {
            $this->context->smarty->assign('errorNotReturnable', true);
        }

        $this->context->smarty->assign('ordersReturn', $ordersReturn);
        $this->context->smarty->assign('returnAllowed', (int)Configuration::get('PS_ORDER_RETURN'));

        $this->setTemplate(_PS_THEME_DIR_.'order-follow.tpl');
    }

}
