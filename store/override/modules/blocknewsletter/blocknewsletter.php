<?php

class BlocknewsletterOverride extends Blocknewsletter
{


	public function install()
	{
		if (!parent::install() || !$this->registerHook('showNesletter'))
			return false;

		return true;
	}

	public function hookShowNesletter($params)
	{
		return $this->hookFooter($params);
	}
}
