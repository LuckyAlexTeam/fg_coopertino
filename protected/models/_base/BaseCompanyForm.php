<?php

/**
 * This is the model class for table "company_form".
 *
 * The followings are the available columns in table 'company_form':
 * @property integer $id
 * @property integer $user_id
 * @property integer $position
 * @property string $scope_activity
 * @property string $workplace
 * @property integer $professional_experience
 * @property integer $travel_ability
 *
 * The followings are the available model relations:
 * @property User $user
 * @property PositionList $position0
 * @property AllProfexpiriensList $professionalExperience
 * @property CompanyLanguageToForm[] $companyLanguageToForms
 * @property ReasonToCompanyForm[] $reasonToCompanyForms
 */
class BaseCompanyForm extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CompanyForm the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'company_form';
	}

	/**
	 * @return array validation rules for model attributes.
	 */ 
	 
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, position, workplace, professional_experience, travel_ability', 'required'),
			array('user_id, position, professional_experience, travel_ability', 'numerical', 'integerOnly'=>true),
			array('scope_activity, workplace', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user_id, position, scope_activity, workplace, professional_experience, travel_ability', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'position0' => array(self::BELONGS_TO, 'PositionList', 'position'),
			'professionalExperience' => array(self::BELONGS_TO, 'AllProfexpiriensList', 'professional_experience'),
			'companyLanguageToForms' => array(self::HAS_MANY, 'CompanyLanguageToForm', 'company_form_id'),
			'reasonToCompanyForms' => array(self::HAS_MANY, 'ReasonToCompanyForm', 'company_form_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'position' => 'Position',
			'scope_activity' => 'Scope Activity',
			'workplace' => 'Workplace',
			'professional_experience' => 'Professional Experience',
			'travel_ability' => 'Travel Ability',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('position',$this->position);
		$criteria->compare('scope_activity',$this->scope_activity,true);
		$criteria->compare('workplace',$this->workplace,true);
		$criteria->compare('professional_experience',$this->professional_experience);
		$criteria->compare('travel_ability',$this->travel_ability);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}