<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="language" content="ru" />
<title>Админка</title>
<!-- blueprint CSS framework -->
<link rel="stylesheet" type="text/css" href="../css/style.css" media="screen, projection" />
<link rel="stylesheet" type="text/css" href="../css/jquery-ui.css" />
<!--[if lt IE 9]>
	<script src="http://css3-mediaqueries-js.googlecode.com/files/css3-mediaqueries.js"></script>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script type="text/javascript" src="../js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="../js/jquery-ui.js"></script>
<script type="text/javascript" src="../js/jquery.cookie.js"></script>
<link rel="icon" href="/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />
</head>
<body>
<div class="wrapper">
<? if ( Yii::app()->user->getState('rol') == 'admin' ) {  ?>

	<ul id="menu">
	<li>
		<a href="#">Языки</a>
		<ul>
			<li><a href="index.php?r=site/site&lan=en&view=main">Главная</a></li>
			<li><a href="index.php?r=site/site&lan=en&view=visitor">Посетитель</a></li>
			<li><a href="index.php?r=site/site&lan=en&view=classic">Классик</a></li>
			<li><a href="index.php?r=site/site&lan=en&view=vip">VIP</a></li>
			<li><a href="index.php?r=site/site&lan=en&view=corporate">Корпоратив</a></li>
			<li>Общие для пакетов</li>
			<li><a href="index.php?r=site/site&lan=en&view=general">Общее</a></li>
			<li><a href="index.php?r=site/site&lan=en&view=physical">Физические данные</a></li>
			<li><a href="index.php?r=site/site&lan=en&view=professional">Профессиональные данные</a></li>
			<li>Общие для страниц</li>
			<li><a href="index.php?r=site/site&lan=en&view=all">Общие для страниц</a></li>
			<li><a href="index.php?r=site/site&lan=en&view=select">Выпадающие списки select</a></li>
		</ul>
	</li>
	<li><a href="#">Конструктор блоков</a>
		<ul>
			<li><a href="index.php?r=site/block">Блоки</a></li>
			<li><a href="index.php?r=site/maket">Макеты</a></li>
			<li><a href="index.php?r=site/konst">Новый конструктор</a></li>
		</ul>
	</li>
	<li><a href="#">Статистика</a>
		<ul>
			<li><a href="./stat_site.html">Посещаемость сайта</a></li>
			<li><a href="./stat_customers.html">Статистика пользователей</a></li>
		</ul>
	</li>
	<li><a href="#">Команда</a>
		<ul>
			<li><a href="index.php?r=site/team&lan=en">Управление блоком</a></li>
		</ul>
	</li>
	<li><a href="#">Пользователи</a>
		<ul>
			<li><a href="index.php?r=site/users">Зарегистрированные</a></li>
			<li><a href="index.php?r=site/users">Активность пользователей</a></li>
			<li><a href="index.php?r=site/users">Приглашение</a></li>
		</ul>
	</li>
	<li><a href="#">Администрация сайта</a>
		<ul>
			<li><a href="index.php?r=site/users">Список администраторов</a></li>
		</ul>
	</li>
	<li><a href="#">Настройки сайта</a>
		<ul>
			<li><a href="index.php?r=site/price">Прайс</a></li>
			<li><a href="index.php?r=site/mail">Mail - регистрация</a></li>
			<li><a href="index.php?r=site/mailos">Mail - обратная связь</a></li>
		</ul>
	</li>
	<li class="exit"><a href="index.php?r=site/logout">Выход</a>
	</li>
</ul>


<?
}; 
echo $content; 
?>
</div>


</body>
</html>
