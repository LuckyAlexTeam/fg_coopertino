<?
class SiteAllController extends CController {
	public $layout='/layouts/mainall';
	public $arr_menu;
	public $arr_lang;
	public $arr_site;
	public $arr_header;
	public $arr_cont;
	public $tlanguage;
	
	public $breadcrumbs;
	/**
	 * Declares class-based actions.
	 */
	public function actions() {
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}
	//
	public function actionAbout() {
		$this->render('about');
	}
	public function actionServices() {
		$this->render('services');
	}
	public function actionMentors() {
		$this->render('mentors');
	}
	public function actionFaq() {
		$this->render('faq');
	}
	public function actionContacts() {
		$this->render('contacts');
	}
}