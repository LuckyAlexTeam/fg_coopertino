<?php
class Site extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    public function tableName() {
        return 'site';
    }
	// Чтение для страниц 
    public function getAllWid($lan, $view) {
		$criteria = new CDbCriteria;
		$criteria->condition = "view = '{$view}' AND language = '{$lan}'  ";
		$criteria->order = " cod";
		$list = self::model()->findAll($criteria);
		$arr = Array();
		foreach ($list as $lab) {
			$id = $lab['id'];
			$arr[$id]['id'] = $lab['id'];
			$arr[$id]['view'] = $lab['view'];
			$arr[$id]['cod'] = $lab['cod'];
			$arr[$id]['cod_up'] = $lab['cod_up'];
			$arr[$id]['other'] = $lab['other'];
			$arr[$id]['npp'] = $lab['npp'];
			$arr[$id]['npp_up'] = $lab['npp_up'];
			$arr[$id]['language'] = $lab['language'];
			$arr[$id]['name'] = $lab['name'];
			$arr[$id]['img'] = $lab['img'];
		};
		
	}
	// id view Имя view cod Код поля language Код языка name
    public function getAll($lan, $view) {
		$criteria = new CDbCriteria;
		//$criteria->condition = "view = '{$view}' AND ( (language = '{$lan}') OR (language = 'ru') )  ";
		$criteria->condition = "view = '{$view}' AND language = 'ru'  ";
		$criteria->order = " npp, cod_up, cod";
		$list = self::model()->findAll($criteria);
		$arr = Array();
		
		//id view cod language name
		foreach ($list as $lab) {
			$id = $lab['view'] . '_' . $lab['cod'];
			$arr[$id]['id'] = $lab['id'];
			$arr[$id]['view'] = $lab['view'];
			$arr[$id]['cod_up'] = $lab['cod_up'];
			$arr[$id]['cod'] = $lab['cod'];
			$arr[$id]['other'] = $lab['other'];
			$arr[$id]['npp'] = $lab['npp'];
			$arr[$id]['npp_up'] = $lab['npp_up'];
			$arr[$id]['language'] = $lab['language'];
			$arr[$id]['name'] = $lab['name'];
			$arr[$id]['img'] = $lab['img'];
			//
			$arr[$id]['name2'] = "";
			$arr[$id]['id2'] = "0";
			$arr[$id]['language2'] = "";
			//
			$arr[$id]['name3'] = "";
			$arr[$id]['id3'] = "0";
			$arr[$id]['language3'] = "";
			//
			$arr[$id]['name4'] = "";
			$arr[$id]['id4'] = "0";
			$arr[$id]['language4'] = "";
		};
		if (count($arr) == 0) {return $arr;};
		//var_dump($arr);
		//
		$criteria->condition = "view = '{$view}' AND language = 'en'  ";
		$list = self::model()->findAll($criteria);
		foreach ($list as $lab) {
			$id = $lab['view'] . '_' . $lab['cod'];
			if ( isset( $arr[$id]['id']) ) {
				$arr[$id]['name2'] = $lab['name'];
				$arr[$id]['id2'] = $lab['id'];
				$arr[$id]['language2'] = $lab['language'];
			};
		};
		$criteria->condition = "view = '{$view}' AND language = 'fr' ";
		$list = self::model()->findAll($criteria);
		foreach ($list as $lab) {
			$id = $lab['view'] . '_' . $lab['cod'];
			if ( isset( $arr[$id]['id']) ) {
				$arr[$id]['name3'] = $lab['name'];
				$arr[$id]['id3'] = $lab['id'];
				$arr[$id]['language3'] = $lab['language'];
			};
		};
		$criteria->condition = "view = '{$view}' AND language = 'ch' ";
		$list = self::model()->findAll($criteria);
		foreach ($list as $lab) {
			$id = $lab['view'] . '_' . $lab['cod'];
			if ( isset( $arr[$id]['id']) ) {
				$arr[$id]['name4'] = $lab['name'];
				$arr[$id]['id4'] = $lab['id'];
				$arr[$id]['language4'] = $lab['language'];
			};
		};
		//
		return $arr;
    }
	public function getAllTeam($lan) {
		$criteria = new CDbCriteria;
		$criteria->condition = "view = 'main' AND language = 'ru' AND LEFT(cod,2) = 't_' ";
		$criteria->order = " cod";
		$list = self::model()->findAll($criteria);
		$arr = Array();
		foreach ($list as $lab) {
			$id = $lab['view'] . '_' . $lab['cod'];
			$arr[$id]['id'] = $lab['id'];
			$arr[$id]['view'] = $lab['view'];
			$arr[$id]['cod'] = $lab['cod'];
			$arr[$id]['other'] = $lab['other'];
			$arr[$id]['language'] = $lab['language'];
			$arr[$id]['name'] = $lab['name'];
			$arr[$id]['img'] = $lab['img'];
			//
			$arr[$id]['name2'] = "";
			$arr[$id]['id2'] = "0";
			$arr[$id]['language2'] = "";
			//
			$arr[$id]['name3'] = "";
			$arr[$id]['id3'] = "0";
			$arr[$id]['language3'] = "";
			//
			$arr[$id]['name4'] = "";
			$arr[$id]['id4'] = "0";
			$arr[$id]['language4'] = "";
		};
		if (count($arr) == 0) {return $arr;};
		
		
		$criteria->condition = "view = 'main' AND language = 'en' AND LEFT(cod,2) = 't_' ";
		$list = self::model()->findAll($criteria);
		foreach ($list as $lab) {
			$id = $lab['view'] . '_' . $lab['cod'];
			if ( isset( $arr[$id]['id']) ) {
				$arr[$id]['name2'] = $lab['name'];
				$arr[$id]['id2'] = $lab['id'];
				$arr[$id]['language2'] = $lab['language'];
			};
		};
		$criteria->condition = "view = 'main' AND language = 'fr' AND LEFT(cod,2) = 't_' ";
		$list = self::model()->findAll($criteria);
		foreach ($list as $lab) {
			$id = $lab['view'] . '_' . $lab['cod'];
			if ( isset( $arr[$id]['id']) ) {
				$arr[$id]['name3'] = $lab['name'];
				$arr[$id]['id3'] = $lab['id'];
				$arr[$id]['language3'] = $lab['language'];
			};
		};
		$criteria->condition = "view = 'main' AND language = 'ch' AND LEFT(cod,2) = 't_'";
		$list = self::model()->findAll($criteria);
		foreach ($list as $lab) {
			$id = $lab['view'] . '_' . $lab['cod'];
			if ( isset( $arr[$id]['id']) ) {
				$arr[$id]['name4'] = $lab['name'];
				$arr[$id]['id4'] = $lab['id'];
				$arr[$id]['language4'] = $lab['language'];
			};
		};
		//
		return $arr;
	}
	//
	public function Add() {
		var_dump($_GET);
		print "<hr>";
		var_dump($_POST);
		print "<hr>";
		var_dump($_FILES);
		if ( count($_FILES) == 0 ) {
			$img = 0;
			$nam = $_POST['new_name'];
			$nam2 = $_POST['new_name2'];
			$nam3 = $_POST['new_name3'];
			$nam4 = $_POST['new_name4'];
		} else {
			$img = 1;
			$nam = $_FILES['img']['name'];
			$nam2 = $_FILES['img']['name'];
			$nam3 = $_FILES['img']['name'];
			$nam4 = $_FILES['img']['name'];
			move_uploaded_file($_FILES['img']['tmp_name'], '../images/' . $_FILES['img']['name']);
		};
		//
		$this->setAttribute( 'id' , '' );
		$this->setAttribute( 'view' , $_REQUEST['new_view'] );
		$this->setAttribute( 'cod' , $_REQUEST['new_cod'] );
		$this->setAttribute( 'cod_up' , $_REQUEST['new_cod_up'] );
		
		$this->setAttribute( 'img' , $img);
		$this->setAttribute( 'language' , 'ru' );
		$this->setAttribute( 'name' , $nam );
		$this->isNewRecord=true;
		$ttt = $this->insert();
		//
		$this->setAttribute( 'id' , '' );
		$this->setAttribute( 'view' , $_REQUEST['new_view'] );
		$this->setAttribute( 'cod' , $_REQUEST['new_cod'] );
		$this->setAttribute( 'cod_up' , $_REQUEST['new_cod_up'] );
		$this->setAttribute( 'img' , $img);
		$this->setAttribute( 'language' , 'en' );
		$this->setAttribute( 'name' , $nam2 );
		$this->isNewRecord=true;
		$ttt = $this->insert();
		//
		$this->setAttribute( 'id' , '' );
		$this->setAttribute( 'view' , $_REQUEST['new_view'] );
		$this->setAttribute( 'cod' , $_REQUEST['new_cod'] );
		$this->setAttribute( 'cod_up' , $_REQUEST['new_cod_up'] );
		$this->setAttribute( 'img' , $img);
		$this->setAttribute( 'language' , 'fr' );
		$this->setAttribute( 'name' , $nam3 );
		$this->isNewRecord=true;
		$ttt = $this->insert();
		//
		$this->setAttribute( 'id' , '' );
		$this->setAttribute( 'view' , $_REQUEST['new_view'] );
		$this->setAttribute( 'cod' , $_REQUEST['new_cod'] );
		$this->setAttribute( 'cod_up' , $_REQUEST['new_cod_up'] );
		$this->setAttribute( 'img' , $img);
		$this->setAttribute( 'language' , 'ch' );
		$this->setAttribute( 'name' , $nam4 );
		$this->isNewRecord=true;
		$ttt = $this->insert();
	}
	//
	public function Upd() {
		var_dump($_GET);
		print "<hr>";
		var_dump($_POST);
		print "<hr>";
		var_dump($_FILES);
		if ( count($_FILES) > 0 ) {
			$img = 1;
			$nam = $_FILES['img']['name'];
			$nam2 = $_FILES['img']['name'];
			move_uploaded_file($_FILES['img']['tmp_name'], '../images/' . $_FILES['img']['name']);
			//
			$this->setPrimaryKey($_GET['id']);
			//$this->setAttribute( 'id' , $_GET['id'] );
			$this->setAttribute( 'name' , $_FILES['img']['name']);
			$this->setAttribute( 'img' , 1);
			$this->isNewRecord=false;
			$ttt = $this->update();
			//
			$this->setAttribute( 'id' , $_GET['id2'] );
			$this->setPrimaryKey($_GET['id2']);
			//$this->setAttribute( 'id' , $_GET['id2'] );
			$this->setAttribute( 'name' , $_FILES['img']['name']);
			$this->setAttribute( 'img' , 1);
			$this->isNewRecord=false;
			$ttt = $this->update();
			//
			return;
		};
		foreach ($_POST as $key => $val) {
			//print "<br>key={$key} , val={$val}, arr=";
			$arr = mb_split ('_', $key);
			//print_r($arr);
			if ($arr[0] == "upd") {
				//print " , name2 =" . $_POST['name2_'.$arr[1]];
				if ($arr[1] != '0') {
					$this->setAttribute( 'id' , $arr[1] );
					$this->setAttribute( 'name' , $_POST['name2_'.$arr[1]] );

					$this->isNewRecord=false;
					$ttt = $this->update();
				} else {
					$this->setAttribute( 'id' , '' );
					$this->setAttribute( 'language' , 'en' );
					$this->setAttribute( 'name' , $nam2 );
					$this->setAttribute( 'img' , 0);

					$this->isNewRecord=true;
					$ttt = $this->insert();
				};
				if ($arr[2] != '0') {
					$this->setAttribute( 'id' , $arr[2] );
					$this->setAttribute( 'name' , $_POST['name3_'.$arr[2]] );
					$this->isNewRecord=false;
					$ttt = $this->update();
				} else {
					$this->setAttribute( 'id' , '' );
					$this->setAttribute( 'language' , 'fr' );
					$this->setAttribute( 'name' , $nam3 );
					$this->setAttribute( 'img' , 0);
					$this->isNewRecord=true;
					$ttt = $this->insert();
				};
				if ($arr[3] != '0') {
					$this->setAttribute( 'id' , $arr[3] );
					$this->setAttribute( 'name' , $_POST['name4_'.$arr[3]] );
					$this->isNewRecord=false;
					$ttt = $this->update();
				} else {
					$this->setAttribute( 'id' , '' );
					$this->setAttribute( 'language' , 'ch' );
					$this->setAttribute( 'name' , $nam4 );
					$this->setAttribute( 'img' , 0);
					$this->isNewRecord=true;
					$ttt = $this->insert();
				};
			};
		};
	}
	
	public function Ajax() {
		//
		$this->setAttribute( 'name' , Yii::app()->request->getPost('name') );
		$this->setAttribute( 'npp' , Yii::app()->request->getPost('npp') );
		$this->setAttribute( 'npp_up' , Yii::app()->request->getPost('npp_up') );
		$this->setAttribute( 'other' , Yii::app()->request->getPost('other') );
		$this->setPrimaryKey(Yii::app()->request->getPost('id'));
		$this->isNewRecord=false;
		$tt1 = $this->update();
		//
		if ($_POST['id2'] != 0) {
			$this->setAttribute( 'id', Yii::app()->request->getPost('id2'));
			$this->setAttribute( 'name' , Yii::app()->request->getPost('name2') );
			$this->setAttribute( 'npp' , Yii::app()->request->getPost('npp') );
			$this->setAttribute( 'npp_up' , Yii::app()->request->getPost('npp_up') );
			$this->setAttribute( 'other' , Yii::app()->request->getPost('other') );
			$this->setPrimaryKey(Yii::app()->request->getPost('id2'));
			$this->isNewRecord=false;
			$tt2= $this->update();
		} else {
			$this->setAttribute( 'id' , '' );
			$this->setAttribute( 'language' , 'en' );
			$this->setAttribute( 'name' , $_POST['name2'] );
			$this->setAttribute( 'cod' , $_POST['cod'] );
			$this->setAttribute( 'cod_up' , $_POST['cod_up'] );
			$this->setAttribute( 'npp' , Yii::app()->request->getPost('npp') );
			$this->setAttribute( 'npp_up' , Yii::app()->request->getPost('npp_up') );
			$this->setAttribute( 'other' , Yii::app()->request->getPost('other') );
			$this->setAttribute( 'view' , $_POST['view'] );
			$this->setAttribute( 'img' , 0);
			$this->isNewRecord=true;
			$ttt = $this->insert();
		};
		//
		if ($_POST['id3'] != 0) {
			$this->setAttribute( 'id', Yii::app()->request->getPost('id3'));
			$this->setAttribute( 'name' , Yii::app()->request->getPost('name3') );
			$this->setAttribute( 'npp' , Yii::app()->request->getPost('npp') );
			$this->setAttribute( 'npp_up' , Yii::app()->request->getPost('npp_up') );
			$this->setAttribute( 'other' , Yii::app()->request->getPost('other') );
			$this->setPrimaryKey(Yii::app()->request->getPost('id3'));
			$this->isNewRecord=false;
			$tt3 = $this->update();
		} else {
			$this->setAttribute( 'id' , '' );
			$this->setAttribute( 'language' , 'fr' );
			$this->setAttribute( 'name' , $_POST['name3'] );
			$this->setAttribute( 'cod' , $_POST['cod'] );
			$this->setAttribute( 'cod_up' , $_POST['cod_up'] );
			$this->setAttribute( 'npp' , Yii::app()->request->getPost('npp') );
			$this->setAttribute( 'npp_up' , Yii::app()->request->getPost('npp_up') );
			$this->setAttribute( 'other' , Yii::app()->request->getPost('other') );
			$this->setAttribute( 'view' , $_POST['view'] );
			$this->setAttribute( 'img' , 0);
			$this->isNewRecord=true;
			$ttt = $this->insert();
		};
		//
		if ($_POST['id4'] != 0) {
			$this->setAttribute( 'id', Yii::app()->request->getPost('id4'));
			$this->setAttribute( 'name' , Yii::app()->request->getPost('name4') );
			$this->setAttribute( 'npp' , Yii::app()->request->getPost('npp') );
			$this->setAttribute( 'npp_up' , Yii::app()->request->getPost('npp_up') );
			$this->setAttribute( 'other' , Yii::app()->request->getPost('other') );
			$this->setPrimaryKey(Yii::app()->request->getPost('id4'));
			$this->isNewRecord=false;
			$tt4 = $this->update();
		} else {
			$this->setAttribute( 'id' , '' );
			$this->setAttribute( 'language' , 'ch' );
			$this->setAttribute( 'name' , $_POST['name4'] );
			$this->setAttribute( 'cod' , $_POST['cod'] );
			$this->setAttribute( 'cod_up' , $_POST['cod_up'] );
			$this->setAttribute( 'npp' , Yii::app()->request->getPost('npp') );
			$this->setAttribute( 'npp_up' , Yii::app()->request->getPost('npp_up') );
			$this->setAttribute( 'other' , Yii::app()->request->getPost('other') );
			$this->setAttribute( 'view' , $_POST['view'] );
			$this->setAttribute( 'img' , 0);
			$this->isNewRecord=true;
			$ttt = $this->insert();
		};
		
		print '{"otv":"ok", "id":"'.$_POST['id'].'", "id2":"'.$_POST['id2'].'", "id3":"'.$_POST['id3'].'", "id4":"'.$_POST['id4'].
		'", "tt1":"'.$tt1.'", "tt2":"'.$tt2.'", "tt3":"'.$tt3.'", "tt4":"'.$tt4.
		'"}';
		return;
	}
}