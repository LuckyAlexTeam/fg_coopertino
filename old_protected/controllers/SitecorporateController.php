<?php

class SitecorporateController extends CController {
	public $layout='/layouts/maincorporate';
	//
	public $arr_menu;
	public $arr_lang;
	public $arr_site;
	public $arr_header;
	public $arr_cont;
	public $tlanguage;
	
	public $breadcrumbs;
	//
	public function actions() {
		return array();
	}

	public function actionIndex() {
		$this->site_stranica();
		$this->render('index');
	}
	public function site_stranica() {
		$model=new Language();
		$this->tlanguage = $model->getLanguage();
		$this->arr_lang = $model->getAll();
		$modea=new Site();
		$this->arr_site  = $modea->getAll($this->tlanguage, 'corporate');
		$modeb=new SiteHeader();
		$this->arr_header = $modeb->getAll($this->tlanguage, 'index');
	}
}
?>