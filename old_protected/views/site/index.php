<!--
<?
$modea=new Site();
$lll = 'ru';
if ( isset($_COOKIE['language'])) { $lll = $_COOKIE['language']; };
$opt  = $modea->getAll($lll, 'select');
//var_dump($opt);
//var_dump($this->arr_price);
/*
<select>
<==$opt['sel_prof']=>
</select>
<select><==$opt['sel_country']=></select>
*/
?>
-->

<a name="b1" id="b1"></a>
<div class="block b1" id="home">
	<div class="border1">
		<div class="subh">
			<?=$this->arr_site["1"]?><!--Самый большой выбор в мире -->
		</div>
		<div class="h">
			<?=$this->arr_site["2"]?><!-- моделей, модельных агенств, фотографов, экспертов моды -->
		</div>
		<div class="subh">
			<?=$this->arr_site["3"]?><!-- и прочих fashion профессионалов -->
		</div>
		<div class="menu2">
			<a class="menu2e" id="but_join"><?=$this->arr_site["r1"]?></a>
			<a class="menu2e" id="but_signin"><?=$this->arr_site["r2"]?></a>
		</div>
	</div>
</div>
<div class="clear"></div>
<a name="b2" id="b2"></a>
<div class="block b2" id="about">
	<div class="about">
		<div class="col-ab">
			<div class="sub-col-ab">
				<div class="col-icon">
					<a href="/about"><i class="fa fa-4x fa-info-circle" aria-hidden="true"></i>
					<br/><?=$this->arr_site["a_2_6"]?></a>
				</div>
			</div>
		</div>
		
		<div class="col-ab">
			<div class="sub-col-ab">
				<div class="col-icon">
					<a href="/services"><i class="fa fa-4x fa-cogs" aria-hidden="true"></i>
					<br/><?=$this->arr_site["a_2_7"]?></a>
				</div>
			</div>
		</div>
		
		<div class="col-ab">
			<div class="sub-col-ab">
				<div class="col-icon">
					<a href="/mentors"><i class="fa fa-4x fa-users" aria-hidden="true"></i>
					<br/><?=$this->arr_site["a_2_8"]?></a>
				</div>
			</div>
		</div>
		
		<div class="col-ab">
			<div class="sub-col-ab">
				<div class="col-icon">
					<a href="/faq"><i class="fa fa-4x fa-question-circle" aria-hidden="true"></i>
					<br/><?=$this->arr_site["a_2_9"]?></a>
				</div>
			</div>
		</div>
		
		<div class="col-ab">
			<div class="sub-col-ab">
				<div class="col-icon">
					<a href="/contacts"><i class="fa fa-4x fa-envelope-o" aria-hidden="true"></i>
					<br/><?=$this->arr_site["a_2_10"]?></a>
				</div>
			</div>
		</div>
		
		<h1><?=$this->arr_site["a_5"]?><!-- Думай шире. Думай глобально. Мечтай. --></h1>
		<div class="ln"></div>
	</div>
</div>

<a name="b3" id="b3"></a> <? /*  ПРЕИМУЩЕСТВА */ ?>
<div class="block b3" id="features">
	<div class="features">
		<div class="wrap-f">
			<div class="f-1-2">
				<div class="carousel-pagination">
					<ul class="list-pagination">
						<li class="c-p current">
							<a class="slid" id="s0" sme="0" href="#s1"><?=$this->arr_site["f_m_1"]?><!-- models--></a>
						</li>
						<li class="c-p">
							<a class="slid" id="s-1" sme="-1" href="#s2"><?=$this->arr_site["f_m_2"]?><!-- photographers --></a>
						</li>
						<li class="c-p">
							<a class="slid" id="s-2" sme="-2" href="#s3"><?=$this->arr_site["f_m_3"]?><!-- Model Agencies --></a>
						</li>
						<li class="c-p">
							<a class="slid" id="s-3" sme="-3" href="#s4"><?=$this->arr_site["f_m_4"]?><!-- Fashion Experts --></a>
						</li>
						<li class="c-p">
							<a class="slid" id="s-4" sme="-4" href="#s5"><?=$this->arr_site["f_m_5"]?><!--Other Professionals--></a>
						</li>
					</ul>
				</div>
				<div class="carousel">
					<div class="car-wrap">
						<div class="f-2-3"> 
							<div id="ulsl" onmousedown="return false" onselectstart="return false">
								<div class="item">
									<div class="i-i-w">
										<div class="item-img">
											<img src="../images/f_s_f1.jpg">
										</div>
										<?=$this->arr_site["f_s_1"]?>
									</div>
								</div>
								<div class="item">
									<div class="i-i-w">
										<div class="item-img">
											<img src="../images/f_s_f2.jpg">
										</div>
										<?=$this->arr_site["f_s_2"]?>
									</div>
								</div>
								<div class="item">
									<div class="i-i-w">
										<div class="item-img">
											<img src="../images/f_s_f3.jpg">
										</div>
										<?=$this->arr_site["f_s_3"]?>
									</div>
								</div>
								<div class="item">
									<div class="i-i-w">
										<div class="item-img">
											<img src="../images/f_s_f4.jpg">
										</div>
										<?=$this->arr_site["f_s_4"]?>
									</div>
								</div>
								<div class="item">
									<div class="i-i-w">
										<div class="item-img">
											<img src="../images/f_s_f5.jpg">
										</div>
										<?=$this->arr_site["f_s_5"]?>
									</div>
								</div>
								<div class="clear"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
				
			<div class="f-2-2">
				<p class="have"><?=$this->arr_site["a_3_6"]?></p>
				<div class="have-list">
					<p><?=$this->arr_site["f_m_6"]?></p>
					<p><?=$this->arr_site["f_m_1"]?></p>
					<hr style="width:30%;">
					<p><?=$this->arr_site["f_m_2"]?></p>
					<hr style="width:50%;">
					<p><?=$this->arr_site["f_m_3"]?></p>
					<hr style="width:20%;">
					<p><?=$this->arr_site["f_m_4"]?></p>
					<hr style="width:80%;">
				</div>
			</div>
			
			<div class="clear"></div>
			
			<div class="video-wrapper">
				<div class="video-responsive">
					<iframe src="https://www.youtube.com/embed/iGaF4tKUl0o" frameborder="0" allowfullscreen></iframe>
				</div>
				<div class="video-responsive">
					<iframe src="https://www.youtube.com/embed/sfbfaeG7EJU" frameborder="0" allowfullscreen></iframe>
				</div>
				<div class="clear"></div>
			</div>
			
			<div class="clear"></div>
			
		</div>
	</div>
</div>

<a name="b4" id="b4"></a> <? /* Команда */ ?>
<div class="block b4" id="team">
	<div class="border2">
		<div class="yln"></div>
		<img class="t-i" src="img/creative-team.png">
		<p><?=$this->arr_site["t_zag"]?>.</p>
		<div class="carousel">
			<div class="arrow l-arrow">
				<img src="../images/prev.png" id="but_team_next">
			</div>
			<div class="car-wrap" id="team_slid">
				
			<? /* Цикл по членам команды */ ?>
			<? $kol_team = 0; ?>
			<? for ($i = 1; $i<10; ++$i) { 
				if ( isset($this->arr_site["t_{$i}_1"]) ) { ++$kol_team; /* Проверка есть-ли ещё один член команды */ ?>
			
				<div class="team-item" id="team_<?=$i?>">
					<p class="team-name"><?=$this->arr_site["t_{$i}_1"]?><br><span><?=$this->arr_site["t_{$i}_2"]?></span></p>
					<div class="team-inner">
						<div class="team-img">
							<img src="../images/t_f<?=$i?>.jpg" alt="">
						</div>
						<div class="team-description">
							<?=$this->arr_site["t_{$i}_3"]?>
						</div>
						<div class="clear"></div>
						<div class="social-networks">
							<a href="mailto:<?=$this->arr_site["t_{$i}_4"]?>"><?=$this->arr_site["t_{$i}_4"]?></a>
						</div>
					</div>
				</div>
				<? }; /* конец проверки */ ?>
			<? }; /* конец цикла */ ?>
			
				
			</div>
			<div class="arrow r-arrow">
				<img src="../images/next.png" id="but_team_prev">
			</div>
			<div class="clear"></div>
		</div>
	</div>
</div>

<a name="b5" id="b5"></a>
<div class="block b5" id="prices">
	<div class="price">
		<p class="p-h"><?=$this->arr_site["p_1"]?></p>
		<p class="p-sh"><?=$this->arr_site["p_2"]?></p>
		<div class="price-container">
			<div class="price-item">
				<div class="price-inner">
					<p class="price-title">
						<?=$this->arr_site["p_3"]?><!-- Account -->
						<br>
						<span class="price-name"><?=$this->arr_site["p_4"]?><!-- Classic --></span>
						<span class="shortline black"></span>
					</p>
					<p class="price-plan"><span><?=$this->arr_price["classic"]?>$</span>&nbsp;/&nbsp;<?=$this->arr_site["p_7"]?></p>
					<p class="price-description"><?=$this->arr_site["pr_1_1"]?></p>
						<a href="#popup-register" data-tarif="5" class="price-btn" id="join1"><?=$this->arr_site["r1"]?><!-- JOIN --></a>
						<div class="clear"></div>
				</div>
			</div>
		
			<div class="price-item">
				<div class="price-inner">
					<p class="price-title">
					<?=$this->arr_site["p_3"]?><!-- Account -->
					<br>
					<span class="price-name"><?=$this->arr_site["p_5"]?><!-- VIP --></span>
					<span class="shortline black"></span>
					</p>
					<p class="price-plan"><span><?=$this->arr_price["vip"]?>$</span>&nbsp;/&nbsp;<?=$this->arr_site["p_7"]?></p>
					<p class="price-description"><?=$this->arr_site["pr_2_1"]?></p>
					<a href="#popup-register" data-tarif="6" class="price-btn" id="join2"><?=$this->arr_site["r1"]?><!-- JOIN --></a>
					<div class="clear"></div>
				</div>
			</div>
		
			<div class="price-item">
				<div class="price-inner">
					<p class="price-title">
					<?=$this->arr_site["p_3"]?><!-- Account -->
					<br>
					<span class="price-name"><?=$this->arr_site["p_6"]?><!-- Corporate --></span>
					<span class="shortline black"></span>
					</p>
					<p class="price-plan"><span><?=$this->arr_price["corporate"]?>$</span>&nbsp;/&nbsp;<?=$this->arr_site["p_7"]?></p>
					<p class="price-description"><?=$this->arr_site["pr_3_1"]?></p>
					<a href="#popup-register" data-tarif="7" class="price-btn" id="join3"><?=$this->arr_site["r1"]?><!-- JOIN --></a>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>
</div>

<a name="b6" id="b6"></a>
<div class="block b6" id="contactus">
	<img class="c-img" src="img/contactus.png" alt="Обратная связь" />
	<div class="contact">
		<div class="c-f">
				<div class="c-f-i">
					<input placeholder="<?=$this->arr_site["reg_6"]?> *" type="text" class="form-control" id="b6_1" value="" size="0">
				</div>
				<div class="c-f-i">
					<input placeholder="<?=$this->arr_site["reg_8"]?> *" type="text" class="form-control" id="b6_2" value="" size="0">
				</div>
				<div class="c-f-i">
					<select class="form-control f-c-s" id="b6_3"><?=$this->arr_site["sel_1"]?></select>
				</div>
				<div class="c-f-i">
					<textarea placeholder="<?=$this->arr_site["reg_20"]?> *" id="b6_4" cols="40" rows="5" class="form-control"></textarea>
				</div>
				<div class="c-f-i tc">
					<i class="fa fa2 fa-envelope-o"></i><input class="button-submit" type="submit" id="b6_but" name="web_form_submit" value="<?=$this->arr_site["reg_21"]?>">
				</div>
				<div class="c-f-i" style="display:none;" id="b6_soo">
					Thank you for your message. <br>We will answer you as soon as possible.
				</div>
		</div>
	</div>
</div>

<? /* Регистрация */ ?>
<div id="lay-hid_join" class="lay-hid">
	<div class="lay-hid-wrap">
		<form id="form_reg" action="/registration" method="post">
		<div id="lay-hid_join_close" class="lay-hid_close">Х</div>
		<div id="lay-hid_join_forma" class="join-form">
			<h1><?=$this->arr_site["reg_1"]?></h1>
			<p class="c-p"><?=$this->arr_site["reg_2"]?></p>
				<label class="custom-radio"><input type="radio" name="reg_pac" id="reg_pa1" value="vis" checked></input><span><div></div></span><p><?=$this->arr_site["reg_3"]?></p></label>
				<label class="custom-radio"><input type="radio" name="reg_pac" id="reg_pa2" value="cla"></input><span><div></div></span><p> <?=$this->arr_site["p_4"]?></p></label>
				<label class="custom-radio"><input type="radio" name="reg_pac" id="reg_pa3" value="vip"></input><span><div></div></span><p> <?=$this->arr_site["p_5"]?></p></label>
				<label class="custom-radio"><input type="radio" name="reg_pac" id="reg_pa4" value="cor"></input><span><div></div></span><p> <?=$this->arr_site["p_6"]?></p></label>
				<input class="form-field" type="text" name="reg_nam" id="reg_nam" placeholder="<?=$this->arr_site["reg_6"]?>*">
				<input class="form-field" type="text" name="reg_lna" id="reg_lna" placeholder="<?=$this->arr_site["reg_7"]?>*">
				<input class="form-field" type="text" name="reg_mai" id="reg_mai" placeholder="<?=$this->arr_site["reg_8"]?>*">
				<input class="form-field" type="text" name="reg_pho" id="reg_pho" placeholder="<?=$this->arr_site["reg_9"]?>*">
				<select class="form-sel" name="reg_lan" id="reg_lan">
					<option value=""><?=$this->arr_site["reg_4"]?>*</option>
					<option value="ru">ru</option>
					<option value="en">en</option>
					<option value="fr">fr</option>
					<option value="ch">ch</option>
				</select>
				<select class="form-sel" name="reg_loc" id="reg_loc">
					<option value=""><?=$this->arr_site["reg_5"]?>*</option>
					<?=$opt['sel_country']?>
				</select>
			<div id="reg_gr1" style="display:none">
					<select class="form-sel" name="reg_kpr" id="reg_kpr">
						<option><?=$this->arr_site["reg_22"]?>*</option>
						<option>1</option>
						<option>2</option>
						<option>3</option>
						<option>4</option>
					</select>
					<select class="form-sel" name="reg_ypr" id="reg_ypr">
						<option><?=$this->arr_site["reg_23"]?>*</option>
						<?=$opt['sel_prof']?>
					</select>
				<div class="form-pay">
					<p><?=$this->arr_site["reg_24"]?>*</p>
					<textarea name="reg_sum" id="reg_sum"></textarea>
				</div>
			</div>
			<font id="error"></font>
			<div>
				<input type="submit" id="lay-hid_join_but" class="form-btn" value="<?=$this->arr_site["reg_10"]?>">
			</div>
		</form>
		</div>
	</div>
</div>
<? /* Авторизация */ ?>
<div id="lay-hid_signin" class="lay-hid">
	<div class="lay-hid-wrap">
		<div id="lay-hid_signin_close" class="lay-hid_close">Х</div>
		<div id="lay-hid_signin_forma" class="sign-form">
			<form id="form_sig" action="signin" method="post">
			<h1><?=$this->arr_site["reg_15"]?></h1>
			<input class="form-th" type="text" name="sig_mai" id="sig_mai" placeholder="<?=$this->arr_site["reg_8"]?>">
			<input class="form-th" type="password" name="sig_pas" id="sig_pas" placeholder="<?=$this->arr_site["reg_11"]?>">
			<div class="clear"></div>
			<label for="checkbox" class="custom-radio chckbx"><input id="checkbox" type="checkbox" name="sig_rem"><span><div></div></span><p><?=$this->arr_site["reg_12"]?></p></label>
			<input type="button" id="lay-hid_sigin_fyp" class="form-fgt" value="<?=$this->arr_site["reg_13"]?>?">
			<div id="sig_error">
			
			</div>
			<div class="clear"></div>
			<input type="submit" id="lay-hid_sigin_but" class="form-btn" value="<?=$this->arr_site["reg_14"]?>">
			</form>
		</div>
	</div>
</div>

<? /* Напоминание пароля*/ ?>
<div id="lay-hid_forgot" class="lay-hid">
	<div class="lay-hid-wrap">
		<div id="lay-hid_forgot_close" class="lay-hid_close">Х</div>
		<div id="lay-hid_forgot_forma" style="margin:auto; width: 30%; text-align:center; color:#fff; margin-top: -20%;">
			<h1><?=$this->arr_site["reg_16"]?></h1>
			<input class="form-field" type="text" id="for_mai" placeholder="<?=$this->arr_site["reg_8"]?>">
			<p><?=$this->arr_site["reg_17"]?></p>
			<input type="button" id="lay-hid_forgot_but" class="form-btn" value="<?=$this->arr_site["reg_18"]?>">
		</div>
	</div>
</div>


<? /* Регистрация */ ?>
<? /*
<div id="lay-hid_register" class="lay-hid">
<div id="lay-hid_register_close" class="lay-hid_close">Х</div>
registration
</div>
*/ ?>

<? /* Thank you for your message. We will answer you as soon as possible. */ ?>

<script>
var tek_slid = 0;
var mou_dow = 0;
var mou_x = 0;
var mou_t = 0;
var napr = '';
$(document).ready(function() {
		<? /* Скролл сдайдера -  ПРЕИМУЩЕСТВА */ ?>
	$('#b6_but').click( function() {
		p1 = $('#b6_1').val();
		p2 = $('#b6_2').val();
		p3 = $('#b6_3').val();
		p4 = $('#b6_4').val();
		//alert(p1+p2+p3+p4);
		$('#b6_soo').css('display','block');
		$.ajax({
			url: 'index.php?r=site/soo',
			type: 'post',
			async: true,
			data: 'nam='+$('#b6_1').val() +'&mai='+$('#b6_2').val() +'&sel='+$('#b6_3').val() +'&soo='+$('#b6_4').val(),
			dataType: 'json',
			success: function(json) {
				
			}
		});
		/*
		sme1 = $('.item').css('width');
		sme2 = Number(sme1.replace(/\D+/g,""));
		sme3 = sme2 * $(this).attr('sme');
		tek_slid = $(this).attr('sme');
		$("#ulsl").animate({ left: sme3}, { duration: 800, easing: "swing" });
		$(".c-p").removeClass("current");
		$(this).parent().addClass("current");
		*/
		return false;
	});
	<? /* Скролл сдайдера -  ПРЕИМУЩЕСТВА */ ?>
	$('.slid').click( function() {
		sme1 = $('.item').css('width');
		sme2 = Number(sme1.replace(/\D+/g,""));
		sme3 = sme2 * $(this).attr('sme');
		tek_slid = $(this).attr('sme');
		$("#ulsl").animate({ left: sme3}, { duration: 800, easing: "swing" });
		$(".c-p").removeClass("current");
		$(this).parent().addClass("current");
		return false;
	});
	$(window).resize(function () {
		sme1 = $('.item').css('width');
		sme2 = Number(sme1.replace(/\D+/g,""));
		sme3 = sme2 * tek_slid;
		$("#ulsl").css('left',sme3);
		//console.log(tek_slid, sme1);
	});
	<? /* Скролл сдайдера -  ПРЕИМУЩЕСТВА - таскание мышкой, пальцем*/ ?>
	//
	$('.item').mousemove(function(eventObject){
		if (mou_dow == 1) {
			sme1 = $("#ulsl").css('left');
			sme2 = Number(sme1.replace(/\D+/g,"")); // Math.abs(  
			mou_t = eventObject.pageX;
			//if (mou_x > mou_t) { smed = mou_x - mou_t; } else { smed = mou_t - mou_x;};
			smed = mou_x - mou_t;
			sme3 = (sme2 + Number(smed)) * (-1);
			$("#ulsl").css('left',sme3);
			mou_x = mou_t;
			if (smed > 0) {napr = 'L'};
			if (smed < 0) {napr = 'R'};
			if (smed == 0) {napr = ''};
			//console.log('mou_dow=' +mou_dow+ ' mou_x=' + mou_x + ' sme1=' + sme1 + " sme2="+sme2 + " smed="+smed + 'napr='+ napr + ' X='+eventObject.pageX            + " Y="+eventObject.pageY);
		};
	});
	<? /* Нажали */ ?>
	$('.item').mousedown(function(eventObject){
		mou_dow = 1; mou_x = eventObject.pageX; mou_t = mou_x;
	});
	<? /* Отпустили */ ?>
	$('.item').mouseup(function(eventObject){
		mou_dow = 0; 
		if ((napr == 'L') && (tek_slid != -4)) { --tek_slid; };
		if ((napr == 'R') && (tek_slid != 0) ) { ++tek_slid; };
		$('#s'+tek_slid).click();
	});
	<? /* Ушли за item равносильно отпустили - но ни чего не делаем */ ?>
	$('.item').mouseout(function(){
		mou_dow = 0; napr = ''; // console.log(mou_dow);
	});
	<? /* Регистрация - открыть */ ?>
	$('#but_join').click( function() {
		$('#lay-hid_join').css('display','block');
		$("#lay-hid_join").animate({ opacity: 1}, { duration: 1000, easing: "swing" });
		$("#lay-hid_join_forma").animate({ 'margin-top': 20}, { duration: 1000, easing: "swing" });
	});
	<? /* Регистрация - закрыть */ ?>
	$('#lay-hid_join_close').click( function() {
		$("#lay-hid_join").fadeOut('slow', function() {
			$("#lay-hid_join_forma").css('margin-top','-20%');
			$('#lay-hid_join').css('display','none');
			$('#lay-hid_join').css('opacity','0.1');
		});
	});
	<? /* Регистрация - проверить email  */ ?>
	$('#reg_mai').change( function() {
		//alert('ok');
		$.ajax({
			url: 'index.php?r=site/email',
			type: 'post',
			async: true,
			data: 'email='+$('#reg_mai').val(),
			dataType: 'json',
			success: function(json) {
				if (json['otv'] == "ok") { $('#error').text(""); return;};
				$('#error').text("Такой email уже есть");
			}
		});
	});
	<? /* Регистрация - скрыть/открыть последние реквизиты - форма */ ?>
	$('[id ^= "reg_pa"]').change( function() {
		if ($('#reg_pa1').prop('checked')) {
			$('#reg_gr1').css('display','none');
		} else {
			$('#reg_gr1').css('display','inline-block');
		};
	});
	<? /* Регистрация - проверить все реквизиты - форма */ ?>
	$('#form_reg').submit( function() {
		soo = "";
		if ($('#reg_nam').val() == "") {soo += "<br>Имя пустое";};
		if ($('#reg_lna').val() == "") {soo += "<br>Фамилия пустая";};
		if ($('#reg_mai').val() == "") {soo += "<br>Email пустой"; };
		if ($('#reg_pho').val() == "") {soo += "<br>Телефон пустой"; };
		if (!$('#reg_pa1').prop('checked')) {
			if ($('#reg_kpr').val() == "") {soo += "<br>Кол-во профессий пусто"; };
			if ($('#reg_ypr').val() == "") {soo += "<br>Профессия пуста"; };
			if ($('#reg_sum').val() == "") {soo += "<br>Резюме пустое"; };
		};
		if (soo != "") {$('#error').html(soo); return false;};
		return true;
	});
	<? /* Авторизация - открыть */ ?>
	$('#but_signin').click( function() {
		$('#lay-hid_signin').css('display','block');
		$("#lay-hid_signin").animate({ opacity: 1}, { duration: 1000, easing: "swing" });
		$("#lay-hid_signin_forma").animate({ 'margin-top': 20}, { duration: 1000, easing: "swing" });
	});
	<? /* Авторизация - закрыть */ ?>
	$('#lay-hid_signin_close').click( function() {
		$("#lay-hid_signin").fadeOut('slow', function() {
			$("#lay-hid_signin_forma").css('margin-top','-20%');
			$('#lay-hid_signin').css('display','none');
			$('#lay-hid_signin').css('opacity','0.1');
		});
	});
	<? /* Авторизация - авторизация - форма */ ?>
	$('#form_sig').submit( function() {
		soo = "";
		if ($('#sig_mai').val() == "") {soo += "<br>Email пустой"; };
		if ($('#sig_pas').val() == "") {soo += "<br>Пароль пустой"; };
		if (soo != "") {$('#sig_error').html(soo); return false;};
		return true;
	});
	<? /* Напоминание - открыть */ ?>
	$('#lay-hid_sigin_fyp').click( function() {
		$('#lay-hid_signin_close').click(); <? /* Закрыть авторизацию */ ?>
		$('#lay-hid_forgot').css('display','block');
		$("#lay-hid_forgot").animate({ opacity: 1}, { duration: 1000, easing: "swing" });
		$("#lay-hid_forgot_forma").animate({ 'margin-top': 20}, { duration: 1000, easing: "swing" });
	});
	<? /* Напоминание - закрыть */ ?>
	$('#lay-hid_forgot_close').click( function() {
		$("#lay-hid_forgot").fadeOut('slow', function() {
			$("#lay-hid_forgot_forma").css('margin-top','-20%');
			$('#lay-hid_forgot').css('display','none');
			$('#lay-hid_forgot').css('opacity','0.1');
		});
	});
	<? /* Прайс 1*/ ?>
	$('#join1').click( function() {
		$('#reg_pa1').prop('checked',false);
		$('#reg_pa2').prop('checked',true);
		$('#reg_pa3').prop('checked',false);
		$('#reg_pa4').prop('checked',false);
		$('#reg_gr1').css('display','inline-block');
		$('#but_join').click(); 
	});
	<? /* Прайс 2*/ ?>
	$('#join2').click( function() {
		$('#reg_pa1').prop('checked',false);
		$('#reg_pa2').prop('checked',false);
		$('#reg_pa3').prop('checked',true);
		$('#reg_pa4').prop('checked',false);
		$('#reg_gr1').css('display','inline-block');
		$('#but_join').click(); 
	});
	<? /* Прайс 3*/ ?>
	$('#join3').click( function() {
		$('#reg_pa1').prop('checked',false);
		$('#reg_pa2').prop('checked',false);
		$('#reg_pa3').prop('checked',false);
		$('#reg_gr1').css('display','inline-block');
		$('#reg_pa4').prop('checked',true);
		$('#but_join').click(); 
	});
	<? /* Галерея команды - скроллинг - кнопка следующий (вправо) */ ?>
	kol_team = <?=$kol_team?>;
	left_team = 0;
	$('#but_team_prev').click( function() {
		wid1 = $('#team_slid').css('width');
		wid2 = Number(wid1.replace(/\D+/g,"")); <? /* Ширина окна показа */ ?> 
		sme1 = $('.team-item').css('width');
		sme2 = Number(sme1.replace(/\D+/g,""))+20; <? /* Ширина 1 элемента */ ?> 
		//www1 = sme2 * kol_team; <? /* Ширина всех элементов */ ?> 
		kkk1 = Math.floor(wid2 / sme2); <? /* Сколько элементов влезает в окно */ ?> 
		kkk2 = wid2 / sme2; <? /* Сколько элементов влезает в окно */ ?> 
		mmm1 = kol_team - kkk1; <? /* макс элементов слева */ ?> 
		//console.log('prev =>>= wid2='+wid2 +' , sme2='+sme2  +' , kkk1='+mmm1 +' , mmm1='+mmm1 +' , left_team='+left_team);
		aaa = '=>>= окно wid2='+wid2 +' , элем sme2='+sme2  +' , кол.эл.в окне kkk1='+kkk1 +' , макс слева mmm1='+mmm1 +' , left_team='+left_team;
		//$('#otl').text(aaa);
		if (left_team < (mmm1-0) ) {
		<? /* ...........*****wid2****..... */ ?>
			       <? /* |====kkk1====|===== ===== =sme2=   www1*/ ?>
		      <? /* LLLLL|===== =====|===== ===== */ ?>
		<? /* LLLLL LLLLL|===== =====|===== */ ?>
  <? /* LLLLL LLLLL LLLLL|===== =====| */ ?>
		//if (left_team < (kol_team -3)) {
			++left_team;
			//sme3 = sme2 * $(this).attr('sme');
			sme3 = left_team * sme2;
			$('#team_' + left_team).css('display', 'none'); 
			//$('#team_' + left_team).css('margin-left', '-'+sme3+'px'); 
		};
	});
	<? /* Галерея команды - скроллинг - кнопка предыдущий (cлева) $kol_team*/ ?>
	$('#but_team_next').click( function() {
		wid1 = $('#team_slid').css('width');
		wid2 = Number(wid1.replace(/\D+/g,"")); <? /* Ширина окна показа */ ?> 
		sme1 = $('.team-item').css('width');
		sme2 = Number(sme1.replace(/\D+/g,""))+20; <? /* Ширина 1 элемента */ ?> 
		www1 = sme2 * kol_team; <? /* Ширина всех элементов */ ?> 
		kkk1 = Math.floor(wid2 / sme2); <? /* Сколько элементов влезает в окно */ ?> 
		kkk2 = wid2 / sme2; <? /* Сколько элементов влезает в окно */ ?> 
		mmm1 = kol_team - kkk1; <? /* макс элементов слева */ ?> 
		//console.log('prev =>>= wid2='+wid2 +'sme2='+sme2 +'www1='+www1 +'kkk1='+mmm1 +'  mmm1='+mmm1 +'left_team='+left_team);
		aaa = '=>>= окно wid2='+wid2 +' , элем sme2='+sme2  +' , кол.эл.в окне kkk1='+kkk1 +' , макс слева mmm1='+mmm1 +' , left_team='+left_team;
		//$('#otl').text(aaa);
		if (left_team > 0) {
			$('#team_' + left_team).css('display', 'inline-block'); 
			--left_team;
			sme3 = left_team * sme2;
			//$('#team_1').css('margin-left', '-'+sme3+'px'); 
			//$('#team_' + left_team).css('display', 'inline-block'); 
		};
	});
	$(window).resize(function () {
		wid1 = $('#team_slid').css('width');
		wid2 = Number(wid1.replace(/\D+/g,"")); <? /* Ширина окна показа */ ?> 
		sme1 = $('.team-item').css('width');
		sme2 = Number(sme1.replace(/\D+/g,""))+20; <? /* Ширина 1 элемента */ ?> 
		//www1 = sme2 * kol_team; <? /* Ширина всех элементов */ ?> 
		kkk1 = Math.floor(wid2 / sme2); <? /* Сколько элементов влезает в окно */ ?>
		kkk2 = wid2 / sme2; <? /* Сколько элементов влезает в окно */ ?> 
		eee1 = kkk1 * sme2;
		//$('.car-wrap').css('width', eee1+'px'); 
		mmm1 = kol_team - kkk1; <? /* макс элементов слева */ ?> 
		//console.log('resize =>>= wid2='+wid2 +' sme2='+sme2  +' kkk1='+kkk1 +'  mmm1='+mmm1 +'left_team='+left_team);
		aaa = '=>>= окно wid1='+wid1+' , wid2='+wid2 +' , элем sme2='+sme2  +' , кол.эл.в окне kkk1='+kkk1 + '='+kkk2 +' , макс слева mmm1='+mmm1 +' , left_team='+left_team;
		//$('#otl').text(aaa);
		if (left_team > 0) {
			sme3 = left_team * sme2;
			//$('#team_1').css('margin-left', '-'+sme3+'px'); 
		};
	});
});
</script>
