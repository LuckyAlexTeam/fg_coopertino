<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{advancedeucompliance}prestashop>advancedeucompliance_bb8956c67b82c7444a80c6b2433dd8b4'] = '您想卸除這個模組嗎？';
$_MODULE['<{advancedeucompliance}prestashop>advancedeucompliance_cac77d4c7de337f13ad1e89a8cecbc7f'] = '運輸：1 至 3 星期';
$_MODULE['<{advancedeucompliance}prestashop>advancedeucompliance_eeb992239a8454067cd7a47bbf0df359'] = '運輸：3 至 6 星期';
$_MODULE['<{advancedeucompliance}prestashop>advancedeucompliance_f4a0d7cb0cd45214c8ca5912c970de13'] = '含稅';
$_MODULE['<{advancedeucompliance}prestashop>advancedeucompliance_befcac0f9644a7abee43e69f49252ac4'] = '未稅';
$_MODULE['<{advancedeucompliance}prestashop>advancedeucompliance_5da618e8e4b89c66fe86e32cdafde142'] = '從';
$_MODULE['<{advancedeucompliance}prestashop>advancedeucompliance_89bea8045e50a337d8ce9849a4e1633c'] = '不含運費';
$_MODULE['<{advancedeucompliance}prestashop>advancedeucompliance_ea21841da70e6405af19fabc4ff8bdd9'] = '缺少';
$_MODULE['<{advancedeucompliance}prestashop>advancedeucompliance_fc724a540ce30b0b634163c922d60bed'] = '設定儲存成功!';
$_MODULE['<{advancedeucompliance}prestashop>advancedeucompliance_23e162ba6bf36c7560ebbc868d9e2552'] = '法律聲明';
$_MODULE['<{advancedeucompliance}prestashop>advancedeucompliance_c5f29bb36f9158d2e00f5d4dc213a0ff'] = '隱私';
$_MODULE['<{advancedeucompliance}prestashop>advancedeucompliance_3ad6cb6100190c8f81e99fd52debf7ef'] = '標籤';
$_MODULE['<{advancedeucompliance}prestashop>advancedeucompliance_00d23a76e43b46dae9ec7aa9dcbebb32'] = '啟用';
$_MODULE['<{advancedeucompliance}prestashop>advancedeucompliance_b9f5c797ebbf55adccdd8539a65a0241'] = '停用';
$_MODULE['<{advancedeucompliance}prestashop>advancedeucompliance_c9cc8cce247e49bae79f15173ce97354'] = '儲存';
$_MODULE['<{advancedeucompliance}prestashop>advancedeucompliance_98f770b0af18ca763421bac22b4b6805'] = '功能：';
$_MODULE['<{advancedeucompliance}prestashop>email_attachments_form_e29252902fd4eaee6279ea5b60bc9092'] = '電子郵件範本';
$_MODULE['<{advancedeucompliance}prestashop>email_attachments_form_c9cc8cce247e49bae79f15173ce97354'] = '儲存';
$_MODULE['<{advancedeucompliance}prestashop>legal_cms_manager_form_c9cc8cce247e49bae79f15173ce97354'] = '儲存';
$_MODULE['<{advancedeucompliance}prestashop>hookoverridetosdisplay_97abaa921b2270031573362f57aad4a4'] = '條款及細則';


return $_MODULE;
