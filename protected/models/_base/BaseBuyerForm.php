<?php

/**
 * This is the model class for table "buyer_form".
 *
 * The followings are the available columns in table 'buyer_form':
 * @property integer $id
 * @property integer $user_id
 * @property integer $buyer_location
 * @property string $product_range
 * @property integer $trading
 * @property integer $professional_expiriens
 * @property integer $travel_ability
 *
 * The followings are the available model relations:
 * @property User $user
 * @property TradingList $trading0
 * @property AllProfexpiriensList $professionalExpiriens
 * @property ProductRangeToBuyerForm[] $productRangeToBuyerForms
 */
class BaseBuyerForm extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return BuyerForm the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'buyer_form';
	}

	/**
	 * @return array validation rules for model attributes.
	 */ 
	 
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, buyer_location, trading, professional_expiriens, travel_ability', 'required'),
			array('user_id, buyer_location, trading, professional_expiriens, travel_ability', 'numerical', 'integerOnly'=>true),
			array('product_range', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user_id, buyer_location, product_range, trading, professional_expiriens, travel_ability', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'trading0' => array(self::BELONGS_TO, 'TradingList', 'trading'),
			'professionalExpiriens' => array(self::BELONGS_TO, 'AllProfexpiriensList', 'professional_expiriens'),
			'productRangeToBuyerForms' => array(self::HAS_MANY, 'ProductRangeToBuyerForm', 'buyer_form_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'buyer_location' => 'Buyer Location',
			'product_range' => 'Product Range',
			'trading' => 'Trading',
			'professional_expiriens' => 'Professional Expiriens',
			'travel_ability' => 'Travel Ability',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('buyer_location',$this->buyer_location);
		$criteria->compare('product_range',$this->product_range,true);
		$criteria->compare('trading',$this->trading);
		$criteria->compare('professional_expiriens',$this->professional_expiriens);
		$criteria->compare('travel_ability',$this->travel_ability);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}