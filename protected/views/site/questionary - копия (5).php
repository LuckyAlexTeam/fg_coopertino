<!-- Анкета Модель 
<style type="text/css">
	.photographer, .agency, .model, .designer {
		display: none;
	}
	.expert, .stylist {
		display: inline-block;
	}
</style> -->
<style type="text/css">
	.pageblock, .mod, .fot, .diz, .sti, .art, .mua {
		display: none;
	}
</style>

<!--
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
-->
<script type="text/javascript">
	$(document).ready(function(){
		//alert(55);
		$(".p-check").change(function(){
			//alert(66);
			var router = $(this).children("input").attr('name');
			var trigger = '';
			//alert(router);
			//$('.pageblock').attr('style', 'display:none;');
			if(router == 'model'){
				trigger = '.mod';
			}
			if(router == 'photographer'){
				trigger = '.fot';
			}
			if(router == 'designer'){
				trigger = '.diz';
			}
			if(router == 'agency'){
				trigger = '.mua';
			}
			if(router == 'Fashion Expert'){
				trigger = '.sti';
			}
			//alert("1: " + router + ", 2: " + trigger);
			$(trigger).toggle();
			router = '';
			trigger = '';
			//
		});
	});
</script>

<div class="swich">
	<label class="p-check"><input type="checkbox" name="model"><span><div></div></span>model</label> | <label class="p-check"><input type="checkbox" name="photographer"><span><div></div></span>photographer</label> | <label class="p-check"><input type="checkbox" name="designer"><span><div></div></span>designer</label> | <label class="p-check"><input type="checkbox" name="agency"><span><div></div></span>agency</label> | <label class="p-check"><input type="checkbox" name="Fashion Expert"><span><div></div></span>Fashion Expert</label>
</div>
<div class="person-wrapper">
<!-- Эксперты -->
	<form class="acc-prof expert">
		<div class="d-wrap">
			<div class="details-item">
				*Specialization:
			</div>
			<div class="details-data general-input">
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Users[Brand]"><span><div></div></span>Brand</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Users[Buyer]"><span><div></div></span>Buyer</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Users[Customer]"><span><div></div></span>Customer</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Users[CompanyRepresentative]"><span><div></div></span>Company Representative</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Users[Media]"><span><div></div></span>Media</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Users[Stylist]"><span><div></div></span>Stylist</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Users[HairStylistArtist]"><span><div></div></span>Hair Stylist/Artist</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Users[MUA]"><span><div></div></span>MUA</label>
				</div>
			</div>
		</div>
<!-- Эксперты Конец -->
	<h2>Подробно заполните Вашу анкету</h2><!-- Модель, Фотограф, Дезайнер, Эксперт -->
		<h2>General information</h2>
		<span class="hr"></span>
		<div class="d-wrap model designer photographer buyer customer repres media"><!-- Модель, Фотограф, Дизайнер, Buyer, Customer, Representative, Media -->
			<div class="details-item">
				*First Name:
			</div>
			<div class="details-data general-input">
				<input type="text" class="f-n" placeholder="First Name *"></input>
			</div>
		</div>
		<div class="d-wrap agency"><!-- Агентство -->
			<div class="details-item">
				*Name:
			</div>
			<div class="details-data general-input">
				<input type="text" class="a-n" placeholder="Name *"></input>
			</div>
		</div>
		<div class="d-wrap model designer photographer buyer customer repres media"><!-- Модель, Фотограф, Дезайнер, Buyer, Customer, Representative, Media -->
			<div class="details-item">
				*Last Name:
			</div>
			<div class="details-data general-input">
				<input type="text" class="l-n" placeholder="Last Name *"></input>
			</div>
		</div>
		<div class="d-wrap brand"><!-- Brand -->
			<div class="details-item">
				*Name of company:
			</div>
			<div class="details-data general-input">
				<input type="text" class="a-n" placeholder="Name of company*"></input>
			</div>
		</div>
		<div class="d-wrap brand"><!-- Brand -->
			<div class="details-item">
				*Brand location:
			</div>
			<div class="details-data general-input">
				<div class="loc loc-m">
					<label class="q-check"><input type="checkbox" name="Users[China]" id = 'cb15' onchange = 'showOrHide("cb15", "cat15");'/><span><div></div></span>China:</label>
					<div class="loc loc-ch" id="cat15">
						<label class="q-check"><input type="checkbox" name="Users[Beijing]"><span><div></div></span>Beijing</label>
						<label class="q-check"><input type="checkbox" name="Users[Hong Kong]"><span><div></div></span>Hong Kong</label>
						<label class="q-check"><input type="checkbox" name="Users[Shanghai]"><span><div></div></span>Shanghai</label>
						<label class="q-check"><input type="checkbox" name="Users[Ch-Other]"><span><div></div></span>Other</label>
					</div>
				</div>
				<div class="loc loc-m">
					<label class="q-check"><input type="checkbox" name="Users[England]" id = 'cb16' onchange = 'showOrHide("cb16", "cat16");'/><span><div></div></span>England:</label>
					<div class="loc loc-en" id="cat16">
						<label class="q-check"><input type="checkbox" name="Users[London]"><span><div></div></span>London</label>
						<label class="q-check"><input type="checkbox" name="Users[Londonarea]"><span><div></div></span>London area</label>
						<label class="q-check"><input type="checkbox" name="Users[En-Other]"><span><div></div></span>Other</label>
					</div>
				</div>
				<div class="loc loc-m">
						<label class="q-check"><input type="checkbox" name="Users[France]" id = 'cb17' onchange = 'showOrHide("cb17", "cat17");'/><span><div></div></span>France:</label>
					<div class="loc loc-fr" id="cat17">
						<label class="q-check"><input type="checkbox" name="Users[Paris]"><span><div></div></span>Paris</label>
						<label class="q-check"><input type="checkbox" name="Users[Parisarea]"><span><div></div></span>Paris area</label>
						<label class="q-check"><input type="checkbox" name="Users[Fr-Other]"><span><div></div></span>Other</label>
					</div>
				</div>
				<div class="loc loc-m">
						<label class="q-check"><input type="checkbox" name="Users[Germany]" id = 'cb18' onchange = 'showOrHide("cb18", "cat18");'/><span><div></div></span>Germany:</label>
					<div class="loc loc-ge" id="cat18">
						<label class="q-check"><input type="checkbox" name="Users[Berlin]"><span><div></div></span>Berlin</label>
						<label class="q-check"><input type="checkbox" name="Users[Berlinarea]"><span><div></div></span>Berlin area</label>
						<label class="q-check"><input type="checkbox" name="Users[Ge-Other]"><span><div></div></span>Other</label>
					</div>
				</div>
				<div class="loc loc-m">
						<label class="q-check"><input type="checkbox" name="Users[Italy]" id='cb19' onchange = 'showOrHide("cb19", "cat19");'/><span><div></div></span>Italy:</label>
					<div class="loc loc-it" id="cat19">
						<label class="q-check"><input type="checkbox" name="Users[Milan]"><span><div></div></span>Milan</label>
						<label class="q-check"><input type="checkbox" name="Users[Milanarea]"><span><div></div></span>Milan area</label>
						<label class="q-check"><input type="checkbox" name="Users[Rome]"><span><div></div></span>Rome</label>
						<label class="q-check"><input type="checkbox" name="Users[Romearea]"><span><div></div></span>Rome area</label>
						<label class="q-check"><input type="checkbox" name="Users[It-Other]"><span><div></div></span>Other</label>
					</div>
				</div>
				<div class="loc loc-m">
						<label class="q-check"><input type="checkbox" name="Users[USA]" id = 'cb20' onchange = 'showOrHide("cb20", "cat20");'/><span><div></div></span>USA:</label>
					<div class="loc loc-usa" id="cat20">
						<label class="q-check"><input type="checkbox" name="Users[LosAngeles]"><span><div></div></span>Los Angeles</label>
						<label class="q-check"><input type="checkbox" name="Users[LosAngelesarea]"><span><div></div></span>Los Angeles area</label>
						<label class="q-check"><input type="checkbox" name="Users[NewYork]"><span><div></div></span>New York</label>
						<label class="q-check"><input type="checkbox" name="Users[NewYorkarea]"><span><div></div></span>New York area</label>
						<label class="q-check"><input type="checkbox" name="Users[USA-Other]"><span><div></div></span>Other</label>
					</div>
				</div>
				<div class="loc loc-m">
						<label class="q-check"><input type="checkbox" name="Users[Ukraine]"><span><div></div></span>Ukraine</label>
				</div>
				<div class="loc loc-m">
						<label class="q-check"><input type="checkbox" name="Users[Russia]"><span><div></div></span>Russia</label>
				</div>
				<div class="loc loc-m">
						<label class="q-check"><input type="checkbox" name="Users[Other]"><span><div></div></span>Other</label>
				</div>
			</div>
		</div>
		<div class="d-wrap model designer photographer agency brand buyer customer repres media"><!-- Модель, Фотограф, Дезайнер, Brand, Buyer, Customer, Representative, Media -->
			<div class="details-item">
				*Phone number:
			</div>
			<div class="details-data general-input">
				<input type="phone" class="ph-n" placeholder="Phone Number *" name="Users[PhoneNumber]"></input>
			</div>
		</div>
		<div class="d-wrap model designer photographer agency brand buyer customer repres media"><!-- Модель, Фотограф, Дезайнер, Brand, Buyer, Customer, Representative, Media -->
			<div class="details-item">
				*E-mail:
			</div>
			<div class="details-data general-input">
				<input type="text" class="em" placeholder="E-mail *"  name="Users[E-mail]"></input>
			</div>
		</div>
		<div class="d-wrap model designer photographer agency brand buyer customer repres media"><!-- Модель, Фотограф, Дезайнер, Brand, Buyer, Customer, Representative, Media -->
			<div class="details-item">
				*Skype:
			</div>
			<div class="details-data general-input">
				<input type="text" class="sk" placeholder="Skype *" name="Users[Skype]"></input>
			</div>
		</div>
		<div class="d-wrap model"><!-- Модель -->
			<div class="details-item">
				Agency Info:
			</div>
			<div class="details-data general-input">
				<input type="text" class="a-i" placeholder="Agency Info:" name="Users[AgencyInfo]"></input>
			</div>
		</div>
		<div class="d-wrap model designer photographer expert agency brand buyer customer repres media"><!-- Модель, Фотограф, Дезайнер, Brand, Buyer, Customer, Representative, Media -->
			<div class="details-item">
				Web-site:
			</div>
			<div class="details-data general-input">
				<input type="text" class="w-s" placeholder="Web-site:" name="Users[Web-site]"></input>
			</div>
		</div>
		<div class="d-wrap model designer photographer buyer customer repres media"><!-- Модель, Фотограф, Дезайнер, Buyer, Customer, Representative, Media -->
			<div class="details-item">
				*Gender:
			</div>
			<div class="details-data general-input">
				<select class="ge">
					<option>Male</option>
					<option>Female</option>
				</select>
			</div>
		</div>
		<div class="d-wrap model designer photographer buyer customer repres media"><!-- Модель, Фотограф, Дезайнер, Buyer, Customer, Representative, Media -->
			<div class="details-item">
				*Date of Birth:
			</div>
			<div class="details-data general-input">
				<input type="text" class="d-b" name="Users[DateofBirth]"></input>
			</div>
		</div>
		<div class="d-wrap model designer photographer buyer customer repres media"><!-- Модель, Фотограф, Дезайнер, Buyer, Customer, Representative, Media -->
			<div class="details-item">
				Choose what to show:
			</div>
			<div class="details-data general-input">
				<label class="q-rad">
					<input type="radio" name="Users[metric]" id="age" value="Age"></input>
					<span><div></div></span>Age</label>
				<label class="q-rad">
					<input type="radio" name="Users[metric]" id="date" value="Date of Birth" checked></input>
				<span><div></div></span>Date of Birth</label>
			</div>
		</div>
		<div class="d-wrap model"><!-- Модель -->
			<div class="details-item">
				*Do you have your Legal Guardian’s agree [required for models under 18]:
			</div>
			<div class="details-data general-input">
				<select class="guardians">
					<option>Yes</option>
					<option>No</option>
				</select>
			</div>
		</div>
		<div class="d-wrap model designer photographer agency buyer customer repres media"><!-- Модель, Фотограф, Дезайнер, Агентство, Buyer, Customer, Representative, Media -->
			<div class="details-item">
				*Location:
			</div>
			<div class="details-data general-input">
				<script type="text/javascript">
					function showOrHide(cb, cat) {
						cb = document.getElementById(cb);
						cat = document.getElementById(cat);
						if (cb.checked) cat.style.display = "block";
					else cat.style.display = "none";
					}
				</script>
				<div class="loc loc-m">
					<label class="q-check"><input type="checkbox" name="Users[LocChina]" id = 'cb1' onchange = 'showOrHide("cb1", "cat1");'/><span><div></div></span>China:</label>
					<div class="loc loc-ch" id="cat1">
						<label class="q-check"><input type="checkbox" name="Users[LocBeijing]"><span><div></div></span>Beijing</label>
						<label class="q-check"><input type="checkbox" name="Users[LocHongKong]"><span><div></div></span>Hong Kong</label>
						<label class="q-check"><input type="checkbox" name="Users[LocShanghai]"><span><div></div></span>Shanghai</label>
						<label class="q-check"><input type="checkbox" name="Users[LocCh-Other]"><span><div></div></span>Other</label>
					</div>
				</div>
				<div class="loc loc-m">
					<label class="q-check"><input type="checkbox" name="Users[LocEngland]" id = 'cb2' onchange = 'showOrHide("cb2", "cat2");'/><span><div></div></span>England:</label>
					<div class="loc loc-en" id="cat2">
						<label class="q-check"><input type="checkbox" name="Users[LocLondon]"><span><div></div></span>London</label>
						<label class="q-check"><input type="checkbox" name="Users[LocLondonarea]"><span><div></div></span>London area</label>
						<label class="q-check"><input type="checkbox" name="Users[LocEn-Other]"><span><div></div></span>Other</label>
					</div>
				</div>
				<div class="loc loc-m">
						<label class="q-check"><input type="checkbox" name="Users[LocFrance]" id = 'cb3' onchange = 'showOrHide("cb3", "cat3");'/><span><div></div></span>France:</label>
					<div class="loc loc-fr" id="cat3">
						<label class="q-check"><input type="checkbox" name="Users[LocParis]"><span><div></div></span>Paris</label>
						<label class="q-check"><input type="checkbox" name="Users[LocParisarea]"><span><div></div></span>Paris area</label>
						<label class="q-check"><input type="checkbox" name="Users[LocFr-Other]"><span><div></div></span>Other</label>
					</div>
				</div>
				<div class="loc loc-m">
						<label class="q-check"><input type="checkbox" name="Users[LocGermany]" id = 'cb4' onchange = 'showOrHide("cb4", "cat4");'/><span><div></div></span>Germany:</label>
					<div class="loc loc-ge" id="cat4">
						<label class="q-check"><input type="checkbox" name="Users[LocBerlin]"><span><div></div></span>Berlin</label>
						<label class="q-check"><input type="checkbox" name="Users[LocBerlinarea]"><span><div></div></span>Berlin area</label>
						<label class="q-check"><input type="checkbox" name="Users[LocGe-Other]"><span><div></div></span>Other</label>
					</div>
				</div>
				<div class="loc loc-m">
						<label class="q-check"><input type="checkbox" name="Users[LocItaly]" id = 'cb5' onchange = 'showOrHide("cb5", "cat5");'/><span><div></div></span>Italy:</label>
					<div class="loc loc-it" id="cat5">
						<label class="q-check"><input type="checkbox" name="Users[LocMilan]"><span><div></div></span>Milan</label>
						<label class="q-check"><input type="checkbox" name="Users[LocMilanarea]"><span><div></div></span>Milan area</label>
						<label class="q-check"><input type="checkbox" name="Users[LocRome]"><span><div></div></span>Rome</label>
						<label class="q-check"><input type="checkbox" name="Users[LocRomearea]"><span><div></div></span>Rome area</label>
						<label class="q-check"><input type="checkbox" name="Users[LocIt-Other]"><span><div></div></span>Other</label>
					</div>
				</div>
				<div class="loc loc-m">
						<label class="q-check"><input type="checkbox" name="Users[LocUSA]" id = 'cb6' onchange = 'showOrHide("cb6", "cat6");'/><span><div></div></span>USA:</label>
					<div class="loc loc-usa" id="cat6">
						<label class="q-check"><input type="checkbox" name="Users[LocLosAngeles]"><span><div></div></span>Los Angeles</label>
						<label class="q-check"><input type="checkbox" name="Users[LocLosAngelesarea]"><span><div></div></span>Los Angeles area</label>
						<label class="q-check"><input type="checkbox" name="Users[LocNewYork]"><span><div></div></span>New York</label>
						<label class="q-check"><input type="checkbox" name="Users[LocNewYorkarea]"><span><div></div></span>New York area</label>
						<label class="q-check"><input type="checkbox" name="Users[LocUSA-Other]"><span><div></div></span>Other</label>
					</div>
				</div>
				<div class="loc loc-m">
						<label class="q-check"><input type="checkbox" name="Users[LocUkraine]"><span><div></div></span>Ukraine</label>
				</div>
				<div class="loc loc-m">
						<label class="q-check"><input type="checkbox" name="Users[LocRussia]"><span><div></div></span>Russia</label>
				</div>
				<div class="loc loc-m">
						<label class="q-check"><input type="checkbox" name="Users[LocOther]"><span><div></div></span>Other</label>
				</div>
			</div>
		</div>
		<div class="d-wrap agency"><!-- Агентство -->
			<div class="details-item">
				*Address:
			</div>
			<div class="details-data general-input">
				<input type="text" class="a-a" placeholder="Address *" name="Users[AgeAddress]"></input>
			</div>
		</div>
		<div class="d-wrap agency"><!-- Агентство -->
			<div class="details-item">
				Official social media:
			</div>
			<div class="details-data general-input">
				<input type="text" class="a-osm" placeholder="Official social media:" name="Users[Officialsocialmedia]"></input>
			</div>
		</div>
		<!-- Модель -->
		<h2>Модель</h2>
		<h2>Physical Details</h2>
		<span class="hr"></span>
		<div class="d-wrap">
			<div class="details-item">
				Choose you metrical system:
			</div>
			<div class="details-data general-input">
				<label class="q-rad">
					<input type="radio" name="Users[Phymetric]" id="cm" value="cm" checked></input>
					<span><div></div></span>cm</label>
				<label class="q-rad">
					<input type="radio" name="Users[Phymetric]" id="in" value="in"></input>
				<span><div></div></span>in</label>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Height:
			</div>
			<div class="details-data general-input">
				<input type="number" min="61" max="229" class="he" placeholder="165 cm" name="Users[PhyHeight]"></input>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Weight:
			</div>
			<div class="details-data general-input">
				<input type="number" min="30" max="170" class="we" placeholder="65 kg" name="Users[PhyWeight]"></input>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Chest:
			</div>
			<div class="details-data general-input">
				<input type="number" min="54" max="230" class="che" placeholder="85 cm" name="Users[PhyChest]"></input>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Waist:
			</div>
			<div class="details-data general-input">
				<input type="number" min="48" max="134" class="wa" placeholder="60 cm" name="Users[PhyWaist]"></input>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Hips:
			</div>
			<div class="details-data general-input">
				<input type="number" min="48" max="134" class="hips" placeholder="95 cm" name="Users[PhyHips]"></input>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Dress/Jacket, EU:
			</div>
			<div class="details-data general-input">
				<input type="number" min="32" max="60" class="dre" placeholder="38 EU" name="Users[PhyDressJacket]"></input>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Cup:
			</div>
			<div class="details-data general-input">
				<input type="text" class="cup" placeholder="C" name="Users[PhyCup]"></input>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Shoe, EU:
			</div>
			<div class="details-data general-input">
				<input type="number" min="34" max="47" class="shoe" placeholder="37 EU" name="Users[PhyShoe]"></input>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Ethnicity:
			</div>
			<div class="details-data general-input">
				<select class="ethn">
					<option>African</option>
					<option>Asian</option>
					<option>European</option>
					<option>Hispanic</option>
					<option>Indian</option>
					<option>Latin American</option>
					<option>Mediterranean</option>
					<option>Near East</option>
					<option>Scandinavian</option>
					<option>Mixed</option>
					<option>Other тот пункт поле для ввода текста</option>ЭЭЭЭЭЭЭЭЭЭЭ
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Skin color:
			</div>
			<div class="details-data general-input">
				<select class="sc-c">
					<option>Pale white</option>
					<option>White</option>
					<option>Olive</option>
					<option>Light brown</option>
					<option>Brown</option>
					<option>Dark brown</option>
					<option>Black</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Eye color:
			</div>
			<div class="details-data general-input">
				<select class="ey-c">
					<option>Black</option>
					<option>Blue</option>
					<option>Green</option>
					<option>Hazel</option>
					<option>Other</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Natural hair color:
			</div>
			<div class="details-data general-input">
				<select class="nh-c">
					<option>Blonde</option>
					<option>Strawberry Blonde</option>
					<option>Auburn</option>
					<option>Ginger</option>
					<option>Light Brown</option>
					<option>Brown</option>
					<option>Dark Brown</option>
					<option>Salt & Pepper</option>
					<option>Black</option>
					<option>Other</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Hair coloration:
			</div>
			<div class="details-data general-input">
				<div class="h-col-m">
					<label class="q-check"><input type="checkbox" name="Users[HairNotcolored]"><span><div></div></span>Not colored</label>
				</div>
				<div class="h-col-m">
					<label class="q-check"><input type="checkbox" name="Users[HairColored]" id = 'cb7' onchange = 'showOrHide("cb7", "cat7");'/><span><div></div></span>Colored</label>
				</div>
				<div class="h-col-h" id="cat7">
						<label class="q-check"><input type="checkbox" name="Users[HairBlonde]"><span><div></div></span>Blonde</label>
						<label class="q-check"><input type="checkbox" name="Users[HairStrawberyBlonde]"><span><div></div></span>Strawbery Blonde</label>
						<label class="q-check"><input type="checkbox" name="Users[HairAuburn]"><span><div></div></span>Auburn</label>
						<label class="q-check"><input type="checkbox" name="Users[HairGinger]"><span><div></div></span>Ginger</label>
						<label class="q-check"><input type="checkbox" name="Users[HairRed]"><span><div></div></span>Red</label>
						<label class="q-check"><input type="checkbox" name="Users[HairLightBrown]"><span><div></div></span>Light Brown</label>
						<label class="q-check"><input type="checkbox" name="Users[HairBrown]"><span><div></div></span>Brown</label>
						<label class="q-check"><input type="checkbox" name="Users[HairDarkBrown]"><span><div></div></span>Dark Brown</label>
						<label class="q-check"><input type="checkbox" name="Users[HairSaltPepper]"><span><div></div></span>Salt & Pepper</label>
						<label class="q-check"><input type="checkbox" name="Users[HairBlack]"><span><div></div></span>Black</label>
						<label class="q-check"><input type="checkbox" name="Users[HairBlackBlue]"><span><div></div></span>Black Blue</label>
						<label class="q-check"><input type="checkbox" name="Users[HairOther]"><span><div></div></span>Other</label>
				</div>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Readness to change color:
			</div>
			<div class="details-data general-input">
				<select>
					<option>Ready to change hair color</option>
					<option>not ready to change hair color</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Hair lenght:
			</div>
			<div class="details-data general-input">
				<select>
					<option>Bald</option>
					<option>Short</option>
					<option>Medium</option>
					<option>Shoulder length</option>
					<option>Very long</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Tattoos:
			</div>
			<div class="details-data general-input">
				<div class="tat-m">
					<label class="q-check"><input type="checkbox" name="Users[TattoosNotattoos]"><span><div></div></span>No tattoos</label>
				</div>
				<div class="tat-m">
					<label class="q-check"><input type="checkbox" name="Users[TattoosHastattoos]" id = 'cb8' onchange = 'showOrHide("cb8", "cat8");'/><span><div></div></span>Has tattoos</label>
				</div>
				<div class="tat-h" id="cat8">
						<label class="q-check"><input type="checkbox" name="Users[TattoosNeck]"><span><div></div></span>Neck</label>
						<label class="q-check"><input type="checkbox" name="Users[TattoosShoulder]"><span><div></div></span>Shoulder</label>
						<label class="q-check"><input type="checkbox" name="Users[TattoosBiceps]"><span><div></div></span>Biceps</label>
						<label class="q-check"><input type="checkbox" name="Users[TattoosWrist]"><span><div></div></span>Wrist</label>
						<label class="q-check"><input type="checkbox" name="Users[TattoosChest]"><span><div></div></span>Chest</label>
						<label class="q-check"><input type="checkbox" name="Users[TattoosBelly]"><span><div></div></span>Belly</label>
						<label class="q-check"><input type="checkbox" name="Users[TattoosBack]"><span><div></div></span>Back</label>
						<label class="q-check"><input type="checkbox" name="Users[TattoosLoin]"><span><div></div></span>Loin</label>
						<label class="q-check"><input type="checkbox" name="Users[TattoosHip]"><span><div></div></span>Hip</label>
						<label class="q-check"><input type="checkbox" name="Users[TattoosShin]"><span><div></div></span>Shin</label>
						<label class="q-check"><input type="checkbox" name="Users[TattoosAnkle]"><span><div></div></span>Ankle</label>
						<label class="q-check"><input type="checkbox" name="Users[TattoosFoot]"><span><div></div></span>Foot</label>
						<label class="q-check"><input type="checkbox" name="Users[TattoosTat-Others]"><span><div></div></span>Others</label>
				</div>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Piercing:
			</div>
			<div class="details-data general-input">
				<div class="pier-m">
					<label class="q-check"><input type="checkbox" name="Users[PiercingNopiercing]"><span><div></div></span>No piercing (earlobes don't count)</label>
				</div>
				<div class="pier-m">
					<label class="q-check"><input type="checkbox" name="Users[PiercingHasotherpiercing]" id = 'cb9' onchange = 'showOrHide("cb9", "cat9");'/><span><div></div></span>Has other piercing:</label>
				</div>
				<div class="pier-h" id="cat9">
						<label class="q-check"><input type="checkbox" name="Users[PiercingEartunnel]"><span><div></div></span>Ear tunnel</label>
						<label class="q-check"><input type="checkbox" name="Users[PiercingEyebrows]"><span><div></div></span>Eyebrows</label>
						<label class="q-check"><input type="checkbox" name="Users[PiercingTongue]"><span><div></div></span>Tongue</label>
						<label class="q-check"><input type="checkbox" name="Users[PiercingNose]"><span><div></div></span>Nose</label>
						<label class="q-check"><input type="checkbox" name="Users[PiercingNipple]"><span><div></div></span>Nipple</label>
						<label class="q-check"><input type="checkbox" name="Users[PiercingNavel]"><span><div></div></span>Navel</label>
						<label class="q-check"><input type="checkbox" name="Users[PiercingBodyimplants]"><span><div></div></span>Body implants</label>
						<label class="q-check"><input type="checkbox" name="Users[PiercingPier-Other]"><span><div></div></span>Other</label>
				</div>
			</div>
		</div>
<!-- Professional -->
<!-- Модель -->
	<div class="pageblock mod">
		<h2>Professional Details</h2>
		<span class="hr"></span>
		<div class="d-wrap">
			<div class="details-item">
				*Modeling categories/Genre:
			</div>
			<div class="details-data general-input">
				<div class="mc-g-m">
					<label class="q-check"><input type="checkbox" name="Users[GeneAlternativemodel]"><span><div></div></span>Alternative model</label>
				</div>
				<div class="mc-g-m">
					<label class="q-check"><input type="checkbox" name="Users[GeneBodyPaint]"><span><div></div></span>Body Paint</label>
				</div>
				<div class="mc-g-m">
					<label class="q-check"><input type="checkbox" name="Users[GeneBodyParts]" id = 'cb10' onchange = 'showOrHide("cb10", "cat10");'/><span><div></div></span>Body Parts:</label>
				</div>
				<div class="mc-g-h" id="cat10">
						<label class="q-check"><input type="checkbox" name="Users[GeneEars]"><span><div></div></span>Ears</label>
						<label class="q-check"><input type="checkbox" name="Users[GeneEyes]"><span><div></div></span>Eyes</label>
						<label class="q-check"><input type="checkbox" name="Users[GeneFingers]"><span><div></div></span>Fingers</label>
						<label class="q-check"><input type="checkbox" name="Users[GeneFoot]"><span><div></div></span>Foot</label>
						<label class="q-check"><input type="checkbox" name="Users[GeneHair]"><span><div></div></span>Hair</label>
						<label class="q-check"><input type="checkbox" name="Users[GeneHands]"><span><div></div></span>Hands</label>
						<label class="q-check"><input type="checkbox" name="Users[GeneLips]"><span><div></div></span>Lips</label>
						<label class="q-check"><input type="checkbox" name="Users[GeneNeck]"><span><div></div></span>Neck</label>
				</div>
				<div class="mc-g-m">
					<label class="q-check"><input type="checkbox" name="Users[GenePrintphotomodel]" id = 'cb11' onchange = 'showOrHide("cb11", "cat11");'/><span><div></div></span>Print photo model:</label>
				</div>
				<div class="mc-g-h" id="cat11">
						<label class="q-check"><input type="checkbox" name="Users[GeneEditorial]"><span><div></div></span>Editorial</label>
						<label class="q-check"><input type="checkbox" name="Users[GenePrintads]"><span><div></div></span>Print ads</label>
				</div>
				<div class="mc-g-m">
					<label class="q-check"><input type="checkbox" name="Users[GeneHightfashionmodel]"><span><div></div></span>Hight fashion model</label>
				</div>
				<div class="mc-g-m">
					<label class="q-check"><input type="checkbox" name="Users[GeneRunwaymodel]"><span><div></div></span>Runway model</label>
				</div>
				<div class="mc-g-m">
					<label class="q-check"><input type="checkbox" name="Users[GeneGlamourmodel]" id = 'cb12' onchange = 'showOrHide("cb12", "cat12");'/><span><div></div></span>Glamour model:</label>
				</div>
				<div class="mc-g-h" id="cat12">
						<label class="q-check"><input type="checkbox" name="Users[GeneLingerieBodymodel]"><span><div></div></span>Lingerie/Body model</label>
						<label class="q-check"><input type="checkbox" name="Users[GeneSwimwearmodel]"><span><div></div></span>Swimwear model</label>
				</div>
				<div class="mc-g-m">
					<label class="q-check"><input type="checkbox" name="Users[GeneFitnessmodel]"><span><div></div></span>Fitness model</label>
				</div>
				<div class="mc-g-m">
					<label class="q-check"><input type="checkbox" name="Users[GeneBeautymodel]"><span><div></div></span>Beauty model (hair/makeup)</label>
				</div>
				<div class="mc-g-m">
					<label class="q-check"><input type="checkbox" name="Users[GeneNude]"><span><div></div></span>Nude</label>
				</div>
				<div class="mc-g-m">
					<label class="q-check"><input type="checkbox" name="Users[GenePromotionalmodel]"><span><div></div></span>Promotional model</label>
				</div>
				<div class="mc-g-m">
					<label class="q-check"><input type="checkbox" name="Users[GenePlus-sizedmodel]"><span><div></div></span>Plus-sized model</label>
				</div>
				<div class="mc-g-m">
					<label class="q-check"><input type="checkbox" name="Users[GeneStreetFashion]"><span><div></div></span>Street Fashion</label>
				</div>
				<div class="mc-g-m">
					<label class="q-check"><input type="checkbox" name="Users[GeneSportmodel]"><span><div></div></span>Sport model</label>
				</div>
				<div class="mc-g-m">
					<label class="q-check"><input type="checkbox" name="Users[GeneSeniormodel]"><span><div></div></span>Senior model (40+)</label>
				</div>
				<div class="mc-g-m">
					<label class="q-check"><input type="checkbox" name="Users[GeneVideo]"><span><div></div></span>Video</label>
				</div>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Professional Experience:
			</div>
			<div class="details-data general-input">
				<select>
					<option>Up to 1 year</option>
					<option>1-5 years</option>
					<option>More that 5 years</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				Additional Skills:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Users[SkillsActor]"><span><div></div></span>Actor or Actress</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[SkillsFilm]"><span><div></div></span>Film Acting</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[SkillsTV]"><span><div></div></span>TV Acting</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[SkillsDance]"><span><div></div></span>Dance</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[SkillsMusic]"><span><div></div></span>Music</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[SkillsExtreme]"><span><div></div></span>Extreme Sports</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[SkillsStage]"><span><div></div></span>Stage Agting</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[SkillsCommercial]"><span><div></div></span>Commercial Acting</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[SkillsSinger]"><span><div></div></span>Singer</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[SkillsPerformance]"><span><div></div></span>Performance Artist</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[SkillsVoice]"><span><div></div></span>Voice Over Talent</label>
				</div>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Languages:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Users[LanguagesChinese]"><span><div></div></span>Chinese</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[LanguagesEnglish]"><span><div></div></span>English</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[LanguagesFrench]"><span><div></div></span>French</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[LanguagesGerman]"><span><div></div></span>German</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[LanguagesItalian]"><span><div></div></span>Italian</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[LanguagesRussian]"><span><div></div></span>Russian</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[LanguagesSpanish]"><span><div></div></span>Spanish</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[LanguagesUkrainian]"><span><div></div></span>Ukrainian</label>
				</div>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				Compensation:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Users[CompensationPaymentOnly]"><span><div></div></span>Payment Only</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[CompensationPhotoTimeForPrint]"><span><div></div></span>Photo/Time For Print</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[CompensationInexchangeof]"><span><div></div></span>In exchange of experience</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[CompensationTestShoot]"><span><div></div></span>Test Shoot</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[CompensationNegotiable]"><span><div></div></span>Negotiable</label>
				</div>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				Rates for photoshoot:
			</div>
			<div class="details-data general-input">
				<input type="text" class="r-f-ph" placeholder="per hour" name="Users[Ratesperhour]"></input>
				<input type="text" class="r-f-ph" placeholder="half day/4 hours" name="Users[Rateshalfday]"></input>
				<input type="text" class="r-f-ph" placeholder="full day/8 hours" name="Users[Ratesfullday]"></input>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				Rates for Runway:
			</div>
			<div class="details-data general-input">
				<input type="text" class="r-f-r" placeholder="rates for runway" name="Users[ratesforrunway]"></input>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Contacted or not:
			</div>
			<div class="details-data general-input">
				<select>
					<option>Contacted with the agency</option>
					<option>Not contacted</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Travel Ability:
			</div>
			<div class="details-data general-input">
				<select>
					<option>Ready to travel</option>
					<option>Work within location</option>
				</select>
			</div>
		</div>
	</div>
<!-- Модель Конец -->
<!-- Фотограф -->
	<div class="pageblock fot">
		<h2>Фотограф</h2>
		<h2>Professional Details</h2>
		<span class="hr"></span>
		<div class="d-wrap">
			<div class="details-item">
				*Professional Experience:
			</div>
			<div class="details-data general-input">
				<select>
					<option>Up to 1 year</option>
					<option>1-5 years</option>
					<option>More that 5 years</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Work areas:
			</div>
			<div class="details-data general-input">
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Users[WorkAreasAdvertising]"><span><div></div></span>Advertising photographers</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Users[WorkAreasBeautyHair]"><span><div></div></span>Beauty & Hair photographers</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Users[WorkAreasBlackWite]"><span><div></div></span>Black & Wite photographers</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Users[WorkAreasChildren]"><span><div></div></span>Children photographers</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Users[WorkAreasCelebrity]"><span><div></div></span>Celebrity/Entertainment</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Users[WorkAreasFashionEditorials]"><span><div></div></span>Fashion/Editorials photographers</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Users[WorkAreasFitnessbody]"><span><div></div></span>Fitness/body photographers</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Users[WorkAreasNude]"><span><div></div></span>Nude</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Users[WorkAreasPeoplelifestyle]"><span><div></div></span>People & lifestyle photographers</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Users[WorkAreasPortrait]"><span><div></div></span>Portrait photographers</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Users[WorkAreasSportsAction]"><span><div></div></span>Sports/Action photographers</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Users[WorkAreasReportage]"><span><div></div></span>Reportage photographers</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Users[WorkAreasRunwayFashion]"><span><div></div></span>Runway/Fashion Event photographers</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Users[WorkAreasSwimwearlingerie]"><span><div></div></span>Swimwear & lingerie photographers</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Users[WorkAreasTravel]"><span><div></div></span>Travel photographers</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Users[WorkAreasUnderwater]"><span><div></div></span>Underwater photographers</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Users[WorkAreasPhotopost]"><span><div></div></span>Photo post production (retouching)</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Users[WorkAreasFilmVideo]"><span><div></div></span>Film/Video production</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Users[WorkAreasCommercialproduction]" id='cb13' onchange='showOrHide("cb13", "cat13");'/><span><div></div></span>Commercial production:</label>
				</div>
				<div  class="w-a-h" id="cat13">
						<label class="q-check"><input type="checkbox" name="Users[WorkAreasTVVideoProduction]"><span><div></div></span>TV/Video Production</label>
						<label class="q-check"><input type="checkbox" name="Users[WorkAreasFilmVideoPost]"><span><div></div></span>Film/Video Post Production & CGI</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Users[WorkAreasCGI]" id='cb14' onchange='showOrHide("cb14", "cat14");'/><span><div></div></span>CGI & 3D (CGI & 3D):</label>
				</div>
				<div  class="w-a-h" id="cat14">
						<label class="q-check"><input type="checkbox" name="Users[WorkAreasMotiongraphics]"><span><div></div></span>Motion graphics</label>
						<label class="q-check"><input type="checkbox" name="Users[WorkAreasSounddesignscoring]"><span><div></div></span>Sound design/scoring</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Users[WorkAreasRentalstudios]"><span><div></div></span>Rental studios</label>
				</div>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Work type:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Users[WorkTypeEditorials]"><span><div></div></span>Editorials</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[WorkTypeRunway]"><span><div></div></span>Runway</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[WorkTypeBeauty]"><span><div></div></span>Beauty</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[WorkTypePortraitsCelebrity]"><span><div></div></span>Portraits/Celebrity</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[WorkTypeGlamourNude]"><span><div></div></span>Glamour/Nude</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[WorkTypeCommercialAdvertising]"><span><div></div></span>Commercial/Advertising</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[WorkTypeStreetFashion]"><span><div></div></span>Street Fashion</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[WorkTypeCreativeProjects]"><span><div></div></span>Creative Projects</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[WorkTypeTestShoot]"><span><div></div></span>Test Shoot</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[WorkTypeWT-Other]"><span><div></div></span>Other</label>
				</div>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				Type of camera equipment:
			</div>
			<div class="details-data general-input">
				<input type="text" name="Users[cameracam-enquip]" placeholder="Type of camera equipment">
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Studio:
			</div>
			<div class="details-data general-input">
				<select name="Users[Studiocam-enquip]">
					<option>Provided</option>
					<option>Not provided</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Macke-Up Artist:
			</div>
			<div class="details-data general-input">
				<select name="Users[StudioMacke-Up]">
					<option>Provided</option>
					<option>Not provided</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Hair Stylist:
			</div>
			<div class="details-data general-input">
				<select name="Users[StudiStylist]">
					<option>Provided</option>
					<option>Not provided</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				Compensation:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Users[CompensationPaymentOnly]"><span><div></div></span>Payment Only</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[CompensationPhotoTimeForPrint]"><span><div></div></span>Photo/Time For Print</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[CompensationInexchangeofexperience]"><span><div></div></span>In exchange of experience</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[CompensationTestShoot]"><span><div></div></span>Test Shoot</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[CompensationNegotiable]"><span><div></div></span>Negotiable</label>
				</div>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				Rates for photoshoot:
			</div>
			<div class="details-data general-input">
				<input type="text" class="r-f-ph" placeholder="per hour" name="Users[photoshootperhour]"></input>
				<input type="text" class="r-f-ph" placeholder="half day/4 hours" name="Users[photoshoothalfdayhours]"></input>
				<input type="text" class="r-f-ph" placeholder="full day/8 hours" name="Users[photoshootfulldayhours]"></input>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Travel Ability:
			</div>
			<div class="details-data general-input">
				<select name="Users[TravelAbility]">
					<option>Ready to travel</option>
					<option>Work within location</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				Main reason to be on site:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Users[reasonTosellservices]"><span><div></div></span>To sell services</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[reasonToserchfor]"><span><div></div></span>To serch for clients</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[reasonSubcontacts]"><span><div></div></span>Subcontacts</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[reasonTosignupfor]"><span><div></div></span>To sign up for master classes</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[reasonTocarryout]"><span><div></div></span>To carry out master classes</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[reasonToparticipateincompetition]"><span><div></div></span>To participate in competitions</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[reasonToserchformodels]"><span><div></div></span>To serch for models</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[reasonExperienceexchange]"><span><div></div></span>Experience exchange</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[reasonToestablishnewcontacts]"><span><div></div></span>To establish new contacts</label>
				</div>
			</div>
		</div>
	</div>
<!-- Фотограф Конец -->
<!-- Дизайнер -->
	<div class="pageblock diz">
		<h2>Дизайнер</h2>
		<h2>Professional Details</h2>
		<span class="hr"></span>
		<div class="d-wrap">
			<div class="details-item">
				*Work type:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Users[DisDetFashiondesign]"><span><div></div></span>Fashion design</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[DisDetProductdesign]"><span><div></div></span>Product design</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[DisDetCostumedesign]"><span><div></div></span>Costume design</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[DisDetInteriorstylin]"><span><div></div></span>Interior stylin</label>
				</div>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Contacted or not:
			</div>
			<div class="details-data general-input">
				<select name="Users[Contactedornot]">
					<option>Contacted</option>
					<option>Not contacted</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Professional experience:
			</div>
			<div class="details-data general-input">
				<select name="Users[Professionalexperience]">
					<option>Up to 10 years</option>
					<option>10-20 years</option>
					<option>More that 20 years</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				Workplace:
			</div>
			<div class="details-data general-input">
				<input type="text" class="f-n" placeholder="Workplace" name="Users[ProfessionalWorkplace]"></input>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				Education/Professional courses/Master classes:
			</div>
			<div class="details-data general-input">
				<input type="text" class="f-n" placeholder="Education/Professional courses/Master classes" name="Users[EducationProfessional]"></input>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				Compensation:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Users[DisCompensationPaymentOnly]"><span><div></div></span>Payment Only</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[DisCompensationInexchangeofexperience]"><span><div></div></span>In exchange of experience</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[DisCompensationTestShoot]"><span><div></div></span>Test Shoot</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[DisCompensationNegotiable]"><span><div></div></span>Negotiable</label>
				</div>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Languages:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Users[DisLanguagesChinese]"><span><div></div></span>Chinese</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[DisLanguagesEnglish]"><span><div></div></span>English</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[DisLanguagesFrench]"><span><div></div></span>French</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[DisLanguagesGerman]"><span><div></div></span>German</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[DisLanguagesItalian]"><span><div></div></span>Italian</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[DisLanguagesRussian]"><span><div></div></span>Russian</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[DisLanguagesSpanish]"><span><div></div></span>Spanish</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[DisLanguagesUkrainian]"><span><div></div></span>Ukrainian</label>
				</div>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Travel Ability:
			</div>
			<div class="details-data general-input">
				<select name="Users[TravelAbility]">
					<option>Ready to travel</option>
					<option>Work within location</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				Main reason to be on site:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MainreasontoTosearchfor]"><span><div></div></span>To search for work offers</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MainreasontoTosignnew]"><span><div></div></span>To sign new contracts</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MainreasontoExperience]"><span><div></div></span>Experience exchange</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MainreasontoTopromoteinterests]"><span><div></div></span>To promote interests/trademark/services</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MainreasontoCooperationwithotherfashion]"><span><div></div></span>Cooperation with other fashion professionals</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MainreasontoTosignupformaster]"><span><div></div></span>To sign up for master classes</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MainreasontoTocarryout]"><span><div></div></span>To carry out master classes</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MainreasontoFashionWeeks]"><span><div></div></span>Fashion Weeks, ShowRooms</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MainreasontoAnaccesstoexclusive]"><span><div></div></span>An access to exclusive worldwide closed-type VIP events</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MainreasontoTosearchforworkoffer]"><span><div></div></span>To search for work offers</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MainreasontoToearchforphotographers]"><span><div></div></span>To search for photographers</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MainreasontoTosearchformodels]"><span><div></div></span>To search for models</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MainreasontoToparticipateincompetitionandcastings]"><span><div></div></span>To participate in competition and castings</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MainreasontoTomonitorthefashionnews]"><span><div></div></span>To monitor the fashion news</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MainreasontoTosellservices]"><span><div></div></span>To sell services</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MainreasontoSubcontacts]"><span><div></div></span>Subcontacts</label>
				</div>
			</div>
		</div>
	</div>
<!-- Дизайнер Конец  -->
<!-- Стилист -->
	<div class="pageblock sti">
		<h2>Стилист</h2>
		<div class="d-wrap stylist">
			<div class="details-item">
				*Work type:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Users[StyHairstylist]"><span><div></div></span>Hair stylist</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[StyMake-upstylist]"><span><div></div></span>Make-up stylist</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[StyNailstylist]"><span><div></div></span>Nail stylist</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[StyWardrobestylist]"><span><div></div></span>Wardrobe stylist/personal shopper</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[StyClothesstylist]"><span><div></div></span>Clothes stylist</label>
				</div>
			</div>
		</div>
		<div class="d-wrap stylist">
			<div class="details-item">
				*Genre work with:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Users[GenreworkwithEditorials]"><span><div></div></span>Editorials</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[GenreworkwithRunway]"><span><div></div></span>Runway</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[GenreworkwithBeauty]"><span><div></div></span>Beauty</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[GenreworkwithPortraitsCelebrity]"><span><div></div></span>Portraits/Celebrity</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[GenreworkwithGlamourNude]"><span><div></div></span>Glamour/Nude</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[GenreworkwithCommercialAdvertising]"><span><div></div></span>Commercial/Advertising</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[GenreworkwithStreetFashion]"><span><div></div></span>Street Fashion</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[GenreworkwithCreativeProjects]"><span><div></div></span>Creative Projects</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[GenreworkwithTesthoot]"><span><div></div></span>Test Shoot</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[GenreworkwithWT-Other]"><span><div></div></span>Other</label>
				</div>
			</div>
		</div>
		<div class="d-wrap stylist">
			<div class="details-item">
				*Contacted or not:
			</div>
			<div class="details-data general-input">
				<select name="Users[StyContacted]">
					<option>Contacted</option>
					<option>Not contacted</option>
				</select>
			</div>
		</div>
		<div class="d-wrap stylist">
			<div class="details-item">
				Workplace:
			</div>
			<div class="details-data general-input">
				<input type="text" class="f-n" placeholder="Workplace" name="Users[StyWorkplace]"></input>
			</div>
		</div>
		<div class="d-wrap stylist">
			<div class="details-item">
				Education/Professional courses/Master classes:
			</div>
			<div class="details-data general-input">
				<input type="text" class="f-n" placeholder="Education/Professional courses/Master classes" name="Users[StyEducationProfessional]"></input>
			</div>
		</div>
		<div class="d-wrap stylist">
			<div class="details-item">
				*Professional experience:
			</div>
			<div class="details-data general-input">
				<select name="Users[StyProfessionalexperience]">
					<option>Up to 10 years</option>
					<option>10-20 years</option>
					<option>More that 20 years</option>
				</select>
			</div>
		</div>
		<div class="d-wrap stylist">
			<div class="details-item">
				Compensation:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Users[StyPaymentOnly]"><span><div></div></span>Payment Only</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[StyInexchangeofexperience]"><span><div></div></span>In exchange of experience</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[StyTestShoot]"><span><div></div></span>Test Shoot</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[StyNegotiable]"><span><div></div></span>Negotiable</label>
				</div>
			</div>
		</div>
	</div>
<!-- Стилист Конец  -->
<!-- Артист -->
	<div class="pageblock art">
		<h2>Артист</h2>
		<div class="d-wrap artist">
			<div class="details-item">
				*Genre work with:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Users[ArtEditorials]"><span><div></div></span>Editorials</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[ArtRunway]"><span><div></div></span>Runway</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[ArtBeauty]"><span><div></div></span>Beauty</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[ArtPortraitsCelebrity]"><span><div></div></span>Portraits/Celebrity</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[ArtGlamourNude]"><span><div></div></span>Glamour/Nude</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[ArtCommercialAdvertising]"><span><div></div></span>Commercial/Advertising</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[ArtStreetFashion]"><span><div></div></span>Street Fashion</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[ArtCreativeProjects]"><span><div></div></span>Creative Projects</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[ArtTestShoot]"><span><div></div></span>Test Shoot</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[ArtWT-Other]"><span><div></div></span>Other</label>
				</div>
			</div>
		</div>
		<div class="d-wrap artist">
			<div class="details-item">
				*Contacted or not:
			</div>
			<div class="details-data general-input">
				<select name="Users[ArtContacted]">
					<option>Contacted</option>
					<option>Not contacted</option>
				</select>
			</div>
		</div>
		<div class="d-wrap artist">
			<div class="details-item">
				Workplace:
			</div>
			<div class="details-data general-input">
				<input type="text" class="f-n" placeholder="Workplace" name="Users[ArtWorkplace]"></input>
			</div>
		</div>
		<div class="d-wrap artist">
			<div class="details-item">
				Education/Professional courses/Master classes:
			</div>
			<div class="details-data general-input">
				<input type="text" class="f-n" placeholder="Education/Professional courses/Master classes" name="Users[ArtcoursesMaster]"></input>
			</div>
		</div>
		<div class="d-wrap artist">
			<div class="details-item">
				Cosmetic brands using in work:
			</div>
			<div class="details-data general-input">
				<input type="text" class="f-n" placeholder="Cosmetic brands using in work" name="Users[ArtCosmeticbrands]"></input>
			</div>
		</div>
		<div class="d-wrap artist">
			<div class="details-item">
				*Professional experience:
			</div>
			<div class="details-data general-input">
				<select name="Users[ArtProfessionalexperience]">
					<option>Up to 10 years</option>
					<option>10-20 years</option>
					<option>More that 20 years</option>
				</select>
			</div>
		</div>
		<div class="d-wrap artist">
			<div class="details-item">
				Compensation:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Users[ArtPaymentOnly]"><span><div></div></span>Payment Only</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[ArtInexchangeofexperience]"><span><div></div></span>In exchange of experience</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[ArtTestShoot]"><span><div></div></span>Test Shoot</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[ArtNegotiable]"><span><div></div></span>Negotiable</label>
				</div>
			</div>
		</div>
	</div>
<!-- Артист Конец -->
<!-- MUA -->
	<div class="pageblock mua">
		<h2>МУА</h2>
		<div class="d-wrap mua">
			<div class="details-item">
				*Genre work with:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MuaEditorials]"><span><div></div></span>Editorials</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MuaRunway]"><span><div></div></span>Runway</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MuaBeauty]"><span><div></div></span>Beauty</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MuaPortraitsCelebrity]"><span><div></div></span>Portraits/Celebrity</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MuaGlamourNude]"><span><div></div></span>Glamour/Nude</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MuaCommercialAdvertising]"><span><div></div></span>Commercial/Advertising</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MuaStreetFashion]"><span><div></div></span>Street Fashion</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MuaCreativeProjects]"><span><div></div></span>Creative Projects</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MuaTestShoot]"><span><div></div></span>Test Shoot</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MuaWT-Other]"><span><div></div></span>Other</label>
				</div>
			</div>
		</div>
		<div class="d-wrap mua">
			<div class="details-item">
				*Contacted or not:
			</div>
			<div class="details-data general-input">
				<select name="Users[MuaContacted]">
					<option>Contacted</option>
					<option>Not contacted</option>
				</select>
			</div>
		</div>
		<div class="d-wrap mua">
			<div class="details-item">
				Workplace:
			</div>
			<div class="details-data general-input">
				<input type="text" class="f-n" placeholder="Workplace" name="Users[MuaWorkplace]"></input>
			</div>
		</div>
		<div class="d-wrap mua">
			<div class="details-item">
				Education/Professional courses/Master classes:
			</div>
			<div class="details-data general-input">
				<input type="text" class="f-n" placeholder="Education/Professional courses/Master classes" name="Users[MuaEducation]"></input>
			</div>
		</div>
		<div class="d-wrap mua">
			<div class="details-item">
				Cosmetic brands using in work:
			</div>
			<div class="details-data general-input">
				<input type="text" class="f-n" placeholder="Cosmetic brands using in work" name="Users[MuaCosmetic]"></input>
			</div>
		</div>
		<div class="d-wrap mua">
			<div class="details-item">
				*Professional experience:
			</div>
			<div class="details-data general-input">
				<select name="Users[Muaexperience]">
					<option>Up to 10 years</option>
					<option>10-20 years</option>
					<option>More that 20 years</option>
				</select>
			</div>
		</div>
		<div class="d-wrap mua">
			<div class="details-item">
				Compensation:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MuaPaymentOnly]"><span><div></div></span>Payment Only</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MuaInexchangeofexperience]"><span><div></div></span>In exchange of experience</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MuaTestShoot]"><span><div></div></span>Test Shoot</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MuaNegotiable]"><span><div></div></span>Negotiable</label>
				</div>
			</div>
		</div>
	</div>
<!-- MUA Конец -->
		<div class="d-wrap">
			<div class="details-item">
				*Languages:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MuaChinese]"><span><div></div></span>Chinese</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MuaEnglish]"><span><div></div></span>English</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MuaFrench]"><span><div></div></span>French</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MuaGerman]"><span><div></div></span>German</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MuaItalian]"><span><div></div></span>Italian</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MuaRussian]"><span><div></div></span>Russian</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MuaSpanish]"><span><div></div></span>Spanish</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MuaUkrainian]"><span><div></div></span>Ukrainian</label>
				</div>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Travel Ability:
			</div>
			<div class="details-data general-input">
				<select name="Users[MuaTravelAbility]">
					<option>Ready to travel</option>
					<option>Work within location</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				Main reason to be on site:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MuaTosignnewcontracts]"><span><div></div></span>To sign new contracts</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MuaExperienceexchange]"><span><div></div></span>Experience exchange</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MuaTopromoteintereststrademarkservices]"><span><div></div></span>To promote interests/trademark/services</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MuaCooperationwithotherfashionprofessionals]"><span><div></div></span>Cooperation with other fashion professionals</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MuaTosignupformasterclasses]"><span><div></div></span>To sign up for master classes</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MuaTocarryoutmasterclasses]"><span><div></div></span>To carry out master classes</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MuaFashionWeeksShowRooms]"><span><div></div></span>Fashion Weeks, ShowRooms</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MuaAnaccesstoexclusiveworldwide]"><span><div></div></span>An access to exclusive worldwide closed-type VIP events</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MuaTosearchforworkoffers]"><span><div></div></span>To search for work offers</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MuaToearchforworkoffers]"><span><div></div></span>To search for work offers</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MuaTosearchforphotographers]"><span><div></div></span>To search for photographers</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MuaTosearchfomodels]"><span><div></div></span>To search for models</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MuaToparticipateincompetitionandcastings]"><span><div></div></span>To participate in competition and castings</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MuaTomonitorthefashionnews]"><span><div></div></span>To monitor the fashion news</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MuaTosellservices]"><span><div></div></span>To sell services</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MuaSubcontacts]"><span><div></div></span>Subcontacts</label>
				</div>
			</div>
		</div>
<?=Yii::app()->user->getState('id_user') ?> - 
<?=Yii::app()->user->getState('package') ?> - 
<?=Yii::app()->user->getState('prof') ?>
<!-- Анкета Агентство -->
<div class="person-wrapper agency">
	<h2>Подробно заполните анкету (агентство)</h2>
		<h2>Model Requirements</h2>
		<span class="hr"></span>

		<div class="d-wrap">
			<div class="details-item">
				*Gender:
			</div>
			<div class="details-data general-input">
				<select name="Users[AgeGender]">
					<option>Male</option>
					<option>Female</option>
					<option>Male and female</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Age:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Users[AgeChildren]"><span><div></div></span>Children</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[AgeTeens]"><span><div></div></span>Teens</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[AgeAdults]"><span><div></div></span>Adults</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[AgeFSenior]"><span><div></div></span>Senior (40+)</label>
				</div>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Model category:
			</div>
			<div class="details-data general-input">
				<div class="mc-m">
					<label class="q-check"><input type="checkbox" name="Users[AgeAlternativemodel]"><span><div></div></span>Alternative model</label>
				</div>
				<div class="mc-m">
					<label class="q-check"><input type="checkbox" name="Users[AgeBodyPaint]"><span><div></div></span>Body Paint</label>
				</div>
				<div class="mc-m">
					<label class="q-check"><input type="checkbox" name="Users[AgeBodyParts]" id='cb21' onchange = 'showOrHide("cb21", "cat21");'/><span><div></div></span>Body Parts:</label>
				</div>
				<div class="mc-h" id="cat21">
						<label class="q-check"><input type="checkbox" name="Users[AgeEars]"><span><div></div></span>Ears</label>
						<label class="q-check"><input type="checkbox" name="Users[AgeEyes]"><span><div></div></span>Eyes</label>
						<label class="q-check"><input type="checkbox" name="Users[AgeFingers]"><span><div></div></span>Fingers</label>
						<label class="q-check"><input type="checkbox" name="Users[AgeFoot]"><span><div></div></span>Foot</label>
						<label class="q-check"><input type="checkbox" name="Users[AgeHair]"><span><div></div></span>Hair</label>
						<label class="q-check"><input type="checkbox" name="Users[AgeHands]"><span><div></div></span>Hands</label>
						<label class="q-check"><input type="checkbox" name="Users[AgeLips]"><span><div></div></span>Lips</label>
						<label class="q-check"><input type="checkbox" name="Users[AgeNeck]"><span><div></div></span>Neck</label>
				</div>
				<div class="mc-m">
					<label class="q-check"><input type="checkbox" name="Users[AgePrintphotomodel]" id='cb22' onchange = 'showOrHide("cb22", "cat22");'/><span><div></div></span>Print photo model:</label>
				</div>
				<div class="mc-h" id="cat22">
						<label class="q-check"><input type="checkbox" name="Users[AgeEditorial]"><span><div></div></span>Editorial</label>
						<label class="q-check"><input type="checkbox" name="Users[AgePrintads]"><span><div></div></span>Print ads</label>
				</div>
				<div class="mc-m">
					<label class="q-check"><input type="checkbox" name="Users[AgeHightfashionmodel]"><span><div></div></span>Hight fashion model</label>
				</div>
				<div class="mc-m">
					<label class="q-check"><input type="checkbox" name="Users[AgeRunwaymodel]"><span><div></div></span>Runway model</label>
				</div>
				<div class="mc-m">
					<label class="q-check"><input type="checkbox" name="Users[AgeGlamourmodel]" id='cb23' onchange = 'showOrHide("cb23", "cat23");'/><span><div></div></span>Glamour model:</label>
				</div>
				<div class="mc-h" id="cat23">
						<label class="q-check"><input type="checkbox" name="Users[AgeLingeryBodymodel]"><span><div></div></span>Lingery/Body model</label>
						<label class="q-check"><input type="checkbox" name="Users[AgeSwimearmodel]"><span><div></div></span>Swimear model</label>
				</div>
				<div class="mc-m">
					<label class="q-check"><input type="checkbox" name="Users[AgeFitnessmodel]"><span><div></div></span>Fitness model</label>
				</div>
				<div class="mc-m">
					<label class="q-check"><input type="checkbox" name="Users[AgeBeautymodel]"><span><div></div></span>Beauty model (hair/makeup)</label>
				</div>
				<div class="mc-m">
					<label class="q-check"><input type="checkbox" name="Users[AgeNude]"><span><div></div></span>Nude</label>
				</div>
				<div class="mc-m">
					<label class="q-check"><input type="checkbox" name="Users[AgePromotionalmodel]"><span><div></div></span>Promotional model</label>
				</div>
				<div class="mc-m">
					<label class="q-check"><input type="checkbox" name="Users[AgePlus-sizedmodel]"><span><div></div></span>Plus-sized model</label>
				</div>
				<div class="mc-m">
					<label class="q-check"><input type="checkbox" name="Users[AgeStreetFashion]"><span><div></div></span>Street Fashion</label>
				</div>
				<div class="mc-m">
					<label class="q-check"><input type="checkbox" name="Users[AgeSportmodel]"><span><div></div></span>Sport model</label>
				</div>
				<div class="mc-m">
					<label class="q-check"><input type="checkbox" name="Users[AgeSeniormodel]"><span><div></div></span>Senior model (40+)</label>
				</div>
				<div class="mc-m">
					<label class="q-check"><input type="checkbox" name="Users[AgeVideo]"><span><div></div></span>Video</label>
				</div>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				Other requirements:
			</div>
			<div class="details-data general-input">
				<textarea placeholder="Other requirements" name="Users[Agetextarea]"></textarea>
			</div>
		</div>

		<h2>Professional Details</h2>
		<span class="hr"></span>
		<div class="d-wrap">
			<div class="details-item">
				*Professional experience:
			</div>
			<div class="details-data general-input">
				<select name="Users[Ageexperience]">
					<option>Up to 10 years</option>
					<option>10-20 years</option>
					<option>More that 20 years</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				Total models:
			</div>
			<div class="details-data general-input">
				<input type="number" name="Users[Agetot-models]">
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Type/Specialization:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Users[AgeAgency]"><span><div></div></span>Agency</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[AgePublisher]"><span><div></div></span>Publisher</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[AgeModelSite]"><span><div></div></span>Model Site</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[AgeModelScout]"><span><div></div></span>Model Scout</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[AgeEventManager]"><span><div></div></span>Event Manager/Staffing company</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[AgeCastingDirector]"><span><div></div></span>Casting Director</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[AgeAdvertiser]"><span><div></div></span>Advertiser</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[AgeManager]"><span><div></div></span>Manager</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[AgeStilist]"><span><div></div></span>Stilist+MUA</label>
				</div>
				<div>
					Other:<input type="text" name="Users[AgeSpec-Other]" placeholder="Other">
				</div>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				Mine reason to be on site:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Users[AgeWorldvisibility]"><span><div></div></span>World visibility</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[AgeModelpromotion]"><span><div></div></span>Model promotion</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[AgePRRepresentation]"><span><div></div></span>PR&Representation</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[AgeTocarryoutmasterclasses]"><span><div></div></span>To carry out master classes</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[AgeSubcontracts]"><span><div></div></span>Subcontracts</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[AgeToestablishnewcontacts]"><span><div></div></span>To establish new contacts</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[AgeToserchformodels]"><span><div></div></span>To serch for models</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[AgeTopromoteservices]"><span><div></div></span>To promote services</label>
				</div>
			</div>
		</div>
</div>
<!-- Анкета Агентство Конец -->
<!-- Brand -->
<div class="person-wrapper brand">
	<h2>Подробно заполните анкету (бренд)</h2>
		<h2>Professional Details</h2>
		<span class="hr"></span>
		<div class="d-wrap">
			<div class="details-item">
				*Professional experience:
			</div>
			<div class="details-data general-input">
				<select name="Users[BraProfessionalexperience]">
					<option>Up to 5 years</option>
					<option>5-10 years</option>
					<option>More that 10 years</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Product range:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BraClothes]"><span><div></div></span>Clothes</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BraUnderwear]"><span><div></div></span>Underwear</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BraSwimwear]"><span><div></div></span>Swimwear</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BraShoes]"><span><div></div></span>Shoes</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BraAccessories]"><span><div></div></span>Accessories</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BraHandbags]"><span><div></div></span>Handbags</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BraJewelry]"><span><div></div></span>Jewelry</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BraBijouterie]"><span><div></div></span>Bijouterie</label>
				</div>
				<div>
					Other:<input type="text" name="Users[BraSpec-Other]" placeholder="Other">
				</div>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				Trading:
			</div>
			<div class="details-data general-input">
				<select class="ge" name="Users[BraTrading]">
					<option>Domestic market</option>
					<option>International market</option>
					<option>Domestic and international market</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				Main reason to be on site:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BraTosearchfornewclientspartners]"><span><div></div></span>To search for new clients, partners</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BraTosignnewcontracts]"><span><div></div></span>To sign new contracts</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BraExperienceexchange]"><span><div></div></span>Experience exchange</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BraTopromoteintereststrademarkservices]"><span><div></div></span>To promote interests/trademark/services</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BraCooperationwithother]"><span><div></div></span>Cooperation with other fashion professionals</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BraToignupformasterclasses]"><span><div></div></span>To sign up for master classes</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BraTocarryoutmasterclasses]"><span><div></div></span>To carry out master classes</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BraFashionWeeksShowRooms]"><span><div></div></span>Fashion Weeks, ShowRooms</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BraAnaccesstoexclusive]"><span><div></div></span>An access to exclusive worldwide closed-type VIP events</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BraTosearchforworkoffers]"><span><div></div></span>To search for work offers</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BraTosearchforworkoffers]"><span><div></div></span>To search for work offers</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BraTosearchforphotographers]"><span><div></div></span>To search for photographers</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BraTosearchformodels]"><span><div></div></span>To search for models</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BraToparticipateincompetitionandcastings]"><span><div></div></span>To participate in competition and castings</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BraTomonitorthefashionnews]"><span><div></div></span>To monitor the fashion news</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BraTosellservices]"><span><div></div></span>To sell services</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BraSubcontacts]"><span><div></div></span>Subcontacts</label>
				</div>
			</div>
		</div>
</div>
<!-- Brand Конец -->
<!-- Buyer -->
<div class="person-wrapper buyer">
	<h2>Подробно заполните анкету (buyer)</h2>
		<h2>Professional Details</h2>
		<span class="hr"></span>
		<div class="d-wrap">
			<div class="details-item">
				*Clients work for:
			</div>
			<div class="details-data general-input">
				<input type="text" class="w-s" placeholder="Clients work for *:" name="Users[BuyClients]"></input>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Clients location:
			</div>
			<div class="details-data general-input">
				<div class="loc loc-m">
					<label class="q-check"><input type="checkbox" name="Users[BuyChina]" id = 'cb15' onchange = 'showOrHide("cb15", "cat15");'/><span><div></div></span>China:</label>
					<div class="loc loc-ch" id="cat15">
						<label class="q-check"><input type="checkbox" name="Users[BuyBeijing]"><span><div></div></span>Beijing</label>
						<label class="q-check"><input type="checkbox" name="Users[BuyHongKong]"><span><div></div></span>Hong Kong</label>
						<label class="q-check"><input type="checkbox" name="Users[BuyShanghai]"><span><div></div></span>Shanghai</label>
						<label class="q-check"><input type="checkbox" name="Users[BuyCh-Other]"><span><div></div></span>Other</label>
					</div>
				</div>
				<div class="loc loc-m">
					<label class="q-check"><input type="checkbox" name="Users[BuyEngland]" id = 'cb16' onchange = 'showOrHide("cb16", "cat16");'/><span><div></div></span>England:</label>
					<div class="loc loc-en" id="cat16">
						<label class="q-check"><input type="checkbox" name="Users[BuyLondon]"><span><div></div></span>London</label>
						<label class="q-check"><input type="checkbox" name="Users[BuyLondonarea]"><span><div></div></span>London area</label>
						<label class="q-check"><input type="checkbox" name="Users[BuyEn-Other]"><span><div></div></span>Other</label>
					</div>
				</div>
				<div class="loc loc-m">
						<label class="q-check"><input type="checkbox" name="Users[BuyFrance]" id = 'cb17' onchange = 'showOrHide("cb17", "cat17");'/><span><div></div></span>France:</label>
					<div class="loc loc-fr" id="cat17">
						<label class="q-check"><input type="checkbox" name="Users[BuyParis]"><span><div></div></span>Paris</label>
						<label class="q-check"><input type="checkbox" name="Users[BuyParisarea]"><span><div></div></span>Paris area</label>
						<label class="q-check"><input type="checkbox" name="Users[BuyFr-Other]"><span><div></div></span>Other</label>
					</div>
				</div>
				<div class="loc loc-m">
						<label class="q-check"><input type="checkbox" name="Users[BuyGermany]" id = 'cb18' onchange = 'showOrHide("cb18", "cat18");'/><span><div></div></span>Germany:</label>
					<div class="loc loc-ge" id="cat18">
						<label class="q-check"><input type="checkbox" name="Users[BuyBerlin]"><span><div></div></span>Berlin</label>
						<label class="q-check"><input type="checkbox" name="Users[BuyBerlinarea]"><span><div></div></span>Berlin area</label>
						<label class="q-check"><input type="checkbox" name="Users[BuyGe-Other]"><span><div></div></span>Other</label>
					</div>
				</div>
				<div class="loc loc-m">
						<label class="q-check"><input type="checkbox" name="Users[BuyItaly]" id='cb19' onchange = 'showOrHide("cb19", "cat19");'/><span><div></div></span>Italy:</label>
					<div class="loc loc-it" id="cat19">
						<label class="q-check"><input type="checkbox" name="Users[BuyMilan]"><span><div></div></span>Milan</label>
						<label class="q-check"><input type="checkbox" name="Users[BuyMilanarea]"><span><div></div></span>Milan area</label>
						<label class="q-check"><input type="checkbox" name="Users[BuyRome]"><span><div></div></span>Rome</label>
						<label class="q-check"><input type="checkbox" name="Users[BuyRomearea]"><span><div></div></span>Rome area</label>
						<label class="q-check"><input type="checkbox" name="Users[BuyIt-Other]"><span><div></div></span>Other</label>
					</div>
				</div>
				<div class="loc loc-m">
						<label class="q-check"><input type="checkbox" name="Users[BuyUSA]" id = 'cb20' onchange = 'showOrHide("cb20", "cat20");'/><span><div></div></span>USA:</label>
					<div class="loc loc-usa" id="cat20">
						<label class="q-check"><input type="checkbox" name="Users[BuyLosAngeles]"><span><div></div></span>Los Angeles</label>
						<label class="q-check"><input type="checkbox" name="Users[BuyLosAngelesarea]"><span><div></div></span>Los Angeles area</label>
						<label class="q-check"><input type="checkbox" name="Users[BuyNewYork]"><span><div></div></span>New York</label>
						<label class="q-check"><input type="checkbox" name="Users[BuyNewYorkarea]"><span><div></div></span>New York area</label>
						<label class="q-check"><input type="checkbox" name="Users[BuyUSA-Othe]"><span><div></div></span>Other</label>
					</div>
				</div>
				<div class="loc loc-m">
						<label class="q-check"><input type="checkbox" name="Users[BuyUkraine]"><span><div></div></span>Ukraine</label>
				</div>
				<div class="loc loc-m">
						<label class="q-check"><input type="checkbox" name="Users[BuyRussia]"><span><div></div></span>Russia</label>
				</div>
				<div class="loc loc-m">
						<label class="q-check"><input type="checkbox" name="Users[BuyOther]"><span><div></div></span>Other</label>
				</div>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				Brands work with:
			</div>
			<div class="details-data general-input">
				<input type="text" class="w-s" placeholder="Brands work with:" name="Users[BuyBrands]"></input>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Product range:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BuyrangeClothes]"><span><div></div></span>Clothes</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BuyrangeUnderwear]"><span><div></div></span>Underwear</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BuyrangeSwimwear]"><span><div></div></span>Swimwear</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BuyrangeShoes]"><span><div></div></span>Shoes</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BuyrangeAccessories]"><span><div></div></span>Accessories</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BuyrangeHandbags]"><span><div></div></span>Handbags</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BuyrangeJewelry]"><span><div></div></span>Jewelry</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BuyrangeBijouterie]"><span><div></div></span>Bijouterie</label>
				</div>
				<div>
					Other:<input type="text" name="Users[BuyrangeSpec-Other]" placeholder="Other">
				</div>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				Trading:
			</div>
			<div class="details-data general-input">
				<select class="ge" name="Users[BuyTrading]">
					<option>Domestic market</option>
					<option>International market</option>
					<option>Domestic and international market</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Professional experience:
			</div>
			<div class="details-data general-input">
				<select name="Users[Buyexperience]">
					<option>Up to 5 years</option>
					<option>5-10 years</option>
					<option>More that 10 years</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Languages:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BuyLanChinese]"><span><div></div></span>Chinese</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BuyLanEnglish]"><span><div></div></span>English</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BuyLanFrench]"><span><div></div></span>French</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BuyLanGerman]"><span><div></div></span>German</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BuyLanItalian]"><span><div></div></span>Italian</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BuyLanRussian]"><span><div></div></span>Russian</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BuyLanSpanish]"><span><div></div></span>Spanish</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BuyLanUkrainian]"><span><div></div></span>Ukrainian</label>
				</div>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Travel Ability:
			</div>
			<div class="details-data general-input">
				<select name="Users[BuyTravelAbility]">
					<option>Ready to travel</option>
					<option>Work within location</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				Main reason to be on site:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BuyTosearcfortners]"><span><div></div></span>To search for new clients, partners</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BuyTosignnewcontracts]"><span><div></div></span>To sign new contracts</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BuyExperienceexchange]"><span><div></div></span>Experience exchange</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BuyTopromotentereststrademarkservices]"><span><div></div></span>To promote interests/trademark/services</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BuyCooperatiowithotherfashionprofessionals]"><span><div></div></span>Cooperation with other fashion professionals</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BuyTosignupformasterclasses]"><span><div></div></span>To sign up for master classes</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BuyTocarryoutmasterclasses]"><span><div></div></span>To carry out master classes</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BuyFashionWeeksShowRooms]"><span><div></div></span>Fashion Weeks, ShowRooms</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BuyAnaccestoexclusive]"><span><div></div></span>An access to exclusive worldwide closed-type VIP events</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BuyTosearchforworkoffers]"><span><div></div></span>To search for work offers</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BuyTosearchforphotographer]"><span><div></div></span>To search for photographers</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BuyTosearchformodels]"><span><div></div></span>To search for models</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BuyToparticipatein]"><span><div></div></span>To participate in competition and castings</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BuyTomonitorthefashion]"><span><div></div></span>To monitor the fashion news</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BuyTosellservices]"><span><div></div></span>To sell services</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[BuySubcontacts]"><span><div></div></span>Subcontacts</label>
				</div>
			</div>
		</div>
</div>
<!-- Buyer Конец -->
<!-- Customer -->
<!-- Зполняется в общей General -->
<!-- Customer Конец -->
<!-- Company Representative -->
<div class="person-wrapper repres">
	<h2>Подробно заполните анкету (company representative)</h2>
		<h2>Professional Details</h2>
		<span class="hr"></span>
		<div class="d-wrap">
			<div class="details-item">
				*Position:
			</div>
			<div class="details-data general-input">
				<select name="Users[CoProfessional]">
					<option>Manager</option>
					<option>Director</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				Scope of activity:
			</div>
			<div class="details-data general-input">
				<input type="text" class="w-s" placeholder="Scope of activity:" name="Users[Coactivity]"></input>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Workplace:
			</div>
			<div class="details-data general-input">
				<input type="text" class="w-s" placeholder="Workplace *:" name="Users[CoWorkplace]"></input>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Professional experience:
			</div>
			<div class="details-data general-input">
				<select name="Users[Coexperience]">
					<option>Up to 5 years</option>
					<option>5-10 years</option>
					<option>More that 10 years</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Languages:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Users[CoLanChinese]"><span><div></div></span>Chinese</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[CoLanEnglish]"><span><div></div></span>English</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[CoLanFrench]"><span><div></div></span>French</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[CoLanGerman]"><span><div></div></span>German</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[CoLanItalian]"><span><div></div></span>Italian</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[CoLanRussian]"><span><div></div></span>Russian</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[CoLanSpanish]"><span><div></div></span>Spanish</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[CoLanUkrainian]"><span><div></div></span>Ukrainian</label>
				</div>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Travel Ability:
			</div>
			<div class="details-data general-input">
				<select name="Users[CoTravelAbility]">
					<option>Ready to travel</option>
					<option>Work within location</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				Main reason to be on site:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Users[CoTosearchfornewclientspartners]"><span><div></div></span>To search for new clients, partners</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[CoTosignnewcontracts]"><span><div></div></span>To sign new contracts</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[CoExperienceexchange]"><span><div></div></span>Experience exchange</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[CoTopromoteintereststrademarkservices]"><span><div></div></span>To promote interests/trademark/services</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[CoCooperationwithotherfashionprofessionals]"><span><div></div></span>Cooperation with other fashion professionals</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[CoTosignupformasterclasses]"><span><div></div></span>To sign up for master classes</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[CoTocarryoutmasterclasses]"><span><div></div></span>To carry out master classes</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[CoFashionWeeksShowRooms]"><span><div></div></span>Fashion Weeks, ShowRooms</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[CoAnaccesstoexclusive]"><span><div></div></span>An access to exclusive worldwide closed-type VIP events</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[CoTosearchforworkffers]"><span><div></div></span>To search for work offers</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[CoTosearchforworkoffers]"><span><div></div></span>To search for work offers</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[CoTosearchforphotographer]"><span><div></div></span>To search for photographers</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[CoTosearchformodels]"><span><div></div></span>To search for models</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[CoToparticipateincompetitionandcastings]"><span><div></div></span>To participate in competition and castings</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[CoTomonitortheashionnews]"><span><div></div></span>To monitor the fashion news</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[CoTosellservices]"><span><div></div></span>To sell services</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[CoSubcontacts]"><span><div></div></span>Subcontacts</label>
				</div>
			</div>
		</div>
</div>
<!-- Media -->
<div class="person-wrapper media">
	<h2>Подробно заполните анкету (media)</h2>
		<h2>Professional Details</h2>
		<span class="hr"></span>
		<div class="d-wrap">
			<div class="details-item">
				*Name of magazine/blog/online journal:
			</div>
			<div class="details-data general-input">
				<input type="text" class="w-s" placeholder="Name of magazine/blog/online journal *:" name="Users[MeName]"></input>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Position:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MeChiefEditor]"><span><div></div></span>Chief Editor</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MeEditor]"><span><div></div></span>Editor</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MeBlogger]"><span><div></div></span>Blogger</label>
				</div>
				<div>
					Other:<input type="text" class="w-s" placeholder="Name of magazine/blog/online journal *:" name="Users[MeOther]"></input>
				</div>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Professional experience:
			</div>
			<div class="details-data general-input">
				<select name="Users[MeProfessionalexperience]">
					<option>Up to 5 years</option>
					<option>5-10 years</option>
					<option>More that 10 years</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Languages:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MeLanChinese]"><span><div></div></span>Chinese</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MeLanEnglish]"><span><div></div></span>English</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MeLanFrench]"><span><div></div></span>French</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MeLanGerman]"><span><div></div></span>German</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MeLanItalian]"><span><div></div></span>Italian</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MeLanRussian]"><span><div></div></span>Russian</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MeLanSpanish]"><span><div></div></span>Spanish</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MeLanUkrainian]"><span><div></div></span>Ukrainian</label>
				</div>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				Travel Ability:
			</div>
			<div class="details-data general-input">
				<select name="Users[MeTravelAbility]">
					<option>Ready to travel</option>
					<option>Work within location</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				Main reason to be on site:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MeTosearchfornewclientspartners]"><span><div></div></span>To search for new clients, partners</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MeTosignnewcontracts]"><span><div></div></span>To sign new contracts</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MeExperienceexchange]"><span><div></div></span>Experience exchange</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MeTopromoteintereststrademarkservices]"><span><div></div></span>To promote interests/trademark/services</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MeCooperationwithotherfashionprofessionals]"><span><div></div></span>Cooperation with other fashion professionals</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MeTosignupformasterclasses]"><span><div></div></span>To sign up for master classes</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MeTocarryoutmasterclasses]"><span><div></div></span>To carry out master classes</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MeFashionWeeksShowRooms]"><span><div></div></span>Fashion Weeks, ShowRooms</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MeAnaccesstoexclusive]"><span><div></div></span>An access to exclusive worldwide closed-type VIP events</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MeTosearchforworkffers]"><span><div></div></span>To search for work offers</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MeTosearchforworkoffers]"><span><div></div></span>To search for work offers</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MeTosearchforphotographer]"><span><div></div></span>To search for photographers</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MeTosearchformodels]"><span><div></div></span>To search for models</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MeToparticipateincompetitionandcastings]"><span><div></div></span>To participate in competition and castings</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MeTomonitortheashionnews]"><span><div></div></span>To monitor the fashion news</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MeTosellservices]"><span><div></div></span>To sell services</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Users[MeSubcontacts]"><span><div></div></span>Subcontacts</label>
				</div>
			</div>
		</div>
</div>
<!-- Media Конец -->
</form>


Pay Form
<form class="pay-form" target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">

<!-- Вывод суммы -->
<div class="pay-sum">
	<h2>К оплате</h2>
	<span class="hr"></span>
	<div class="sum-item">
		<!-- Профессия 1 полная цена -->
		<h3>Дизайнер</h3>
		<div class="sum-price">
			25$
		</div>
	</div>
	<div class="plus">
		+
	</div>
	<div class="sum-item">
		<!-- Профессия 2 скидка 20% -->
		<h3>Стилист</h3>
		<div class="sum-price">
			20$
		</div>
	</div>
	<div class="plus">
		+
	</div>
	<div class="sum-item">
		<!-- Профессия 3 скидка 40% -->
		<h3>Представитель компании</h3>
		<div class="sum-price">
			15$
		</div>
	</div>
</div>


<!-- Вывод суммы Конец -->

<input type="hidden" name="cmd" value="_s-xclick">

<input type="hidden" name="hosted_button_id" value="2QPMN4EWCXFNN">

<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_cart_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">

<img alt="" border="0" src="https://www.paypalobjects.com/ru_RU/i/scr/pixel.gif" width="1" height="1">

</form>





<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">

<input type="hidden" name="cmd" value="_s-xclick">

<input type="hidden" name="encrypted" value="-----BEGIN PKCS7-----MIIIGQYJKoZIhvcNAQcEoIIICjCCCAYCAQExggEwMIIBLAIBADCBlDCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20CAQAwDQYJKoZIhvcNAQEBBQAEgYCV2+Uv/9PIsa7mMzLVZeWB/2gDndWBP+oPtkQJwzViiTRWVl7ont3ziudxrwn4K24PqQYtTiM8G5ESi8JnK9Acz5WIrUxzF6Zu4WncUzL8Ux944faGWIpVsiTEe08YdzYXyVux1yuMfKr+oXw8VsRAqtjWQqpHKciGud+lrlwNJzELMAkGBSsOAwIaBQAwggGVBgkqhkiG9w0BBwEwFAYIKoZIhvcNAwcECLIs3P6n6FCsgIIBcEKza+ANWhs4vA+tO73m2OXmpDRbzKnuVkq4eU85AnMdkRg/TRVJ3huDqs6wbcqtxrjZNFmnFhKCFANVvTIWobsBM2glmuT0ZpIOY2dqWgvtw09dEsL8N0A0ik6KAVwkqaq1SLRUTrHXsoyEC8pqgIPXBM9O02wAiRZjg7y8DisFRBEuHYaqaYIUC/Hs23KQW+Hu+PgCEqAf28wdddUna6Y9IS7VQ0u6rsgKckJuEh9qhrMFlE6aRBeflOqTKShtop9HM4tQMTFBC6VtjxxJAlFlYnjjBvlp/yCBU4Nf9Cyqxc09zHPEK/yYNAfEYV7ogjKha8RaMKFB4OpCAOc0uypLRtgfYGdEqssi2iIOWXMpA4cZVpZecZuB27qcLkSbeG5nLamxeLCoq9XpX7uyG+DA+pIrR3BXEwNiqA7rrY+kog0F+KVFe6Dxh3Vzj11bUHHtceNbCVIPdj9VCLKULQTZH7R+XxtqvMzPD24DviPOoIIDhzCCA4MwggLsoAMCAQICAQAwDQYJKoZIhvcNAQEFBQAwgY4xCzAJBgNVBAYTAlVTMQswCQYDVQQIEwJDQTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEUMBIGA1UEChMLUGF5UGFsIEluYy4xEzARBgNVBAsUCmxpdmVfY2VydHMxETAPBgNVBAMUCGxpdmVfYXBpMRwwGgYJKoZIhvcNAQkBFg1yZUBwYXlwYWwuY29tMB4XDTA0MDIxMzEwMTMxNVoXDTM1MDIxMzEwMTMxNVowgY4xCzAJBgNVBAYTAlVTMQswCQYDVQQIEwJDQTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEUMBIGA1UEChMLUGF5UGFsIEluYy4xEzARBgNVBAsUCmxpdmVfY2VydHMxETAPBgNVBAMUCGxpdmVfYXBpMRwwGgYJKoZIhvcNAQkBFg1yZUBwYXlwYWwuY29tMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDBR07d/ETMS1ycjtkpkvjXZe9k+6CieLuLsPumsJ7QC1odNz3sJiCbs2wC0nLE0uLGaEtXynIgRqIddYCHx88pb5HTXv4SZeuv0Rqq4+axW9PLAAATU8w04qqjaSXgbGLP3NmohqM6bV9kZZwZLR/klDaQGo1u9uDb9lr4Yn+rBQIDAQABo4HuMIHrMB0GA1UdDgQWBBSWn3y7xm8XvVk/UtcKG+wQ1mSUazCBuwYDVR0jBIGzMIGwgBSWn3y7xm8XvVk/UtcKG+wQ1mSUa6GBlKSBkTCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb22CAQAwDAYDVR0TBAUwAwEB/zANBgkqhkiG9w0BAQUFAAOBgQCBXzpWmoBa5e9fo6ujionW1hUhPkOBakTr3YCDjbYfvJEiv/2P+IobhOGJr85+XHhN0v4gUkEDI8r2/rNk1m0GA8HKddvTjyGw/XqXa+LSTlDYkqI8OwR8GEYj4efEtcRpRYBxV8KxAW93YDWzFGvruKnnLbDAF6VR5w/cCMn5hzGCAZowggGWAgEBMIGUMIGOMQswCQYDVQQGEwJVUzELMAkGA1UECBMCQ0ExFjAUBgNVBAcTDU1vdW50YWluIFZpZXcxFDASBgNVBAoTC1BheVBhbCBJbmMuMRMwEQYDVQQLFApsaXZlX2NlcnRzMREwDwYDVQQDFAhsaXZlX2FwaTEcMBoGCSqGSIb3DQEJARYNcmVAcGF5cGFsLmNvbQIBADAJBgUrDgMCGgUAoF0wGAYJKoZIhvcNAQkDMQsGCSqGSIb3DQEHATAcBgkqhkiG9w0BCQUxDxcNMTYwNTAyMTcwMTQyWjAjBgkqhkiG9w0BCQQxFgQUzOhWoTCCaMGhhgcPP4+NUxn538gwDQYJKoZIhvcNAQEBBQAEgYAuCjdrSvOTJU0CFr7SVk6YgTaG04gIIO1r4GxVy/pktbM1xhh6K90EmtFhLJ+Xlv9LRoSDl4tD84KKAlifkpy6akUVSDBz/8KXrePJYmY0RVtSzk2L47oxapB1ilJjh66PTcNCmhwd7Xjw1kB6qFu2DR9iAVqyti0TiDjwHr6nSQ==-----END PKCS7-----

">

<input type="image" src="http://fashingreatness.ru/img/PayPal.png" border="0" name="submit" alt="PayPal — более безопасный и легкий способ оплаты через Интернет!">

<img alt="" border="0" src="https://www.paypalobjects.com/ru_RU/i/scr/pixel.gif" width="1" height="1">

</form>

<script>
$(document).ready(function() {
	<? /* select страны */ ?>
	$('#qes_county').change( function() {
		aaa = $(this).val();
		bbb = aaa.substr(0,4);
		if (bbb != "loc-") {
			$("#qes_county option:selected").css('color','red')
			return; 
		};
		
		$('.loc').css('display', 'none');
		$('.loc-m').css('display', '');
		$('.'+aaa).css('display', 'block');
		//$(this).css('color','red')
		//$("#qes_county option[value='" +$.cookie('sel_3')+ "']").attr("selected", "selected");
		//$("#qes_county option[selected]").css('color','red');
		$("#qes_county option[value='"+aaa+"']").css('color','red')
		//alert(aaa +"--"+bbb);
	});
});
</script>