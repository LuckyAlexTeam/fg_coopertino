{foreach from=$menuCategTree.children item=menuCategory}
<li>
    <span class="js_menu_item">{$menuCategory.name}</span>
    <div class="js_menu_cont" style="display: none;">
        <ul class="dd_menu_list">
            {foreach from=$menuCategory.children item=menuSubcategory name=menuSubForeach}
                <li><a href="{$menuSubcategory.link}">{$menuSubcategory.name}</a></li>
                {if $smarty.foreach.menuSubForeach.iteration % 4 == 0}
                    </ul>
                    <ul class="dd_menu_list">
                {/if}
            {/foreach}
        </ul>
    </div>
</li>
{/foreach}
