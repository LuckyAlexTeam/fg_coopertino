<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class CmsController extends CmsControllerCore
{
    /**
     * Initialize cms controller
     * @see FrontController::init()
     */
    public function init()
    {
        parent::init();

    }

    /**
     * Assign template vars related to page content
     * @see FrontController::initContent()
     */
    public function initContent()
    {
        parent::initContent();

        $parent_cat = new CMSCategory(1, $this->context->language->id);

//        d($parent_cat->recurseLiteCategTree(2));

        $this->context->smarty->assign(array(
            'cms_menu' => CMS::getCMSPages($this->context->language->id, 1, true, (int)$this->context->shop->id),
            'cms_menu_new' => $parent_cat->recurseLiteCategTree(2)
        ));

    }
}
