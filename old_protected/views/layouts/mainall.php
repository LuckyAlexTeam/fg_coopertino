<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="language" content="ru" />
<!-- blueprint CSS framework -->
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery-ui.css" />
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.min.css">
<!--[if lt IE 8]>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
<![endif]-->
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/customers-style.css" />
<!-- <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.11.3.js"></script> -->
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.cookie.js"></script>
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<title><?php echo CHtml::encode($this->arr_header['tit']); ?></title>
<meta name="keywords" content="<?php echo CHtml::encode($this->arr_header['key']); ?>" />
<meta name="description" content="<?php echo CHtml::encode($this->arr_header['des']); ?>" />
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/fonts/awesome/css/font-awesome.min.css">
<!--[if IE 7]>
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/fonts/awesome/css/font-awesome-ie7.min.css">
<![endif]-->

</head>
<body>
<div class="container">
	<div class="head">
		<div class="lang">
			<?
			foreach ($this->arr_lang as $key => $val) {
				if ($this->tlanguage == $val['cod']) {$css = "Y";} else {$css = "N";};
				print "<div class='lang_{$css} tlanguage' data='{$css}'>" . $val['cod'] . "</div>";
			};
			?>
			<div class="lang_N tlanguage" data="N">fr</div>
			<div class="lang_N tlanguage" data="N">ch</div>
		</div>
		<div class="title">
			<a class="logo" href="/">
				<span class="slogan"><?=$this->arr_site["slogan"]?></span>
			</a>
			<div class="clear"></div>
		</div>
		<div class="overall-navigation">
			<a href="/">Назад</a>	
		</div>
	</div>
	<div class="content">
		<?php echo $content; ?>
	</div>
</div>
<div class="footer">
<? $this->Widget('WidFooter'); ?>
</div>
<? /* Почему то надо тут указать /js/jquery.cookie.js в header не работает */ ?>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.cookie.js"></script>
<script>
var gl_op = "";
var gl_id  = "";
var gl_id_menu  = "";
$(document).ready(function() {
	$('.tlanguage').click( function() {
		if ($(this).attr('data') != "Y") {
			//$cookie = Yii::app()->request->cookies['cookie_name']->value; // читать
			//Yii::app()->request->cookies['cookie_name'] = new CHttpCookie('cookie_name', $value); // писать
			$.cookie( 'language', $(this).text() );
			window.location.reload();
		};
	});
//	<? /* Скролл меню  $("#content").removeClass( ) $("#content").addClass("blackZone")*/ ?>
//	$('.menu1e').click( function() {
//		if (gl_id_menu != "") {$('#'+gl_id_menu).removeClass(); $('#'+gl_id_menu).addClass("menu1e");};
//		gl_id_menu = $(this).attr('id');
//		$(this).removeClass();
//		$(this).addClass("menu1f");
//		
//		$("html, body").animate({
//			scrollTop: $($(this).attr("href")).offset().top + "px"
//		}, {
//			duration: 800,
//			easing: "swing"
//		});

//		return false;
//	});
//	//
//	$(window).scroll(function(){
//		aa = $(window).scrollTop();
//		a2 = $('#b2').offset().top;
//		a3 = $('#b3').offset().top;
//		a4 = $('#b4').offset().top;
//		a5 = $('#b5').offset().top;
//		a6 = $('#b6').offset().top;
//		if (aa < a2/2) {b2=0;} else {b2=1;}
//		if (aa < (a2+(a2/2)) ) {b3=0;} else {b3=1;}
//		if (aa < (a3+(a2/2)) ) {b4=0;} else {b4=1;}
//		if (aa < (a4+(a2/2)) ) {b5=0;} else {b5=1;}
//		if (aa < (a5+(a2/2)) ) {b6=0;} else {b6=1;}
//		bb = 1+b2+b3+b4+b5+b6;
//		if (gl_id_menu != "") {$('#'+gl_id_menu).removeClass(); $('#'+gl_id_menu).addClass("menu1e");};
//		gl_id_menu = 'menu_b'+bb;
//		$('#'+gl_id_menu).removeClass();
//		$('#'+gl_id_menu).addClass("menu1f");
//	});
});
</script>
<script>
// Cache selectors
var lastId,
	topMenu = $("#top-menu"),
	topMenuHeight = topMenu.outerHeight()+15,
	// All list items
	menuItems = topMenu.find("a"),
	// Anchors corresponding to menu items
	scrollItems = menuItems.map(function(){
		var item = $($(this).attr("href"));
		if (item.length) { return item; }
	});
</script>
<script>
// Bind click handler to menu items
// so we can get a fancy scroll animation
menuItems.click(function(e){
	var href = $(this).attr("href"),
		offsetTop = href === "#" ? 0 : $(href).offset().top;
	$('html, body').stop().animate({ 
		scrollTop: offsetTop
	}, 300);
	e.preventDefault();
});
// Bind to scroll
$(window).scroll(function(){
	// Get container scroll position
	var fromTop = $(this).scrollTop()+topMenuHeight;
	
	// Get id of current scroll item
	var cur = scrollItems.map(function(){
		if ($(this).offset().top < fromTop)
		return this;
	});
	// Get the id of the current element
	cur = cur[cur.length-1];
	var id = cur && cur.length ? cur[0].id : "";
	
	if (lastId !== id) {
		lastId = id;
		// Set/remove active class
		menuItems
			.parent().removeClass("active")
			.end().filter("[href='#"+id+"']").parent().addClass("active");
	}
});
$(window).load(function () {
	$(window).scroll();
});
</script>
</body>
</html>