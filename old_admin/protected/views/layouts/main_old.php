<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="language" content="ru" />
<!-- blueprint CSS framework -->
<link rel="stylesheet" type="text/css" href="../css/screen.css" media="screen, projection" />
<link rel="stylesheet" type="text/css" href="../css/jquery-ui.css" />
<!--[if lt IE 8]>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
<![endif]-->
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
<!-- <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.11.3.js"></script> -->
<script type="text/javascript" src="../js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="../js/jquery-ui.js"></script>
<script type="text/javascript" src="../js/jquery.cookie.js"></script>
<link rel="icon" href="/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />

</head>
<style>
.amenu

</style>
<body>
<div class="amenu">
<a href="index.php?r=site/site&lan=en&view=main"> site-en-main </a> 
<a href="index.php?r=site/site&lan=en&view=visitor"> site-en-visitor </a>
<a href="index.php?r=site/site&lan=en&view=classic"> site-en-classic </a>
<a href="index.php?r=site/site&lan=en&view=vip"> site-en-vip </a>
<a href="index.php?r=site/site&lan=en&view=corporate"> site-en-corporate </a>
<hr>
<a href="index.php?r=site/team&lan=en"> Команда en </a>
<?php echo $content; ?>
</div>
</body>
</html>
