<? /* widget - LatestNews */ ?>

<h2>Latest news</h2>

<span class="hr"></span>
<nav class="ln-nav">
	<ul>
		<li>
			<a href="">фотографии</a>
		</li>
		<li>
			<a href="">подписчики</a>
		</li>
		<li>
			<a href="">вакансии</a>
		</li>
		<li>
			<a href="">написанные рекоендации</a>
		</li>
		<li>
			<a href="">новые модели</a>
		</li>
	</ul>
</nav>
<div class="ln-item">
<!-- Photo News -->
	<div class="ln-icon1">
		<a href="">
			<img src="../img/m.min.jpg" alt="аватарка пользователя">
			<p>Имя/id</p>
		</a>
	</div>

	<div class="ln-desc">
		added new photo to album ""
	</div>

	<div class="ln-icon2">
		<a href="">
			<img src="../img/m.min.jpg" alt="фото со ссылкой на альбом">
		</a>
	</div>

</div>
<div class="ln-item">
<!-- Subscribers News -->
	<div class="ln-icon1">
		<a href="">
			<img src="../img/m.min.jpg" alt="аватарка пользователя">
			<p>Имя/id</p>
		</a>
	</div>

	<div class="ln-desc">
		followed
	</div>

	<div class="ln-icon2">
		<a href="">
			<img src="../img/m.min.jpg" alt="аватарка пользователя">
			<p>Имя/id</p>
		</a>
	</div>

</div>
<div class="ln-item">
<!-- Vacancy News -->
	<div class="ln-icon1">
		<a href="">
			<img src="../img/m.min.jpg" alt="аватарка пользователя">
			<p>Имя/id</p>
		</a>
	</div>

	<div class="ln-desc">
		<a href="">
			added new vacancy ""
		</a>
	</div>

	<div class="ln-icon2">
		<!--a href="">
			<img src="../img/m.min.jpg" alt="фото со ссылкой на альбом">
		</a-->
	</div>

</div>
<div class="ln-item">
<!-- Recommended News -->
	<div class="ln-icon1">
		<a href="">
			<img src="../img/m.min.jpg" alt="аватарка пользователя">
			<p>Имя/id</p>
		</a>
	</div>

	<div class="ln-desc">
		recommended
	</div>

	<div class="ln-icon2">
		<a href="">
			<img src="../img/m.min.jpg" alt="аватарка пользователя">
			<p>Имя/id</p>
		</a>
	</div>

</div>
<div class="ln-item">
<!-- New Models -->
	<div class="ln-icon1">
		<a href="">
			<img src="../img/m.min.jpg" alt="аватарка агентства">
			<p>Имя/id</p>
		</a>
	</div>

	<div class="ln-desc">
		presents a new model
	</div>

	<div class="ln-icon2">
		<a href="">
			<img src="../img/m.min.jpg" alt="аватарка пользователя">
			<p>Имя/id</p>
		</a>
	</div>

</div>