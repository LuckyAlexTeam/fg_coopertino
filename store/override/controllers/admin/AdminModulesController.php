<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class AdminModulesController extends AdminModulesControllerCore
{

    private $_modules_ad = array(
        'blockcart' => array('cartabandonmentpro'),
        //'bloctopmenu' => array('advancedtopmenu'),
        'blocklayered' => array('pm_advancedsearch4')
    );

//    protected $modules_not_update = array('blocklayered');
	protected $modules_not_update = array();

    public function postProcessCallback()
    {
        $return = false;
        $installed_modules = array();

        foreach ($this->map as $key => $method) {
            if (!Tools::getValue($key)) {
                continue;
            }

            /* PrestaShop demo mode */
            if (_PS_MODE_DEMO_) {
                $this->errors[] = Tools::displayError('This functionality has been disabled.');
                return;
            }

            if ($key == 'check') {
                $this->ajaxProcessRefreshModuleList(true);
            } elseif ($key == 'checkAndUpdate') {
                $modules = array();
                $this->ajaxProcessRefreshModuleList(true);
                $modules_on_disk = Module::getModulesOnDisk(true, $this->logged_on_addons, $this->id_employee);

                // Browse modules list
                foreach ($modules_on_disk as $km => $module_on_disk) {
                    if (!Tools::getValue('module_name') && isset($module_on_disk->version_addons) && $module_on_disk->version_addons) {
                        $modules[] = $module_on_disk->name;
                    }
                }

                if (!Tools::getValue('module_name')) {
                    $modules_list_save = implode('|', $modules);
                }
            } elseif (($modules = Tools::getValue($key)) && $key != 'checkAndUpdate' && $key != 'updateAll') {
                if (strpos($modules, '|')) {
                    $modules_list_save = $modules;
                    $modules = explode('|', $modules);
                }

                if (!is_array($modules)) {
                    $modules = (array)$modules;
                }
            } elseif ($key == 'updateAll') {
                $loggedOnAddons = false;

                if (isset($this->context->cookie->username_addons)
                    && isset($this->context->cookie->password_addons)
                    && !empty($this->context->cookie->username_addons)
                    && !empty($this->context->cookie->password_addons)) {
                    $loggedOnAddons = true;
                }

                $allModules = Module::getModulesOnDisk(true, $loggedOnAddons, $this->context->employee->id);
                $upgradeAvailable = 0;
                $modules = array();

                foreach ($allModules as $km => $moduleToUpdate) {
                    if ($moduleToUpdate->installed && isset($moduleToUpdate->version_addons) && $moduleToUpdate->version_addons) {
                        $modules[] = (string)$moduleToUpdate->name;
                    }
                }
            }

            $module_upgraded = array();
            $module_errors = array();
            if (isset($modules)) {
                foreach ($modules as $name) {
                    if (in_array($name, $this->modules_not_update) && $key == 'update')
                    {
                        $this->errors[] = 'Модуль '.$name.' заблокирован для обновления в override/controllers/admin/AdminModulesController.php';
                        continue;
                    }
                    $module_to_update = array();
                    $module_to_update[$name] = null;
                    $full_report = null;
                    // If Addons module, download and unzip it before installing it
                    if (!file_exists(_PS_MODULE_DIR_.$name.'/'.$name.'.php') || $key == 'update' || $key == 'updateAll') {
                        $files_list = array(
                            array('type' => 'addonsNative', 'file' => Module::CACHE_FILE_DEFAULT_COUNTRY_MODULES_LIST, 'loggedOnAddons' => 0),
                            array('type' => 'addonsBought', 'file' => Module::CACHE_FILE_CUSTOMER_MODULES_LIST, 'loggedOnAddons' => 1),
                        );

                        foreach ($files_list as $f) {
                            if (file_exists(_PS_ROOT_DIR_.$f['file'])) {
                                $file = $f['file'];
                                $content = Tools::file_get_contents(_PS_ROOT_DIR_.$file);
                                if ($xml = @simplexml_load_string($content, null, LIBXML_NOCDATA)) {
                                    foreach ($xml->module as $modaddons) {
                                        if (Tools::strtolower($name) == Tools::strtolower($modaddons->name)) {
                                            $module_to_update[$name]['id'] = $modaddons->id;
                                            $module_to_update[$name]['displayName'] = $modaddons->displayName;
                                            $module_to_update[$name]['need_loggedOnAddons'] = $f['loggedOnAddons'];
                                        }
                                    }
                                }
                            }
                        }

                        foreach ($module_to_update as $name => $attr) {
                            if ((is_null($attr) && $this->logged_on_addons == 0) || ($attr['need_loggedOnAddons'] == 1 && $this->logged_on_addons == 0)) {
                                $this->errors[] = sprintf(Tools::displayError('You need to be logged in to your PrestaShop Addons account in order to update the %s module. %s'), '<strong>'.$name.'</strong>', '<a href="#" class="addons_connect" data-toggle="modal" data-target="#modal_addons_connect" title="Addons">'.$this->l('Click here to log in.').'</a>');
                            } elseif (!is_null($attr['id'])) {
                                $download_ok = false;
                                if ($attr['need_loggedOnAddons'] == 0 && file_put_contents(_PS_MODULE_DIR_.$name.'.zip', Tools::addonsRequest('module', array('id_module' => pSQL($attr['id']))))) {
                                    $download_ok = true;
                                } elseif ($attr['need_loggedOnAddons'] == 1 && $this->logged_on_addons && file_put_contents(_PS_MODULE_DIR_.$name.'.zip', Tools::addonsRequest('module', array('id_module' => pSQL($attr['id']), 'username_addons' => pSQL(trim($this->context->cookie->username_addons)), 'password_addons' => pSQL(trim($this->context->cookie->password_addons)))))) {
                                    $download_ok = true;
                                }

                                if (!$download_ok) {
                                    $this->errors[] = sprintf(Tools::displayError('Module %s cannot be upgraded: Error while downloading the latest version.'), '<strong>'.$attr['displayName'].'</strong>');
                                } elseif (!$this->extractArchive(_PS_MODULE_DIR_.$name.'.zip', false)) {
                                    $this->errors[] = sprintf(Tools::displayError('Module %s cannot be upgraded: Error while extracting the latest version.'), '<strong>'.$attr['displayName'].'</strong>');
                                } else {
                                    $module_upgraded[] = $name;
                                }
                            } else {
                                $this->errors[] = sprintf(Tools::displayError('You do not have the rights to update the %s module. Please make sure you are logged in to the PrestaShop Addons account that purchased the module.'), '<strong>'.$name.'</strong>');
                            }
                        }
                    }

                    if (count($this->errors)) {
                        continue;
                    }

                    // Check potential error
                    if (!($module = Module::getInstanceByName(urldecode($name)))) {
                        $this->errors[] = $this->l('Module not found');
                    } elseif (($this->context->mode >= Context::MODE_HOST_CONTRIB) && in_array($module->name, Module::$hosted_modules_blacklist)) {
                        $this->errors[] = Tools::displayError('You do not have permission to access this module.');
                    } elseif ($key == 'install' && $this->tabAccess['add'] !== '1') {
                        $this->errors[] = Tools::displayError('You do not have permission to install this module.');
                    } elseif ($key == 'install' && ($this->context->mode == Context::MODE_HOST) && !Module::isModuleTrusted($module->name)) {
                        $this->errors[] = Tools::displayError('You do not have permission to install this module.');
                    } elseif ($key == 'delete' && ($this->tabAccess['delete'] !== '1' || !$module->getPermission('configure'))) {
                        $this->errors[] = Tools::displayError('You do not have permission to delete this module.');
                    } elseif ($key == 'configure' && ($this->tabAccess['edit'] !== '1' || !$module->getPermission('configure') || !Module::isInstalled(urldecode($name)))) {
                        $this->errors[] = Tools::displayError('You do not have permission to configure this module.');
                    } elseif ($key == 'install' && Module::isInstalled($module->name)) {
                        $this->errors[] = sprintf(Tools::displayError('This module is already installed: %s.'), $module->name);
                    } elseif ($key == 'uninstall' && !Module::isInstalled($module->name)) {
                        $this->errors[] = sprintf(Tools::displayError('This module has already been uninstalled: %s.'), $module->name);
                    } elseif ($key == 'update' && !Module::isInstalled($module->name)) {
                        $this->errors[] = sprintf(Tools::displayError('This module needs to be installed in order to be updated: %s.'), $module->name);
                    } else {
                        // If we install a module, force temporary global context for multishop
                        if (Shop::isFeatureActive() && Shop::getContext() != Shop::CONTEXT_ALL && $method != 'getContent') {
                            $shop_id = (int)Context::getContext()->shop->id;
                            Context::getContext()->tmpOldShop = clone(Context::getContext()->shop);
                            if ($shop_id) {
                                Context::getContext()->shop = new Shop($shop_id);
                            }
                        }

                        //retrocompatibility
                        if (Tools::getValue('controller') != '') {
                            $_POST['tab'] = Tools::safeOutput(Tools::getValue('controller'));
                        }

                        $echo = '';
                        if ($key != 'update' && $key != 'updateAll' && $key != 'checkAndUpdate') {
                            // We check if method of module exists
                            if (!method_exists($module, $method)) {
                                throw new PrestaShopException('Method of module cannot be found');
                            }

                            if ($key == 'uninstall' && !Module::getPermissionStatic($module->id, 'uninstall')) {
                                $this->errors[] = Tools::displayError('You do not have permission to uninstall this module.');
                            }

                            if (count($this->errors)) {
                                continue;
                            }
                            // Get the return value of current method
                            $echo = $module->{$method}();

                            // After a successful install of a single module that has a configuration method, to the configuration page
                            if ($key == 'install' && $echo === true && strpos(Tools::getValue('install'), '|') === false && method_exists($module, 'getContent')) {
                                Tools::redirectAdmin(self::$currentIndex.'&token='.$this->token.'&configure='.$module->name.'&conf=12');
                            }
                        }

                        // If the method called is "configure" (getContent method), we show the html code of configure page
                        if ($key == 'configure' && Module::isInstalled($module->name)) {
                            $this->bootstrap = (isset($module->bootstrap) && $module->bootstrap);
                            if (isset($module->multishop_context)) {
                                $this->multishop_context = $module->multishop_context;
                            }

                            $back_link = self::$currentIndex.'&token='.$this->token.'&tab_module='.$module->tab.'&module_name='.$module->name;
                            $hook_link = 'index.php?tab=AdminModulesPositions&token='.Tools::getAdminTokenLite('AdminModulesPositions').'&show_modules='.(int)$module->id;
                            $trad_link = 'index.php?tab=AdminTranslations&token='.Tools::getAdminTokenLite('AdminTranslations').'&type=modules&lang=';
                            $disable_link = $this->context->link->getAdminLink('AdminModules').'&module_name='.$module->name.'&enable=0&tab_module='.$module->tab;
                            $uninstall_link = $this->context->link->getAdminLink('AdminModules').'&module_name='.$module->name.'&uninstall='.$module->name.'&tab_module='.$module->tab;
                            $reset_link = $this->context->link->getAdminLink('AdminModules').'&module_name='.$module->name.'&reset&tab_module='.$module->tab;
                            $update_link = $this->context->link->getAdminLink('AdminModules').'&checkAndUpdate=1&module_name='.$module->name;

                            $is_reset_ready = false;
                            if (method_exists($module, 'reset')) {
                                $is_reset_ready = true;
                            }

                            $this->context->smarty->assign(
                                array(
                                    'module_name' => $module->name,
                                    'module_display_name' => $module->displayName,
                                    'back_link' => $back_link,
                                    'module_hook_link' => $hook_link,
                                    'module_disable_link' => $disable_link,
                                    'module_uninstall_link' => $uninstall_link,
                                    'module_reset_link' => $reset_link,
                                    'module_update_link' => $update_link,
                                    'trad_link' => $trad_link,
                                    'module_languages' => Language::getLanguages(false),
                                    'theme_language_dir' => _THEME_LANG_DIR_,
                                    'page_header_toolbar_title' => $this->page_header_toolbar_title,
                                    'page_header_toolbar_btn' => $this->page_header_toolbar_btn,
                                    'add_permission' => $this->tabAccess['add'],
                                    'is_reset_ready' => $is_reset_ready,
                                )
                            );

                            // Display checkbox in toolbar if multishop
                            if (Shop::isFeatureActive()) {
                                if (Shop::getContext() == Shop::CONTEXT_SHOP) {
                                    $shop_context = 'shop <strong>'.$this->context->shop->name.'</strong>';
                                } elseif (Shop::getContext() == Shop::CONTEXT_GROUP) {
                                    $shop_group = new ShopGroup((int)Shop::getContextShopGroupID());
                                    $shop_context = 'all shops of group shop <strong>'.$shop_group->name.'</strong>';
                                } else {
                                    $shop_context = 'all shops';
                                }

                                $this->context->smarty->assign(array(
                                    'module' => $module,
                                    'display_multishop_checkbox' => true,
                                    'current_url' => $this->getCurrentUrl('enable'),
                                    'shop_context' => $shop_context,
                                ));
                            }

                            $this->context->smarty->assign(array(
                                'is_multishop' => Shop::isFeatureActive(),
                                'multishop_context' => Shop::CONTEXT_ALL | Shop::CONTEXT_GROUP | Shop::CONTEXT_SHOP
                            ));

                            if (Shop::isFeatureActive() && isset(Context::getContext()->tmpOldShop)) {
                                Context::getContext()->shop = clone(Context::getContext()->tmpOldShop);
                                unset(Context::getContext()->tmpOldShop);
                            }

                            // Display module configuration
                            $header = $this->context->smarty->fetch('controllers/modules/configure.tpl');
                            $configuration_bar = $this->context->smarty->fetch('controllers/modules/configuration_bar.tpl');

                            $output = $header.$echo;

                            if (isset($this->_modules_ad[$module->name])) {
                                $ad_modules = $this->getModulesByInstallation($this->_modules_ad[$module->name]);

                                foreach ($ad_modules['not_installed'] as $key => &$module) {
                                    if (isset($module->addons_buy_url)) {
                                        $module->addons_buy_url = str_replace('utm_source=v1trunk_api', 'utm_source=back-office', $module->addons_buy_url)
                                            .'&utm_medium=related-modules&utm_campaign=back-office-'.strtoupper($this->context->language->iso_code)
                                            .'&utm_content='.(($this->context->mode >= Context::MODE_HOST_CONTRIB) ? 'cloud' : 'download');
                                    }
                                    if (isset($module->description_full) && trim($module->description_full) != '') {
                                        $module->show_quick_view = true;
                                    }
                                }
                                $this->context->smarty->assign(array(
                                    'ad_modules' => $ad_modules,
                                    'currentIndex' => self::$currentIndex
                                ));
                                $ad_bar = $this->context->smarty->fetch('controllers/modules/ad_bar.tpl');
                                $output .= $ad_bar;
                            }

                            $this->context->smarty->assign('module_content', $output.$configuration_bar);
                        } elseif ($echo === true) {
                            $return = 13;
                            if ($method == 'install') {
                                $return = 12;
                                $installed_modules[] = $module->id;
                            }
                        } elseif ($echo === false) {
                            $module_errors[] = array('name' => $name, 'message' => $module->getErrors());
                        }

                        if (Shop::isFeatureActive() && Shop::getContext() != Shop::CONTEXT_ALL && isset(Context::getContext()->tmpOldShop)) {
                            Context::getContext()->shop = clone(Context::getContext()->tmpOldShop);
                            unset(Context::getContext()->tmpOldShop);
                        }
                    }
                    if ($key != 'configure' && Tools::getIsset('bpay')) {
                        Tools::redirectAdmin('index.php?tab=AdminPayment&token='.Tools::getAdminToken('AdminPayment'.(int)Tab::getIdFromClassName('AdminPayment').(int)$this->id_employee));
                    }
                }
            }

            if (count($module_errors)) {
                // If error during module installation, no redirection
                $html_error = $this->generateHtmlMessage($module_errors);
                if ($key == 'uninstall') {
                    $this->errors[] = sprintf(Tools::displayError('The following module(s) could not be uninstalled properly: %s.'), $html_error);
                } else {
                    $this->errors[] = sprintf(Tools::displayError('The following module(s) could not be installed properly: %s.'), $html_error);
                }
                $this->context->smarty->assign('error_module', 'true');
            }
        }

        if ($return) {
            $params = (count($installed_modules)) ? '&installed_modules='.implode('|', $installed_modules) : '';

            // If redirect parameter is present and module installed with success, we redirect on configuration module page
            if (Tools::getValue('redirect') == 'config' && Tools::getValue('module_name') != '' && $return == '12' && Module::isInstalled(pSQL(Tools::getValue('module_name')))) {
                Tools::redirectAdmin('index.php?controller=adminmodules&configure='.Tools::getValue('module_name').'&token='.Tools::getValue('token').'&module_name='.Tools::getValue('module_name').$params);
            }
            Tools::redirectAdmin(self::$currentIndex.'&conf='.$return.'&token='.$this->token.'&tab_module='.$module->tab.'&module_name='.$module->name.'&anchor='.ucfirst($module->name).(isset($modules_list_save) ? '&modules_list='.$modules_list_save : '').$params);
        }

        if (Tools::getValue('update') || Tools::getValue('updateAll') || Tools::getValue('checkAndUpdate')) {
            $updated = '&updated=1';
            if (Tools::getValue('checkAndUpdate')) {
                $updated = '&check=1';
                if (Tools::getValue('module_name')) {
                    $module = Module::getInstanceByName(Tools::getValue('module_name'));
                    if (!Validate::isLoadedObject($module)) {
                        unset($module);
                    }
                }
            }

            foreach ($module_upgraded as $m_name) {
                $file_contents = file_get_contents(_PS_MODULE_DIR_.$m_name.'/'.$m_name.'.php');

                if (!mb_detect_encoding($file_contents, 'UTF-8', true))
                    continue;

                $file_contents = preg_replace('/\bprivate\b/', 'protected', $file_contents);
                file_put_contents(_PS_MODULE_DIR_.$m_name.'/'.$m_name.'.php', $file_contents);
            }


            $module_upgraded = implode('|', $module_upgraded);

            if ($key == 'updateAll') {
                Tools::redirectAdmin(self::$currentIndex.'&token='.$this->token.'&allUpdated=1');
            } elseif (isset($module_upgraded) && $module_upgraded != '') {
                Tools::redirectAdmin(self::$currentIndex.'&token='.$this->token.'&updated=1&module_name='.$module_upgraded);
            } elseif (isset($modules_list_save)) {
                Tools::redirectAdmin(self::$currentIndex.'&token='.$this->token.'&updated=1&module_name='.$modules_list_save);
            } elseif (isset($module)) {
                Tools::redirectAdmin(self::$currentIndex.'&token='.$this->token.$updated.'&tab_module='.$module->tab.'&module_name='.$module->name.'&anchor='.ucfirst($module->name).(isset($modules_list_save) ? '&modules_list='.$modules_list_save : ''));
            }
        }
    }

}
