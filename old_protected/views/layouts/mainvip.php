<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="language" content="ru" />
<!-- blueprint CSS framework -->
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery-ui.css" />
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.min.css">
<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
<![endif]-->
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/customers-style.css" />
<!-- <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.11.3.js"></script> -->
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.cookie.js"></script>
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<title><?php echo CHtml::encode($this->arr_header['tit']); ?> V.I.P.</title>
<meta name="keywords" content="<?php echo CHtml::encode($this->arr_header['key']); ?>" />
<meta name="description" content="<?php echo CHtml::encode($this->arr_header['des']); ?>" />
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/fonts/awesome/css/font-awesome.min.css">
<!--[if IE 7]>
	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/fonts/awesome/css/font-awesome-ie7.min.css">
<![endif]-->
</head>
<body>
<div class="container">
	<div class="c-head">
		<div class="lang">
			<?
			foreach ($this->arr_lang as $key => $val) {
				if ($this->tlanguage == $val['cod']) {$css = "Y";} else {$css = "N";};
				print "<div class='lang_{$css} tlanguage' data='{$css}'>" . $val['cod'] . "</div>";
				};
			?>
			<div class="lang_N tlanguage" data="N">fr</div>
			<div class="lang_N tlanguage" data="N">ch</div>
		</div>
		<div class="title">
			<p class="logo">
				<span class="slogan"><?=$this->arr_site["slogan1"]?></span>
			</p>
		</div>
		<div class="c-navigation">
			<div class="c-nav-text">
				<p>
				<? if (Yii::app()->user->getState('anketa') != 0) { ?>
				<a href="questionary">Анкета</a> &nbsp; 
				<a href="id<?=Yii::app()->user->getState('id_user')?>">Профиль</a><br/>
				<a href="setting">Settings</a>
				<? }; ?>
				&nbsp;<a href="logout">Log Out</a></p>
				
			</div>
			<div class="c-nav-img">
				<img src="./img_avatar_small/<?=$this->tuser['avatar']?>" alt="" id="avatar_img_small" style="width:40px;">
			</div>
		</div>
	</div>
	<div class="content">
		<?php echo $content; ?>
	</div>
</div>
<div class="footer">
<? $this->Widget('WidFooter'); ?>
</div>
</body>
</html>
