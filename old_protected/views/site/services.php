<style type="text/css">
	.head {
		position: relative;
		display: inline-block;
	}
</style>
<div class="static-wrapper">
	<a href="/" class="link-back">Fashion Greatness</a>  >> Our services
	<h2>About Fashion Greatness</h2>
	<span class="hr"></span>
	<nav class="static-nav">
		<ul>
			<li>
				<a href="/about">About FG</a>
			</li>
			<li class="current">
				<a href="/services">Our services</a>
			</li>
			<li>
				<a href="/mentors">Our mentors</a>
			</li>
			<li>
				<a href="/faq">FAQ</a>
			</li>
			<li>
				<a href="/contacts">Contacts</a>
			</li>
		</ul>
	</nav>
</div>