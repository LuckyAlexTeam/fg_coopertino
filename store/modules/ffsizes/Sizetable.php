<?php

class Sizetable extends ObjectModel
{
    public $id;

    /** @var int Sizetable id which country belongs */
    public $id_sizetable;

    /** @var int Category id which Sizetable belongs */
    public $id_brand_category;

    /** @var string Title */
    public $title;

    /** @var string Sizes table html */
    public $html;


    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'ffsizes_sizetable',
        'primary' => 'id_sizetable',
        'multilang' => true,
        'fields' => array(
            'id_sizetable' =>                    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'id_brand_category' =>                array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),

            /* Lang fields */
            'title' =>                        array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'required' => true, 'size' => 100),
            'html' =>                        array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isCleanHtml', 'required' => true),
        ),
    );


    public static function getByBrandId($id_actegory = 0, $id_lang = 0)
    {
        if (!$id_actegory)
            return false;

        if(!$id_lang)
            $id_lang = Configuration::get('PS_LANG_DEFAULT');

        return Db::getInstance()->executeS('SELECT * FROM `'._DB_PREFIX_.'ffsizes_sizetable` fs
         LEFT JOIN `'._DB_PREFIX_.'ffsizes_sizetable_lang` fsl ON fsl.`id_sizetable`=fs.`id_sizetable` AND fsl.`id_lang`='.(int)$id_lang.'  WHERE fs.`id_brand_category`='.$id_actegory, true, false);

    }

    public static function getCategoriesId($id_sizetable){
        $return = array();
        $result = Db::getInstance()->executeS('SELECT `id_category` FROM `'._DB_PREFIX_.'ffsizes_sizetable_category` WHERE `id_sizetable`='.(int)$id_sizetable, true, false);

        foreach ($result as $row){
            $return[] = $row['id_category'];
        }
        return $return;
    }

    public function updateCategoriesId($categories){


        Db::getInstance()->execute('DELETE FROM `'._DB_PREFIX_.'ffsizes_sizetable_category` WHERE `id_sizetable`='.$this->id);

        $data = array();
        foreach($categories as $id_category){
            $data[] = array(
                'id_sizetable' => $this->id,
                'id_category' => $id_category,
            );
        }

        Db::getInstance()->insert('ffsizes_sizetable_category', $data);
        return array();
    }

    public function copyFromPost()
    {
        /* Classical fields */
        foreach ($_POST as $key => $value)
        {
            if (array_key_exists($key, $this) && $key != 'id_'.self::$definition['table'])
                $this->{$key} = $value;
        }

        /* Multilingual fields */

        $class_vars = get_class_vars(get_class($this));
        $fields = array();
        if (isset($class_vars['definition']['fields'])) {
            $fields = $class_vars['definition']['fields'];
        }

        foreach ($fields as $field => $params) {
            if (array_key_exists('lang', $params) && $params['lang']) {
                foreach (Language::getIDs(false) as $id_lang) {
                    if (Tools::isSubmit($field.'_'.(int)$id_lang)) {
                        $this->{$field}[(int)$id_lang] = Tools::getValue($field.'_'.(int)$id_lang);
                    }
                }
            }
        }

    }

    public static function getHtmlByIdCategory($id_actegory = 0, $id_lang = 0)
    {
        if (!$id_actegory)
            return false;

        if(!$id_lang)
            $id_lang = Configuration::get('PS_LANG_DEFAULT');

        return Db::getInstance()->getValue('SELECT fsl.`html` FROM `'._DB_PREFIX_.'ffsizes_sizetable_category` fsc
         LEFT JOIN `'._DB_PREFIX_.'ffsizes_sizetable_lang` fsl ON fsl.`id_sizetable`=fsc.`id_sizetable` AND fsl.`id_lang`='.(int)$id_lang.'
         WHERE fsc.`id_category`='.$id_actegory, true, false);

    }
}
