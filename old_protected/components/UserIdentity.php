<?php


class UserIdentity extends CUserIdentity {
	
	public $language;
	public $package;
	public $username;
	public $password;
	
	public function authenticate() {
		//print "<hr>authenticate()" . $this->username ."-". $this->password;
		$users=User::model()->getLogPas($this->username , $this->password);
		//var_dump($users);
		if(!isset($users[$this->username]))
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		elseif($users[$this->username]!==$this->password)
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
		else  {
			$this->errorCode=self::ERROR_NONE;
			//
			$us=User::model()->getLog($this->username);
			//
			Yii::app()->user->guestName = $this->username;
			Yii::app()->user->setState('package', $us['package']); 
			$this->package = $us['package']. "use";
			//Yii::app()->user->setState('login', $this->username); 
			$this->setState('maillogin', $this->username);
			$this->setState('login', $this->username);
			$this->setState('id_user', $us['id']); 
			$this->setState('package', $us['package']);
			$this->setState('language', $us['language']);
			$this->setState('anketa', $us['anketa']);
			$this->setState('prof', $us['prof']);
			$this->setState('prof1', $us['prof1']);
			$this->setState('prof2', $us['prof2']);
			$this->setState('prof3', $us['prof3']);
			$this->setState('prof4', $us['prof4']);
			$this->setState('rol', $us['rol']);
			//$this->setState('guestName', $this->username);
			//$this->setState('title', $this->username);
			//print "<!-- \r\n";
			//print "<br> maillogin=" . Yii::app()->user->getState('maillogin'); 
			//print "<br> id_user==" . Yii::app()->user->getState('id_user'); 
			//print "<br> package=" . Yii::app()->user->getState('package'); 
			//print "\r\n  -->";
		};
		return !$this->errorCode;
	}
}