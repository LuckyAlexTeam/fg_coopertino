


<div class="panel">

    <div class="panel-heading">
        {l s='Configuration' mod='ffsizes'}
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-3">
                <div class="list-group">
                    {foreach from=$home_categories item=home_category}
                        <a href="{$module_link}&id_home_category={$home_category.id_category}" class="list-group-item{if $home_category.id_category==$id_home_category} active{/if}">{$home_category.name}</a>
                    {/foreach}
                </div>
            </div>
            <div class="col-sm-9">
                <div class="panel">
                    <div class="panel-heading">
                        {l s='List of Size tables of current brand' mod='ffsizes'}
                        {if isset($size_tables) && count($size_tables)>0}<span class="badge">{count($size_tables)}</span>{/if}
				        <span class="panel-heading-action">
                            <a id="desc-category-new" class="list-toolbar-btn" href="{$module_link}&id_home_category={$id_home_category}&add_table">
                                <span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="Add New" data-html="true" data-placement="top">
                                    <i class="process-icon-new"></i>
                                </span>
                            </a>
                        </span>
                    </div>
                    <table class="table">
                            <tr>
                                <th>№</th>
                                <th>{l s='Title of Size table' mod='ffsizes'}</th>
                                <th>{l s='Action' mod='ffsizes'}</th>
                            </tr>
                        {if isset($size_tables) && count($size_tables)>0}
                            {foreach from=$size_tables item=size_item name=sizeLoop}
                                <tr>
                                    <td>{$smarty.foreach.sizeLoop.iteration}</td>
                                    <td>{$size_item.title}</td>
                                    <td>
                                        <a class="btn btn-success" href="{$module_link}&id_home_category={$id_home_category}&edit_table={$size_item.id_sizetable}" role="button">{l s='Edit' mod='ffsizes'}</a>
                                        <a class="btn btn-danger confirm" href="{$module_link}&id_home_category={$id_home_category}&del_table={$size_item.id_sizetable}" data-confirm="Вы уверены, что хотите удалить данную таблицу размеров?" role="button">{l s='Delete' mod='ffsizes'}</a>
                                    </td>
                                </tr>
                            {/foreach}
                        {/if}

                    </table>
                </div>

                {if isset($add_table)}
                    {$add_table_form}
                {/if}
            </div>
        </div>

    </div>

</div>
