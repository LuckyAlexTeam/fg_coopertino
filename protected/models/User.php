<?php

Yii::import('application.models._base.BaseUser');

class User extends BaseUser
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
        
        const GENDER_MEN = 1;
        const GENDER_WOMEN = 0;
        const BIRTH_SHOW_AGE = 0;
        const BIRTH_SHOW_DATE = 1;
        const LEGAL_GUARDIAN_YES = 1;
        const LEGAL_GUARDIAN_NO = 0;
        
        public static function genderTypeLabel($type){
            $labels = array(self::GENDER_MEN=>'Male',
                            self::GENDER_WOMEN=>'Female');
            return isset($labels[$type])?$labels[$type]:null;
        }

        public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('account_type, name, last_name, email, phone, password, country', 'required'),
                        array('email','email'),
                        array('email','unique'),
			array('account_type, gender, country, language', 'numerical', 'integerOnly'=>true),
			array('name, last_name, email, password, login, skype', 'length', 'max'=>100),
			array('phone', 'length', 'max'=>60),
			array('website, avatar', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, account_type, name, last_name, email, phone, password, login, gender, skype, website, date_of_birth, country, language, avatar', 'safe', 'on'=>'search'),
		);
	}
}