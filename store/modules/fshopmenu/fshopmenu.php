<?php
if (!defined('_PS_VERSION_'))
    exit;

class Fshopmenu extends Module
{
    public function __construct()
    {
        $this->name = 'fshopmenu';
        $this->tab = 'social_networks';
        $this->version = '1.0.0';
        $this->author = 'Sergii Khoroshko';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Fashion Menu - верхнее и нижнее меню магазина Fashion Greatness');
        $this->description = $this->l('Fashion Menu - верхнее и нижнее меню магазина Fashion Greatness.');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');

    }

    public function install()
    {
        if (Shop::isFeatureActive())
            Shop::setContext(Shop::CONTEXT_ALL);

        if (!parent::install()
            || !$this->registerHook('displayBrands')
//            || !$this->registerHook('displayMobileTop')
//            || !$this->registerHook('displayFooter')
            )
            return false;
        return true;
    }

    public function hookDisplayBrands($params)
    {
        $id_category = Tools::getValue('id_category');

        if ($this->context->controller->php_self == 'category'){
            $menu_tpl_name = 'full_menu.tpl';
            $maxdepth = 4;
        } else {
            $menu_tpl_name = 'brands_menu.tpl';
            $maxdepth = 2;
        }

        if (!$this->isCached($menu_tpl_name, $this->getCacheId('top-'.$id_category))) {

            $groups = implode(', ', Customer::getGroupsStatic((int)$this->context->customer->id));
            if (!$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
				SELECT DISTINCT c.id_parent, c.id_category, cl.name, cl.description, cl.link_rewrite
				FROM `' . _DB_PREFIX_ . 'category` c
				' . Shop::addSqlAssociation('category', 'c') . '
				LEFT JOIN `' . _DB_PREFIX_ . 'category_lang` cl ON (c.`id_category` = cl.`id_category` AND cl.`id_lang` = ' . (int)$this->context->language->id . Shop::addSqlRestrictionOnLang('cl') . ')
				LEFT JOIN `' . _DB_PREFIX_ . 'category_group` cg ON (cg.`id_category` = c.`id_category`)
				WHERE (c.`active` = 1 OR c.`id_category` = ' . (int)Configuration::get('PS_ROOT_CATEGORY') . ')
				' . ((int)($maxdepth) != 0 ? ' AND `level_depth` <= ' . (int)($maxdepth) : '') . '
				AND cg.`id_group` IN (' . pSQL($groups) . ')
				ORDER BY `level_depth` ASC, ' . (Configuration::get('BLOCK_CATEG_SORT') ? 'cl.`name`' : 'category_shop.`position`') . ' ' . (Configuration::get('BLOCK_CATEG_SORT_WAY') ? 'DESC' : 'ASC'))
            )
                return;

            $resultParents = array();
            $resultIds = array();

            foreach ($result as &$row) {
                $resultParents[$row['id_parent']][] = &$row;
                $resultIds[$row['id_category']] = &$row;
            }
            $menuCategTree = $this->getTree($resultParents, $resultIds, Configuration::get('BLOCK_CATEG_MAX_DEPTH'));
            unset($resultParents, $resultIds);
            $this->smarty->assign('menuCategTree', $menuCategTree);

        }


        $display = $this->display(__FILE__, $menu_tpl_name, $this->getCacheId('top-'.$id_category));

        return $display;
    }

    public function uninstall()
    {
        if (!parent::uninstall())
            return false;
        return true;
    }


    public function hookDisplayTop()
    {
//      TODO:  Сдклать кеширование !!!!

        $menu = '<ul class="menu_list">';
        $menu .= $this->showCatMenu();
        $menu .= '</ul>';
        return $menu;
    }

    public function hookDisplayMobileTop()
    {
//        d($this->context->getMobileDevice());
        return $this->showCatMenu();

    }

    public  function hookDisplayFooter()
    {
        $display = $this->display(__FILE__, 'footer_menu.tpl', $this->getCacheId('footer'));

        return $display;
    }

    private function showCatMenu() {

        $menu_tpl_name = 'category_menu.tpl';

        if ($this->context->getMobileDevice())
            $menu_tpl_name = 'category_menu_mobile.tpl';


        if (!$this->isCached($menu_tpl_name, $this->getCacheId())) {
            $maxdepth = 3;
            $groups = implode(', ', Customer::getGroupsStatic((int)$this->context->customer->id));
            if (!$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
				SELECT DISTINCT c.id_parent, c.id_category, cl.name, cl.description, cl.link_rewrite
				FROM `' . _DB_PREFIX_ . 'category` c
				' . Shop::addSqlAssociation('category', 'c') . '
				LEFT JOIN `' . _DB_PREFIX_ . 'category_lang` cl ON (c.`id_category` = cl.`id_category` AND cl.`id_lang` = ' . (int)$this->context->language->id . Shop::addSqlRestrictionOnLang('cl') . ')
				LEFT JOIN `' . _DB_PREFIX_ . 'category_group` cg ON (cg.`id_category` = c.`id_category`)
				WHERE (c.`active` = 1 OR c.`id_category` = ' . (int)Configuration::get('PS_ROOT_CATEGORY') . ')
				' . ((int)($maxdepth) != 0 ? ' AND `level_depth` <= ' . (int)($maxdepth) : '') . '
				AND cg.`id_group` IN (' . pSQL($groups) . ')
				ORDER BY `level_depth` ASC, ' . (Configuration::get('BLOCK_CATEG_SORT') ? 'cl.`name`' : 'category_shop.`position`') . ' ' . (Configuration::get('BLOCK_CATEG_SORT_WAY') ? 'DESC' : 'ASC'))
            )
                return;

            $resultParents = array();
            $resultIds = array();

            foreach ($result as &$row) {
                $resultParents[$row['id_parent']][] = &$row;
                $resultIds[$row['id_category']] = &$row;
            }
            $menuCategTree = $this->getTree($resultParents, $resultIds, Configuration::get('BLOCK_CATEG_MAX_DEPTH'));
            unset($resultParents, $resultIds);
            $this->smarty->assign('menuCategTree', $menuCategTree);
//            d($menuCategTree);
        }


        $display = $this->display(__FILE__, $menu_tpl_name, $this->getCacheId('top'));

        return $display;

//        return $this->context->smarty->fetch(__DIR__.'/views/templates/category_menu.tpl');
    }

    public function getTree($resultParents, $resultIds, $maxDepth, $id_category = null, $currentDepth = 0)
    {
        if (is_null($id_category))
            $id_category = $this->context->shop->getCategory();
        $children = array();
        if (isset($resultParents[$id_category]) && count($resultParents[$id_category]) && ($maxDepth == 0 || $currentDepth < $maxDepth))
            foreach ($resultParents[$id_category] as $subcat)
                $children[] = $this->getTree($resultParents, $resultIds, $maxDepth, $subcat['id_category'], $currentDepth + 1);
        if (isset($resultIds[$id_category]))
        {
            $link = $this->context->link->getCategoryLink($id_category, $resultIds[$id_category]['link_rewrite']);
            $name = $resultIds[$id_category]['name'];
            $desc = $resultIds[$id_category]['description'];
        }
        else
            $link = $name = $desc = '';

        $return = array(
            'id' => $id_category,
            'link' => $link,
            'name' => $name,
            'desc'=> $desc,
            'children' => $children
        );
        return $return;
    }

    protected function getCacheId($name = null)
    {
        $cache_id = parent::getCacheId();

        if ($name !== null)
            $cache_id .= '|'.$name;

        $categories_cnt = Db::getInstance()->getValue('SELECT COUNT(*) FROM `' . _DB_PREFIX_ . 'category` WHERE active=1');

        return $cache_id.'|'.$categories_cnt;
    }
}