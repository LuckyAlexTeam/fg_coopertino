<!-- Анкета Модель 
<style type="text/css">
	.photographer, .agency, .model, .designer {
		display: none;
	}
	.expert, .stylist {
		display: inline-block;
	}
</style> -->
<style type="text/css">
	.pageblock, .mod, .fot, .diz, .sti, .art, .mua {
		display: none;
	}
</style>

<!--
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
-->
<script type="text/javascript">
	$(document).ready(function(){
		//alert(55);
		$(".p-check").change(function(){
			//alert(66);
			var router = $(this).children("input").attr('name');
			var trigger = '';
			//alert(router);
			//$('.pageblock').attr('style', 'display:none;');
			if(router == 'model'){
				trigger = '.mod';
			}
			if(router == 'photographer'){
				trigger = '.fot';
			}
			if(router == 'designer'){
				trigger = '.diz';
			}
			if(router == 'agency'){
				trigger = '.mua';
			}
			if(router == 'Fashion Expert'){
				trigger = '.sti';
			}
			//alert("1: " + router + ", 2: " + trigger);
			$(trigger).toggle();
			router = '';
			trigger = '';
			//
		});
	});
</script>

<div class="swich">
	<label class="p-check"><input type="checkbox" name="model"><span><div></div></span>model</label> | <label class="p-check"><input type="checkbox" name="photographer"><span><div></div></span>photographer</label> | <label class="p-check"><input type="checkbox" name="designer"><span><div></div></span>designer</label> | <label class="p-check"><input type="checkbox" name="agency"><span><div></div></span>agency</label> | <label class="p-check"><input type="checkbox" name="Fashion Expert"><span><div></div></span>Fashion Expert</label>
</div>
<div class="person-wrapper">
<!-- Эксперты -->
	<form class="acc-prof expert">
		<div class="d-wrap">
			<div class="details-item">
				*Specialization:
			</div>
			<div class="details-data general-input">
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Brand"><span><div></div></span>Brand</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Buyer"><span><div></div></span>Buyer</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Customer"><span><div></div></span>Customer</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Company Representative"><span><div></div></span>Company Representative</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Media"><span><div></div></span>Media</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Stylist"><span><div></div></span>Stylist</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Hair Stylist/Artist"><span><div></div></span>Hair Stylist/Artist</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="MUA"><span><div></div></span>MUA</label>
				</div>
			</div>
		</div>
<!-- Эксперты Конец -->
	<h2>Подробно заполните Вашу анкету</h2><!-- Модель, Фотограф, Дезайнер, Эксперт -->
		<h2>General information</h2>
		<span class="hr"></span>
		<div class="d-wrap model designer photographer buyer customer repres media"><!-- Модель, Фотограф, Дизайнер, Buyer, Customer, Representative, Media -->
			<div class="details-item">
				*First Name:
			</div>
			<div class="details-data general-input">
				<input type="text" class="f-n" placeholder="First Name *"></input>
			</div>
		</div>
		<div class="d-wrap agency"><!-- Агентство -->
			<div class="details-item">
				*Name:
			</div>
			<div class="details-data general-input">
				<input type="text" class="a-n" placeholder="Name *"></input>
			</div>
		</div>
		<div class="d-wrap model designer photographer buyer customer repres media"><!-- Модель, Фотограф, Дезайнер, Buyer, Customer, Representative, Media -->
			<div class="details-item">
				*Last Name:
			</div>
			<div class="details-data general-input">
				<input type="text" class="l-n" placeholder="Last Name *"></input>
			</div>
		</div>
		<div class="d-wrap brand"><!-- Brand -->
			<div class="details-item">
				*Name of company:
			</div>
			<div class="details-data general-input">
				<input type="text" class="a-n" placeholder="Name of company*"></input>
			</div>
		</div>
		<div class="d-wrap brand"><!-- Brand -->
			<div class="details-item">
				*Brand location:
			</div>
			<div class="details-data general-input">
				<div class="loc loc-m">
					<label class="q-check"><input type="checkbox" name="China" id = 'cb15' onchange = 'showOrHide("cb15", "cat15");'/><span><div></div></span>China:</label>
					<div class="loc loc-ch" id="cat15">
						<label class="q-check"><input type="checkbox" name="Beijing"><span><div></div></span>Beijing</label>
						<label class="q-check"><input type="checkbox" name="Hong Kong"><span><div></div></span>Hong Kong</label>
						<label class="q-check"><input type="checkbox" name="Shanghai"><span><div></div></span>Shanghai</label>
						<label class="q-check"><input type="checkbox" name="Ch-Other"><span><div></div></span>Other</label>
					</div>
				</div>
				<div class="loc loc-m">
					<label class="q-check"><input type="checkbox" name="England" id = 'cb16' onchange = 'showOrHide("cb16", "cat16");'/><span><div></div></span>England:</label>
					<div class="loc loc-en" id="cat16">
						<label class="q-check"><input type="checkbox" name="London"><span><div></div></span>London</label>
						<label class="q-check"><input type="checkbox" name="London area"><span><div></div></span>London area</label>
						<label class="q-check"><input type="checkbox" name="En-Other"><span><div></div></span>Other</label>
					</div>
				</div>
				<div class="loc loc-m">
						<label class="q-check"><input type="checkbox" name="France" id = 'cb17' onchange = 'showOrHide("cb17", "cat17");'/><span><div></div></span>France:</label>
					<div class="loc loc-fr" id="cat17">
						<label class="q-check"><input type="checkbox" name="Paris"><span><div></div></span>Paris</label>
						<label class="q-check"><input type="checkbox" name="Paris area"><span><div></div></span>Paris area</label>
						<label class="q-check"><input type="checkbox" name="Fr-Other"><span><div></div></span>Other</label>
					</div>
				</div>
				<div class="loc loc-m">
						<label class="q-check"><input type="checkbox" name="Germany" id = 'cb18' onchange = 'showOrHide("cb18", "cat18");'/><span><div></div></span>Germany:</label>
					<div class="loc loc-ge" id="cat18">
						<label class="q-check"><input type="checkbox" name="Berlin"><span><div></div></span>Berlin</label>
						<label class="q-check"><input type="checkbox" name="Berlin area"><span><div></div></span>Berlin area</label>
						<label class="q-check"><input type="checkbox" name="Ge-Other"><span><div></div></span>Other</label>
					</div>
				</div>
				<div class="loc loc-m">
						<label class="q-check"><input type="checkbox" name="Italy" id='cb19' onchange = 'showOrHide("cb19", "cat19");'/><span><div></div></span>Italy:</label>
					<div class="loc loc-it" id="cat19">
						<label class="q-check"><input type="checkbox" name="Milan"><span><div></div></span>Milan</label>
						<label class="q-check"><input type="checkbox" name="Milan area"><span><div></div></span>Milan area</label>
						<label class="q-check"><input type="checkbox" name="Rome"><span><div></div></span>Rome</label>
						<label class="q-check"><input type="checkbox" name="Rome area"><span><div></div></span>Rome area</label>
						<label class="q-check"><input type="checkbox" name="It-Other"><span><div></div></span>Other</label>
					</div>
				</div>
				<div class="loc loc-m">
						<label class="q-check"><input type="checkbox" name="USA" id = 'cb20' onchange = 'showOrHide("cb20", "cat20");'/><span><div></div></span>USA:</label>
					<div class="loc loc-usa" id="cat20">
						<label class="q-check"><input type="checkbox" name="Los Angeles"><span><div></div></span>Los Angeles</label>
						<label class="q-check"><input type="checkbox" name="Los Angeles area"><span><div></div></span>Los Angeles area</label>
						<label class="q-check"><input type="checkbox" name="New York"><span><div></div></span>New York</label>
						<label class="q-check"><input type="checkbox" name="New York area"><span><div></div></span>New York area</label>
						<label class="q-check"><input type="checkbox" name="USA-Other"><span><div></div></span>Other</label>
					</div>
				</div>
				<div class="loc loc-m">
						<label class="q-check"><input type="checkbox" name="Ukraine"><span><div></div></span>Ukraine</label>
				</div>
				<div class="loc loc-m">
						<label class="q-check"><input type="checkbox" name="Russia"><span><div></div></span>Russia</label>
				</div>
				<div class="loc loc-m">
						<label class="q-check"><input type="checkbox" name="Other"><span><div></div></span>Other</label>
				</div>
			</div>
		</div>
		<div class="d-wrap model designer photographer agency brand buyer customer repres media"><!-- Модель, Фотограф, Дезайнер, Brand, Buyer, Customer, Representative, Media -->
			<div class="details-item">
				*Phone number:
			</div>
			<div class="details-data general-input">
				<input type="phone" class="ph-n" placeholder="Phone Number *"></input>
			</div>
		</div>
		<div class="d-wrap model designer photographer agency brand buyer customer repres media"><!-- Модель, Фотограф, Дезайнер, Brand, Buyer, Customer, Representative, Media -->
			<div class="details-item">
				*E-mail:
			</div>
			<div class="details-data general-input">
				<input type="text" class="em" placeholder="E-mail *"></input>
			</div>
		</div>
		<div class="d-wrap model designer photographer agency brand buyer customer repres media"><!-- Модель, Фотограф, Дезайнер, Brand, Buyer, Customer, Representative, Media -->
			<div class="details-item">
				*Skype:
			</div>
			<div class="details-data general-input">
				<input type="text" class="sk" placeholder="Skype *"></input>
			</div>
		</div>
		<div class="d-wrap model"><!-- Модель -->
			<div class="details-item">
				Agency Info:
			</div>
			<div class="details-data general-input">
				<input type="text" class="a-i" placeholder="Agency Info:"></input>
			</div>
		</div>
		<div class="d-wrap model designer photographer expert agency brand buyer customer repres media"><!-- Модель, Фотограф, Дезайнер, Brand, Buyer, Customer, Representative, Media -->
			<div class="details-item">
				Web-site:
			</div>
			<div class="details-data general-input">
				<input type="text" class="w-s" placeholder="Web-site:"></input>
			</div>
		</div>
		<div class="d-wrap model designer photographer buyer customer repres media"><!-- Модель, Фотограф, Дезайнер, Buyer, Customer, Representative, Media -->
			<div class="details-item">
				*Gender:
			</div>
			<div class="details-data general-input">
				<select class="ge">
					<option>Male</option>
					<option>Female</option>
				</select>
			</div>
		</div>
		<div class="d-wrap model designer photographer buyer customer repres media"><!-- Модель, Фотограф, Дезайнер, Buyer, Customer, Representative, Media -->
			<div class="details-item">
				*Date of Birth:
			</div>
			<div class="details-data general-input">
				<input type="text" class="d-b"></input>
			</div>
		</div>
		<div class="d-wrap model designer photographer buyer customer repres media"><!-- Модель, Фотограф, Дезайнер, Buyer, Customer, Representative, Media -->
			<div class="details-item">
				Choose what to show:
			</div>
			<div class="details-data general-input">
				<label class="q-rad">
					<input type="radio" name="metric" id="age" value="Age"></input>
					<span><div></div></span>Age</label>
				<label class="q-rad">
					<input type="radio" name="metric" id="date" value="Date of Birth" checked></input>
				<span><div></div></span>Date of Birth</label>
			</div>
		</div>
		<div class="d-wrap model"><!-- Модель -->
			<div class="details-item">
				*Do you have your Legal Guardian’s agree [required for models under 18]:
			</div>
			<div class="details-data general-input">
				<select class="guardians">
					<option>Yes</option>
					<option>No</option>
				</select>
			</div>
		</div>
		<div class="d-wrap model designer photographer agency buyer customer repres media"><!-- Модель, Фотограф, Дезайнер, Агентство, Buyer, Customer, Representative, Media -->
			<div class="details-item">
				*Location:
			</div>
			<div class="details-data general-input">
				<script type="text/javascript">
					function showOrHide(cb, cat) {
						cb = document.getElementById(cb);
						cat = document.getElementById(cat);
						if (cb.checked) cat.style.display = "block";
					else cat.style.display = "none";
					}
				</script>
				<div class="loc loc-m">
					<label class="q-check"><input type="checkbox" name="China" id = 'cb1' onchange = 'showOrHide("cb1", "cat1");'/><span><div></div></span>China:</label>
					<div class="loc loc-ch" id="cat1">
						<label class="q-check"><input type="checkbox" name="Beijing"><span><div></div></span>Beijing</label>
						<label class="q-check"><input type="checkbox" name="Hong Kong"><span><div></div></span>Hong Kong</label>
						<label class="q-check"><input type="checkbox" name="Shanghai"><span><div></div></span>Shanghai</label>
						<label class="q-check"><input type="checkbox" name="Ch-Other"><span><div></div></span>Other</label>
					</div>
				</div>
				<div class="loc loc-m">
					<label class="q-check"><input type="checkbox" name="England" id = 'cb2' onchange = 'showOrHide("cb2", "cat2");'/><span><div></div></span>England:</label>
					<div class="loc loc-en" id="cat2">
						<label class="q-check"><input type="checkbox" name="London"><span><div></div></span>London</label>
						<label class="q-check"><input type="checkbox" name="London area"><span><div></div></span>London area</label>
						<label class="q-check"><input type="checkbox" name="En-Other"><span><div></div></span>Other</label>
					</div>
				</div>
				<div class="loc loc-m">
						<label class="q-check"><input type="checkbox" name="France" id = 'cb3' onchange = 'showOrHide("cb3", "cat3");'/><span><div></div></span>France:</label>
					<div class="loc loc-fr" id="cat3">
						<label class="q-check"><input type="checkbox" name="Paris"><span><div></div></span>Paris</label>
						<label class="q-check"><input type="checkbox" name="Paris area"><span><div></div></span>Paris area</label>
						<label class="q-check"><input type="checkbox" name="Fr-Other"><span><div></div></span>Other</label>
					</div>
				</div>
				<div class="loc loc-m">
						<label class="q-check"><input type="checkbox" name="Germany" id = 'cb4' onchange = 'showOrHide("cb4", "cat4");'/><span><div></div></span>Germany:</label>
					<div class="loc loc-ge" id="cat4">
						<label class="q-check"><input type="checkbox" name="Berlin"><span><div></div></span>Berlin</label>
						<label class="q-check"><input type="checkbox" name="Berlin area"><span><div></div></span>Berlin area</label>
						<label class="q-check"><input type="checkbox" name="Ge-Other"><span><div></div></span>Other</label>
					</div>
				</div>
				<div class="loc loc-m">
						<label class="q-check"><input type="checkbox" name="Italy" id = 'cb5' onchange = 'showOrHide("cb5", "cat5");'/><span><div></div></span>Italy:</label>
					<div class="loc loc-it" id="cat5">
						<label class="q-check"><input type="checkbox" name="Milan"><span><div></div></span>Milan</label>
						<label class="q-check"><input type="checkbox" name="Milan area"><span><div></div></span>Milan area</label>
						<label class="q-check"><input type="checkbox" name="Rome"><span><div></div></span>Rome</label>
						<label class="q-check"><input type="checkbox" name="Rome area"><span><div></div></span>Rome area</label>
						<label class="q-check"><input type="checkbox" name="It-Other"><span><div></div></span>Other</label>
					</div>
				</div>
				<div class="loc loc-m">
						<label class="q-check"><input type="checkbox" name="USA" id = 'cb6' onchange = 'showOrHide("cb6", "cat6");'/><span><div></div></span>USA:</label>
					<div class="loc loc-usa" id="cat6">
						<label class="q-check"><input type="checkbox" name="Los Angeles"><span><div></div></span>Los Angeles</label>
						<label class="q-check"><input type="checkbox" name="Los Angeles area"><span><div></div></span>Los Angeles area</label>
						<label class="q-check"><input type="checkbox" name="New York"><span><div></div></span>New York</label>
						<label class="q-check"><input type="checkbox" name="New York area"><span><div></div></span>New York area</label>
						<label class="q-check"><input type="checkbox" name="USA-Other"><span><div></div></span>Other</label>
					</div>
				</div>
				<div class="loc loc-m">
						<label class="q-check"><input type="checkbox" name="Ukraine"><span><div></div></span>Ukraine</label>
				</div>
				<div class="loc loc-m">
						<label class="q-check"><input type="checkbox" name="Russia"><span><div></div></span>Russia</label>
				</div>
				<div class="loc loc-m">
						<label class="q-check"><input type="checkbox" name="Other"><span><div></div></span>Other</label>
				</div>
			</div>
		</div>
		<div class="d-wrap agency"><!-- Агентство -->
			<div class="details-item">
				*Address:
			</div>
			<div class="details-data general-input">
				<input type="text" class="a-a" placeholder="Address *"></input>
			</div>
		</div>
		<div class="d-wrap agency"><!-- Агентство -->
			<div class="details-item">
				Official social media:
			</div>
			<div class="details-data general-input">
				<input type="text" class="a-osm" placeholder="Official social media:"></input>
			</div>
		</div>
		<!-- Модель -->
		<h2>Модель</h2>
		<h2>Physical Details</h2>
		<span class="hr"></span>
		<div class="d-wrap">
			<div class="details-item">
				Choose you metrical system:
			</div>
			<div class="details-data general-input">
				<label class="q-rad">
					<input type="radio" name="metric" id="cm" value="cm" checked></input>
					<span><div></div></span>cm</label>
				<label class="q-rad">
					<input type="radio" name="metric" id="in" value="in"></input>
				<span><div></div></span>in</label>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Height:
			</div>
			<div class="details-data general-input">
				<input type="number" min="61" max="229" class="he" placeholder="165 cm"></input>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Weight:
			</div>
			<div class="details-data general-input">
				<input type="number" min="30" max="170" class="we" placeholder="65 kg"></input>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Chest:
			</div>
			<div class="details-data general-input">
				<input type="number" min="54" max="230" class="che" placeholder="85 cm"></input>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Waist:
			</div>
			<div class="details-data general-input">
				<input type="number" min="48" max="134" class="wa" placeholder="60 cm"></input>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Hips:
			</div>
			<div class="details-data general-input">
				<input type="number" min="48" max="134" class="hips" placeholder="95 cm"></input>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Dress/Jacket, EU:
			</div>
			<div class="details-data general-input">
				<input type="number" min="32" max="60" class="dre" placeholder="38 EU"></input>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Cup:
			</div>
			<div class="details-data general-input">
				<input type="text" class="cup" placeholder="C"></input>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Shoe, EU:
			</div>
			<div class="details-data general-input">
				<input type="number" min="34" max="47" class="shoe" placeholder="37 EU"></input>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Ethnicity:
			</div>
			<div class="details-data general-input">
				<select class="ethn">
					<option>African</option>
					<option>Asian</option>
					<option>European</option>
					<option>Hispanic</option>
					<option>Indian</option>
					<option>Latin American</option>
					<option>Mediterranean</option>
					<option>Near East</option>
					<option>Scandinavian</option>
					<option>Mixed</option>
					<option>Other тот пункт поле для ввода текста</option>ЭЭЭЭЭЭЭЭЭЭЭ
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Skin color:
			</div>
			<div class="details-data general-input">
				<select class="sc-c">
					<option>Pale white</option>
					<option>White</option>
					<option>Olive</option>
					<option>Light brown</option>
					<option>Brown</option>
					<option>Dark brown</option>
					<option>Black</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Eye color:
			</div>
			<div class="details-data general-input">
				<select class="ey-c">
					<option>Black</option>
					<option>Blue</option>
					<option>Green</option>
					<option>Hazel</option>
					<option>Other</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Natural hair color:
			</div>
			<div class="details-data general-input">
				<select class="nh-c">
					<option>Blonde</option>
					<option>Strawberry Blonde</option>
					<option>Auburn</option>
					<option>Ginger</option>
					<option>Light Brown</option>
					<option>Brown</option>
					<option>Dark Brown</option>
					<option>Salt & Pepper</option>
					<option>Black</option>
					<option>Other</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Hair coloration:
			</div>
			<div class="details-data general-input">
				<div class="h-col-m">
					<label class="q-check"><input type="checkbox" name="Not colored"><span><div></div></span>Not colored</label>
				</div>
				<div class="h-col-m">
					<label class="q-check"><input type="checkbox" name="Colored" id = 'cb7' onchange = 'showOrHide("cb7", "cat7");'/><span><div></div></span>Colored</label>
				</div>
				<div class="h-col-h" id="cat7">
						<label class="q-check"><input type="checkbox" name="Blonde"><span><div></div></span>Blonde</label>
						<label class="q-check"><input type="checkbox" name="Strawbery Blonde"><span><div></div></span>Strawbery Blonde</label>
						<label class="q-check"><input type="checkbox" name="Auburn"><span><div></div></span>Auburn</label>
						<label class="q-check"><input type="checkbox" name="Ginger"><span><div></div></span>Ginger</label>
						<label class="q-check"><input type="checkbox" name="Red"><span><div></div></span>Red</label>
						<label class="q-check"><input type="checkbox" name="Light Brown"><span><div></div></span>Light Brown</label>
						<label class="q-check"><input type="checkbox" name="Brown"><span><div></div></span>Brown</label>
						<label class="q-check"><input type="checkbox" name="Dark Brown"><span><div></div></span>Dark Brown</label>
						<label class="q-check"><input type="checkbox" name="Salt & Pepper"><span><div></div></span>Salt & Pepper</label>
						<label class="q-check"><input type="checkbox" name="Black"><span><div></div></span>Black</label>
						<label class="q-check"><input type="checkbox" name="Black Blue"><span><div></div></span>Black Blue</label>
						<label class="q-check"><input type="checkbox" name="Other"><span><div></div></span>Other</label>
				</div>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Readness to change color:
			</div>
			<div class="details-data general-input">
				<select>
					<option>Ready to change hair color</option>
					<option>not ready to change hair color</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Hair lenght:
			</div>
			<div class="details-data general-input">
				<select>
					<option>Bald</option>
					<option>Short</option>
					<option>Medium</option>
					<option>Shoulder length</option>
					<option>Very long</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Tattoos:
			</div>
			<div class="details-data general-input">
				<div class="tat-m">
					<label class="q-check"><input type="checkbox" name="No tattoos"><span><div></div></span>No tattoos</label>
				</div>
				<div class="tat-m">
					<label class="q-check"><input type="checkbox" name="Has tattoos" id = 'cb8' onchange = 'showOrHide("cb8", "cat8");'/><span><div></div></span>Has tattoos</label>
				</div>
				<div class="tat-h" id="cat8">
						<label class="q-check"><input type="checkbox" name="Neck"><span><div></div></span>Neck</label>
						<label class="q-check"><input type="checkbox" name="Shoulder"><span><div></div></span>Shoulder</label>
						<label class="q-check"><input type="checkbox" name="Biceps"><span><div></div></span>Biceps</label>
						<label class="q-check"><input type="checkbox" name="Wrist"><span><div></div></span>Wrist</label>
						<label class="q-check"><input type="checkbox" name="Chest"><span><div></div></span>Chest</label>
						<label class="q-check"><input type="checkbox" name="Belly"><span><div></div></span>Belly</label>
						<label class="q-check"><input type="checkbox" name="Back"><span><div></div></span>Back</label>
						<label class="q-check"><input type="checkbox" name="Loin"><span><div></div></span>Loin</label>
						<label class="q-check"><input type="checkbox" name="Hip"><span><div></div></span>Hip</label>
						<label class="q-check"><input type="checkbox" name="Shin"><span><div></div></span>Shin</label>
						<label class="q-check"><input type="checkbox" name="Ankle"><span><div></div></span>Ankle</label>
						<label class="q-check"><input type="checkbox" name="Foot"><span><div></div></span>Foot</label>
						<label class="q-check"><input type="checkbox" name="Tat-Others"><span><div></div></span>Others</label>
				</div>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Piercing:
			</div>
			<div class="details-data general-input">
				<div class="pier-m">
					<label class="q-check"><input type="checkbox" name="No piercing"><span><div></div></span>No piercing (earlobes don't count)</label>
				</div>
				<div class="pier-m">
					<label class="q-check"><input type="checkbox" name="Has other piercing" id = 'cb9' onchange = 'showOrHide("cb9", "cat9");'/><span><div></div></span>Has other piercing:</label>
				</div>
				<div class="pier-h" id="cat9">
						<label class="q-check"><input type="checkbox" name="Ear tunnel"><span><div></div></span>Ear tunnel</label>
						<label class="q-check"><input type="checkbox" name="Eyebrows"><span><div></div></span>Eyebrows</label>
						<label class="q-check"><input type="checkbox" name="Tongue"><span><div></div></span>Tongue</label>
						<label class="q-check"><input type="checkbox" name="Nose"><span><div></div></span>Nose</label>
						<label class="q-check"><input type="checkbox" name="Nipple"><span><div></div></span>Nipple</label>
						<label class="q-check"><input type="checkbox" name="Navel"><span><div></div></span>Navel</label>
						<label class="q-check"><input type="checkbox" name="Body implants"><span><div></div></span>Body implants</label>
						<label class="q-check"><input type="checkbox" name="Pier-Other"><span><div></div></span>Other</label>
				</div>
			</div>
		</div>
<!-- Professional -->
<!-- Модель -->
	<div class="pageblock mod">
		<h2>Professional Details</h2>
		<span class="hr"></span>
		<div class="d-wrap">
			<div class="details-item">
				*Modeling categories/Genre:
			</div>
			<div class="details-data general-input">
				<div class="mc-g-m">
					<label class="q-check"><input type="checkbox" name="Alternative model"><span><div></div></span>Alternative model</label>
				</div>
				<div class="mc-g-m">
					<label class="q-check"><input type="checkbox" name="Body Paint"><span><div></div></span>Body Paint</label>
				</div>
				<div class="mc-g-m">
					<label class="q-check"><input type="checkbox" name="Body Parts" id = 'cb10' onchange = 'showOrHide("cb10", "cat10");'/><span><div></div></span>Body Parts:</label>
				</div>
				<div class="mc-g-h" id="cat10">
						<label class="q-check"><input type="checkbox" name="Ears"><span><div></div></span>Ears</label>
						<label class="q-check"><input type="checkbox" name="Eyes"><span><div></div></span>Eyes</label>
						<label class="q-check"><input type="checkbox" name="Fingers"><span><div></div></span>Fingers</label>
						<label class="q-check"><input type="checkbox" name="Foot"><span><div></div></span>Foot</label>
						<label class="q-check"><input type="checkbox" name="Hair"><span><div></div></span>Hair</label>
						<label class="q-check"><input type="checkbox" name="Hands"><span><div></div></span>Hands</label>
						<label class="q-check"><input type="checkbox" name="Lips"><span><div></div></span>Lips</label>
						<label class="q-check"><input type="checkbox" name="Neck"><span><div></div></span>Neck</label>
				</div>
				<div class="mc-g-m">
					<label class="q-check"><input type="checkbox" name="Print photo model" id = 'cb11' onchange = 'showOrHide("cb11", "cat11");'/><span><div></div></span>Print photo model:</label>
				</div>
				<div class="mc-g-h" id="cat11">
						<label class="q-check"><input type="checkbox" name="Editorial"><span><div></div></span>Editorial</label>
						<label class="q-check"><input type="checkbox" name="Print ads"><span><div></div></span>Print ads</label>
				</div>
				<div class="mc-g-m">
					<label class="q-check"><input type="checkbox" name="Hight fashion model"><span><div></div></span>Hight fashion model</label>
				</div>
				<div class="mc-g-m">
					<label class="q-check"><input type="checkbox" name="Runway model"><span><div></div></span>Runway model</label>
				</div>
				<div class="mc-g-m">
					<label class="q-check"><input type="checkbox" name="Glamour model" id = 'cb12' onchange = 'showOrHide("cb12", "cat12");'/><span><div></div></span>Glamour model:</label>
				</div>
				<div class="mc-g-h" id="cat12">
						<label class="q-check"><input type="checkbox" name="Lingerie/Body model"><span><div></div></span>Lingerie/Body model</label>
						<label class="q-check"><input type="checkbox" name="Swimwear model"><span><div></div></span>Swimwear model</label>
				</div>
				<div class="mc-g-m">
					<label class="q-check"><input type="checkbox" name="Fitness model"><span><div></div></span>Fitness model</label>
				</div>
				<div class="mc-g-m">
					<label class="q-check"><input type="checkbox" name="Beauty model"><span><div></div></span>Beauty model (hair/makeup)</label>
				</div>
				<div class="mc-g-m">
					<label class="q-check"><input type="checkbox" name="Nude"><span><div></div></span>Nude</label>
				</div>
				<div class="mc-g-m">
					<label class="q-check"><input type="checkbox" name="Promotional model"><span><div></div></span>Promotional model</label>
				</div>
				<div class="mc-g-m">
					<label class="q-check"><input type="checkbox" name="Plus-sized model"><span><div></div></span>Plus-sized model</label>
				</div>
				<div class="mc-g-m">
					<label class="q-check"><input type="checkbox" name="Street Fashion"><span><div></div></span>Street Fashion</label>
				</div>
				<div class="mc-g-m">
					<label class="q-check"><input type="checkbox" name="Sport model"><span><div></div></span>Sport model</label>
				</div>
				<div class="mc-g-m">
					<label class="q-check"><input type="checkbox" name="Senior model"><span><div></div></span>Senior model (40+)</label>
				</div>
				<div class="mc-g-m">
					<label class="q-check"><input type="checkbox" name="Video"><span><div></div></span>Video</label>
				</div>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Professional Experience:
			</div>
			<div class="details-data general-input">
				<select>
					<option>Up to 1 year</option>
					<option>1-5 years</option>
					<option>More that 5 years</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				Additional Skills:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Negotiable"><span><div></div></span>Actor or Actress</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Negotiable"><span><div></div></span>Film Acting</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Negotiable"><span><div></div></span>TV Acting</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Negotiable"><span><div></div></span>Dance</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Negotiable"><span><div></div></span>Music</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Negotiable"><span><div></div></span>Extreme Sports</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Negotiable"><span><div></div></span>Stage Agting</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Negotiable"><span><div></div></span>Commercial Acting</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Negotiable"><span><div></div></span>Singer</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Negotiable"><span><div></div></span>Performance Artist</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Negotiable"><span><div></div></span>Voice Over Talent</label>
				</div>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Languages:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Chinese"><span><div></div></span>Chinese</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="English"><span><div></div></span>English</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="French"><span><div></div></span>French</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="German"><span><div></div></span>German</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Italian"><span><div></div></span>Italian</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Russian"><span><div></div></span>Russian</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Spanish"><span><div></div></span>Spanish</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Ukrainian"><span><div></div></span>Ukrainian</label>
				</div>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				Compensation:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Payment Only"><span><div></div></span>Payment Only</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Photo/Time For Print"><span><div></div></span>Photo/Time For Print</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="In exchange of experience"><span><div></div></span>In exchange of experience</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Test Shoot"><span><div></div></span>Test Shoot</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Negotiable"><span><div></div></span>Negotiable</label>
				</div>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				Rates for photoshoot:
			</div>
			<div class="details-data general-input">
				<input type="text" class="r-f-ph" placeholder="per hour"></input>
				<input type="text" class="r-f-ph" placeholder="half day/4 hours"></input>
				<input type="text" class="r-f-ph" placeholder="full day/8 hours"></input>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				Rates for Runway:
			</div>
			<div class="details-data general-input">
				<input type="text" class="r-f-r" placeholder="rates for runway"></input>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Contacted or not:
			</div>
			<div class="details-data general-input">
				<select>
					<option>Contacted with the agency</option>
					<option>Not contacted</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Travel Ability:
			</div>
			<div class="details-data general-input">
				<select>
					<option>Ready to travel</option>
					<option>Work within location</option>
				</select>
			</div>
		</div>
	</div>
<!-- Модель Конец -->
<!-- Фотограф -->
	<div class="pageblock fot">
		<h2>Фотограф</h2>
		<h2>Professional Details</h2>
		<span class="hr"></span>
		<div class="d-wrap">
			<div class="details-item">
				*Professional Experience:
			</div>
			<div class="details-data general-input">
				<select>
					<option>Up to 1 year</option>
					<option>1-5 years</option>
					<option>More that 5 years</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Work areas:
			</div>
			<div class="details-data general-input">
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Advertising photographers"><span><div></div></span>Advertising photographers</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Beauty & Hair photographers"><span><div></div></span>Beauty & Hair photographers</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Black & Wite photographers"><span><div></div></span>Black & Wite photographers</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Children photographers"><span><div></div></span>Children photographers</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Celebrity/Entertainment"><span><div></div></span>Celebrity/Entertainment</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Fashion/Editorials photographers"><span><div></div></span>Fashion/Editorials photographers</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Fitness/body photographers"><span><div></div></span>Fitness/body photographers</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Nude"><span><div></div></span>Nude</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="People & lifestyle photographers"><span><div></div></span>People & lifestyle photographers</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Portrait photographers"><span><div></div></span>Portrait photographers</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Sports/Action photographers"><span><div></div></span>Sports/Action photographers</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Reportage photographers"><span><div></div></span>Reportage photographers</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Runway/Fashion Event photographers"><span><div></div></span>Runway/Fashion Event photographers</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Swimwear & lingerie photographers"><span><div></div></span>Swimwear & lingerie photographers</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Travel photographers"><span><div></div></span>Travel photographers</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Underwater photographers"><span><div></div></span>Underwater photographers</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Photo post production"><span><div></div></span>Photo post production (retouching)</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Film/Video production"><span><div></div></span>Film/Video production</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Commercial production" id='cb13' onchange='showOrHide("cb13", "cat13");'/><span><div></div></span>Commercial production:</label>
				</div>
				<div  class="w-a-h" id="cat13">
						<label class="q-check"><input type="checkbox" name="TV/Video Production"><span><div></div></span>TV/Video Production</label>
						<label class="q-check"><input type="checkbox" name="Film/Video Post"><span><div></div></span>Film/Video Post Production & CGI</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="CGI" id='cb14' onchange='showOrHide("cb14", "cat14");'/><span><div></div></span>CGI & 3D (CGI & 3D):</label>
				</div>
				<div  class="w-a-h" id="cat14">
						<label class="q-check"><input type="checkbox" name="Motion graphics"><span><div></div></span>Motion graphics</label>
						<label class="q-check"><input type="checkbox" name="Sound design/scoring"><span><div></div></span>Sound design/scoring</label>
				</div>
				<div class="w-a-m">
					<label class="q-check"><input type="checkbox" name="Rental studios"><span><div></div></span>Rental studios</label>
				</div>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Work type:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Editorials"><span><div></div></span>Editorials</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Runway"><span><div></div></span>Runway</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Beauty"><span><div></div></span>Beauty</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Portraits/Celebrity"><span><div></div></span>Portraits/Celebrity</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Glamour/Nude"><span><div></div></span>Glamour/Nude</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Commercial/Advertising"><span><div></div></span>Commercial/Advertising</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Street Fashion"><span><div></div></span>Street Fashion</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Creative Projects"><span><div></div></span>Creative Projects</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Test Shoot"><span><div></div></span>Test Shoot</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="WT-Other"><span><div></div></span>Other</label>
				</div>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				Type of camera equipment:
			</div>
			<div class="details-data general-input">
				<input type="text" name="cam-enquip" placeholder="Type of camera equipment">
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Studio:
			</div>
			<div class="details-data general-input">
				<select>
					<option>Provided</option>
					<option>Not provided</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Macke-Up Artist:
			</div>
			<div class="details-data general-input">
				<select>
					<option>Provided</option>
					<option>Not provided</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Hair Stylist:
			</div>
			<div class="details-data general-input">
				<select>
					<option>Provided</option>
					<option>Not provided</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				Compensation:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Payment Only"><span><div></div></span>Payment Only</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Photo/Time For Print"><span><div></div></span>Photo/Time For Print</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="In exchange of experience"><span><div></div></span>In exchange of experience</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Test Shoot"><span><div></div></span>Test Shoot</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Negotiable"><span><div></div></span>Negotiable</label>
				</div>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				Rates for photoshoot:
			</div>
			<div class="details-data general-input">
				<input type="text" class="r-f-ph" placeholder="per hour"></input>
				<input type="text" class="r-f-ph" placeholder="half day/4 hours"></input>
				<input type="text" class="r-f-ph" placeholder="full day/8 hours"></input>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Travel Ability:
			</div>
			<div class="details-data general-input">
				<select>
					<option>Ready to travel</option>
					<option>Work within location</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				Main reason to be on site:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="To sell services"><span><div></div></span>To sell services</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To serch for clients"><span><div></div></span>To serch for clients</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Subcontacts"><span><div></div></span>Subcontacts</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To sign up for master classes"><span><div></div></span>To sign up for master classes</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To carry out master classes"><span><div></div></span>To carry out master classes</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To participate in competition and castings"><span><div></div></span>To participate in competitions</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To serch for models"><span><div></div></span>To serch for models</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Experience exchange"><span><div></div></span>Experience exchange</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To establish new contacts"><span><div></div></span>To establish new contacts</label>
				</div>
			</div>
		</div>
	</div>
<!-- Фотограф Конец -->
<!-- Дизайнер -->
	<div class="pageblock diz">
		<h2>Дизайнер</h2>
		<h2>Professional Details</h2>
		<span class="hr"></span>
		<div class="d-wrap">
			<div class="details-item">
				*Work type:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Fashion design"><span><div></div></span>Fashion design</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Product design"><span><div></div></span>Product design</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Costume design"><span><div></div></span>Costume design</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Interior stylin"><span><div></div></span>Interior stylin</label>
				</div>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Contacted or not:
			</div>
			<div class="details-data general-input">
				<select>
					<option>Contacted</option>
					<option>Not contacted</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Professional experience:
			</div>
			<div class="details-data general-input">
				<select>
					<option>Up to 10 years</option>
					<option>10-20 years</option>
					<option>More that 20 years</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				Workplace:
			</div>
			<div class="details-data general-input">
				<input type="text" class="f-n" placeholder="Workplace"></input>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				Education/Professional courses/Master classes:
			</div>
			<div class="details-data general-input">
				<input type="text" class="f-n" placeholder="Education/Professional courses/Master classes"></input>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				Compensation:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Payment Only"><span><div></div></span>Payment Only</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="In exchange of experience"><span><div></div></span>In exchange of experience</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Test Shoot"><span><div></div></span>Test Shoot</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Negotiable"><span><div></div></span>Negotiable</label>
				</div>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Languages:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Chinese"><span><div></div></span>Chinese</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="English"><span><div></div></span>English</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="French"><span><div></div></span>French</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="German"><span><div></div></span>German</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Italian"><span><div></div></span>Italian</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Russian"><span><div></div></span>Russian</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Spanish"><span><div></div></span>Spanish</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Ukrainian"><span><div></div></span>Ukrainian</label>
				</div>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Travel Ability:
			</div>
			<div class="details-data general-input">
				<select>
					<option>Ready to travel</option>
					<option>Work within location</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				Main reason to be on site:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="To search for work offers"><span><div></div></span>To search for work offers</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To sign new contracts"><span><div></div></span>To sign new contracts</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Experience exchange"><span><div></div></span>Experience exchange</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To promote interests/trademark/services"><span><div></div></span>To promote interests/trademark/services</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Cooperation with other fashion professionals"><span><div></div></span>Cooperation with other fashion professionals</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To sign up for master classes"><span><div></div></span>To sign up for master classes</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To carry out master classes"><span><div></div></span>To carry out master classes</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Fashion Weeks, ShowRooms"><span><div></div></span>Fashion Weeks, ShowRooms</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="An access to exclusive worldwide closed-type VIP events"><span><div></div></span>An access to exclusive worldwide closed-type VIP events</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To search for work offers"><span><div></div></span>To search for work offers</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To search for photographers"><span><div></div></span>To search for photographers</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To search for models"><span><div></div></span>To search for models</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To participate in competition and castings"><span><div></div></span>To participate in competition and castings</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To monitor the fashion news"><span><div></div></span>To monitor the fashion news</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To sell services"><span><div></div></span>To sell services</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Subcontacts"><span><div></div></span>Subcontacts</label>
				</div>
			</div>
		</div>
	</div>
<!-- Дизайнер Конец  -->
<!-- Стилист -->
	<div class="pageblock sti">
		<h2>Стилист</h2>
		<div class="d-wrap stylist">
			<div class="details-item">
				*Work type:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Hair stylist"><span><div></div></span>Hair stylist</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Make-up stylist"><span><div></div></span>Make-up stylist</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Nail stylist"><span><div></div></span>Nail stylist</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Wardrobe stylist/personal shopper"><span><div></div></span>Wardrobe stylist/personal shopper</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Clothes stylist"><span><div></div></span>Clothes stylist</label>
				</div>
			</div>
		</div>
		<div class="d-wrap stylist">
			<div class="details-item">
				*Genre work with:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Editorials"><span><div></div></span>Editorials</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Runway"><span><div></div></span>Runway</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Beauty"><span><div></div></span>Beauty</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Portraits/Celebrity"><span><div></div></span>Portraits/Celebrity</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Glamour/Nude"><span><div></div></span>Glamour/Nude</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Commercial/Advertising"><span><div></div></span>Commercial/Advertising</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Street Fashion"><span><div></div></span>Street Fashion</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Creative Projects"><span><div></div></span>Creative Projects</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Test Shoot"><span><div></div></span>Test Shoot</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="WT-Other"><span><div></div></span>Other</label>
				</div>
			</div>
		</div>
		<div class="d-wrap stylist">
			<div class="details-item">
				*Contacted or not:
			</div>
			<div class="details-data general-input">
				<select>
					<option>Contacted</option>
					<option>Not contacted</option>
				</select>
			</div>
		</div>
		<div class="d-wrap stylist">
			<div class="details-item">
				Workplace:
			</div>
			<div class="details-data general-input">
				<input type="text" class="f-n" placeholder="Workplace"></input>
			</div>
		</div>
		<div class="d-wrap stylist">
			<div class="details-item">
				Education/Professional courses/Master classes:
			</div>
			<div class="details-data general-input">
				<input type="text" class="f-n" placeholder="Education/Professional courses/Master classes"></input>
			</div>
		</div>
		<div class="d-wrap stylist">
			<div class="details-item">
				*Professional experience:
			</div>
			<div class="details-data general-input">
				<select>
					<option>Up to 10 years</option>
					<option>10-20 years</option>
					<option>More that 20 years</option>
				</select>
			</div>
		</div>
		<div class="d-wrap stylist">
			<div class="details-item">
				Compensation:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Payment Only"><span><div></div></span>Payment Only</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="In exchange of experience"><span><div></div></span>In exchange of experience</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Test Shoot"><span><div></div></span>Test Shoot</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Negotiable"><span><div></div></span>Negotiable</label>
				</div>
			</div>
		</div>
	</div>
<!-- Стилист Конец  -->
<!-- Артист -->
	<div class="pageblock art">
		<h2>Артист</h2>
		<div class="d-wrap artist">
			<div class="details-item">
				*Genre work with:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Editorials"><span><div></div></span>Editorials</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Runway"><span><div></div></span>Runway</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Beauty"><span><div></div></span>Beauty</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Portraits/Celebrity"><span><div></div></span>Portraits/Celebrity</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Glamour/Nude"><span><div></div></span>Glamour/Nude</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Commercial/Advertising"><span><div></div></span>Commercial/Advertising</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Street Fashion"><span><div></div></span>Street Fashion</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Creative Projects"><span><div></div></span>Creative Projects</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Test Shoot"><span><div></div></span>Test Shoot</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="WT-Other"><span><div></div></span>Other</label>
				</div>
			</div>
		</div>
		<div class="d-wrap artist">
			<div class="details-item">
				*Contacted or not:
			</div>
			<div class="details-data general-input">
				<select>
					<option>Contacted</option>
					<option>Not contacted</option>
				</select>
			</div>
		</div>
		<div class="d-wrap artist">
			<div class="details-item">
				Workplace:
			</div>
			<div class="details-data general-input">
				<input type="text" class="f-n" placeholder="Workplace"></input>
			</div>
		</div>
		<div class="d-wrap artist">
			<div class="details-item">
				Education/Professional courses/Master classes:
			</div>
			<div class="details-data general-input">
				<input type="text" class="f-n" placeholder="Education/Professional courses/Master classes"></input>
			</div>
		</div>
		<div class="d-wrap artist">
			<div class="details-item">
				Cosmetic brands using in work:
			</div>
			<div class="details-data general-input">
				<input type="text" class="f-n" placeholder="Cosmetic brands using in work"></input>
			</div>
		</div>
		<div class="d-wrap artist">
			<div class="details-item">
				*Professional experience:
			</div>
			<div class="details-data general-input">
				<select>
					<option>Up to 10 years</option>
					<option>10-20 years</option>
					<option>More that 20 years</option>
				</select>
			</div>
		</div>
		<div class="d-wrap artist">
			<div class="details-item">
				Compensation:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Payment Only"><span><div></div></span>Payment Only</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="In exchange of experience"><span><div></div></span>In exchange of experience</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Test Shoot"><span><div></div></span>Test Shoot</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Negotiable"><span><div></div></span>Negotiable</label>
				</div>
			</div>
		</div>
	</div>
<!-- Артист Конец -->
<!-- MUA -->
	<div class="pageblock mua">
		<h2>МУА</h2>
		<div class="d-wrap mua">
			<div class="details-item">
				*Genre work with:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Editorials"><span><div></div></span>Editorials</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Runway"><span><div></div></span>Runway</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Beauty"><span><div></div></span>Beauty</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Portraits/Celebrity"><span><div></div></span>Portraits/Celebrity</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Glamour/Nude"><span><div></div></span>Glamour/Nude</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Commercial/Advertising"><span><div></div></span>Commercial/Advertising</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Street Fashion"><span><div></div></span>Street Fashion</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Creative Projects"><span><div></div></span>Creative Projects</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Test Shoot"><span><div></div></span>Test Shoot</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="WT-Other"><span><div></div></span>Other</label>
				</div>
			</div>
		</div>
		<div class="d-wrap mua">
			<div class="details-item">
				*Contacted or not:
			</div>
			<div class="details-data general-input">
				<select>
					<option>Contacted</option>
					<option>Not contacted</option>
				</select>
			</div>
		</div>
		<div class="d-wrap mua">
			<div class="details-item">
				Workplace:
			</div>
			<div class="details-data general-input">
				<input type="text" class="f-n" placeholder="Workplace"></input>
			</div>
		</div>
		<div class="d-wrap mua">
			<div class="details-item">
				Education/Professional courses/Master classes:
			</div>
			<div class="details-data general-input">
				<input type="text" class="f-n" placeholder="Education/Professional courses/Master classes"></input>
			</div>
		</div>
		<div class="d-wrap mua">
			<div class="details-item">
				Cosmetic brands using in work:
			</div>
			<div class="details-data general-input">
				<input type="text" class="f-n" placeholder="Cosmetic brands using in work"></input>
			</div>
		</div>
		<div class="d-wrap mua">
			<div class="details-item">
				*Professional experience:
			</div>
			<div class="details-data general-input">
				<select>
					<option>Up to 10 years</option>
					<option>10-20 years</option>
					<option>More that 20 years</option>
				</select>
			</div>
		</div>
		<div class="d-wrap mua">
			<div class="details-item">
				Compensation:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Payment Only"><span><div></div></span>Payment Only</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="In exchange of experience"><span><div></div></span>In exchange of experience</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Test Shoot"><span><div></div></span>Test Shoot</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Negotiable"><span><div></div></span>Negotiable</label>
				</div>
			</div>
		</div>
	</div>
<!-- MUA Конец -->
		<div class="d-wrap">
			<div class="details-item">
				*Languages:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Chinese"><span><div></div></span>Chinese</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="English"><span><div></div></span>English</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="French"><span><div></div></span>French</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="German"><span><div></div></span>German</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Italian"><span><div></div></span>Italian</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Russian"><span><div></div></span>Russian</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Spanish"><span><div></div></span>Spanish</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Ukrainian"><span><div></div></span>Ukrainian</label>
				</div>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Travel Ability:
			</div>
			<div class="details-data general-input">
				<select>
					<option>Ready to travel</option>
					<option>Work within location</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				Main reason to be on site:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="To sign new contracts"><span><div></div></span>To sign new contracts</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Experience exchange"><span><div></div></span>Experience exchange</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To promote interests/trademark/services"><span><div></div></span>To promote interests/trademark/services</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Cooperation with other fashion professionals"><span><div></div></span>Cooperation with other fashion professionals</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To sign up for master classes"><span><div></div></span>To sign up for master classes</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To carry out master classes"><span><div></div></span>To carry out master classes</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Fashion Weeks, ShowRooms"><span><div></div></span>Fashion Weeks, ShowRooms</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="An access to exclusive worldwide closed-type VIP events"><span><div></div></span>An access to exclusive worldwide closed-type VIP events</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To search for work offers"><span><div></div></span>To search for work offers</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To search for work offers"><span><div></div></span>To search for work offers</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To search for photographers"><span><div></div></span>To search for photographers</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To search for models"><span><div></div></span>To search for models</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To participate in competition and castings"><span><div></div></span>To participate in competition and castings</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To monitor the fashion news"><span><div></div></span>To monitor the fashion news</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To sell services"><span><div></div></span>To sell services</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Subcontacts"><span><div></div></span>Subcontacts</label>
				</div>
			</div>
		</div>
<?=Yii::app()->user->getState('id_user') ?> - 
<?=Yii::app()->user->getState('package') ?> - 
<?=Yii::app()->user->getState('prof') ?>
<!-- Анкета Агентство -->
<div class="person-wrapper agency">
	<h2>Подробно заполните анкету (агентство)</h2>
		<h2>Model Requirements</h2>
		<span class="hr"></span>

		<div class="d-wrap">
			<div class="details-item">
				*Gender:
			</div>
			<div class="details-data general-input">
				<select>
					<option>Male</option>
					<option>Female</option>
					<option>Male and female</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Age:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Female"><span><div></div></span>Children</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Female"><span><div></div></span>Teens</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Female"><span><div></div></span>Adults</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Female"><span><div></div></span>Senior (40+)</label>
				</div>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Model category:
			</div>
			<div class="details-data general-input">
				<div class="mc-m">
					<label class="q-check"><input type="checkbox" name="Alternative model"><span><div></div></span>Alternative model</label>
				</div>
				<div class="mc-m">
					<label class="q-check"><input type="checkbox" name="Body Paint"><span><div></div></span>Body Paint</label>
				</div>
				<div class="mc-m">
					<label class="q-check"><input type="checkbox" name="Body Parts" id='cb21' onchange = 'showOrHide("cb21", "cat21");'/><span><div></div></span>Body Parts:</label>
				</div>
				<div class="mc-h" id="cat21">
						<label class="q-check"><input type="checkbox" name="Ears"><span><div></div></span>Ears</label>
						<label class="q-check"><input type="checkbox" name="Eyes"><span><div></div></span>Eyes</label>
						<label class="q-check"><input type="checkbox" name="Fingers"><span><div></div></span>Fingers</label>
						<label class="q-check"><input type="checkbox" name="Foot"><span><div></div></span>Foot</label>
						<label class="q-check"><input type="checkbox" name="Hair"><span><div></div></span>Hair</label>
						<label class="q-check"><input type="checkbox" name="Hands"><span><div></div></span>Hands</label>
						<label class="q-check"><input type="checkbox" name="Lips"><span><div></div></span>Lips</label>
						<label class="q-check"><input type="checkbox" name="Neck"><span><div></div></span>Neck</label>
				</div>
				<div class="mc-m">
					<label class="q-check"><input type="checkbox" name="Print photo model" id='cb22' onchange = 'showOrHide("cb22", "cat22");'/><span><div></div></span>Print photo model:</label>
				</div>
				<div class="mc-h" id="cat22">
						<label class="q-check"><input type="checkbox" name="Editorial"><span><div></div></span>Editorial</label>
						<label class="q-check"><input type="checkbox" name="Print ads"><span><div></div></span>Print ads</label>
				</div>
				<div class="mc-m">
					<label class="q-check"><input type="checkbox" name="Hight fashion model"><span><div></div></span>Hight fashion model</label>
				</div>
				<div class="mc-m">
					<label class="q-check"><input type="checkbox" name="Runway model"><span><div></div></span>Runway model</label>
				</div>
				<div class="mc-m">
					<label class="q-check"><input type="checkbox" name="Glamour model" id='cb23' onchange = 'showOrHide("cb23", "cat23");'/><span><div></div></span>Glamour model:</label>
				</div>
				<div class="mc-h" id="cat23">
						<label class="q-check"><input type="checkbox" name="Lingery/Body model"><span><div></div></span>Lingery/Body model</label>
						<label class="q-check"><input type="checkbox" name="Swimear model"><span><div></div></span>Swimear model</label>
				</div>
				<div class="mc-m">
					<label class="q-check"><input type="checkbox" name="Fitness model"><span><div></div></span>Fitness model</label>
				</div>
				<div class="mc-m">
					<label class="q-check"><input type="checkbox" name="Beauty model"><span><div></div></span>Beauty model (hair/makeup)</label>
				</div>
				<div class="mc-m">
					<label class="q-check"><input type="checkbox" name="Nude"><span><div></div></span>Nude</label>
				</div>
				<div class="mc-m">
					<label class="q-check"><input type="checkbox" name="Promotional model"><span><div></div></span>Promotional model</label>
				</div>
				<div class="mc-m">
					<label class="q-check"><input type="checkbox" name="Plus-sized model"><span><div></div></span>Plus-sized model</label>
				</div>
				<div class="mc-m">
					<label class="q-check"><input type="checkbox" name="Street Fashion"><span><div></div></span>Street Fashion</label>
				</div>
				<div class="mc-m">
					<label class="q-check"><input type="checkbox" name="Sport model"><span><div></div></span>Sport model</label>
				</div>
				<div class="mc-m">
					<label class="q-check"><input type="checkbox" name="Senior model"><span><div></div></span>Senior model (40+)</label>
				</div>
				<div class="mc-m">
					<label class="q-check"><input type="checkbox" name="Video"><span><div></div></span>Video</label>
				</div>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				Other requirements:
			</div>
			<div class="details-data general-input">
				<textarea placeholder="Other requirements"></textarea>
			</div>
		</div>

		<h2>Professional Details</h2>
		<span class="hr"></span>
		<div class="d-wrap">
			<div class="details-item">
				*Professional experience:
			</div>
			<div class="details-data general-input">
				<select>
					<option>Up to 10 years</option>
					<option>10-20 years</option>
					<option>More that 20 years</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				Total models:
			</div>
			<div class="details-data general-input">
				<input type="number" name="tot-models">
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Type/Specialization:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Agency"><span><div></div></span>Agency</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Publisher"><span><div></div></span>Publisher</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Model Site"><span><div></div></span>Model Site</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Model Scout"><span><div></div></span>Model Scout</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Event Manager"><span><div></div></span>Event Manager/Staffing company</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Casting Director"><span><div></div></span>Casting Director</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Advertiser"><span><div></div></span>Advertiser</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Manager"><span><div></div></span>Manager</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Stilist"><span><div></div></span>Stilist+MUA</label>
				</div>
				<div>
					Other:<input type="text" name="Spec-Other" placeholder="Other">
				</div>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				Mine reason to be on site:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="World visibility"><span><div></div></span>World visibility</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Model promotion"><span><div></div></span>Model promotion</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="PR&Representation"><span><div></div></span>PR&Representation</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To carry out master classes"><span><div></div></span>To carry out master classes</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Subcontracts"><span><div></div></span>Subcontracts</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To establish new contacts"><span><div></div></span>To establish new contacts</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To serch for models"><span><div></div></span>To serch for models</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To promote services"><span><div></div></span>To promote services</label>
				</div>
			</div>
		</div>
</div>
<!-- Анкета Агентство Конец -->
<!-- Brand -->
<div class="person-wrapper brand">
	<h2>Подробно заполните анкету (бренд)</h2>
		<h2>Professional Details</h2>
		<span class="hr"></span>
		<div class="d-wrap">
			<div class="details-item">
				*Professional experience:
			</div>
			<div class="details-data general-input">
				<select>
					<option>Up to 5 years</option>
					<option>5-10 years</option>
					<option>More that 10 years</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Product range:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Clothes"><span><div></div></span>Clothes</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Underwear"><span><div></div></span>Underwear</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Swimwear"><span><div></div></span>Swimwear</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Shoes"><span><div></div></span>Shoes</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Accessories"><span><div></div></span>Accessories</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Handbags"><span><div></div></span>Handbags</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Jewelry"><span><div></div></span>Jewelry</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Bijouterie"><span><div></div></span>Bijouterie</label>
				</div>
				<div>
					Other:<input type="text" name="Spec-Other" placeholder="Other">
				</div>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				Trading:
			</div>
			<div class="details-data general-input">
				<select class="ge">
					<option>Domestic market</option>
					<option>International market</option>
					<option>Domestic and international market</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				Main reason to be on site:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="To search for new clients, partners"><span><div></div></span>To search for new clients, partners</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To sign new contracts"><span><div></div></span>To sign new contracts</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Experience exchange"><span><div></div></span>Experience exchange</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To promote interests/trademark/services"><span><div></div></span>To promote interests/trademark/services</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Cooperation with other fashion professionals"><span><div></div></span>Cooperation with other fashion professionals</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To sign up for master classes"><span><div></div></span>To sign up for master classes</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To carry out master classes"><span><div></div></span>To carry out master classes</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Fashion Weeks, ShowRooms"><span><div></div></span>Fashion Weeks, ShowRooms</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="An access to exclusive worldwide closed-type VIP events"><span><div></div></span>An access to exclusive worldwide closed-type VIP events</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To search for work offers"><span><div></div></span>To search for work offers</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To search for work offers"><span><div></div></span>To search for work offers</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To search for photographers"><span><div></div></span>To search for photographers</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To search for models"><span><div></div></span>To search for models</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To participate in competition and castings"><span><div></div></span>To participate in competition and castings</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To monitor the fashion news"><span><div></div></span>To monitor the fashion news</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To sell services"><span><div></div></span>To sell services</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Subcontacts"><span><div></div></span>Subcontacts</label>
				</div>
			</div>
		</div>
</div>
<!-- Brand Конец -->
<!-- Buyer -->
<div class="person-wrapper buyer">
	<h2>Подробно заполните анкету (buyer)</h2>
		<h2>Professional Details</h2>
		<span class="hr"></span>
		<div class="d-wrap">
			<div class="details-item">
				*Clients work for:
			</div>
			<div class="details-data general-input">
				<input type="text" class="w-s" placeholder="Clients work for *:"></input>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Clients location:
			</div>
			<div class="details-data general-input">
				<div class="loc loc-m">
					<label class="q-check"><input type="checkbox" name="China" id = 'cb15' onchange = 'showOrHide("cb15", "cat15");'/><span><div></div></span>China:</label>
					<div class="loc loc-ch" id="cat15">
						<label class="q-check"><input type="checkbox" name="Beijing"><span><div></div></span>Beijing</label>
						<label class="q-check"><input type="checkbox" name="Hong Kong"><span><div></div></span>Hong Kong</label>
						<label class="q-check"><input type="checkbox" name="Shanghai"><span><div></div></span>Shanghai</label>
						<label class="q-check"><input type="checkbox" name="Ch-Other"><span><div></div></span>Other</label>
					</div>
				</div>
				<div class="loc loc-m">
					<label class="q-check"><input type="checkbox" name="England" id = 'cb16' onchange = 'showOrHide("cb16", "cat16");'/><span><div></div></span>England:</label>
					<div class="loc loc-en" id="cat16">
						<label class="q-check"><input type="checkbox" name="London"><span><div></div></span>London</label>
						<label class="q-check"><input type="checkbox" name="London area"><span><div></div></span>London area</label>
						<label class="q-check"><input type="checkbox" name="En-Other"><span><div></div></span>Other</label>
					</div>
				</div>
				<div class="loc loc-m">
						<label class="q-check"><input type="checkbox" name="France" id = 'cb17' onchange = 'showOrHide("cb17", "cat17");'/><span><div></div></span>France:</label>
					<div class="loc loc-fr" id="cat17">
						<label class="q-check"><input type="checkbox" name="Paris"><span><div></div></span>Paris</label>
						<label class="q-check"><input type="checkbox" name="Paris area"><span><div></div></span>Paris area</label>
						<label class="q-check"><input type="checkbox" name="Fr-Other"><span><div></div></span>Other</label>
					</div>
				</div>
				<div class="loc loc-m">
						<label class="q-check"><input type="checkbox" name="Germany" id = 'cb18' onchange = 'showOrHide("cb18", "cat18");'/><span><div></div></span>Germany:</label>
					<div class="loc loc-ge" id="cat18">
						<label class="q-check"><input type="checkbox" name="Berlin"><span><div></div></span>Berlin</label>
						<label class="q-check"><input type="checkbox" name="Berlin area"><span><div></div></span>Berlin area</label>
						<label class="q-check"><input type="checkbox" name="Ge-Other"><span><div></div></span>Other</label>
					</div>
				</div>
				<div class="loc loc-m">
						<label class="q-check"><input type="checkbox" name="Italy" id='cb19' onchange = 'showOrHide("cb19", "cat19");'/><span><div></div></span>Italy:</label>
					<div class="loc loc-it" id="cat19">
						<label class="q-check"><input type="checkbox" name="Milan"><span><div></div></span>Milan</label>
						<label class="q-check"><input type="checkbox" name="Milan area"><span><div></div></span>Milan area</label>
						<label class="q-check"><input type="checkbox" name="Rome"><span><div></div></span>Rome</label>
						<label class="q-check"><input type="checkbox" name="Rome area"><span><div></div></span>Rome area</label>
						<label class="q-check"><input type="checkbox" name="It-Other"><span><div></div></span>Other</label>
					</div>
				</div>
				<div class="loc loc-m">
						<label class="q-check"><input type="checkbox" name="USA" id = 'cb20' onchange = 'showOrHide("cb20", "cat20");'/><span><div></div></span>USA:</label>
					<div class="loc loc-usa" id="cat20">
						<label class="q-check"><input type="checkbox" name="Los Angeles"><span><div></div></span>Los Angeles</label>
						<label class="q-check"><input type="checkbox" name="Los Angeles area"><span><div></div></span>Los Angeles area</label>
						<label class="q-check"><input type="checkbox" name="New York"><span><div></div></span>New York</label>
						<label class="q-check"><input type="checkbox" name="New York area"><span><div></div></span>New York area</label>
						<label class="q-check"><input type="checkbox" name="USA-Other"><span><div></div></span>Other</label>
					</div>
				</div>
				<div class="loc loc-m">
						<label class="q-check"><input type="checkbox" name="Ukraine"><span><div></div></span>Ukraine</label>
				</div>
				<div class="loc loc-m">
						<label class="q-check"><input type="checkbox" name="Russia"><span><div></div></span>Russia</label>
				</div>
				<div class="loc loc-m">
						<label class="q-check"><input type="checkbox" name="Other"><span><div></div></span>Other</label>
				</div>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				Brands work with:
			</div>
			<div class="details-data general-input">
				<input type="text" class="w-s" placeholder="Brands work with:"></input>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Product range:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Clothes"><span><div></div></span>Clothes</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Underwear"><span><div></div></span>Underwear</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Swimwear"><span><div></div></span>Swimwear</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Shoes"><span><div></div></span>Shoes</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Accessories"><span><div></div></span>Accessories</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Handbags"><span><div></div></span>Handbags</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Jewelry"><span><div></div></span>Jewelry</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Bijouterie"><span><div></div></span>Bijouterie</label>
				</div>
				<div>
					Other:<input type="text" name="Spec-Other" placeholder="Other">
				</div>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				Trading:
			</div>
			<div class="details-data general-input">
				<select class="ge">
					<option>Domestic market</option>
					<option>International market</option>
					<option>Domestic and international market</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Professional experience:
			</div>
			<div class="details-data general-input">
				<select>
					<option>Up to 5 years</option>
					<option>5-10 years</option>
					<option>More that 10 years</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Languages:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Chinese"><span><div></div></span>Chinese</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="English"><span><div></div></span>English</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="French"><span><div></div></span>French</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="German"><span><div></div></span>German</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Italian"><span><div></div></span>Italian</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Russian"><span><div></div></span>Russian</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Spanish"><span><div></div></span>Spanish</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Ukrainian"><span><div></div></span>Ukrainian</label>
				</div>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Travel Ability:
			</div>
			<div class="details-data general-input">
				<select>
					<option>Ready to travel</option>
					<option>Work within location</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				Main reason to be on site:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="To search for new clients, partners"><span><div></div></span>To search for new clients, partners</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To sign new contracts"><span><div></div></span>To sign new contracts</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Experience exchange"><span><div></div></span>Experience exchange</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To promote interests/trademark/services"><span><div></div></span>To promote interests/trademark/services</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Cooperation with other fashion professionals"><span><div></div></span>Cooperation with other fashion professionals</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To sign up for master classes"><span><div></div></span>To sign up for master classes</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To carry out master classes"><span><div></div></span>To carry out master classes</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Fashion Weeks, ShowRooms"><span><div></div></span>Fashion Weeks, ShowRooms</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="An access to exclusive worldwide closed-type VIP events"><span><div></div></span>An access to exclusive worldwide closed-type VIP events</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To search for work offers"><span><div></div></span>To search for work offers</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To search for work offers"><span><div></div></span>To search for work offers</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To search for photographers"><span><div></div></span>To search for photographers</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To search for models"><span><div></div></span>To search for models</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To participate in competition and castings"><span><div></div></span>To participate in competition and castings</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To monitor the fashion news"><span><div></div></span>To monitor the fashion news</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To sell services"><span><div></div></span>To sell services</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Subcontacts"><span><div></div></span>Subcontacts</label>
				</div>
			</div>
		</div>
</div>
<!-- Buyer Конец -->
<!-- Customer -->
<!-- Зполняется в общей General -->
<!-- Customer Конец -->
<!-- Company Representative -->
<div class="person-wrapper repres">
	<h2>Подробно заполните анкету (company representative)</h2>
		<h2>Professional Details</h2>
		<span class="hr"></span>
		<div class="d-wrap">
			<div class="details-item">
				*Position:
			</div>
			<div class="details-data general-input">
				<select>
					<option>Manager</option>
					<option>Director</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				Scope of activity:
			</div>
			<div class="details-data general-input">
				<input type="text" class="w-s" placeholder="Scope of activity:"></input>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Workplace:
			</div>
			<div class="details-data general-input">
				<input type="text" class="w-s" placeholder="Workplace *:"></input>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Professional experience:
			</div>
			<div class="details-data general-input">
				<select>
					<option>Up to 5 years</option>
					<option>5-10 years</option>
					<option>More that 10 years</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Languages:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Chinese"><span><div></div></span>Chinese</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="English"><span><div></div></span>English</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="French"><span><div></div></span>French</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="German"><span><div></div></span>German</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Italian"><span><div></div></span>Italian</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Russian"><span><div></div></span>Russian</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Spanish"><span><div></div></span>Spanish</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Ukrainian"><span><div></div></span>Ukrainian</label>
				</div>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Travel Ability:
			</div>
			<div class="details-data general-input">
				<select>
					<option>Ready to travel</option>
					<option>Work within location</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				Main reason to be on site:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="To search for new clients, partners"><span><div></div></span>To search for new clients, partners</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To sign new contracts"><span><div></div></span>To sign new contracts</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Experience exchange"><span><div></div></span>Experience exchange</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To promote interests/trademark/services"><span><div></div></span>To promote interests/trademark/services</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Cooperation with other fashion professionals"><span><div></div></span>Cooperation with other fashion professionals</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To sign up for master classes"><span><div></div></span>To sign up for master classes</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To carry out master classes"><span><div></div></span>To carry out master classes</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Fashion Weeks, ShowRooms"><span><div></div></span>Fashion Weeks, ShowRooms</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="An access to exclusive worldwide closed-type VIP events"><span><div></div></span>An access to exclusive worldwide closed-type VIP events</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To search for work offers"><span><div></div></span>To search for work offers</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To search for work offers"><span><div></div></span>To search for work offers</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To search for photographers"><span><div></div></span>To search for photographers</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To search for models"><span><div></div></span>To search for models</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To participate in competition and castings"><span><div></div></span>To participate in competition and castings</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To monitor the fashion news"><span><div></div></span>To monitor the fashion news</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To sell services"><span><div></div></span>To sell services</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Subcontacts"><span><div></div></span>Subcontacts</label>
				</div>
			</div>
		</div>
</div>
<!-- Media -->
<div class="person-wrapper media">
	<h2>Подробно заполните анкету (media)</h2>
		<h2>Professional Details</h2>
		<span class="hr"></span>
		<div class="d-wrap">
			<div class="details-item">
				*Name of magazine/blog/online journal:
			</div>
			<div class="details-data general-input">
				<input type="text" class="w-s" placeholder="Name of magazine/blog/online journal *:"></input>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Position:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Chief Editor "><span><div></div></span>Chief Editor</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Editor"><span><div></div></span>Editor</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Blogger"><span><div></div></span>Blogger</label>
				</div>
				<div>
					Other:<input type="text" class="w-s" placeholder="Name of magazine/blog/online journal *:"></input>
				</div>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Professional experience:
			</div>
			<div class="details-data general-input">
				<select>
					<option>Up to 5 years</option>
					<option>5-10 years</option>
					<option>More that 10 years</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Languages:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="Chinese"><span><div></div></span>Chinese</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="English"><span><div></div></span>English</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="French"><span><div></div></span>French</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="German"><span><div></div></span>German</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Italian"><span><div></div></span>Italian</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Russian"><span><div></div></span>Russian</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Spanish"><span><div></div></span>Spanish</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Ukrainian"><span><div></div></span>Ukrainian</label>
				</div>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				Travel Ability:
			</div>
			<div class="details-data general-input">
				<select>
					<option>Ready to travel</option>
					<option>Work within location</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				Main reason to be on site:
			</div>
			<div class="details-data general-input">
				<div>
					<label class="q-check"><input type="checkbox" name="To search for new clients, partners"><span><div></div></span>To search for new clients, partners</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To sign new contracts"><span><div></div></span>To sign new contracts</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Experience exchange"><span><div></div></span>Experience exchange</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To promote interests/trademark/services"><span><div></div></span>To promote interests/trademark/services</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Cooperation with other fashion professionals"><span><div></div></span>Cooperation with other fashion professionals</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To sign up for master classes"><span><div></div></span>To sign up for master classes</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To carry out master classes"><span><div></div></span>To carry out master classes</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Fashion Weeks, ShowRooms"><span><div></div></span>Fashion Weeks, ShowRooms</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="An access to exclusive worldwide closed-type VIP events"><span><div></div></span>An access to exclusive worldwide closed-type VIP events</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To search for work offers"><span><div></div></span>To search for work offers</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To search for work offers"><span><div></div></span>To search for work offers</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To search for photographers"><span><div></div></span>To search for photographers</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To search for models"><span><div></div></span>To search for models</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To participate in competition and castings"><span><div></div></span>To participate in competition and castings</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To monitor the fashion news"><span><div></div></span>To monitor the fashion news</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="To sell services"><span><div></div></span>To sell services</label>
				</div>
				<div>
					<label class="q-check"><input type="checkbox" name="Subcontacts"><span><div></div></span>Subcontacts</label>
				</div>
			</div>
		</div>
</div>
<!-- Media Конец -->
</form>


Pay Form
<form class="pay-form" target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">

<!-- Вывод суммы -->
<div class="pay-sum">
	<h2>К оплате</h2>
	<span class="hr"></span>
	<div class="sum-item">
		<!-- Профессия 1 полная цена -->
		<h3>Дизайнер</h3>
		<div class="sum-price">
			25$
		</div>
	</div>
	<div class="plus">
		+
	</div>
	<div class="sum-item">
		<!-- Профессия 2 скидка 20% -->
		<h3>Стилист</h3>
		<div class="sum-price">
			20$
		</div>
	</div>
	<div class="plus">
		+
	</div>
	<div class="sum-item">
		<!-- Профессия 3 скидка 40% -->
		<h3>Представитель компании</h3>
		<div class="sum-price">
			15$
		</div>
	</div>
</div>


<!-- Вывод суммы Конец -->

<input type="hidden" name="cmd" value="_s-xclick">

<input type="hidden" name="hosted_button_id" value="2QPMN4EWCXFNN">

<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_cart_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">

<img alt="" border="0" src="https://www.paypalobjects.com/ru_RU/i/scr/pixel.gif" width="1" height="1">

</form>





<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">

<input type="hidden" name="cmd" value="_s-xclick">

<input type="hidden" name="encrypted" value="-----BEGIN PKCS7-----MIIIGQYJKoZIhvcNAQcEoIIICjCCCAYCAQExggEwMIIBLAIBADCBlDCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20CAQAwDQYJKoZIhvcNAQEBBQAEgYCV2+Uv/9PIsa7mMzLVZeWB/2gDndWBP+oPtkQJwzViiTRWVl7ont3ziudxrwn4K24PqQYtTiM8G5ESi8JnK9Acz5WIrUxzF6Zu4WncUzL8Ux944faGWIpVsiTEe08YdzYXyVux1yuMfKr+oXw8VsRAqtjWQqpHKciGud+lrlwNJzELMAkGBSsOAwIaBQAwggGVBgkqhkiG9w0BBwEwFAYIKoZIhvcNAwcECLIs3P6n6FCsgIIBcEKza+ANWhs4vA+tO73m2OXmpDRbzKnuVkq4eU85AnMdkRg/TRVJ3huDqs6wbcqtxrjZNFmnFhKCFANVvTIWobsBM2glmuT0ZpIOY2dqWgvtw09dEsL8N0A0ik6KAVwkqaq1SLRUTrHXsoyEC8pqgIPXBM9O02wAiRZjg7y8DisFRBEuHYaqaYIUC/Hs23KQW+Hu+PgCEqAf28wdddUna6Y9IS7VQ0u6rsgKckJuEh9qhrMFlE6aRBeflOqTKShtop9HM4tQMTFBC6VtjxxJAlFlYnjjBvlp/yCBU4Nf9Cyqxc09zHPEK/yYNAfEYV7ogjKha8RaMKFB4OpCAOc0uypLRtgfYGdEqssi2iIOWXMpA4cZVpZecZuB27qcLkSbeG5nLamxeLCoq9XpX7uyG+DA+pIrR3BXEwNiqA7rrY+kog0F+KVFe6Dxh3Vzj11bUHHtceNbCVIPdj9VCLKULQTZH7R+XxtqvMzPD24DviPOoIIDhzCCA4MwggLsoAMCAQICAQAwDQYJKoZIhvcNAQEFBQAwgY4xCzAJBgNVBAYTAlVTMQswCQYDVQQIEwJDQTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEUMBIGA1UEChMLUGF5UGFsIEluYy4xEzARBgNVBAsUCmxpdmVfY2VydHMxETAPBgNVBAMUCGxpdmVfYXBpMRwwGgYJKoZIhvcNAQkBFg1yZUBwYXlwYWwuY29tMB4XDTA0MDIxMzEwMTMxNVoXDTM1MDIxMzEwMTMxNVowgY4xCzAJBgNVBAYTAlVTMQswCQYDVQQIEwJDQTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEUMBIGA1UEChMLUGF5UGFsIEluYy4xEzARBgNVBAsUCmxpdmVfY2VydHMxETAPBgNVBAMUCGxpdmVfYXBpMRwwGgYJKoZIhvcNAQkBFg1yZUBwYXlwYWwuY29tMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDBR07d/ETMS1ycjtkpkvjXZe9k+6CieLuLsPumsJ7QC1odNz3sJiCbs2wC0nLE0uLGaEtXynIgRqIddYCHx88pb5HTXv4SZeuv0Rqq4+axW9PLAAATU8w04qqjaSXgbGLP3NmohqM6bV9kZZwZLR/klDaQGo1u9uDb9lr4Yn+rBQIDAQABo4HuMIHrMB0GA1UdDgQWBBSWn3y7xm8XvVk/UtcKG+wQ1mSUazCBuwYDVR0jBIGzMIGwgBSWn3y7xm8XvVk/UtcKG+wQ1mSUa6GBlKSBkTCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb22CAQAwDAYDVR0TBAUwAwEB/zANBgkqhkiG9w0BAQUFAAOBgQCBXzpWmoBa5e9fo6ujionW1hUhPkOBakTr3YCDjbYfvJEiv/2P+IobhOGJr85+XHhN0v4gUkEDI8r2/rNk1m0GA8HKddvTjyGw/XqXa+LSTlDYkqI8OwR8GEYj4efEtcRpRYBxV8KxAW93YDWzFGvruKnnLbDAF6VR5w/cCMn5hzGCAZowggGWAgEBMIGUMIGOMQswCQYDVQQGEwJVUzELMAkGA1UECBMCQ0ExFjAUBgNVBAcTDU1vdW50YWluIFZpZXcxFDASBgNVBAoTC1BheVBhbCBJbmMuMRMwEQYDVQQLFApsaXZlX2NlcnRzMREwDwYDVQQDFAhsaXZlX2FwaTEcMBoGCSqGSIb3DQEJARYNcmVAcGF5cGFsLmNvbQIBADAJBgUrDgMCGgUAoF0wGAYJKoZIhvcNAQkDMQsGCSqGSIb3DQEHATAcBgkqhkiG9w0BCQUxDxcNMTYwNTAyMTcwMTQyWjAjBgkqhkiG9w0BCQQxFgQUzOhWoTCCaMGhhgcPP4+NUxn538gwDQYJKoZIhvcNAQEBBQAEgYAuCjdrSvOTJU0CFr7SVk6YgTaG04gIIO1r4GxVy/pktbM1xhh6K90EmtFhLJ+Xlv9LRoSDl4tD84KKAlifkpy6akUVSDBz/8KXrePJYmY0RVtSzk2L47oxapB1ilJjh66PTcNCmhwd7Xjw1kB6qFu2DR9iAVqyti0TiDjwHr6nSQ==-----END PKCS7-----

">

<input type="image" src="http://fashingreatness.ru/img/PayPal.png" border="0" name="submit" alt="PayPal — более безопасный и легкий способ оплаты через Интернет!">

<img alt="" border="0" src="https://www.paypalobjects.com/ru_RU/i/scr/pixel.gif" width="1" height="1">

</form>

<script>
$(document).ready(function() {
	<? /* select страны */ ?>
	$('#qes_county').change( function() {
		aaa = $(this).val();
		bbb = aaa.substr(0,4);
		if (bbb != "loc-") {
			$("#qes_county option:selected").css('color','red')
			return; 
		};
		
		$('.loc').css('display', 'none');
		$('.loc-m').css('display', '');
		$('.'+aaa).css('display', 'block');
		//$(this).css('color','red')
		//$("#qes_county option[value='" +$.cookie('sel_3')+ "']").attr("selected", "selected");
		//$("#qes_county option[selected]").css('color','red');
		$("#qes_county option[value='"+aaa+"']").css('color','red')
		//alert(aaa +"--"+bbb);
	});
});
</script>