<?php
class Site extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    public function tableName() {
        return 'site';
    }
	// id view Имя view cod Код поля language Код языка name
    public function getAll($lan, $view) {
		$criteria = new CDbCriteria;
		$criteria->condition = "( (view = '{$view}') OR (view = 'all') ) AND language = '{$lan}' ";
		$criteria->order = "npp, view , cod";
		$list = self::model()->findAll($criteria);
		$arr = Array();
		foreach ($list as $lab) {
			$tbl = explode ( "#", $lab['cod']);
			if (count($tbl)>1) {
				if ( !isset($arr[$tbl[0]]) ) {$arr[$tbl[0]] = "";};
				$arr[$tbl[0]] .= "<option value='{$lab['cod']}'>{$lab['name']}</option>";
			} else {
				$arr[$lab['cod']] = $lab['name'];
			};
		};
		return $arr;
    }
	public function getAllPole($lan, $view) {
		$criteria = new CDbCriteria;
		$criteria->condition = "( (view = '{$view}') OR (view = 'all') ) AND language = '{$lan}' ";
		$criteria->order = "npp, view , cod";
		$list = self::model()->findAll($criteria);
		$arr = Array();
		foreach ($list as $lab) {
			$tbl = explode ( "#", $lab['cod']);
			if (count($tbl)>1) {
				if ( !isset($arr[$tbl[0]]) ) {$arr[$tbl[0]] = "";};
				$arr[$tbl[0]] .= "<option value='{$lab['cod']}'>{$lab['name']}</option>";
			} else {
				$arr[$lab['cod']] = $lab['name'];
			};
		};
		return $arr;
    }
	public function getAllTab($lan) {
		$criteria = new CDbCriteria;
		$criteria->condition = " view = 'select' AND language = '{$lan}' ";
		$criteria->order = "npp, view , cod";
		$list = self::model()->findAll($criteria);
		$arr = Array();
		foreach ($list as $lab) {
			//$arr[$lab['cod']]['0'] = $lab['name'];
			//if ($lab['cod_up']] != "") {
				$arr[$lab['cod']] = array("name"=>$lab['name'], "up"=>$lab['cod_up'], "other"=>$lab['other']);
			//};
		};
		return $arr;
	}
}
?>