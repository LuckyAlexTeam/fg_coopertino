<div id="top-menu">
    <ul class="top_menu">
        {foreach from=$menuCategTree.children item=menuCategory}
            <li><a href="{$menuCategory.link}">{$menuCategory.name}</a></li>
        {/foreach}
    </ul>
</div>
