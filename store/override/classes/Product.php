<?php
class Product extends ProductCore {
    public static function getProductProperties($id_lang, $row, Context $context = null)
    {

        if (!$row['id_product']) {
            return false;
        }

        if ($context == null) {
            $context = Context::getContext();
        }

        $id_product_attribute = $row['id_product_attribute'] = (!empty($row['id_product_attribute']) ? (int)$row['id_product_attribute'] : null);

        // Product::getDefaultAttribute is only called if id_product_attribute is missing from the SQL query at the origin of it:
        // consider adding it in order to avoid unnecessary queries
        $row['allow_oosp'] = Product::isAvailableWhenOutOfStock($row['out_of_stock']);
        if (Combination::isFeatureActive() && $id_product_attribute === null
            && ((isset($row['cache_default_attribute']) && ($ipa_default = $row['cache_default_attribute']) !== null)
                || ($ipa_default = Product::getDefaultAttribute($row['id_product'], !$row['allow_oosp'])))) {
            $id_product_attribute = $row['id_product_attribute'] = $ipa_default;
        }
        if (!Combination::isFeatureActive() || !isset($row['id_product_attribute'])) {
            $id_product_attribute = $row['id_product_attribute'] = 0;
        }

        // Tax
        $usetax = Tax::excludeTaxeOption();

        $cache_key = $row['id_product'].'-'.$id_product_attribute.'-'.$id_lang.'-'.(int)$usetax;
        if (isset($row['id_product_pack'])) {
            $cache_key .= '-pack'.$row['id_product_pack'];
        }

        if (isset(self::$producPropertiesCache[$cache_key])) {
            return array_merge($row, self::$producPropertiesCache[$cache_key]);
        }

        // Datas
        $row['category'] = Category::getLinkRewrite((int)$row['id_category_default'], (int)$id_lang);
        $row['link'] = $context->link->getProductLink((int)$row['id_product'], $row['link_rewrite'], $row['category'], $row['ean13']);

        $row['attribute_price'] = 0;
        if ($id_product_attribute) {
            $row['attribute_price'] = (float)Product::getProductAttributePrice($id_product_attribute);
        }

        $row['price_tax_exc'] = Product::getPriceStatic(
            (int)$row['id_product'],
            false,
            $id_product_attribute,
            (self::$_taxCalculationMethod == PS_TAX_EXC ? 2 : 6)
        );

        if (self::$_taxCalculationMethod == PS_TAX_EXC) {
            $row['price_tax_exc'] = Tools::ps_round($row['price_tax_exc'], 2);
            $row['price'] = Product::getPriceStatic(
                (int)$row['id_product'],
                true,
                $id_product_attribute,
                6
            );
            $row['price_without_reduction'] = Product::getPriceStatic(
                (int)$row['id_product'],
                false,
                $id_product_attribute,
                2,
                null,
                false,
                false
            );
        } else {
            $row['price'] = Tools::ps_round(
                Product::getPriceStatic(
                    (int)$row['id_product'],
                    true,
                    $id_product_attribute,
                    6
                ),
                (int)Configuration::get('PS_PRICE_DISPLAY_PRECISION')
            );
            $row['price_without_reduction'] = Product::getPriceStatic(
                (int)$row['id_product'],
                true,
                $id_product_attribute,
                6,
                null,
                false,
                false
            );
        }

        $row['reduction'] = Product::getPriceStatic(
            (int)$row['id_product'],
            (bool)$usetax,
            $id_product_attribute,
            6,
            null,
            true,
            true,
            1,
            true,
            null,
            null,
            null,
            $specific_prices
        );

        $row['specific_prices'] = $specific_prices;

        $row['quantity'] = Product::getQuantity(
            (int)$row['id_product'],
            0,
            isset($row['cache_is_pack']) ? $row['cache_is_pack'] : null
        );

        $row['quantity_all_versions'] = $row['quantity'];

        if ($row['id_product_attribute']) {
            $row['quantity'] = Product::getQuantity(
                (int)$row['id_product'],
                $id_product_attribute,
                isset($row['cache_is_pack']) ? $row['cache_is_pack'] : null
            );
        }

        $row['id_image'] = ProductCore::defineProductImage($row, $id_lang);

        $id_image2 = Db::getInstance()->getValue('SELECT id_image FROM `'._DB_PREFIX_.'image` WHERE id_product='.$row['id_product'].' AND cover IS NULL');
        if ($id_image2)
            $row['id_image2'] = $row['id_product'].'-'.$id_image2;

        $row['features'] = Product::getFrontFeaturesStatic((int)$id_lang, $row['id_product']);

        $row['attachments'] = array();
        if (!isset($row['cache_has_attachments']) || $row['cache_has_attachments']) {
            $row['attachments'] = Product::getAttachmentsStatic((int)$id_lang, $row['id_product']);
        }

        $row['virtual'] = ((!isset($row['is_virtual']) || $row['is_virtual']) ? 1 : 0);

        // Pack management
        $row['pack'] = (!isset($row['cache_is_pack']) ? Pack::isPack($row['id_product']) : (int)$row['cache_is_pack']);
        $row['packItems'] = $row['pack'] ? Pack::getItemTable($row['id_product'], $id_lang) : array();
        $row['nopackprice'] = $row['pack'] ? Pack::noPackPrice($row['id_product']) : 0;
        if ($row['pack'] && !Pack::isInStock($row['id_product'])) {
            $row['quantity'] = 0;
        }

        $row['customization_required'] = false;
        if (isset($row['customizable']) && $row['customizable'] && Customization::isFeatureActive()) {
            if (count(Product::getRequiredCustomizableFieldsStatic((int)$row['id_product']))) {
                $row['customization_required'] = true;
            }
        }

        $row['sizes_string'] = null;
        $sizes_string = '';
        $attributes_info = Product::getAttributesInformationsByProduct((int)$row['id_product'], true);
//        d($attributes_info);
        foreach ( $attributes_info as $a_info ) {
            if($a_info['group'] == 'Size') {
                $sizes_string .= $a_info['attribute'] . ', ';
            }
        }
        $row['sizes_string'] = substr($sizes_string, 0, -2 );

//        d($row);

        $row = Product::getTaxesInformations($row, $context);
        self::$producPropertiesCache[$cache_key] = $row;
        return self::$producPropertiesCache[$cache_key];
    }

    /**
     * @todo Remove existing module condition
     * @param int $id_product
     */
    public static function getAttributesInformationsByProduct($id_product, $for_sizes = false)
    {
        if($for_sizes) {
            $result = Db::getInstance()->executeS('
                    SELECT DISTINCT a.`id_attribute`, a.`id_attribute_group`, al.`name` as `attribute`, agl.`name`, agl.`public_name` as `group`
                    FROM `'._DB_PREFIX_.'attribute` a
                    LEFT JOIN `'._DB_PREFIX_.'attribute_lang` al
                        ON (a.`id_attribute` = al.`id_attribute` AND al.`id_lang` = '.(int)Context::getContext()->language->id.')
                    LEFT JOIN `'._DB_PREFIX_.'attribute_group_lang` agl
                        ON (a.`id_attribute_group` = agl.`id_attribute_group` AND agl.`id_lang` = '.(int)Context::getContext()->language->id.')
                    LEFT JOIN `'._DB_PREFIX_.'product_attribute_combination` pac
                        ON (a.`id_attribute` = pac.`id_attribute`)
                    LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa
                        ON (pac.`id_product_attribute` = pa.`id_product_attribute`)
                    LEFT JOIN `'._DB_PREFIX_.'stock_available` sa
                        ON (pac.`id_product_attribute` = sa.`id_product_attribute`)
                    '.Shop::addSqlAssociation('product_attribute', 'pa').'
                    '.Shop::addSqlAssociation('attribute', 'pac').'
                    WHERE pa.`id_product` = '.(int)$id_product.' AND sa.`quantity`>0');
        } else {
            // if blocklayered module is installed we check if user has set custom attribute name
            if (Module::isInstalled('blocklayered') && Module::isEnabled('blocklayered'))
            {
                $nb_custom_values = Db::getInstance()->executeS('
                SELECT DISTINCT la.`id_attribute`, la.`url_name` as `attribute`
                FROM `'._DB_PREFIX_.'attribute` a
                LEFT JOIN `'._DB_PREFIX_.'product_attribute_combination` pac
                    ON (a.`id_attribute` = pac.`id_attribute`)
                LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa
                    ON (pac.`id_product_attribute` = pa.`id_product_attribute`)
                '.Shop::addSqlAssociation('product_attribute', 'pa').'
                LEFT JOIN `'._DB_PREFIX_.'layered_indexable_attribute_lang_value` la
                    ON (la.`id_attribute` = a.`id_attribute` AND la.`id_lang` = '.(int)Context::getContext()->language->id.')
                WHERE la.`url_name` IS NOT NULL AND la.`url_name` != \'\'
                AND pa.`id_product` = '.(int)$id_product);

                if (!empty($nb_custom_values))
                {
                    $tab_id_attribute = array();
                    foreach ($nb_custom_values as $attribute)
                    {
                        $tab_id_attribute[] = $attribute['id_attribute'];

                        $group = Db::getInstance()->executeS('
                        SELECT g.`id_attribute_group`, g.`url_name` as `group`
                        FROM `'._DB_PREFIX_.'layered_indexable_attribute_group_lang_value` g
                        LEFT JOIN `'._DB_PREFIX_.'attribute` a
                            ON (a.`id_attribute_group` = g.`id_attribute_group`)
                        WHERE a.`id_attribute` = '.(int)$attribute['id_attribute'].'
                        AND g.`id_lang` = '.(int)Context::getContext()->language->id.'
                        AND g.`url_name` IS NOT NULL AND g.`url_name` != \'\'');
                        if (empty($group))
                        {
                            $group = Db::getInstance()->executeS('
                            SELECT g.`id_attribute_group`, g.`name` as `group`
                            FROM `'._DB_PREFIX_.'attribute_group_lang` g
                            LEFT JOIN `'._DB_PREFIX_.'attribute` a
                                ON (a.`id_attribute_group` = g.`id_attribute_group`)
                            WHERE a.`id_attribute` = '.(int)$attribute['id_attribute'].'
                            AND g.`id_lang` = '.(int)Context::getContext()->language->id.'
                            AND g.`name` IS NOT NULL');
                        }
                        $result[] = array_merge($attribute, $group[0]);
                    }
                    $values_not_custom = Db::getInstance()->executeS('
                    SELECT DISTINCT a.`id_attribute`, a.`id_attribute_group`, al.`name` as `attribute`, agl.`name` as `group`
                    FROM `'._DB_PREFIX_.'attribute` a
                    LEFT JOIN `'._DB_PREFIX_.'attribute_lang` al
                        ON (a.`id_attribute` = al.`id_attribute` AND al.`id_lang` = '.(int)Context::getContext()->language->id.')
                    LEFT JOIN `'._DB_PREFIX_.'attribute_group_lang` agl
                        ON (a.`id_attribute_group` = agl.`id_attribute_group` AND agl.`id_lang` = '.(int)Context::getContext()->language->id.')
                    LEFT JOIN `'._DB_PREFIX_.'product_attribute_combination` pac
                        ON (a.`id_attribute` = pac.`id_attribute`)
                    LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa
                        ON (pac.`id_product_attribute` = pa.`id_product_attribute`)
                    '.Shop::addSqlAssociation('product_attribute', 'pa').'
                    '.Shop::addSqlAssociation('attribute', 'pac').'
                    WHERE pa.`id_product` = '.(int)$id_product.'
                    AND a.`id_attribute` NOT IN('.implode(', ', $tab_id_attribute).')');
                    $result = array_merge($values_not_custom, $result);
                }
                else
                {
                    $result = Db::getInstance()->executeS('
                    SELECT DISTINCT a.`id_attribute`, a.`id_attribute_group`, al.`name` as `attribute`, agl.`name` as `group`
                    FROM `'._DB_PREFIX_.'attribute` a
                    LEFT JOIN `'._DB_PREFIX_.'attribute_lang` al
                        ON (a.`id_attribute` = al.`id_attribute` AND al.`id_lang` = '.(int)Context::getContext()->language->id.')
                    LEFT JOIN `'._DB_PREFIX_.'attribute_group_lang` agl
                        ON (a.`id_attribute_group` = agl.`id_attribute_group` AND agl.`id_lang` = '.(int)Context::getContext()->language->id.')
                    LEFT JOIN `'._DB_PREFIX_.'product_attribute_combination` pac
                        ON (a.`id_attribute` = pac.`id_attribute`)
                    LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa
                        ON (pac.`id_product_attribute` = pa.`id_product_attribute`)
                    '.Shop::addSqlAssociation('product_attribute', 'pa').'
                    '.Shop::addSqlAssociation('attribute', 'pac').'
                    WHERE pa.`id_product` = '.(int)$id_product);
                }
            }
            else
            {
                $result = Db::getInstance()->executeS('
                SELECT DISTINCT a.`id_attribute`, a.`id_attribute_group`, al.`name` as `attribute`, agl.`name` as `group`
                FROM `'._DB_PREFIX_.'attribute` a
                LEFT JOIN `'._DB_PREFIX_.'attribute_lang` al
                    ON (a.`id_attribute` = al.`id_attribute` AND al.`id_lang` = '.(int)Context::getContext()->language->id.')
                LEFT JOIN `'._DB_PREFIX_.'attribute_group_lang` agl
                    ON (a.`id_attribute_group` = agl.`id_attribute_group` AND agl.`id_lang` = '.(int)Context::getContext()->language->id.')
                LEFT JOIN `'._DB_PREFIX_.'product_attribute_combination` pac
                    ON (a.`id_attribute` = pac.`id_attribute`)
                LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa
                    ON (pac.`id_product_attribute` = pa.`id_product_attribute`)
                '.Shop::addSqlAssociation('product_attribute', 'pa').'
                '.Shop::addSqlAssociation('attribute', 'pac').'
                WHERE pa.`id_product` = '.(int)$id_product);
            }
        }

        return $result;
    }

}