{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA

*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<!-- MODULE Block contact infos -->
{*<div class="footer-block">*}
	<section class="footer-block col-sm-2" id="block_contact_infos">
		<div>
			<h4>{l s='About FG Store' mod='blockcontactinfos'}</h4>
			<ul class="toggle-footer">
				<li>
					<i class="icon-external-link"></i>{l s='Powered by:' mod='blockcontactinfos'}
					<span><a href="http://fashiongreatness.com">fashiongreatness.com</a></span>
				</li>
				{*{if $blockcontactinfos_company != ''}*}
					{*<li>*}
						{*<i class="icon-map-marker"></i>{$blockcontactinfos_company|escape:'html':'UTF-8'}{if $blockcontactinfos_address != ''}, {$blockcontactinfos_address|escape:'html':'UTF-8'}{/if}*}
					{*</li>*}
				{*{/if}*}
				{if $blockcontactinfos_phone != ''}
					<li>
						<i class="icon-phone"></i>{l s='Call us now:' mod='blockcontactinfos'}
						<span>{$blockcontactinfos_phone|escape:'html':'UTF-8'}</span>
					</li>
					<li>
						<i class="icon-info"></i>{l s='Working hours:' mod='blockcontactinfos'}
						<span>{l s='from 10 a.m. to 19 p.m. CET.' mod='blockcontactinfos'}</span>
					</li>
				{/if}
				{if $blockcontactinfos_email != ''}
					<li>
						<i class="icon-envelope-alt"></i>{l s='Email:' mod='blockcontactinfos'}
						<span>{mailto address=$blockcontactinfos_email|escape:'html':'UTF-8' encode="hex"}</span>
					</li>
				{/if}

			</ul>
		</div>
	</section>
{*</div>*}

<!-- /MODULE Block contact infos -->
