{include file='./brands_menu.tpl'}

{if $category->level_depth == 4}
   {*<pre>*}
    {*{$category|var_dump}*}

    {foreach from=$menuCategTree.children item=menuCategory}
        {if $menuCategory.id == $brand_id}
            {*<pre>*}
            {*{$menuCategory|var_dump}*}

            <div class="top_submenu_wrapper" id="top-submenu">
                <ul class="centered border_bottom">
                    {foreach from=$menuCategory.children item=menuSubCategory name=foreachTopSub}
                        {*<pre>*}
                        {*{$category->id_parent|var_dump}*}
                        {*{$menuSubCategory.id|var_dump}*}
                        <li {if $menuSubCategory.id == $category->id_parent} class="active"{/if}><a href="#" data-menu-id="{$smarty.foreach.foreachTopSub.iteration}">{$menuSubCategory.name}</a></li>
                    {/foreach}
                </ul>
                <div class="menuArrow"></div>
                <div class="menuClose"></div>
            </div>

            <div class="full_menu clearfix">
                <div class="col-xs-12 col-sm-12" >
                    {foreach from=$menuCategory.children item=menuSubCategory name=foreachSub}
                        <ul class="centered {if $menuSubCategory.id == $category->id_parent} active{/if}" id="top-subsubmenu-{$smarty.foreach.foreachSub.iteration}">
                            {foreach from=$menuSubCategory.children item=menuSubSubCategory name=foreachSubSub}
                                {if $menuSubSubCategory.id == $category->id}
                                    <li class="active">{$menuSubSubCategory.name}</li>
                                {else}
                                    <li><a href="{$menuSubSubCategory.link}">{$menuSubSubCategory.name}</a></li>
                                {/if}


                            {/foreach}
                        </ul>
                    {/foreach}
                </div>
            </div>
        {/if}
    {/foreach}
{else}
    {foreach from=$menuCategTree.children item=menuCategory}
        {if $menuCategory.id == $category->id}

            <div class="top_submenu_wrapper" id="top-submenu">
                <ul class="centered border_bottom">
                    {foreach from=$menuCategory.children item=menuSubCategory name=foreachTopSub}
                        <li {if $smarty.foreach.foreachTopSub.iteration == 1} class="active"{/if}><a href="#" data-menu-id="{$smarty.foreach.foreachTopSub.iteration}">{$menuSubCategory.name}</a></li>
                    {/foreach}
                </ul>
                <div class="menuArrow"></div>
                <div class="menuClose"></div>
            </div>

            <div class="full_menu clearfix">
                <div class="col-xs-12 col-sm-12" >
                    {foreach from=$menuCategory.children item=menuSubCategory name=foreachSub}
                        <ul class="centered {if $smarty.foreach.foreachSub.iteration == 1} active{/if}" id="top-subsubmenu-{$smarty.foreach.foreachSub.iteration}">
                            {foreach from=$menuSubCategory.children item=menuSubSubCategory name=foreachSubSub}

                                <li><a href="{$menuSubSubCategory.link}">{$menuSubSubCategory.name}</a></li>

                            {/foreach}
                        </ul>
                    {/foreach}
                </div>
            </div>
        {/if}
    {/foreach}

{/if}


