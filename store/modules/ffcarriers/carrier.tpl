{*<h4>Детали доставки Самовывоз Карвари</h4>*}
<input type="hidden" name="shippocarrier" value="1">
<input type="hidden" name="alias" value="Shippo multicarrier">
{*<input type="hidden" name="clean_state" value="true">*}
{*<input id="carvaricarrier-city" type="hidden" name="city" value="{$store.city}">*}
{*<input id="carvaricarrier-address1" type="hidden" name="address1" value="{$store.address1}">*}
{*<input type="hidden" name="" value="">*}
<br/>

{if count($products_by_manufacturer)>1}
    <h4>{l s='Your cart consist of more than one package , because the items is situated in different warhouses.'}</h4>
    <h4>{l s='Please, select delivery options for each package.'}</h4>
{else}
    <h4>{l s='Please, select delivery options for your package.'}</h4>
{/if}
{foreach from=$products_by_manufacturer item=package key=id_maufacturer}
    {if count($package.rates)>0}
        <table class="resume table table-bordered shippo_delivery">
        {foreach from=$package.rates item=provider}
             <tr>
                <td rowspan="{count($provider.options)+1}" class="shippo_logo">
                    <img src="{$provider.provider_image_200}"  width="120px" height="120px"/>
                </td>
            </tr>
                {foreach from=$provider.options item=rate}
                    <tr>
                        <td>
                            <input type="radio" name="shippo[{$id_maufacturer}][servicelevel_token]" id="optionsRadios_{$rate.servicelevel_token}" value="{$rate.servicelevel_token}" {if isset($selected_options[$id_maufacturer][$rate.servicelevel_token])} checked{/if} required />
                            <input type="hidden" name="shippo[{$id_maufacturer}][{$rate.servicelevel_token}][amount]" value="{$rate.amount}" />
                            <input type="hidden" name="shippo[{$id_maufacturer}][{$rate.servicelevel_token}][currency]" value="{$rate.currency}" />
                            <input type="hidden" name="shippo[{$id_maufacturer}][{$rate.servicelevel_token}][provider]" value="{$provider.provider}" />

                            {$rate.servicelevel_name}, <strong>{$rate.duration_terms} </strong>

                        </td>
                        <td class="shippo_price">
                            <strong>{$rate.amount} {$rate.currency}</strong>
                        </td>
                    </tr>
                {/foreach}
        {/foreach}
        </table>
    {else}
        <h4>{l s='There are no options for this address. Please contact us.'}</h4>

    {/if}
{/foreach}


    {*<div class="input_block">*}
        {*<label>Магазин</label>*}
        {*<select name="id_store" required>*}
            {*<option value=""></option>*}
            {*{foreach from=$stores item=store}*}
                {*<option value="{$store.id_store}" data-city="{$store.city}" data-address1="{$store.address1}">{$store.city}, {$store.name}, {$store.address1}</option>*}
            {*{/foreach}*}
        {*</select>*}
        {*<span id="opc_carvaricarrier_errors" class="error_msg"></span>*}
    {*</div>*}

<div class="clearfix"></div>

