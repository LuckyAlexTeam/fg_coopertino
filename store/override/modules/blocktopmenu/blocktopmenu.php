<?php

class BlocktopmenuOverride extends Blocktopmenu
{

	protected function generateCategoriesMenu($categories, $is_children = 0)
	{
		$html = '';

		foreach ($categories as $key => $category)
		{
			if ($category['level_depth'] > 1)
			{
				$cat = new Category($category['id_category']);
				$link = Tools::HtmlEntitiesUTF8($cat->getLink());
			}
			else
				$link = $this->context->link->getPageLink('index');

			/* Whenever a category is not active we shouldnt display it to customer */
            if ((bool)$category['active'] === false) {
                break;
            }

			$html .= '<li'.(($this->page_name == 'category'
				&& (int)Tools::getValue('id_category') == (int)$category['id_category']) ? ' class="sfHoverForce"' : '').'>';
			$html .= '<a href="'.$link.'" title="'.$category['name'].'">'.$category['name'].'</a>';

			if (isset($category['children']) && !empty($category['children']))
			{
				$html .= '<ul>';
				$html .= $this->generateCategoriesMenu($category['children'], 1);
				$html .= '</ul>';
			}

			$html .= '</li>';
		}

		return $html;
	}

}
