<?php

/**
 * This is the model class for table "{{users}}".
 *
 * The followings are the available columns in table '{{users}}':
 * @property string $id
 * @property string $login
 * @property string $password
 * @property string $email
 * @property integer $status
 * @property integer $subscribe
 * @property string $created
 * @property string $activationKey
 *
 * The followings are the available model relations:
 * @property Messages[] $messages
 */
class Users extends CActiveRecord {
	
	public $ara = Array();
	
    public $rememberMe;
    public $key;
    public $captcha;
    public $password2;
    private $_identity;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Users the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
	 
    public function tableName() {
        return 'users';
    }
	
    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
		// Вы должны только определить правила для тех атрибутов, которые будут получать пользовательский ввод.
        return array();
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
	// id 	login 	password 	fio 	otdel Отдел	doljnost Должность	phone 	vphone Внутренний телефон	remail Рабочий email	skipe 	icq 	data_roj Дата рождения	rol 	status
	public function attributeLabels() {
		return array(
			'h0' => 'id',
			'h1' => 'Логин',
			'h2' => 'Пароль',
			'h3' => 'Ф.И.О.',
			'h4'=>'Отдел',
			'h5'=>'Должность',
			'h6' => 'Телефон',
			'h7' => 'Внутр.телефон',
			'h8' => 'Рабочий email',
			'h9' => 'Skipe',
			'h10' => 'ICQ',
			'h11' => 'Дата рождения',
			'h12' => 'Роль',
			'h13' => 'Статус',
		);
	}
	// return array соответствия полей формы (rx)(ajax) и имен БД ($ara)
	public function attributeLabelsAjax() {
		return array(
			'r0' => 'id',
			'r1' => 'login',
			'r2' => 'password',
			'r3' => 'fio',
			'r4' => 'otdel',
			'r5' => 'doljnost',
			'r6' => 'phone',
			'r7' => 'vphone',
			'r8' => 'remail',
			'r9' => 'skipe',
			'r10' => 'icq',
			'r11' => 'data_roj',
			'r12' => 'rol',
			'r13' => 'status',
		);
	}
	// return array поля формы ($arp)
	public function formaLabels() {
		return array(
			'nam' => 'Справочник пользователей',
			'ins' => 'Добавить новую строку',
			'kol' => '14', // кол-во полей формы из базы
			'url' => 'site0/users' // url для ajax 
		);
	}
    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.
/*
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('login', $this->login, true);
        $criteria->compare('password', $this->password, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('phone', $this->phone, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('subscribe', $this->subscribe);
        $date_search = $this->date_search($this->created);
        $criteria->addBetweenCondition('created', $date_search['from'], $date_search['untill']); 
        $date_search = $this->date_search($this->changed);
        $criteria->addBetweenCondition('changed', $date_search['from'], $date_search['untill']); 

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
*/
    }
	// 	id login password name last_name email phone kol_prof prof summary language location package phpsessid 	
	public function getAll() {
		$list = self::model()->findAll();
		$atr = self::model()->getAttributes();
		$ark = array_keys ($atr);
		$array = array();
		foreach ($list as $value) {
			$rab = array ();
			for ($i = 0; $i<count($ark); ++$i) {
				$rab[$ark[$i]] = $value->$ark[$i];
			};
			$array[$value->id] = $rab; 
		};
		return $array;
	}
	// retur json - ответ на ajax запрос
	public function AjaxMy() {
		$oper = Yii::app()->request->getPost('oper');
		if ($oper == "del") {
			$id = Yii::app()->request->getPost('id');
			$ret = $this->deleteByPk($id);
			echo '{"otv":"del", "id":"'.$id.'", "ret":"'.$ret.'"}';
			return;
		};
		$arr = $this->attributeLabelsAjax();
		$ark = array_keys ($arr);
		
		$rab = "";
		for ($i=1; $i<count($ark); ++$i) {
			$this->setAttribute( $arr[$ark[$i]] , Yii::app()->request->getPost($ark[$i]) );
				$rab .= ', "'. $ark[$i] .'":"'. $this->getAttribute( $arr[$ark[$i]] ) .'"';
		};
		if ($oper == "upd") {
			$this->setAttribute( 'id'     , Yii::app()->request->getPost('id') );
			$id = Yii::app()->request->getPost('id');
			$this->setPrimaryKey(Yii::app()->request->getPost('id'));
			$this->isNewRecord=false;
			$ttt = $this->update();
			echo '{"otv":"upd", "id":"'.$id.'"'.$rab .'}';
			return;
		};
		if ($oper == "ins") {
			$ttt = $this->insert();
			$id = $this->getPrimaryKey();
			echo '{"otv":"ins", "id":"'.$id.'"'.$rab .'}';
			return;
		};
		echo '{"otv":"err", "soo":"!!! ОШИБКА !!!"}';
	}
	//
    public static function getIdByEmail($email) {
        $model = self::model()->findByAttributes(array('email' => $email)); ;  
        if ($model)
            return $model->id;
        return '';
    }

    public static function getEmail($id) {
        $model = self::model()->findByPk($id);  
        if ($model)
            return $model->email;
        return '';
    }

    public static function getStatus($id) {
        $model = self::model()->findByPk($id);  
        if ($model)
            return $model->status;
        return '';
    }

    protected function beforeSave() {
        if ($this->isNewRecord) {
            // Хешировать пароль
           //$this->password = $this->hashPassword($this->password);
		   $this->password = $this->password;
        } 
        return parent::beforeSave();
    }

    public function hashPassword($password) {
		return $password;
        //return md5($password);
    }

    /**
     * Собственное правило для проверки
     * Данный метод является связующем звеном с UserIdentity
     *
     * @param $attribute
     * @param $params
     */
    public function authenticate($attribute, $params) {
        // Проверяем были ли ошибки в других правилах валидации.
        // если были - нет смысла выполнять проверку
        if (!$this->hasErrors()) {
            // Создаем экземпляр класса UserIdentity
            // и передаем в его конструктор введенный пользователем логин и пароль (с формы)
            $identity = new UserIdentity($this->login, $this->password);
            // Выполняем метод authenticate (о котором мы с вами говорили пару абзацев назад)
            // Он у нас проверяет существует ли такой пользователь и возвращает ошибку (если она есть)
            // в $identity->errorCode
            $identity->authenticate();

            // Теперь мы проверяем есть ли ошибка..    
            switch ($identity->errorCode) {
                // Если ошибки нету...
                case UserIdentity::ERROR_NONE: {
                        // Данная строчка говорит что надо выдать пользователю
                        // соответствующие куки о том что он зарегистрирован, срок действий
                        // у которых указан вторым параметром. 
                        Yii::app()->user->login($identity, 0);
                        break;
                    }
                case UserIdentity::ERROR_USERNAME_INVALID: {
                        // Если логин был указан наверно - создаем ошибку
                        $this->addError('login', 'Пользователь не существует!');
                        break;
                    }
                case UserIdentity::ERROR_PASSWORD_INVALID: {
                        // Если пароль был указан наверно - создаем ошибку
                        $this->addError('password', 'Вы указали неверный пароль!');
                        break;
                    }
            }
        }
    }

    /**
     * Logs in the user using the given username and password in the model.
     * @return boolean whether login is successful
     */
    public function login() {
        if ($this->_identity === null) {
            $this->_identity = new UserIdentity($this->login, $this->password);
            $this->_identity->authenticate();
        }
        if ($this->_identity->errorCode === UserIdentity::ERROR_NONE) {
            $duration = $this->rememberMe ? 3600 * 24 * 30 : 0; // 30 days
            Yii::app()->user->login($this->_identity, $duration);
            return true;
        }
        else
            return false;
    }
	public function Sel($id = "sel_users") {
		$list = self::model()->findAll(); 
		$sele = "<select id='{$id}' name='{$id}'><option value='0' selected>Добавить</option>";
		foreach ($list as $value) {
			$sele .= "<option value='{$value->id}'>" . $value->fio . "</option>";
		};
		$sele .= "</select>";
		return $sele;
	}
	// Формировать Таблицу
	public function Tab() {
		$list = self::model()->findAll();
		$arr = Array();
		foreach ($list as $value) { $arr[$value->id] = $value->fio; };
		return $arr;
	}
}