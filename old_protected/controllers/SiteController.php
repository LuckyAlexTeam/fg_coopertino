<?php

class SiteController extends CController {
	public $layout='/layouts/main';
	public $arr_menu;
	public $arr_lang;
	public $arr_site;
	public $arr_block;
	public $arr_mak;
	public $arr_pole;
	public $arr_header;
	public $arr_cont;
	public $arr_price;
	public $tlanguage;
	public $tuser;
	public $sv_ch; // свой - чужой = sv / ch
	public $ch_user; // id user чужой 
	
	public $breadcrumbs;
	/**
	 * Declares class-based actions.
	 */
	public function actions() {
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}
	//
	public function actionTZ() {
		$this->site_stranica();
		$this->render('klient');
	}
	public function actionHome() {
		$this->site_stranica();
		$this->render('home');
	}
	//
	// Страницы доступные только зарегистрированным пользователям и достипна только мне
	//
	public function actionQuestionary() {
		$this->layout = '/layouts/mainvip';
		$this->site_stranica();
		$this->render('questionary');
	}
	public function actionSetting() {
		
		$modez=new Vhod;
		$pac = $modez->getUser();
		if ($pac != '') {									// Профиль != "" - Значит есть авторизация
			$ank = Yii::app()->user->getState('anketa'); 	// Анкета заполнена или нет 
															// Если НЕТ (0)то показать анкету site/questionary 
															// Если ДА (1) то показать мой профиль (My Page) id55777 =  site/visitor ; site/classic ; site/vip ; site/corporate
			if ($ank == 0) {
				$this->actionQuestionary();
			} else {
				$pac = Yii::app()->user->getState('package');
				$lan = Yii::app()->user->getState('language');
				$use = Yii::app()->user->getState('id_user');
				Yii::app()->user->setState('ch_user', Yii::app()->user->getState('id_user') );
				Yii::app()->user->setState('sv_ch', 'sv');
				$model=new Language();
				$this->tlanguage = $model->getLanguage();
				$this->arr_lang = $model->getAll();
				$modea=new Site();
				//$this->arr_site  = $modea->getAll($this->tlanguage, 'visitor');
				$this->arr_site  = $modea->getAll($lan, 'visitor');
				$modeb=new SiteHeader();
				$this->arr_header = $modeb->getAll($this->tlanguage, 'index');
				$modec=new Users();
				$this->tuser = $modec->getKart($use);
				$this->layout = '/layouts/mainvip';
				$this->render('setting');
			};
		} else {
			$this->actionIndex();
		};
	}
	public function actionSett() {
		
		$modez=new Vhod;
		$pac = $modez->getUser();
		if ($pac != '') {									// Профиль != "" - Значит есть авторизация
			$ank = Yii::app()->user->getState('anketa'); 	// Анкета заполнена или нет 
															// Если НЕТ (0)то показать анкету site/questionary 
															// Если ДА (1) то показать мой профиль (My Page) id55777 =  site/visitor ; site/classic ; site/vip ; site/corporate
			if ($ank == 0) {
				$this->actionQuestionary();
			} else {
				$pac = Yii::app()->user->getState('package');
				$lan = Yii::app()->user->getState('language');
				$use = Yii::app()->user->getState('id_user');
				Yii::app()->user->setState('ch_user', Yii::app()->user->getState('id_user') );
				Yii::app()->user->setState('sv_ch', 'sv');
				
				$modbl=new SiteBlock();
				$this->arr_block = $modbl->getAll();
				//print "<hr>";
				//var_dump($this->arr_block);
				
				$modma=new SiteMaket();
				$this->arr_mak = $modma->getAll();
				
				$model=new Language();
				$this->tlanguage = $model->getLanguage();
				$this->arr_lang = $model->getAll();
				$modea=new Site();
				//$this->arr_site  = $modea->getAll($this->tlanguage, 'visitor');
				$this->arr_site  = $modea->getAll($lan, 'visitor');
				$this->arr_pole  = $modea->getAllPole($lan, 'general');
				$modeb=new SiteHeader();
				$this->arr_header = $modeb->getAll($this->tlanguage, 'index');
				$modec=new Users();
				$this->tuser = $modec->getKart($use);
				$this->layout = '/layouts/mainvip';
				$this->render('sett');
			};
		} else {
			$this->actionIndex();
		};
	}
	
	public function actionPaypal() {
		$this->layout = '/layouts/mainvip';
		$this->site_stranica();
		$this->render('paypal');
	}
	//
	// Страницы доступные всем
	//
	public function actionIndex() {
		$modez=new Vhod;
		$pac = $modez->getUser();
		if ($pac != '') {
			$ank = Yii::app()->user->getState('anketa'); 	// Анкета заполнена или нет 
															// Если НЕТ (0)то показать анкету site/questionary 
															// Если ДА (1) то показать мой профиль (My Page) id55777 =  site/visitor ; site/classic ; site/vip ; site/corporate
			if ($ank == 0) {
				$this->redirect('questionary');
			} else {
				$pac = Yii::app()->user->getState('package');
				$lan = Yii::app()->user->getState('language');
				$use = Yii::app()->user->getState('id_user');
				$model=new Language();
				$this->tlanguage = $model->getLanguage();
				$this->arr_lang = $model->getAll();
				$modea=new Site();
				//$this->arr_site  = $modea->getAll($this->tlanguage, 'visitor');
				$this->arr_site  = $modea->getAll($lan, 'visitor');
				$modeb=new SiteHeader();
				$this->arr_header = $modeb->getAll($this->tlanguage, 'index');
				$modec=new Users();
				$this->tuser = $modec->getKart($use);
				$this->redirect('id'.$use);
			};
		};
		$this->layout = '/layouts/main';
		$this->site_stranica();
		$modex=new Nastroyki;
		$this->arr_price = $modex->getPrice();
		$this->render('index');
	}
	public function actionKlient() {
		$this->layout = '/layouts/mainvisitor';
		$this->site_stranica();
		if ( isset($_GET['id']) ) {
			$this->ch_user = $_GET['id'];
			Yii::app()->user->setState('ch_user', $_GET['id']);
			if ($this->tuser['id'] == $_GET['id']) { $this->sv_ch = "sv"; } else { $this->sv_ch = "ch"; };
			Yii::app()->user->setState('sv_ch', $this->sv_ch);
		} else {
			$this->sv_ch = Yii::app()->user->getState('sv_ch');
			
		};
		$modec=new Users();
		$this->tuser = $modec->getKart( Yii::app()->user->getState('id_user') );

		$pac = Yii::app()->user->getState('package');
		
		if ($pac == 'vis') {$this->render('visitor'); return; };
		if ($pac == 'cla') {$this->render('classic'); return; };
		if ($pac == 'vip') {$this->render('vip'); return; };
		if ($pac == 'cor') {$this->render('corporate'); return; };
		$this->render('klient');
		//$this->render('index');
	}
	public function actionAbout() {
		$this->layout = '/layouts/mainall';
		$this->site_stranica();
		$this->render('about');
	}
	public function actionServices() {
		$this->layout = '/layouts/mainall';
		$this->site_stranica();
		$this->render('services');
	}
	public function actionMentors() {
		$this->layout = '/layouts/mainall';
		$this->site_stranica();
		$this->render('mentors');
	}
	public function actionFaq() {
		$this->layout = '/layouts/mainall';
		$this->site_stranica();
		$this->render('faq');
	}
	public function actionContacts() {
		$this->layout = '/layouts/mainall';
		$this->site_stranica();
		$this->render('contacts');
	}
	public function actionWrite() {
		$this->layout = '/layouts/mainall';
		$this->site_stranica();
		$this->render('write');
	}
	//
	public function actionEvents() {
		$this->layout = '/layouts/mainvip';
		$this->site_stranica();
		$this->render('events');
	}
	public function actionJournal() {
		$this->layout = '/layouts/mainvip';
		$this->site_stranica();
		$this->render('journal');
	}
	public function actionJoboffermy() {
		$this->layout = '/layouts/mainvip';
		$this->site_stranica();
		$this->render('joboffermy');
	}
	public function actionJoboffer() {
		$this->layout = '/layouts/mainvip';
		$this->site_stranica();
		$this->render('joboffer');
	}
	public function actionVacancies() {
		$this->layout = '/layouts/mainvip';
		$this->site_stranica();
		$this->render('vacancies');
	}
	public function actionSearchevents() {
		$this->layout = '/layouts/mainvip';
		$this->site_stranica();
		$this->render('searchevents');
	}
	public function actionSearchpeople() {
		$this->layout = '/layouts/mainvip';
		$this->site_stranica();
		$this->render('searchpeople');
	}
	public function actionSearchjob() {
		$this->layout = '/layouts/mainvip';
		$this->site_stranica();
		$this->render('searchjob');
	}
	public function actionRecommendations() {
		$this->layout = '/layouts/mainvip';
		$this->site_stranica();
		$this->render('recommendations');
	}
	public function actionSubscription() {
		$this->layout = '/layouts/mainvip';
		$this->site_stranica();
		$this->render('subscription');
	}
	public function actionError() {
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
	// ---------------------------------------------------------------------------------------------
	// Контроль email при регистрации
	public function actionEmail() {
		$model=new Users();
		$otv = $model->getIdByEmail($_POST['email']);
		if ($otv == '') { print '{"otv":"ok"}'; } else { print '{"otv":"no"}'; };
	}
	// Регистрация - направляем на setting
	// array(7) { ["reg_pac"]=> string(3) "vis" ["reg_nam"]=> string(1) "1" ["reg_lna"]=> string(1) "2" ["reg_mai"]=> string(1) "3" ["reg_pho"]=> string(1) "4" ["reg_lan"]=> string(2) "en" ["reg_loc"]=> string(2) "en" }
	public function actionRegistration() {
		//var_dump($_POST);
		$model=new Users();
		$model->Ins();
		//
		//mail("ser51@ser51.ru","Proba","zzzzzz");
		$txt = 'Глубокоуважаемый Вы зарегистрировались на сайте http://fashingreatness.ru/ Ваш логин = Вашему E-mail, Пароль = ' . substr ( $_COOKIE['PHPSESSID'], 0,4);
		$aaa = '<br> Данные : '.
		'<br> Имя '. $_POST['reg_nam'].
		'<br> Фамилия '. $_POST['reg_lna'].
		'<br> Email'. $_POST['reg_mai'].
		'<br> Телефон '. $_POST['reg_pho'].
		'<br> Язык '. $_POST['reg_lan'].
		'<br> Расположение '. $_POST['reg_loc'].
		'<br> Пакет '. $_POST['reg_pac'].
		'<br> Количество профессий '. $_POST['reg_kpr'].
		'<br> Профессия '. $_POST['reg_ypr'].
		'<br> Резюме '. $_POST['reg_sum'];
		
		$headers= "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=utf-8\r\n";
		if ($_POST['reg_pac'] == 'vis') {
			$modnas=new Nastroyki;
			$mai = $modnas->getStroka('mail', 'visitor');
			//var_dump($mai);
			mail($mai['txt_1'],"Регистрация",$txt.$aaa, $headers); // на админа
			mail($_POST['reg_mai'],"Регистрация",$txt, $headers);
			//$this->redirect('index.php?r=sitevisitor/setting');
		};
		if ($_POST['reg_pac'] == 'cla') {
			$modnas=new Nastroyki;
			$mai = $modnas->getStroka('mail', 'classic');
			//var_dump($mai);
			mail($mai['txt_1'],"Регистрация",$txt.$aaa, $headers); // на админа
			mail($_POST['reg_mai'],"Регистрация",$txt, $headers);
			//$this->redirect('index.php?r=siteclassic/setting');
		};
		if ($_POST['reg_pac'] == 'vip') {
			$modnas=new Nastroyki;
			$mai = $modnas->getStroka('mail', 'vip');
			//var_dump($mai);
			mail($mai['txt_1'],"Регистрация",$txt.$aaa, $headers); // на админа
			mail($_POST['reg_mai'],"Регистрация",$txt, $headers);
			//$this->redirect('index.php?r=sitevip/setting');
		};
		if ($_POST['reg_pac'] == 'cor') {
			$modnas=new Nastroyki;
			$mai = $modnas->getStroka('mail', 'corporate');
			//var_dump($mai);
			mail($mai['txt_1'],"Регистрация",$txt.$aaa, $headers); // на админа
			mail($_POST['reg_mai'],"Регистрация",$txt, $headers);
			//$this->redirect('index.php?r=sitecorporate/setting');
		};
		//
		$model=new LoginForm;
		if ( isset($_POST['sig_rem']) ) { $model->rememberMe = true; } else { $model->rememberMe = false;};
		$model->username = $_POST['reg_mai'];
		$model->password = substr ( $_COOKIE['PHPSESSID'], 0,4);
		$model->login();
		$modez=new Vhod;
		$pac = $modez->getUser();
		$this->redirect('questionary');
		/*
		if ($pac == 'vis') {$this->redirect('index.php?r=sitevisitor/setting');};
		if ($pac == 'cla') {$this->redirect('index.php?r=siteclassic/setting');};
		if ($pac == 'vip') {$this->redirect('index.php?r=sitevip/setting');};
		if ($pac == 'cor') {$this->redirect('index.php?r=sitecorporate/setting');};
		$this->site_stranica();
		$this->render('index');
		return;
		*/
		
	}
	// Авторизация
	// 
	public function actionSignin() {
		$model=new LoginForm;
		if ( isset($_POST['sig_rem']) ) { $model->rememberMe = true; } else { $model->rememberMe = false;};
		$model->username = $_POST['sig_mai'];
		$model->password = $_POST['sig_pas'];
		//$model->login();
		//if($model->validate() && $model->login()) {
		if( $model->login() == false ) {
			$this->actionIndex();
			return;
		};
		$idu = Yii::app()->user->getState('id_user');
		$modez=new Vhod;
		$pac = $modez->getUser();
		$ank = Yii::app()->user->getState('anketa'); 	// Анкета заполнена или нет 
														// Если НЕТ (0)то показать анкету site/questionary 
														// Если ДА (1) то показать мой профиль (My Page) id55777 =  site/visitor ; site/classic ; site/vip ; site/corporate
		if ($ank == 0) {
			$this->redirect('questionary');
		} else {
			$this->redirect('id'.$idu);
		};
		//
		// *************************************************************
		/*
		$identity=new UserIdentity($model->username,$model->password);
		$dd = $identity->authenticate();
		if($dd) {
			$duration=$model->rememberMe ? 3600 : 0; // 30 days *24*30
			Yii::app()->user->login($this->_identity,$duration);
			Yii::app()->user->identityCookie = true;
			Yii::app()->user->setState("identityCookie", true);
			Yii::app()->user->setState("rememberMe", true);
			Yii::app()->user->setState("ser","sssssssssss");
		};
		$modeu=new Users;
		$mod = $modeu->getModelByEmail($_POST['sig_mai']);
		$atr = $mod->getAttributes();
		*/
	}
	// Сообщение с главной страницы
	public function actionSoo() {
		// array(4) { ["nam"]=> string(0) "" ["mai"]=> string(0) "" ["sel"]=> string(7) "sel_1#1" ["soo"]=> string(0) "" }
		//var_dump($_POST);
		//if ($_POST['sel'] == 'cor') {
		$txt = '<h1>Вам сообщение</h1>';
		$aaa = '<br> Данные : '.
		'<br> Имя : '. $_POST['nam'].
		'<br> Email : '. $_POST['mai'].
		'<br> Тема : '. $_POST['sel'].
		'<br> Сообщение : '. $_POST['soo'];
		$headers= "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=utf-8\r\n";
		
		$modnas=new Nastroyki;
		$mai = $modnas->getStroka('mailos', $_POST['sel']);
		var_dump($mai);
		mail($mai['txt_1'],"Вам сообщение",$txt.$aaa, $headers); // на админа
		mail($mai['txt_2'],"Вам сообщение",$txt.$aaa, $headers); // копия
		print '{"otv":"ok","txt":"'.$txt.'","aaa":"'.$aaa.'","header":"'.$headers.'"}';
		return;
	}
	/**
	 * Displays the login page
	 */
	public function actionLogin() {
		$model=new LoginForm;
		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm'])) {
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form 
		$this->render('login',array('model'=>$model));
	}
	public function actionLogout() {
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
	public function actionMenu() {
		$model=new Menu_Role;
		$this->arr_menu = $model->getAll();
	}
	public function site_stranica() {
		$model=new Language();
		$this->tlanguage = $model->getLanguage();
		$this->arr_lang = $model->getAll();
		$modea=new Site();
		$this->arr_site  = $modea->getAll($this->tlanguage, 'main');
		$modeb=new SiteHeader();
		$this->arr_header = $modeb->getAll($this->tlanguage, 'index');
	}
	
	
/* 
		public function actionIndex1111() {
		$modez=new Vhod;
		$pac = $modez->getUser();
		if ($pac == 'vis') {$this->redirect('index.php?r=sitevisitor/index');};
		if ($pac == 'cla') {$this->redirect('index.php?r=siteclassic/index');};
		if ($pac == 'vip') {$this->redirect('index.php?r=sitevip/index');};
		if ($pac == 'cor') {$this->redirect('index.php?r=sitecorporate/index');};
		$this->site_stranica();
		$this->render('index');
		//return;
*/
/*		
		if ( isset ($_COOKIE[Yii::app()->user->getStateKeyPrefix()]) ) {
			print "<hr> cookie  ===== KeyPrefix = ";
			var_dump($_COOKIE[Yii::app()->user->getStateKeyPrefix()]);
			$arr =  explode (":", $_COOKIE[Yii::app()->user->getStateKeyPrefix()] ,3);
			var_dump($arr);
			$aa = $arr[2];
			//$bb = json_decode($aa, true);
			//var_dump($bb); // {i:0;s:3:"с1";i:1;s:3:"с1";i:2;i:3600;i:3;a:0:{}}
			$aa =  str_replace ( "{", "", $aa);
			$aa =  str_replace ( "}", "", $aa);
			var_dump($aa);
			$bb =  explode (";", $aa);
			var_dump($bb);	// array(8) { [0]=> string(3) "i:0" [1]=> string(9) "s:3:"с1"" [2]=> string(3) "i:1" [3]=> string(9) "s:3:"с1"" [4]=> string(3) "i:2"
							// [5]=> string(6) "i:3600" [6]=> string(3) "i:3" [7]=> string(4) "a:0:" }
//
			$cc = explode (":", $bb[1] );
			var_dump($cc);
			
			$log = $cc[2];
			$log =  str_replace ( '"', "", $log);
			var_dump($log);
			
			$pas = $bb[3];
			$pas =  str_replace ( '"', "", $pas);
			var_dump($pas);
			//
			$modeu=new Users;
			$mod = $modeu->getModelByEmail($log);
			print "<hr> Users = ";
			var_dump($mod);
			$atr = $mod->getAttributes();
			print "<hr> atr=";
			var_dump($atr);

//
		}
*/
		//var_dump($_COOKIE[Yii::app()->user->getStateKeyPrefix()]);
		//print " --> ";
		//Yii::app()->user->isGuest;
		//$this->actionLogin();
		//Yii::app()->getSession()->regenerateID(true);
		//$this->site_stranica();
		//$this->render('index');
	//}
	
	
	
	
}