<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property integer $account_type
 * @property string $name
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $phone
 * @property string $password
 * @property string $login
 * @property integer $profession
 * @property integer $gender
 * @property string $skype
 * @property string $website
 * @property string $date_of_birth
 * @property integer $birth_show
 * @property integer $country
 * @property integer $language
 * @property string $avatar
 * @property string $company
 * @property string $agency_info
 * @property integer $legal_guardian
 * @property string $address
 * @property string $social_media
 *
 * The followings are the available model relations:
 * @property BrandLocationToUser[] $brandLocationToUsers
 * @property BuyerForm[] $buyerForms
 * @property CompanyForm[] $companyForms
 * @property MediaForm[] $mediaForms
 * @property ModelForm $modelForm
 * @property SpecializationToUser[] $specializationToUsers
 * @property UserProfession $profession0
 * @property UserAccountType $accountType
 * @property UserCountry $country0
 */
class BaseUser extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */ 
	 
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('account_type, name, last_name, email, phone, password, login, skype, website, date_of_birth, country', 'required'),
			array('account_type, profession, gender, birth_show, country, language, legal_guardian', 'numerical', 'integerOnly'=>true),
			array('name, last_name, email, password, login, skype', 'length', 'max'=>100),
			array('first_name, website, avatar, company, address, social_media', 'length', 'max'=>255),
			array('phone', 'length', 'max'=>60),
			array('agency_info', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, account_type, name, first_name, last_name, email, phone, password, login, profession, gender, skype, website, date_of_birth, birth_show, country, language, avatar, company, agency_info, legal_guardian, address, social_media', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'brandLocationToUsers' => array(self::HAS_MANY, 'BrandLocationToUser', 'user_id'),
			'buyerForms' => array(self::HAS_MANY, 'BuyerForm', 'user_id'),
			'companyForms' => array(self::HAS_MANY, 'CompanyForm', 'user_id'),
			'mediaForms' => array(self::HAS_MANY, 'MediaForm', 'user_id'),
			'modelForm' => array(self::HAS_ONE, 'ModelForm', 'id'),
			'specializationToUsers' => array(self::HAS_MANY, 'SpecializationToUser', 'user_id'),
			'profession0' => array(self::BELONGS_TO, 'UserProfession', 'profession'),
			'accountType' => array(self::BELONGS_TO, 'UserAccountType', 'account_type'),
			'country0' => array(self::BELONGS_TO, 'UserCountry', 'country'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'account_type' => 'Account Type',
			'name' => 'Name',
			'first_name' => 'First Name',
			'last_name' => 'Last Name',
			'email' => 'Email',
			'phone' => 'Phone',
			'password' => 'Password',
			'login' => 'Login',
			'profession' => 'Profession',
			'gender' => 'Gender',
			'skype' => 'Skype',
			'website' => 'Website',
			'date_of_birth' => 'Date Of Birth',
			'birth_show' => 'Birth Show',
			'country' => 'Country',
			'language' => 'Language',
			'avatar' => 'Avatar',
			'company' => 'Company',
			'agency_info' => 'Agency Info',
			'legal_guardian' => 'Legal Guardian',
			'address' => 'Address',
			'social_media' => 'Social Media',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('account_type',$this->account_type);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('login',$this->login,true);
		$criteria->compare('profession',$this->profession);
		$criteria->compare('gender',$this->gender);
		$criteria->compare('skype',$this->skype,true);
		$criteria->compare('website',$this->website,true);
		$criteria->compare('date_of_birth',$this->date_of_birth,true);
		$criteria->compare('birth_show',$this->birth_show);
		$criteria->compare('country',$this->country);
		$criteria->compare('language',$this->language);
		$criteria->compare('avatar',$this->avatar,true);
		$criteria->compare('company',$this->company,true);
		$criteria->compare('agency_info',$this->agency_info,true);
		$criteria->compare('legal_guardian',$this->legal_guardian);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('social_media',$this->social_media,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}