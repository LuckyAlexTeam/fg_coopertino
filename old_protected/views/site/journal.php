<div class="person-wrapper">
	<div class="j-main">
		<h2>Journal</h2>
		<span class="hr"></span>
		<div class="nav-bar">
			<ul class="nav2">
				<li class="active">
					<a href="#fn">FG News</a>
				</li>
				<li>
					<a href="#fs">Fashion Shows</a>
				</li>
				<li>
					<a href="#p">Persons</a>
				</li>
				<li>
					<a href="#fl">Fashion life</a>
				</li>
				<li>
					<a href="#b">Blog</a>
				</li>
			</ul>
		</div>
		<div class="j-item">
			<div class="j-img">
				<img src="">
			</div>
			<div class="j-txt">
				<h3></h3>
				<p></p>
				<br>
				<p></p>
			</div>
		</div>
		<div class="j-item">
			<div class="j-img">
				<img src="">
			</div>
			<div class="j-txt">
				<h3></h3>
				<p></p>
				<br>
				<p></p>
			</div>
		</div>
	</div>
	<div class="j-sidebar">
		<div class="j-t-port">
			<div class="j-t-head">
				Top profile
			</div>
			<div class="j-port-cont">
				<a href="">
					<img src="">
				</a>
			</div>
		</div>
		<div class="j-t-phot">
			<div class="j-t-head">
				Top photo
			</div>
			<div class="j-phot-cont">
				<a href="">
					<img src="">
				</a>
				<a href="">
					<img src="">
				</a>
				<a href="">
					<img src="">
				</a>
				<a href="">
					<img src="">
				</a>
				<a href="">
					<img src="">
				</a>
				<a href="">
					<img src="">
				</a>
				<a href="">
					<img src="">
				</a>
				<a href="">
					<img src="">
				</a>
				<a href="">
					<img src="">
				</a>
			</div>
		</div>
	</div>
</div>