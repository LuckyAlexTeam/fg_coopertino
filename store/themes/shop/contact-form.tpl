{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{capture name=path}{l s='Contact'}{/capture}
<h1 class="page-heading bottom-indent">
    {l s='Customer service'}
    - {if isset($customerThread) && $customerThread}{l s='Your reply'}{else}{l s='Contact us'}{/if}
</h1>

<p>
    {l s='Welcome to the Customer Care service for fashiongreatness.com.'}
    <br><br>
    {l s='For instant answers to frequent enquiries about fashiongreatness.com and your online shopping experience, please browse our Customer Care topics.'}
    <br>
</p>

{if isset($confirmation)}
    <p class="alert alert-success">{l s='Your message has been successfully sent to our team.'}</p>
    <ul class="footer_links clearfix">
        <li>
            <a class="btn btn-default button button-small" href="{$base_dir}">
				<span>
					<i class="icon-chevron-left"></i>{l s='Home'}
				</span>
            </a>
        </li>
    </ul>
{elseif isset($alreadySent)}
    <p class="alert alert-warning">{l s='Your message has already been sent.'}</p>
    <ul class="footer_links clearfix">
        <li>
            <a class="btn btn-default button button-small" href="{$base_dir}">
				<span>
					<i class="icon-chevron-left"></i>{l s='Home'}
				</span>
            </a>
        </li>
    </ul>
{else}
    {include file="$tpl_dir./errors.tpl"}

    <div>

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#for_clients" aria-controls="for_clients" role="tab" data-toggle="tab">{l s='For clients'}</a></li>
            <li role="presentation"><a href="#for_distr" aria-controls="for_distr" role="tab" data-toggle="tab">{l s='For distributors'}</a></li>

        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="for_clients">
                <form id="contact-form" action="{$request_uri}" method="post" class="contact-form-box" enctype="multipart/form-data">
                    <fieldset>
                        {*<h3 class="page-subheading">{l s='send a message'}</h3>*}

                        <span>* {l s='Required fields'}</span><br/><br/>
                        <div class="clearfix">
                            <div class="col-xs-12 col-md-8">
                                {if !$is_logged}
                                    <p class="form-group">
                                        <label for="firstname">{l s='First Name'}<sup>*</sup></label>
                                        <input class="styler" id="firstname" type="text" name="firstname" required value="">
                                    </p>
                                    <p class="form-group">
                                        <label for="lastname">{l s='Last Name'}<sup>*</sup></label>
                                        <input class="styler" id="lastname" type="text" name="lastname" required value="">
                                    </p>
                                    <p class="form-group">
                                        <label for="email">{l s='Email address'}<sup>*</sup></label>
                                        {if isset($customerThread.email)}
                                            <input class="styler grey" type="text" id="email" name="from" required
                                                   value="{$customerThread.email|escape:'html':'UTF-8'}" readonly="readonly"/>
                                        {else}
                                            <input class="styler grey validate" type="text" id="email" name="from" required
                                                   data-validate="isEmail" value="{$email|escape:'html':'UTF-8'}"/>
                                        {/if}
                                    </p>
                                    <p class="form-group">
                                        <label for="phone">{l s='Phone'}</label>
                                        <input class="styler" id="phone" type="text" name="phone" value="">
                                    </p>
                                {/if}
                                <div class="form-group selector1">
                                    <label>{l s='Order reference'}</label><br/>
                                    {if !isset($customerThread.id_order) && isset($is_logged) && $is_logged}
                                        <select name="id_order" class="mw300">
                                            <option value="0">{l s='-- Choose --'}</option>
                                            {foreach from=$orderList item=order}
                                                <option value="{$order.value|intval}"{if $order.selected|intval} selected="selected"{/if}>{$order.label|escape:'html':'UTF-8'}</option>
                                            {/foreach}
                                        </select>
                                    {elseif !isset($customerThread.id_order) && empty($is_logged)}
                                        <input class="styler grey" type="text" name="id_order" id="id_order"
                                               value="{if isset($customerThread.id_order) && $customerThread.id_order|intval > 0}{$customerThread.id_order|intval}{else}{if isset($smarty.post.id_order) && !empty($smarty.post.id_order)}{$smarty.post.id_order|escape:'html':'UTF-8'}{/if}{/if}"/>
                                    {elseif $customerThread.id_order|intval > 0}
                                        <input class="styler grey" type="text" name="id_order" id="id_order"
                                               value="{if isset($customerThread.reference) && $customerThread.reference}{$customerThread.reference|escape:'html':'UTF-8'}{else}{$customerThread.id_order|intval}{/if}"
                                               readonly="readonly"/>
                                    {/if}
                                </div>


                                <div class="form-group selector1">
                                    <label for="id_contact">{l s='Subject Heading'}<sup>*</sup></label>
                                    {if isset($customerThread.id_contact) && $customerThread.id_contact && $contacts|count}
                                    {assign var=flag value=true}
                                    {foreach from=$contacts item=contact}
                                        {if $contact.id_contact == $customerThread.id_contact}
                                            <input type="text" class="styler" id="contact_name" name="contact_name"
                                                   value="{$contact.name|escape:'html':'UTF-8'}" readonly="readonly"/>
                                            <input type="hidden" name="id_contact" value="{$contact.id_contact|intval}"/>
                                            {$flag=false}
                                        {/if}
                                            {/foreach}
                                            {if $flag && isset($contacts.0.id_contact)}
                                                <input type="text" class="styler" id="contact_name" name="contact_name"
                                                       value="{$contacts.0.name|escape:'html':'UTF-8'}" readonly="readonly"/>
                                                <input type="hidden" name="id_contact" value="{$contacts.0.id_contact|intval}"/>
                                            {/if}
                                        </div>
                                        {else}
                                        <br/>
                                        <select id="id_contact" class="mw300" name="id_contact" required><br/>
                                            <option value="0">{l s='-- Choose --'}</option>
                                            {foreach from=$contacts item=contact}
                                                <option value="{$contact.id_contact|intval}"{if isset($smarty.request.id_contact) && $smarty.request.id_contact == $contact.id_contact} selected="selected"{/if}>{$contact.name|escape:'html':'UTF-8'}</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                    <p id="desc_contact0" class="desc_contact{if isset($smarty.request.id_contact)} unvisible{/if}">
                                        &nbsp;</p>
                                    {foreach from=$contacts item=contact}
                                        <p id="desc_contact{$contact.id_contact|intval}"
                                           class="desc_contact contact-title{if !isset($smarty.request.id_contact) || $smarty.request.id_contact|intval != $contact.id_contact|intval} unvisible{/if}">
                                            <i class="icon-comment-alt"></i>{$contact.description|escape:'html':'UTF-8'}
                                        </p>
                                    {/foreach}
                                    {/if}


                                <div class="form-group">
                                    <label for="message">{l s='Message'}<sup>*</sup></label>
                                    <textarea class="styler" id="message" name="message" required minlength="10">{if isset($message)}{$message|escape:'html':'UTF-8'|stripslashes}{/if}</textarea>
                                </div>

                                {if !$is_logged}
                                    <div class="form-group selector1">
                                        <label for="id_country">{l s='Country'}</label><br/>
                                        <select id="id_country" class="mw300" name="id_country">
                                            <option value="0">{l s='-- Choose --'}</option>
                                            {foreach from=$countries item=country}
                                                <option value="{$country.id_country|intval}"{if isset($smarty.request.id_country) && $smarty.request.id_country == $country.id_country} selected="selected"{/if}>{$country.name|escape:'html':'UTF-8'}</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                {/if}
                            </div>
                        </div>



                        <div class="submit">
                            <button type="submit" name="submitMessage" id="submitMessage"
                                    class="button btn btn-default button-medium"><span>{l s='Send'}<i
                                            class="icon-chevron-right right"></i></span></button>
                        </div>
                    </fieldset>
                </form>
            </div>
            <div role="tabpanel" class="tab-pane" id="for_distr">

                <form id="contact-form-distr" action="{$request_uri}" method="post" class="contact-form-box" enctype="multipart/form-data">
                    <fieldset>
                        <h4>
                            {l s="Our online store offers favorable conditions for cooperation for wholesale customers. If you are a distributor engaged in wholesale, owner of boutique or chain of shops, we are open for cooperation and are accepting applications. In order to enable us to offer you more information regarding the terms of cooperation taking into account your preferences/wishes, please answer the following questions:"}
                        </h4>

                        <span>* {l s='Required fields'}</span><br/><br/>
                        <div class="clearfix">
                            <div class="col-xs-12 col-md-8">
                                {if !$is_logged}
                                    <p class="form-group">
                                        <label for="firstname">{l s='First Name'}<sup>*</sup></label>
                                        <input class="styler" id="firstname" type="text" name="firstname" required value="">
                                    </p>
                                    <p class="form-group">
                                        <label for="lastname">{l s='Last Name'}<sup>*</sup></label>
                                        <input class="styler" id="lastname" type="text" name="lastname" required value="">
                                    </p>
                                    <p class="form-group">
                                        <label for="email">{l s='Email address'}<sup>*</sup></label>
                                        {if isset($customerThread.email)}
                                            <input class="styler grey" type="text" id="email" name="from" required
                                                   value="{$customerThread.email|escape:'html':'UTF-8'}" readonly="readonly"/>
                                        {else}
                                            <input class="styler grey validate" type="text" id="email" name="from" required
                                                   data-validate="isEmail" value="{$email|escape:'html':'UTF-8'}"/>
                                        {/if}
                                    </p>
                                    <p class="form-group">
                                        <label for="phone">{l s='Phone'}</label>
                                        <input class="styler" id="phone" type="text" name="phone" value="">
                                    </p>
                                {else}
                                       <input id="firstname" type="hidden" name="firstname" required value="{$customer->firstname}">
                                       <input id="lastname" type="hidden" name="lastname" required value="{$customer->lastname}">
                                       <input id="email" type="hidden" name="email" required value="{$customer->email}">
                                {/if}


                                    <p class="form-group">
                                        <label for="description">{l s='Brief description of your company'}<sup>*</sup></label>
                                        <input class="styler" id="description" type="text" name="description" required value="">
                                    </p>
                                    <p class="form-group">
                                        <label for="regions">{l s='Regions you are working in'}</label>
                                        <input class="styler" id="regions" type="text" name="regions" value="">
                                    </p>
                                    <p class="form-group">
                                        <label for="clients">{l s='Clients you work with'}</label>
                                        <input class="styler" id="clients" type="text" name="clients" value="">
                                    </p>

                                    <p class="form-group">
                                        <label for="regions_interested">{l s='Which region you are interested in'}<sup>*</sup></label>
                                        <input class="styler" id="regions_interested" type="text" name="regions_interested" required value="">
                                    </p>
                                    <p class="form-group">
                                        <label for="products_interested">{l s='Which products you are interested in'}<sup>*</sup></label>
                                        <input class="styler" id="products_interested" type="text" name="products_interested" required value="">
                                    </p>



                                <div class="form-group">
                                    <label for="suggestions">{l s='Other suggestions'}</label>
                                    <textarea class="styler" id="suggestions" name="suggestions">{if isset($message)}{$message|escape:'html':'UTF-8'|stripslashes}{/if}</textarea>
                                </div>

                                <p class="form-group">
                                    <label for="products_interested">{l s='You presentation file'}</label>
                                    <br/><input type="file" name="fileUpload">
                                </p>

                                {if !$is_logged}
                                    <div class="form-group selector1">
                                        <label for="id_country">{l s='Country'}</label><br/>
                                        <select id="id_country" class="mw300" name="id_country">
                                            <option value="0">{l s='-- Choose --'}</option>
                                            {foreach from=$countries item=country}
                                                <option value="{$country.id_country|intval}"{if isset($smarty.request.id_country) && $smarty.request.id_country == $country.id_country} selected="selected"{/if}>{$country.name|escape:'html':'UTF-8'}</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                {/if}
                            </div>
                        </div>



                        <div class="submit">
                            <button type="submit" name="submitMessageDistr" id="submitMessageDistr"
                                    class="button btn btn-default button-medium"><span>{l s='Send'}<i
                                            class="icon-chevron-right right"></i></span></button>
                        </div>
                    </fieldset>
                </form>
            </div>

        </div>

    </div>


{/if}
{addJsDefL name='contact_fileDefaultHtml'}{l s='No file selected' js=1}{/addJsDefL}
{addJsDefL name='contact_fileButtonHtml'}{l s='Choose File' js=1}{/addJsDefL}
