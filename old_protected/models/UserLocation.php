<?php
/*
В табл. users_location хранятся коды из таб. site
и в форму отдаются как коды, так и наименования из site
- в форме в виде checkbox = <label><input type="checkbox" name="sel_country#1" id="sel_country#1" checked>Hong Kong</label>

*/
class UserLocation extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    public function tableName() {
        return 'users_location';
    }
	// Связь с site.cod = users_location.country AND site.language = 
	// Связь с site.cod = users_location.city    AND site.language = 
	public function relations() {
		return array(
			'nm_country' => array(self::HAS_ONE, 'Site', array('cod'=>'country')),
			'nm_city'    => array(self::HAS_ONE, 'Site', array('cod'=>'city')),
		);
	}
	// id	login	country		city
    public function getAll($use) {
		$criteria = new CDbCriteria;
		$criteria->condition = " login = '{$use}' ";
		$criteria->order = "country, city";
		$list = self::model()->findAll($criteria);
		$arr = Array();
		foreach ($list as $lab) {
			$arr[$lab['id']]['country'] = $lab['country'];
			$arr[$lab['id']]['city'] = $lab['city'];
			$arr[$lab['id']]['nm_city'] = $lab->nm_city['name'];
			$arr[$lab['id']]['nm_country'] = $lab->nm_country['name'];
		};
		return $arr;
    }
}
?>