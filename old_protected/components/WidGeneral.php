<?php

class WidGeneral  extends CWidget {
	
	public $items=array();
	public $arm = array();
	public $arh = array();
	public $art = array(); // БД site select в виде таблицы
	public $ars = array(); // БД users
	public $arl = array(); // БД userl_location
	public $edit = "Y";
	// Макет
	private $vis = array("name", "last_name", "phone", "email", "skype", "agency", "website", "gender", "date_of_birth", "guardians", "locationall");
	
	public function run() {
		$pac = Yii::app()->user->getState('package');
		$lan = Yii::app()->user->getState('language');
		$uid = Yii::app()->user->getState('id_user'); // Зарегистрированный текущий пользователь = я (свой)
		$vid = Yii::app()->user->getState('ch_user'); // Пользователь чей профиль просматривает чужой пользователь (ch = чужой) или свой
		$log = Yii::app()->user->getState('login');
		
		$model=new Site;
		$this->arh = $model->getAll($lan, 'general'); // Названия всех полей для данного виджета на нужном языке
		$this->art = $model->getAllTab($lan);
		if ($this->items['cod'] == "visitor") { $this->arm = $this->vis; }; // Макет - перечень полей из БД и их последовательность
		$this->arm = $this->vis; // Макет для всех пакетов и профессий одинаков
		$modus=new Users;
		if ($vid != $uid) { $this->edit="N"; $uid=$vid;};
		$this->ars = $modus->getKart($uid);
		$modul=new UserLocation;
		$this->arl = $modul->getAll($log);
		$this->render('general');
	}
}
?>