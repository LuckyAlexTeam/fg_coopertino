<?php

/**
 * This is the model class for table "brand_form".
 *
 * The followings are the available columns in table 'brand_form':
 * @property integer $id
 * @property integer $user_id
 * @property integer $professional_expiriens
 * @property string $product_range
 * @property integer $trading
 *
 * The followings are the available model relations:
 * @property TradingList $trading0
 * @property AllProfexpiriensList $professionalExpiriens
 * @property BuyerLanguageToForm[] $buyerLanguageToForms
 * @property ProductRangeToBrandForm[] $productRangeToBrandForms
 * @property ReasonToBrandForm[] $reasonToBrandForms
 * @property ReasonToBuyerForm[] $reasonToBuyerForms
 */
class BaseBrandForm extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return BrandForm the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'brand_form';
	}

	/**
	 * @return array validation rules for model attributes.
	 */ 
	 
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, professional_expiriens, trading', 'required'),
			array('user_id, professional_expiriens, trading', 'numerical', 'integerOnly'=>true),
			array('product_range', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user_id, professional_expiriens, product_range, trading', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'trading0' => array(self::BELONGS_TO, 'TradingList', 'trading'),
			'professionalExpiriens' => array(self::BELONGS_TO, 'AllProfexpiriensList', 'professional_expiriens'),
			'buyerLanguageToForms' => array(self::HAS_MANY, 'BuyerLanguageToForm', 'buyer_form_id'),
			'productRangeToBrandForms' => array(self::HAS_MANY, 'ProductRangeToBrandForm', 'brand_form_id'),
			'reasonToBrandForms' => array(self::HAS_MANY, 'ReasonToBrandForm', 'brand_form_id'),
			'reasonToBuyerForms' => array(self::HAS_MANY, 'ReasonToBuyerForm', 'buyer_form_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'professional_expiriens' => 'Professional Expiriens',
			'product_range' => 'Product Range',
			'trading' => 'Trading',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('professional_expiriens',$this->professional_expiriens);
		$criteria->compare('product_range',$this->product_range,true);
		$criteria->compare('trading',$this->trading);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}