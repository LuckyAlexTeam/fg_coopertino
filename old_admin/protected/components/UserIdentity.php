<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate() {
		$users=User::model()->getLogPas($this->username , $this->password);
		//var_dump($users);
		/*
		$users=array(
			// username => password
			'demo'=>'demo',
			'admin'=>'admin',
		);
		*/
		if(!isset($users[$this->username]))
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		elseif($users[$this->username]!==$this->password)
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
		else  {
			$this->errorCode=self::ERROR_NONE;

			$us=User::model()->getLog($this->username);
			//
			Yii::app()->user->guestName = $this->username;
			
			
			Yii::app()->user->setState('login', $this->username); 
			$this->setState('login', $this->username); 
			//print "<br> ===== getState = " . Yii::app()->user->getState('login'); 
			$this->setState('id', $us['id']); 
			$this->setState('name', $us['name']); 
			$this->setState('rol', $us['rol']); 
			//$this->setState('sipnom', $us['sipnom']);
			//$this->setState('sippas', $us['sippas']);
			$this->setState('guestName', $this->username);
			//$this->setState('title', $this->username);
		};
		return !$this->errorCode;
	}
}