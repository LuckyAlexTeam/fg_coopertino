<?php
/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class UsersForm extends CFormModel
{
	public $nom;
	public $nom2;
    public $rememberMe;
	public $attributes;
	private $_identity;
	
	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
	//var_dump (Yii::app());
	print "/protected/models/AkciiForm.php - function rules() <br>";
		return array(
			// name, email, subject and body are required
			array('nom', 'nom2'),
			// rememberMe needs to be a boolean
			array('rememberMe', 'boolean'),
			// email has to be a valid email address
			//array('email', 'email'),
			// verifyCode needs to be entered correctly
			//array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
	print "/protected/models/AkciiForm.php - function attributeLabels() <br>";
		return array(
			'rememberMe'=>'Remember me next time',
		);
	}
	
	
}