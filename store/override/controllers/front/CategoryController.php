<?php
class CategoryController extends CategoryControllerCore {

    /**
     * Initializes page content variables
     */
    public $category_public;

    public function setMedia()
    {
        parent::setMedia();
//        $this->addJqueryPlugin('formstyler');
//        $this->addJqueryPlugin('selecter');
    }

    public function initContent()
    {
        parent::initContent();

        if ( $this->category->level_depth == 4 ) {

            $parents = $this->category->getParentsCategories();

            $this->context->smarty->assign(array(
                'brand_name' => $parents[2]['name'],
                'brand_id' => $parents[2]['id_category']
            ));

        }

    }

}