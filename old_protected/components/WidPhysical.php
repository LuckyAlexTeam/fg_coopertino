<?php

class WidPhysical  extends CWidget {
	
	public $items=array();
	public $arm = array();
	public $arh = array();
	public $aro = array(); // БД site select в виде option
	public $art = array(); // БД site select в виде таблицы
	public $ars = array(); // БД users
	public $arl = array(); // БД userl_location
	public $edit = "Y";
	// Макет
	private $vis = array("height", "weight", 'chest', 'waist', 'hips', 'dress', 'cup', 'shoe', 'ethnicity', 'skin_color', 'eye_color', 'natural_hair_color',
		'hair_coloration', 'readiness_to_change_color', 'hair_length', 'tattoos', 'piercing');
	public  $fff = array("inp"   , "inp"   , 'inp'  , 'inp'  , 'inp' , 'inp'  , 'inp', 'inp' , 'sel'      , 'sel'       , 'sel'      , 'sel'               ,
		'inp'            , 'inp'                      , 'sel'        , 'sel'    , 'sel'     );
	public function run() {
		$pac = Yii::app()->user->getState('package');
		$lan = Yii::app()->user->getState('language');
		$uid = Yii::app()->user->getState('id_user'); // Зарегистрированный текущий пользователь = я (свой)
		$vid = Yii::app()->user->getState('ch_user'); // Пользователь чей профиль просматривает чужой пользователь (ch = чужой) или свой
		$log = Yii::app()->user->getState('login');
		
		$model=new Site;
		$this->arh = $model->getAll($lan, 'physical'); // Названия всех полей для данного виджета на нужном языке
		$this->aro = $model->getAll($lan, 'select');
		$this->art = $model->getAllTab($lan);
		if ($this->items['cod'] == "visitor") { $this->arm = $this->vis; }; // Макет - перечень полей из БД и их последовательность
		$this->arm = $this->vis; // Макет для всех пакетов и профессий одинаков
		$modus=new Users;
		if ($vid != $uid) { $this->edit="N"; $uid=$vid;};
		$this->ars = $modus->getKart($uid);
		$modul=new UserLocation;
		$this->arl = $modul->getAll($log);
		$this->render('physical');
	}
}
?>