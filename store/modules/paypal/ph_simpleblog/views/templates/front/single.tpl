<div class="blogpost">
	<div class="row">
		<div class="navigation fbc clearfix">
			<a href="{$link->getPageLink('module-ph_simpleblog-list')}?p={$bp}" class="follow_back">Назад</a>
			<div class="fade_bread_crumbs" style="display: none">
				<ul class="clearfix">
					{*<li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">*}
						{*<a href="/trends" itemprop="url"><span itemprop="title">Блог</span></a>*}
					{*</li>*}
					<li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
						<span itemprop="title">{$post->meta_title}</span>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<h1>{$post->meta_title}</h1>

	<div class="blog-date">{$post->category}, {$post->date_add|date_format:Configuration::get('PH_BLOG_DATEFORMAT')}</div>

	{if $post->featured_image}
		<div class="page_cover">
			<img src="{$post->featured_image}"  alt="{$post->meta_title}" width="100%" />
		</div>
	{/if}

	<div class="row">
		<div class="col-md-12">
			<div class="is_post" style="overflow: auto;">
				{$post->content}
			</div>

			{*<div class="social_block">*}
				{*<div>Розповiдай друзям:</div>*}
				{*<div class="clearfix">*}
				{*</div>*}
			{*</div>*}

			<br/>
			<br/>
			<div class="related_posts">
				<div class="row">
					<div class="col-md-6">
						{if $postPrev}
							<a href="{$postPrev->url}">
								<div class="title">{$postPrev->meta_title}</div>
								<div class="target">{$postPrev->category}, {$postPrev->date_add|date_format:Configuration::get('PH_BLOG_DATEFORMAT')}</div>
							</a>
						{/if}
						&nbsp;
					</div>
					<div class="col-md-6 text-right">
						{if $postNext}
							<a href="{$postNext->url}">
								<div class="title">{$postNext->meta_title}</div>
								<div class="target">{$postNext->category}, {$postNext->date_add|date_format:Configuration::get('PH_BLOG_DATEFORMAT')}</div>
							</a>
						{/if}
						&nbsp;
					</div>
				</div>
			</div>


			<div class="disquss_holder">
				{hook h='displayDisqus' mod='ffdisqus'}
			</div>
		</div>


		{*<div class="col-md-2 text-center">*}
			{*{$RECOMENDED_HOOK}*}
			{*{hook h='blogProducts' mod='homefeatured'}*}
		{*</div>*}
	</div>
</div>




{if Configuration::get('PH_BLOG_FB_INIT')}
<script>
var lang_iso = '{$lang_iso}_{$lang_iso|@strtoupper}';
{literal}(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/"+lang_iso+"/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));



{/literal}
</script>

{/if}


<script>
$(function() {
	$('body').addClass('simpleblog simpleblog-single');
});
{literal}
!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');

(function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/plusone.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
})();
{/literal}
</script>
{*
		{if Configuration::get('PH_BLOG_DISPLAY_LIKES')}
		<div class="blog-post-likes likes_{$post->id_simpleblog_post}" onclick="addRating({$post->id_simpleblog_post});">
			<span class="likes-nb">
				{$post->likes}
			</span>
			<span class="txt">
				{l s='likes'  mod='ph_simpleblog'}
			</span>
		</div>
		{/if}
	*}
{if Configuration::get('PH_BLOG_DISPLAY_LIKES')}
<script>
$(function() {
	var simpleblog_post_id = $(".ph_simpleblog").data("post");
	if ($.cookie('guest_{$cookie->id_guest}_'+simpleblog_post_id) == "voted") 
	{
		$(".blog-post-likes span.likes-nb").addClass("voted");
	}
});

function addRating(simpleblog_post_id){	
	if ($.cookie('guest_{$cookie->id_guest}_'+simpleblog_post_id) != "voted") 
	{
		$.cookie('guest_{$cookie->id_guest}_'+simpleblog_post_id, 'voted');
		var request = $.ajax({
		  	type: "POST",
		  	url: baseDir + 'modules/ph_simpleblog/ajax.php',
		  	data: { 
			  	action:'addRating',
				simpleblog_post_id : simpleblog_post_id 
			},
			success: function(result){             
		    	var data = $.parseJSON(result);
				if (data.status == 'success') 
				{		
					$(".blog-post-likes span.likes-nb").text(data.message).addClass("voted");
				} 
				else 
				{
					alert(data.message);
				}
			}
		}); 		
	} 
	else 
	{
		$.cookie('guest_{$cookie->id_guest}_'+simpleblog_post_id, '');
		var request = $.ajax({
			type: "POST",
			url: baseDir + 'modules/ph_simpleblog/ajax.php',
			data: { 
			  	action:'removeRating',
				simpleblog_post_id : simpleblog_post_id 
			},
			success: function(result){             
		    	var data = $.parseJSON(result);
				if (data.status == 'success') 
				{		
					$(".blog-post-likes span.likes-nb").text(data.message).removeClass("voted");
				} 
				else 
				{
					alert(data.message);
				}
		    }
		});
	}
	return false;
}
</script>
{/if}