<?php

class FeatureValue extends FeatureValueCore {

    public static function getFeatureValuesWithLangActive($id_lang, $id_feature, $custom = false)
    {
        return Db::getInstance()->executeS('
			SELECT *
			FROM `'._DB_PREFIX_.'feature_value` v
			LEFT JOIN `'._DB_PREFIX_.'feature_value_lang` vl
				ON (v.`id_feature_value` = vl.`id_feature_value` AND vl.`id_lang` = '.(int)$id_lang.')
			WHERE v.`id_feature` = '.(int)$id_feature.'
				'.(!$custom ? 'AND (v.`custom` IS NULL OR v.`custom` = 0)' : '').'
				AND (v.`id_feature_value` IN (SELECT fp.`id_feature_value` FROM `'._DB_PREFIX_.'feature_product` fp INNER JOIN `'._DB_PREFIX_.'product_shop` ps ON (ps.`id_product` = fp.`id_product` AND ps.`active`=1) INNER JOIN `'._DB_PREFIX_.'stock_available` sa ON (sa.`id_product` = fp.`id_product` AND sa.`id_product_attribute`=0 AND sa.`quantity`>0)  GROUP BY fp.`id_feature_value`))
			ORDER BY vl.`value` ASC
		');
    }

}