<!-- searchpeople
<br>
свой/чужой <?=Yii::app()->user->getState('sv_ch')?> чужой <?=Yii::app()->user->getState('ch_user')?> свой <?=Yii::app()->user->getState('id_user')?> <br-->

<?php
$results_count = rand(1,999);

$getprofessional='';
if(isset($_GET['professional'])){
	$getprofessional=$_GET['professional'];
}

$getmode='';
if(isset($_GET['mode'])){
	$getmode=$_GET['mode'];
}

function function_title_select($param){
	$return = array(
		'title'=>'',
	);
	
	if(!empty($param['get'])){
		if($param['get']=='model'){
			$return['title'] = 'Модель';
		}elseif($param['get']=='fotomaster'){
			$return['title'] = 'Фотограф';
		}elseif($param['get']=='designer'){
			$return['title'] = 'Дизайнер';
		}elseif($param['get']=='agency'){
			$return['title'] = 'Агентство';
		}elseif($param['get']=='expert'){
			$return['title'] = 'Эксперт';
		}
		
		if($param['mode']=='brand'){
			$return['title'] .= ', Brand';
		}elseif($param['mode']=='company'){
			$return['title'] .= ', Company Representative';
		}elseif($param['mode']=='media'){
			$return['title'] .= ', Media';
		}elseif($param['mode']=='stylist'){
			$return['title'] .= ', Stylist';
		}elseif($param['mode']=='artist'){
			$return['title'] .= ', Hair Stylist/Artist';
		}elseif($param['mode']=='mUA'){
			$return['title'] .= ', MUA';
		}
	}
	
	if(empty($return['title'])){
		$return['title'] = 'Выберите категорию для поиска';
	}
	
	return $return;
}

$big_massiv_s_albomamy = array(
	//''=>array(''=>'',),
	array(
		'name'=>'Editorials',
		'professional'=>'model',
	),
	array(
		'name'=>'Runway',
		'professional'=>'model',
	),
	array(
		'name'=>'Beauty',
		'professional'=>'fotomaster',
	),
	array(
		'name'=>'Portraits/Celebrity',
		'professional'=>'designer',
	),
	array(
		'name'=>'Glamour/Nude',
		'professional'=>'fotomaster',
	),
	array(
		'name'=>'Commercial/Advertising',
		'professional'=>'agency',
	),
	array(
		'name'=>'Street Fashion',
		'professional'=>'fotomaster',
	),
	array(
		'name'=>'Creative Projects',
		'professional'=>'designer',
	),
	array(
		'name'=>'Spec Shoot',
		'professional'=>'expert',
		'mode'=>'media',
	),
	array(
		'name'=>'Videos',
		'professional'=>'expert',
		'defaultclass'=>'fa-video-camera',

	),
);

//print_r($_GET);
/*
+ там фильтр по профессии - вверху справа выпадающий список
+ выбрав профессию сменяется дефолтный блок и открывается перечень альбомов имеющихся в данной профессии...
+ эти миниатюрки фильтруют по альбомам - у альбомов фиксированные названия и в профиле сразу имеются, но пустые...
после выбора альбома открывается страничка на которой отображаются выбранные альбомы всех людей из выбранной профессии с фотографией альбома , которую для обложки альбома выбрали...
пустые альбомы в поиске не учавствуют...
*/
?>

<?php $this->renderPartial('application.views.site.profile_navigation');?>
<div class="search-nav visitor"><!-- ПОИСК ДЛЯ VISITOR - ВЛЕВО ВЫПАДАЕТ -->
	<div class="s-n-wrapper">
		<div class="search-type">
			<h2>Model photos</h2><!-- Сменяемая надпись в зависимости от выбраной категории поиска -->
			<span class="hr"></span>
		</div>
		<div class="prod-result">
			<p>The serch producted <?=$results_count?> results</p><!-- Надпись о колличестве найденых фото -->
		</div>
		<div class="prof-sel">
			<ul>
				<li class="parent" style="padding: 7px 25px;"><? $return=function_title_select(array('get'=>$getprofessional,'mode'=>$getmode)); echo $return['title']; ?>
					<ul>
						<li><a href="<?php echo Yii::app()->createUrl('site/search',array('professional'=>'model'))?>">Модель</a></li>
						<li><a href="<?php echo Yii::app()->createUrl('site/search',array('professional'=>'fotomaster'))?>">Фотограф</a></li>
						<li><a href="<?php echo Yii::app()->createUrl('site/search',array('professional'=>'designer'))?>">Дизайнер</a></li>
						<li><a href="<?php echo Yii::app()->createUrl('site/search',array('professional'=>'agency'))?>">Агентство</a></li>
						<li class="parent" style="padding: 10px 15px;">Эксперт
							<ul>
								<li><a href="<?php echo Yii::app()->createUrl('site/search',array('professional'=>'expert','mode'=>'brand'))?>">Brand</a></li>
								<li><a href="<?php echo Yii::app()->createUrl('site/search',array('professional'=>'expert','mode'=>'company'))?>">Company Representative</a></li>
								<li><a href="<?php echo Yii::app()->createUrl('site/search',array('professional'=>'expert','mode'=>'media'))?>">Media</a></li>
								<li><a href="<?php echo Yii::app()->createUrl('site/search',array('professional'=>'expert','mode'=>'stylist'))?>">Stylist</a></li>
								<li><a href="<?php echo Yii::app()->createUrl('site/search',array('professional'=>'expert','mode'=>'artist'))?>">Hair Stylist/Artist</a></li>
								<li><a href="<?php echo Yii::app()->createUrl('site/search',array('professional'=>'expert','mode'=>'mUA'))?>">MUA</a></li>
							</ul>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="person-wrapper">
	<? if(empty($getprofessional)) { ?>
	<div class="searchStart">
		Это отображается до выбора профессии
	</div>
	<? } ?>
		<div class="sbph">
			<h2>Choose album for searching photo</h2>
			<span class="hr"></span>
			<?
			foreach($big_massiv_s_albomamy as $key => $val){
				if(
					($getprofessional=='')
					OR
					($getprofessional==$val['professional'] AND $getmode=='')
					OR
					($getprofessional==$val['professional'] AND isset($val['mode']) AND $getmode==$val['mode'])
				){
					$defaultclass='fa-camera';
					if(isset($val['defaultclass'])){
						$defaultclass=$val['defaultclass'];
					}
					?>
					<a href="#" popuphref="<?php $getprofessionalx='noprof'; $getmodex='nomode'; if(isset($val['professional'])){$getprofessionalx=$val['professional'];} if(isset($val['mode'])){$getmodex=$val['mode'];} echo Yii::app()->createUrl('site/search',array('professional'=>$getprofessionalx,'mode'=>$getmodex,'album'=>($key+1),))?>" class="idopenModal m-j-albums fa <?=$defaultclass?>">
						<img src="" alt="">
						<div class="caption">
							<?=$val['name']?>
						</div>
					</a>
					<?
				}
			}
			?>
		</div>
<!-- ВЫВОД РЕЗУЛЬТАТОВ ПОИСКА ПО ФОТО ---------------------------------------------------------------------------------------------->
	<div class="search-results">
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
		</div>
		<div class="grid-item">
			<a href="#openModal">
				<img class="grayscale" src="">
			</a>
		</div>
		<div class="pageScroll">
			<p class="scrollLeft"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></p>
			<p class="scrollRight"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></p>
		</div>
	</div>
<!-- ВЫВОД РЕЗУЛЬТАТОВ ПОИСКА ПО ФОТО КОНЕЦ ---------------------------------------------------------------------------------------------->
</div>
<!-- Модальное окно для фото/видео -->
<div id="openModal" class="modal modal-ph">
	<div class="modalWrapper">
		<a class="close" href="#">X</a>
		<img class="popupimg" src="http://img.mota.ru/upload/wallpapers/2016/07/26/13/00/49329/mota.ru_2016072621-preview.jpg">
		<div class="modal-btn"><!-- 4 кнопки: фотограф, хозяин фото, модель, альбом в которам размещена фотография -->
			<p>Нажмите чтобы перейти на страницу</p>
			<a class="" href="#">Фотограф</a>
			<a class="" href="#">Добавил</a>
			<a class="" href="#">Модель</a>
			<a class="popupalb" href="">Альбом</a>
		</div>
		<p></p><!-- рейтинг фото -->
	</div>
</div>
<!--JS-->
<script type="text/javascript">
	$(document).ready(function(){
		$(".idopenModal").click(function(){
			var param1 = $(this).attr("popuphref");
			var param2 = $(this).children('img').attr("src");
			alert(param1);
			alert(param2);
			$("#openModal").attr("style","display:block;");
			$(".popupalb").attr("href",param1);
			$(".popupimg").attr("src",param2);
		});
		$(".close").click(function(){
			$("#openModal").attr("style","display:none;");
			$(".popupimg").attr("src","");
			$(".popupalb").attr("href","");
		});
	});
</script>
<!--JS-->