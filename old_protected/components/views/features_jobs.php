<? /* widget - Features jobs */ ?>

<h2>Featured jobs</h2>

<span class="hr"></span>

<div class="fj-wrap">
	<div class="fj-i-wrap">
		<a href=""><h3>Agency needs Hair Stilist, MUA</h3></a></br>
		<p><b>Professional Experience: </b> </p></br>
		<p><b>Compensation: </b>Payment Only</p></br>
		<p><b>Additional expenses to be covered: </b>Covered</p></br>
		<p><b>Place/Location: </b>Ukraine</p></br>
		<p><b>Number of views: </b>27</p></br>
		<p><b>Who published: </b>Agency</p></br>
		<p><b>Published: </b></p></br>
		<p class="fj-i-date">10/27/2015 12:00:01 am</p></br>
		<p><b>Date of project realization: </b></p></br>
		<p class="fj-i-date">10/27/2015 12:00:01 am</p></br>
		<p><b>Time of project duration: </b></p></br>
		<p class="fj-i-date">10/27/2015 12:00:01 am</p></br>
		<p><b>Expiration date: </b></p></br>
		<p class="fj-i-date">10/27/2015 12:00:01 am</p></br>
	</div>
	<div class="fj-i-wrap">
		<a href=""><h3>Model wanted, MUA</h3></a></br>
		<p><b>Professional Experience: </b> </p></br>
		<p><b>Compensation: </b>In exchange of experience</p></br>
		<p><b>Additional expenses to be covered: </b>Covered</p></br>
		<p><b>Place/Location: </b>London location</p></br>
		<p><b>Number of views: </b>22</p></br>
		<p><b>Who published: </b>Model</p></br>
		<p><b>Published:</b></p></br>
		<p class="fj-i-date">10/29/2015 12:00:01 am</p></br>
		<p><b>Date of project realization:</b></p></br>
		<p class="fj-i-date">10/29/2015 12:00:01 am</p></br>
		<p><b>Time of project duration:</b></p></br>
		<p class="fj-i-date">10/29/2015 12:00:01 am</p></br>
		<p><b>Expiration date:</b></p></br>
		<p class="fj-i-date">10/29/2015 12:00:01 am</p></br>
	</div>
	<div class="fj-i-wrap">
		<a href=""><h3>Model looking for photografer</h3></a></br>
		<p><b>Professional Experience: </b>1-5 years</p></br>
		<p><b>Compensation: </b>Payment Only</p></br>
		<p><b>Additional expenses to be covered: </b>Covered</p></br>
		<p><b>Place/Location: </b>London</p></br>
		<p><b>Number of views: </b>15</p></br>
		<p><b>Who published: </b>Model</p></br>
		<p><b>Published:</b></p></br>
		<p class="fj-i-date">10/30/2015 12:00:01 am</p></br>
		<p><b>Date of project realization:</b></p></br>
		<p class="fj-i-date">10/30/2015 12:00:01 am</p></br>
		<p><b>Time of project duration:</b></p></br>
		<p class="fj-i-date">10/30/2015 12:00:01 am</p></br>
		<p><b>Expiration date::</b></p></br>
		<p class="fj-i-date">10/30/2015 12:00:01 am</p></br>
	</div>
</div>