<!-- Block user information module NAV  -->
{if $is_logged}
	<div class="header_user_info">
			<a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" title="{l s='View my customer account' mod='blockuserinfo'}" class="account" rel="nofollow">
				<span>{$cookie->customer_firstname} {$cookie->customer_lastname}</span>
				<i class="icon-user right"></i>
			</a>
	</div>
{/if}
<div class="header_user_info">
	{if $is_logged}
		<a class="logout" href="{$link->getPageLink('index', true, NULL, "mylogout")|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Log me out' mod='blockuserinfo'}">
			<span>{l s='Sign out' mod='blockuserinfo'}</span>
			<i class="icon-power-off right"></i>
		</a>
	{else}
		<a class="login" href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Log in to your customer account' mod='blockuserinfo'}">
			<span>{l s='Sign in' mod='blockuserinfo'}</span>
			<i class="icon-key right"></i>
		</a>
	{/if}
</div>
<!-- /Block usmodule NAV -->
