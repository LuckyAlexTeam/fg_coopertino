<?php
$module = 'index';
if ($_SERVER['REQUEST_URI'] != '/') {
    try {
        // Для того, что бы через виртуальные адреса можно было также передавать параметры
        // через QUERY_STRING (т.е. через "знак вопроса" - ?param=value),
        // необходимо получить компонент пути - path без QUERY_STRING.
        // Данные, переданные через QUERY_STRING, также как и раньше будут содержаться в
        // суперглобальных массивах $_GET и $_REQUEST.
        $url_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
 
        // Разбиваем виртуальный URL по символу "/"
        $uri_parts = explode('/', trim($url_path, ' /'));
 
        // Если количество частей не кратно 2, значит, в URL присутствует ошибка и такой URL
        // обрабатывать не нужно - кидаем исключение, что бы назначить в блоке catch модуль и действие,
        // отвечающие за показ 404 страницы.
//        if (count($uri_parts) % 2) {
//            throw new Exception();
//        }
 
        $module = array_shift($uri_parts); // Получили имя модуля
        //$action = array_shift($uri_parts); // Получили имя действия
 
        // Получили в $params параметры запроса
//        for ($i=0; $i < count($uri_parts); $i++) {
//            $params[$uri_parts[$i]] = $uri_parts[++$i];
//        }
    } catch (Exception $e) {
        $module = '404';
        //$action = 'main';
    }
}
print "<!-- {$module} -->";


DEFINE("TRASSA", "N");
$yii='/home/luckyale/yii/framework/yii.php';
$config=dirname(__FILE__).'/protected/config/main.php';
defined('YII_DEBUG') or define('YII_DEBUG',true);
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);
DEFINE('ROOT_WWW', '/home/luckyale/public_html/fashingreatness.ru');
require_once($yii);
Yii::createWebApplication($config)->run();
?>