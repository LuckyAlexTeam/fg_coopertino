<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{gsitemap}prestashop>gsitemap_3aaaafde6f9b2701f9e6eb9292e95521'] = 'Google 網站導覽';
$_MODULE['<{gsitemap}prestashop>gsitemap_935a6bc13704a117d22333c3155b5dae'] = '產生您的Google網站導覽檔案';
$_MODULE['<{gsitemap}prestashop>configuration_1c95b77d135b55c84429588c11697ea4'] = '您的網站導覽';
$_MODULE['<{gsitemap}prestashop>configuration_a0bfb8e59e6c13fc8d990781f77694fe'] = '繼續';
$_MODULE['<{gsitemap}prestashop>configuration_0b9b4b91e0a8f59e264202a23d9c57a6'] = '配置您的網站導覽';
$_MODULE['<{gsitemap}prestashop>configuration_f9f90eeaf400d228facde6bc48da5cfb'] = '時常';
$_MODULE['<{gsitemap}prestashop>configuration_745fd0ea7f576f350a0eed4b8c48a8e2'] = '每小時';
$_MODULE['<{gsitemap}prestashop>configuration_bea79186fd7af2da67e59b4b15df5a26'] = '每天 ';
$_MODULE['<{gsitemap}prestashop>configuration_4a11fc05ed694c195f0703605b64da90'] = '每週';
$_MODULE['<{gsitemap}prestashop>configuration_708638881f3bac9d9c8c742c79502811'] = '每月 ';
$_MODULE['<{gsitemap}prestashop>configuration_1bf712896e6077fa0b708e911d8ee0b3'] = '每年 ';
$_MODULE['<{gsitemap}prestashop>configuration_c7561db7a418dd39b2201dfe110ab4a4'] = '永不';
$_MODULE['<{gsitemap}prestashop>configuration_05ff8159ccaef6f0f8391c61a6d0e631'] = '全選';
$_MODULE['<{gsitemap}prestashop>configuration_2ec111ec9826a83509c02bf5e6b797f1'] = '生成網站導覽';
$_MODULE['<{gsitemap}prestashop>configuration_6caa369fd774beef106abbc5cc1e3368'] = '手動：';
$_MODULE['<{gsitemap}prestashop>configuration_3d263eb0233f14872193733387840c80'] = '-或-';
$_MODULE['<{gsitemap}prestashop>configuration_957d27165d1dc5947fb00e57967ffcce'] = '自動：';
$_MODULE['<{gsitemap}prestashop>configuration_98a9d595be84a0687c4b26887977e0c3'] = '全部取消 ';


return $_MODULE;
