<?php

/**
 * This is the model class for table "agency_form".
 *
 * The followings are the available columns in table 'agency_form':
 * @property integer $id
 * @property integer $user_id
 * @property integer $gender
 * @property integer $age_type
 * @property string $other_requirements
 * @property integer $professional_experience
 * @property integer $total_models
 * @property string $type_specialization
 *
 * The followings are the available model relations:
 * @property AgencyAgeType $ageType
 * @property AgencySpecializationToForm[] $agencySpecializationToForms
 * @property ReasonToAgencyForm[] $reasonToAgencyForms
 */
class BaseAgencyForm extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AgencyForm the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'agency_form';
	}

	/**
	 * @return array validation rules for model attributes.
	 */ 
	 
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, gender, age_type, professional_experience, total_models', 'required'),
			array('user_id, gender, age_type, professional_experience, total_models', 'numerical', 'integerOnly'=>true),
			array('type_specialization', 'length', 'max'=>255),
			array('other_requirements', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user_id, gender, age_type, other_requirements, professional_experience, total_models, type_specialization', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'ageType' => array(self::BELONGS_TO, 'AgencyAgeType', 'age_type'),
			'agencySpecializationToForms' => array(self::HAS_MANY, 'AgencySpecializationToForm', 'agency_form_id'),
			'reasonToAgencyForms' => array(self::HAS_MANY, 'ReasonToAgencyForm', 'agency_form_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'gender' => 'Gender',
			'age_type' => 'Age Type',
			'other_requirements' => 'Other Requirements',
			'professional_experience' => 'Professional Experience',
			'total_models' => 'Total Models',
			'type_specialization' => 'Type Specialization',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('gender',$this->gender);
		$criteria->compare('age_type',$this->age_type);
		$criteria->compare('other_requirements',$this->other_requirements,true);
		$criteria->compare('professional_experience',$this->professional_experience);
		$criteria->compare('total_models',$this->total_models);
		$criteria->compare('type_specialization',$this->type_specialization,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}