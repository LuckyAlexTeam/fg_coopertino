<?php

if (!defined('_PS_VERSION_'))
    exit;

require_once(__DIR__.'/Sizetable.php');

class Ffsizes extends Module
{

    public function __construct()
    {
        $this->name = 'ffsizes';
        $this->tab = 'advertising_marketing';
        $this->version = '1.0';
        $this->author = 'Sergii Khoroshko khoroshko@gmail.com';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.5', 'max' => _PS_VERSION_);
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Custom Sizes Module');
        $this->description = $this->l('Adding custom sizes block in product page.');
        $this->confirmUninstall = $this->l('Вы уверены что хотите деинсталировать модуль?');


        $this->img_dir = _PS_IMG_DIR_;
        $this->img_height = 300;
        $this->img_width = 490;
        $this->max_image_size = 4000000;
        $this->imageType = 'jpg';


    }


    public function install()
    {
        if (!parent::install() ||
            !$this->registerHook('showSizesTable') ||
            !$this->installSql() ||
            !$this->registerHook('displayHeader'))
            return false;
        return true;
    }

    protected function installSql()
    {
        $return = true;

        $return = $return && Db::getInstance()->execute('
		CREATE TABLE IF NOT EXISTS  `'._DB_PREFIX_.'ffsizes_sizetable` (
			`id_sizetable` INT NOT NULL AUTO_INCREMENT,
			`id_brand_category` INT NOT NULL,
		PRIMARY KEY (`id_sizetable`),
		INDEX (`id_brand_category`)
		)  ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;');

        $return = $return && Db::getInstance()->execute('
		CREATE TABLE IF NOT EXISTS  `'._DB_PREFIX_.'ffsizes_sizetable_lang` (
			`id_sizetable` INT NOT NULL,
			`id_lang` INT NOT NULL,
			`title` varchar(100),
			`html` text,
		PRIMARY KEY (`id_sizetable`, `id_lang`),
		INDEX (`id_sizetable`)
		)  ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;');

        $return = $return && Db::getInstance()->execute('
		CREATE TABLE IF NOT EXISTS  `'._DB_PREFIX_.'ffsizes_sizetable_category` (
			`id_category` INT NOT NULL,
			`id_sizetable` INT NOT NULL ,
		PRIMARY KEY (`id_category`, `id_sizetable`),
		INDEX (`id_sizetable`)
		)  ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;');

        return $return;
    }

    public function uninstall()
    {

        if (!parent::uninstall() ||
            !$this->uninstallSql())
            return false;

        return true;
    }

    protected function uninstallSql()
    {
        Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'ffsizes_sizetable`');
        Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'ffsizes_sizetable_lang`');
        Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'ffsizes_sizetable_category`');

        return true;
    }

    public function hookDisplayHeader()
    {
        if ($this->context->controller->php_self == 'product'){
            $this->context->controller->addJs(_MODULE_DIR_.$this->name.'/views/js/product.js');
            $this->context->controller->addCss(_MODULE_DIR_.$this->name.'/views/css/product.css');
        }

    }

    public function hookShowSizesTable($params)
    {
        $attribute_name = $params['attribute_name'];
        $id_category = $params['id_category'];

        $sizes_lang = array(
            'Size',
            'Размер',
        );

        if (!in_array($attribute_name, $sizes_lang))
            return '';

        if ($html = Sizetable::getHtmlByIdCategory((int)$id_category, $this->context->language->id)) {
            $this->context->smarty->assign( array(
                'html' => $html
            ));


            return $this->context->smarty->fetch(__DIR__.'/views/templates/product.tpl');
        }


        return '';
    }

    public function getContent()
    {
        $this->context->controller->addJs(_MODULE_DIR_.$this->name.'/views/js/front.js');

        $this->postProcess();

        $id_home_category = 0;

        $home_categories = Category::getHomeCategories($this->context->language->id);

        if (Tools::getValue('id_home_category')) {

            if (Tools::getIsset('add_table') || Tools::getIsset('edit_table')) {
                return $this->renderForm();
            }

            $id_home_category = (int)Tools::getValue('id_home_category');
            $size_tables = Sizetable::getByBrandId($id_home_category);

            $this->context->smarty->assign( array(
                'size_tables' => $size_tables,
            ));


        }

        $this->context->smarty->assign( array(
            'home_categories' => $home_categories,
            'id_home_category' => $id_home_category,
            'module_link' => AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules')
        ));


        return $this->context->smarty->fetch(__DIR__.'/views/templates/ffsizes.tpl');
    }

    protected function postProcess()
    {
        if (Tools::getValue('del_table')){
            $sizetable = new Sizetable((int)Tools::getValue('del_table'));
            $sizetable->delete();

            Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules').'&configure='.$this->name.'&id_home_category='.(int)Tools::getValue('id_home_category'));
        }

        if(Tools::isSubmit('submitNewTable')){
            $sizetable = new Sizetable();
            $sizetable->copyFromPost();
            $sizetable->save();
            $sizetable->updateCategoriesId(Tools::getValue('id_category'));

            Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules').'&configure='.$this->name.'&id_home_category='.(int)Tools::getValue('id_home_category'));
        }

        if(Tools::isSubmit('submitEditTable')){
            $sizetable = new Sizetable((int)Tools::getValue('id_sizetable'));
            $sizetable->copyFromPost();
            $sizetable->save();
            $sizetable->updateCategoriesId(Tools::getValue('id_category'));

            Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules').'&configure='.$this->name.'&id_home_category='.(int)Tools::getValue('id_home_category'));
        }



    }

    protected function renderForm()
    {
        $root = Category::getRootCategory();

        //Generating the tree for the first column
        $tree = new HelperTreeCategoriesCore('id_category'); //The string in param is the ID used by the generated tree
        $tree->setUseCheckBox(true)
            ->setFullTree(true)
            ->setAttribute('is_category_filter', $root->id)
            ->setRootCategory($root->id)
            ->setInputName('id_category'); //Set the name of input. The option "name" of $fields_form doesn't seem to work with "categories_select" type

        if (Tools::getValue('edit_table')){
            $tree->setSelectedCategories(Sizetable::getCategoriesId((int)Tools::getValue('edit_table')));
        }

        $categoryTree = $tree->render();

        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => Tools::getIsset('edit_table') ? $this->l('Edit Sizes table') : $this->l('Create new Sizes table'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => 'hidden',
                        'name' => 'id_brand_category',
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Title'),
                        'name' => 'title',
                        'lang' => true,
                    ),
                    array(
                        'type'  => 'categories_select',
                        'label' => $this->l('Categories'),
                        'desc'    => $this->l('Categories , where to show sizes table'),
                        'name'  => 'id_category',
                        'category_tree'  => $categoryTree //This is the category_tree called in form.tpl
                    ),
                    array(
                        'type' => 'textarea',
                        'label' => $this->l('Sizes table:'),
                        'name' => 'html',
                        'lang' => true,
                        'rows' => 5,
                        'cols' => 40,
                        'autoload_rte' => true,
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save')
                )
            )
        );

        if (Tools::getValue('edit_table')){
            $fields_form['form']['input'][] = array(
                'type' => 'hidden',
                'name' => 'id_sizetable',
            );
        }


        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->show_cancel_button = true;
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form = array();

        $helper->identifier = $this->identifier;
        $helper->submit_action = Tools::getIsset('edit_table') ? 'submitEditTable' : 'submitNewTable';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&id_home_category='.(int)Tools::getValue('id_home_category');
        $helper->back_url = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&id_home_category='.(int)Tools::getValue('id_home_category');
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array($fields_form));
    }

    public function getConfigFieldsValues()
    {
        $return =  array(
            'id_brand_category' => (int)Tools::getValue('id_home_category'),
        );

        if (Tools::getValue('edit_table')){

            $sizetable = new Sizetable((int)Tools::getValue('edit_table'));

            $return = array_merge($return, array(
                'title' => $sizetable->title,
                'html' => $sizetable->html,
                'id_sizetable' => (int)Tools::getValue('edit_table'),
            ));
        }

        return $return;
    }


}