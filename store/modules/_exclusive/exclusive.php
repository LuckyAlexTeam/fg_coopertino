<?php
/* 
* 2016 Exebeche
* First Module
* Free to use with link to http://kololobo.ru on your site
*/
if (!defined('_PS_VERSION_'))
	exit;

class Exclusive extends Module
{
	public function __construct()
	{
		$this->name = 'exclusive';
		$this->tab = 'front_office_features';
		$this->version = '1.0.0';
		$this->author = 'Alexandr Stepanov';
		$this->need_instance = 0;
		$this->ps_version_compliancy =  array('min' => '1.6', 'max' => _PS_VERSION_ );
		$this->bootstrap = true;

		parent::__construct();

		$this->displayName = $this->l('Exclusive');
		$this->description = $this->l('Модуль для эксклюзива на StoreFG');

		$this->confirmUninstall = $this->l('Уверены, что хотите удалить данный модуль?');

		$this->warning = $this->l('No name provided');
	}
	public function install()
	{
		if (Shop::isFeatureActive())
			Shop::setContext(Shop::CONTEXT_ALL);
		if (!parent::install() ||
			!$this->registerHook('displayHomeTab') ||
			!$this->registerHook('displayHomeTabContent')
			return false;
		)
		return true;
	}
	public function displayHomeTabContent ($param)
	$lang_id = (int)Context::getContext()->language->id;
	$category = new Category(1, $lang_id); // 777 - желаемая категория "эксклюзива"
	$products = $category->getProducts($lang_id, 1, 8, 'position'); // 1 - номер страницы (оставить таким же), 8 - количество продуктов на страницу, 'position' - сортировка по позиции в категории
	$this->smarty->assign(
		array(
			'products' => $products,
			'homeSize' => Image::getSize(ImageType::getFormatedName('home')), //размер изображения, оставить таким же
		)
	);
	return $this->display(__FILE__, 'exclusive.tpl');
}