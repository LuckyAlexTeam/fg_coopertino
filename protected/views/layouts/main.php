<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="language" content="ru">
<!-- blueprint CSS framework -->
<link rel="stylesheet" type="text/css" href="/css/screen.css" media="screen, projection">
<link rel="stylesheet" type="text/css" href="/css/jquery-ui.css">
<link rel="stylesheet" href="/css/bootstrap.min.css">
<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="/css/ie.css" media="screen, projection" />
<![endif]-->
<link href="http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="/css/main.css">
<link rel="stylesheet" type="text/css" href="/css/form.css">
<link rel="stylesheet" type="text/css" href="/css/customers-style.css">
<!-- <script type="text/javascript" src="/js/jquery-1.11.3.js"></script> -->
<script type="text/javascript" src="/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="/js/jquery-ui.js"></script>
<script type="text/javascript" src="/js/jquery.cookie.js"></script>
<link rel="icon" href="favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<title>МодаVisitor</title>
<meta name="keywords" content="key index">
<meta name="description" content="des index">
<link rel="stylesheet" href="/fonts/awesome/css/font-awesome.min.css">
<!--[if IE 7]>
	<link rel="stylesheet" href="/fonts/awesome/css/font-awesome-ie7.min.css">
<![endif]-->
</head>
<body>
<div class="container">
	<div class="head" style="<?php echo ($this->id == 'site' && $this->action->id == 'index')?'position:fixed;':'';?>">
		<div class="lang">
                    <a style="color:#fff;" href="<?php echo Yii::app()->createUrl('site/language',array('id'=>'en'));?>"><div class='lang_N tlanguage' data='N'>en</div></a>
                    <a style="color:#fff;" href="<?php echo Yii::app()->createUrl('site/language',array('id'=>'ru'));?>"><div class='lang_Y tlanguage' data='Y'>ru</div></a>			
                    <a style="color:#fff;" href="<?php echo Yii::app()->createUrl('site/language',array('id'=>'fr'));?>"><div class="lang_N tlanguage" data="N">fr</div></a>
                    <a style="color:#fff;" href="<?php echo Yii::app()->createUrl('site/language',array('id'=>'ch'));?>"><div class="lang_N tlanguage" data="N">ch</div></a>
		</div>
		<div class="title">
			<a class="logo" href="/">
				<span class="slogan">эксклюзивная презентация в сети</span>
			</a>
			<div class="clear"></div>
		</div>
                <?php if (Yii::app()->user->isGuest){?>
		<div class="menu1" id="top-menu">
			<ul>
				<li class="active">
					<a href="#b1">ДОМОЙ</a>
				</li>
				<li>
					<a href="#b2">О НАС</a>
				</li>
				<li>
					<a href="#b3">ПРЕИМУЩЕСТВА</a>
				</li>
				<li>
					<a href="#b4">КОМАНДА</a>
				</li>
				<li>
					<a href="#b5">ЦЕНОВЫЕ ПАКЕТЫ</a>
				</li>
				<li>
					<a href="#b6">СВЯЖИТЕСЬ С НАМИ</a>
				</li>
			</ul>
				<div class="clear"></div>
		</div>
                <?php } else {?>
                <div class="c-navigation">
			<div class="c-nav-text" style="margin-right: 20px;">
				<p><a href="<?php echo Yii::app()->createUrl('site/profile',array('id'=>Yii::app()->user->id)); ?>">
					<!--
						задание от Александра 20160811:
						В шапке справа, вместо логина/мейла надпись "Мой профиль". 
					-->
					Мой профиль
					<!--<?php echo Yii::app()->user->name;?>-->
				</a></p>
				<p><a href="./site/settings">Настройки</a>&nbsp;
                                    <a href="<?php echo Yii::app()->createUrl('site/logout');?>">Выход</a></p>
			</div>
			<!-- <div class="c-nav-img">
                             <?php if (!empty(Yii::app()->user->avatar)){?>
				<img style="width:40px;" id="avatar_img_small" alt="Фотография профиля" src="../img_avatar_small/no_avatar.png">
                             <?php } ?>
			</div> -->
		</div>
                <?php }?>
	</div>
	<div class="content">
             <?php echo $content;?>
        </div>
    
<div id="lay-hid_signin" class="lay-hid">
	<div class="lay-hid-wrap">
		<div id="lay-hid_signin_close" class="lay-hid_close">Х</div>
		<div id="lay-hid_signin_forma" class="sign-form">
                    <form id="form_sig" action="<?php echo Yii::app()->createUrl('site/login');?>" method="post">
			<h1>АВТОРИЗОВАТЬСЯ</h1>
			<input class="form-th" type="text" name="LoginForm[username]" id="sig_mai" placeholder="E-MAIL">
			<input class="form-th" type="password" name="LoginForm[password]" id="sig_pas" placeholder="ПАРОЛЬ">
			<div class="clear"></div>
			<label for="checkbox" class="custom-radio chckbx"><input id="checkbox" type="checkbox"  val="" name="LoginForm[rememberMe]"><span><div></div></span><p>ЗАПОМНИТЬ МЕНЯ</p></label>
			<input type="button" id="lay-hid_sigin_fyp" class="form-fgt" value="ЗАБЫЛИ ПАРОЛЬ?">
			<div id="sig_error">
			
			</div>
			<div class="clear"></div>
			<input type="submit" id="lay-hid_sigin_but" class="form-btn" value="ВОЙТИ">
			</form>
		</div>
	</div>
</div>

<div id="lay-hid_forgot" class="lay-hid">
	<div class="lay-hid-wrap">
		<div id="lay-hid_forgot_close" class="lay-hid_close">Х</div>
		<div id="lay-hid_forgot_forma" style="margin:auto; width: 30%; text-align:center; color:#fff; margin-top: -20%;">
			<h1>ЗАБЫЛИ ПАРОЛЬ</h1>
			<input class="form-field" type="text" id="for_mai" placeholder="E-MAIL">
			<p>ЕСЛИ ВЫ ЗАБЫЛИ ПАРОЛЬ, ВВЕДИТЕ E-MAIL. ВАША ИНФОРМАЦИЯ БУДЕТ <br> ОПРАВЛЕНА ВАМ ПО ЭЛЕКТРОННОЙ ПОЧТЕ</p>
			<input type="button" id="lay-hid_forgot_but" class="form-btn" value="ОТПРАВИТЬ">
		</div>
	</div>
</div>
    





<script>
var tek_slid = 0;
var mou_dow = 0;
var mou_x = 0;
var mou_t = 0;
var napr = '';
$(document).ready(function() {
			$('#b6_but').click( function() {
		p1 = $('#b6_1').val();
		p2 = $('#b6_2').val();
		p3 = $('#b6_3').val();
		p4 = $('#b6_4').val();
		//alert(p1+p2+p3+p4);
		$('#b6_soo').css('display','block');
		$.ajax({
			url: 'index.php?r=site/soo',
			type: 'post',
			async: true,
			data: 'nam='+$('#b6_1').val() +'&mai='+$('#b6_2').val() +'&sel='+$('#b6_3').val() +'&soo='+$('#b6_4').val(),
			dataType: 'json',
			success: function(json) {
				
			}
		});
		/*
		sme1 = $('.item').css('width');
		sme2 = Number(sme1.replace(/\D+/g,""));
		sme3 = sme2 * $(this).attr('sme');
		tek_slid = $(this).attr('sme');
		$("#ulsl").animate({ left: sme3}, { duration: 800, easing: "swing" });
		$(".c-p").removeClass("current");
		$(this).parent().addClass("current");
		*/
		return false;
	});
		$('.slid').click( function() {
		sme1 = $('.item').css('width');
		sme2 = Number(sme1.replace(/\D+/g,""));
		sme3 = sme2 * $(this).attr('sme');
		tek_slid = $(this).attr('sme');
		$("#ulsl").animate({ left: sme3}, { duration: 800, easing: "swing" });
		$(".c-p").removeClass("current");
		$(this).parent().addClass("current");
		return false;
	});
	$(window).resize(function () {
		sme1 = $('.item').css('width');
		sme2 = Number(sme1.replace(/\D+/g,""));
		sme3 = sme2 * tek_slid;
		$("#ulsl").css('left',sme3);
		//console.log(tek_slid, sme1);
	});
		//
	$('.item').mousemove(function(eventObject){
		if (mou_dow == 1) {
			sme1 = $("#ulsl").css('left');
			sme2 = Number(sme1.replace(/\D+/g,"")); // Math.abs(  
			mou_t = eventObject.pageX;
			//if (mou_x > mou_t) { smed = mou_x - mou_t; } else { smed = mou_t - mou_x;};
			smed = mou_x - mou_t;
			sme3 = (sme2 + Number(smed)) * (-1);
			$("#ulsl").css('left',sme3);
			mou_x = mou_t;
			if (smed > 0) {napr = 'L'};
			if (smed < 0) {napr = 'R'};
			if (smed == 0) {napr = ''};
			//console.log('mou_dow=' +mou_dow+ ' mou_x=' + mou_x + ' sme1=' + sme1 + " sme2="+sme2 + " smed="+smed + 'napr='+ napr + ' X='+eventObject.pageX            + " Y="+eventObject.pageY);
		};
	});
		$('.item').mousedown(function(eventObject){
		mou_dow = 1; mou_x = eventObject.pageX; mou_t = mou_x;
	});
		$('.item').mouseup(function(eventObject){
		mou_dow = 0; 
		if ((napr == 'L') && (tek_slid != -4)) { --tek_slid; };
		if ((napr == 'R') && (tek_slid != 0) ) { ++tek_slid; };
		$('#s'+tek_slid).click();
	});
		$('.item').mouseout(function(){
		mou_dow = 0; napr = ''; // console.log(mou_dow);
	});
		$('#but_join').click( function() {
		$('#lay-hid_join').css('display','block');
		$("#lay-hid_join").animate({ opacity: 1}, { duration: 1000, easing: "swing" });
		$("#lay-hid_join_forma").animate({ 'margin-top': 20}, { duration: 1000, easing: "swing" });
	});
		$('#lay-hid_join_close').click( function() {
		$("#lay-hid_join").fadeOut('slow', function() {
			$("#lay-hid_join_forma").css('margin-top','-20%');
			$('#lay-hid_join').css('display','none');
			$('#lay-hid_join').css('opacity','0.1');
		});
	});
		$('#reg_mai').change( function() {
		//alert('ok');
		$.ajax({
			url: 'index.php?r=site/email',
			type: 'post',
			async: true,
			data: 'email='+$('#reg_mai').val(),
			dataType: 'json',
			success: function(json) {
				if (json['otv'] == "ok") { $('#error').text(""); return;};
				$('#error').text("Такой email уже есть");
			}
		});
	});
		$('[id ^= "reg_pa"]').change( function() {
		if ($('#reg_pa1').prop('checked')) {
			$('#reg_gr1').css('display','none');
		} else {
			$('#reg_gr1').css('display','inline-block');
		};
	});

		$('#but_signin').click( function() {
		$('#lay-hid_signin').css('display','block');
		$("#lay-hid_signin").animate({ opacity: 1}, { duration: 1000, easing: "swing" });
		$("#lay-hid_signin_forma").animate({ 'margin-top': 20}, { duration: 1000, easing: "swing" });
	});
		$('#lay-hid_signin_close').click( function() {
		$("#lay-hid_signin").fadeOut('slow', function() {
			$("#lay-hid_signin_forma").css('margin-top','-20%');
			$('#lay-hid_signin').css('display','none');
			$('#lay-hid_signin').css('opacity','0.1');
		});
	});
		$('#form_sig').submit( function() {
		soo = "";
		if ($('#sig_mai').val() == "") {soo += "<br>Email пустой"; };
		if ($('#sig_pas').val() == "") {soo += "<br>Пароль пустой"; };
		if (soo != "") {$('#sig_error').html(soo); return false;};
		return true;
	});
		$('#lay-hid_sigin_fyp').click( function() {
		$('#lay-hid_signin_close').click(); 		$('#lay-hid_forgot').css('display','block');
		$("#lay-hid_forgot").animate({ opacity: 1}, { duration: 1000, easing: "swing" });
		$("#lay-hid_forgot_forma").animate({ 'margin-top': 20}, { duration: 1000, easing: "swing" });
	});
		$('#lay-hid_forgot_close').click( function() {
		$("#lay-hid_forgot").fadeOut('slow', function() {
			$("#lay-hid_forgot_forma").css('margin-top','-20%');
			$('#lay-hid_forgot').css('display','none');
			$('#lay-hid_forgot').css('opacity','0.1');
		});
	});
		$('#join1').click( function() {
		$('#reg_pa1').prop('checked',false);
		$('#reg_pa2').prop('checked',true);
		$('#reg_pa3').prop('checked',false);
		$('#reg_pa4').prop('checked',false);
		$('#reg_gr1').css('display','inline-block');
		$('#but_join').click(); 
	});
		$('#join2').click( function() {
		$('#reg_pa1').prop('checked',false);
		$('#reg_pa2').prop('checked',false);
		$('#reg_pa3').prop('checked',true);
		$('#reg_pa4').prop('checked',false);
		$('#reg_gr1').css('display','inline-block');
		$('#but_join').click(); 
	});
		$('#join3').click( function() {
		$('#reg_pa1').prop('checked',false);
		$('#reg_pa2').prop('checked',false);
		$('#reg_pa3').prop('checked',false);
		$('#reg_gr1').css('display','inline-block');
		$('#reg_pa4').prop('checked',true);
		$('#but_join').click(); 
	});
		kol_team = 4;
	left_team = 0;
	$('#but_team_prev').click( function() {
		wid1 = $('#team_slid').css('width');
		wid2 = Number(wid1.replace(/\D+/g,""));  
		sme1 = $('.team-item').css('width');
		sme2 = Number(sme1.replace(/\D+/g,""))+20;  
		//www1 = sme2 * kol_team;  
		kkk1 = Math.floor(wid2 / sme2);  
		kkk2 = wid2 / sme2;  
		mmm1 = kol_team - kkk1;  
		//console.log('prev =>>= wid2='+wid2 +' , sme2='+sme2  +' , kkk1='+mmm1 +' , mmm1='+mmm1 +' , left_team='+left_team);
		aaa = '=>>= окно wid2='+wid2 +' , элем sme2='+sme2  +' , кол.эл.в окне kkk1='+kkk1 +' , макс слева mmm1='+mmm1 +' , left_team='+left_team;
		//$('#otl').text(aaa);
		if (left_team < (mmm1-0) ) {
					       		      		  		//if (left_team < (kol_team -3)) {
			++left_team;
			//sme3 = sme2 * $(this).attr('sme');
			sme3 = left_team * sme2;
			$('#team_' + left_team).css('display', 'none'); 
			//$('#team_' + left_team).css('margin-left', '-'+sme3+'px'); 
		};
	});
		$('#but_team_next').click( function() {
		wid1 = $('#team_slid').css('width');
		wid2 = Number(wid1.replace(/\D+/g,""));  
		sme1 = $('.team-item').css('width');
		sme2 = Number(sme1.replace(/\D+/g,""))+20;  
		www1 = sme2 * kol_team;  
		kkk1 = Math.floor(wid2 / sme2);  
		kkk2 = wid2 / sme2;  
		mmm1 = kol_team - kkk1;  
		//console.log('prev =>>= wid2='+wid2 +'sme2='+sme2 +'www1='+www1 +'kkk1='+mmm1 +'  mmm1='+mmm1 +'left_team='+left_team);
		aaa = '=>>= окно wid2='+wid2 +' , элем sme2='+sme2  +' , кол.эл.в окне kkk1='+kkk1 +' , макс слева mmm1='+mmm1 +' , left_team='+left_team;
		//$('#otl').text(aaa);
		if (left_team > 0) {
			$('#team_' + left_team).css('display', 'inline-block'); 
			--left_team;
			sme3 = left_team * sme2;
			//$('#team_1').css('margin-left', '-'+sme3+'px'); 
			//$('#team_' + left_team).css('display', 'inline-block'); 
		};
	});
	$(window).resize(function () {
		wid1 = $('#team_slid').css('width');
		wid2 = Number(wid1.replace(/\D+/g,""));  
		sme1 = $('.team-item').css('width');
		sme2 = Number(sme1.replace(/\D+/g,""))+20;  
		//www1 = sme2 * kol_team;  
		kkk1 = Math.floor(wid2 / sme2); 		kkk2 = wid2 / sme2;  
		eee1 = kkk1 * sme2;
		//$('.car-wrap').css('width', eee1+'px'); 
		mmm1 = kol_team - kkk1;  
		//console.log('resize =>>= wid2='+wid2 +' sme2='+sme2  +' kkk1='+kkk1 +'  mmm1='+mmm1 +'left_team='+left_team);
		aaa = '=>>= окно wid1='+wid1+' , wid2='+wid2 +' , элем sme2='+sme2  +' , кол.эл.в окне kkk1='+kkk1 + '='+kkk2 +' , макс слева mmm1='+mmm1 +' , left_team='+left_team;
		//$('#otl').text(aaa);
		if (left_team > 0) {
			sme3 = left_team * sme2;
			//$('#team_1').css('margin-left', '-'+sme3+'px'); 
		};
	});
});
</script>
	</div>
    
        <?php if (!($this->id == 'site' && $this->action->id == 'index')){?>
        <div class="footer">
                <a href="/write">
                        <i aria-hidden="true" class="fa fa-4x fa-pencil-square-o"></i>
                        <br>write to us
                </a>
                <a href="/about">
                        <i aria-hidden="true" class="fa fa-4x fa-info-circle"></i>
                        <br>about us
                </a>
                <a href="/services">
                        <i aria-hidden="true" class="fa fa-4x fa-cogs"></i>
                        <br>our services
                </a>
                <a href="/mentors">
                        <i aria-hidden="true" class="fa fa-4x fa-users"></i>
                        <br>our mentors
                </a>
                <a href="/faq">
                        <i aria-hidden="true" class="fa fa-4x fa-question-circle"></i>
                        <br>faq
                </a>
                <a href="/contacts">
                        <i aria-hidden="true" class="fa fa-4x fa-envelope-o"></i>
                        <br>contacts
                </a>
        </div>
    <?php } ?>

<script type="text/javascript" src="/js/jquery.cookie.js"></script>
<script>
var gl_op = "";
var gl_id  = "";
var gl_id_menu  = "";
$(document).ready(function() {
	$('.tlanguage').click( function() {
		if ($(this).attr('data') != "Y") {
			$.cookie( 'language', $(this).text() );
			window.location.reload();
		};
	});
//	//	$('.menu1e').click( function() {
//		if (gl_id_menu != "") {$('#'+gl_id_menu).removeClass(); $('#'+gl_id_menu).addClass("menu1e");};
//		gl_id_menu = $(this).attr('id');
//		$(this).removeClass();
//		$(this).addClass("menu1f");
//		
//		$("html, body").animate({
//			scrollTop: $($(this).attr("href")).offset().top + "px"
//		}, {
//			duration: 800,
//			easing: "swing"
//		});

//		return false;
//	});
//	//
//	$(window).scroll(function(){
//		aa = $(window).scrollTop();
//		a2 = $('#b2').offset().top;
//		a3 = $('#b3').offset().top;
//		a4 = $('#b4').offset().top;
//		a5 = $('#b5').offset().top;
//		a6 = $('#b6').offset().top;
//		if (aa < a2/2) {b2=0;} else {b2=1;}
//		if (aa < (a2+(a2/2)) ) {b3=0;} else {b3=1;}
//		if (aa < (a3+(a2/2)) ) {b4=0;} else {b4=1;}
//		if (aa < (a4+(a2/2)) ) {b5=0;} else {b5=1;}
//		if (aa < (a5+(a2/2)) ) {b6=0;} else {b6=1;}
//		bb = 1+b2+b3+b4+b5+b6;
//		if (gl_id_menu != "") {$('#'+gl_id_menu).removeClass(); $('#'+gl_id_menu).addClass("menu1e");};
//		gl_id_menu = 'menu_b'+bb;
//		$('#'+gl_id_menu).removeClass();
//		$('#'+gl_id_menu).addClass("menu1f");
//	});
});
</script>
<script>
// Cache selectors
var lastId,
	topMenu = $("#top-menu"),
	topMenuHeight = topMenu.outerHeight()+15,
	// All list items
	menuItems = topMenu.find("a"),
	// Anchors corresponding to menu items
	scrollItems = menuItems.map(function(){
		var item = $($(this).attr("href"));
		if (item.length) { return item; }
	});
</script>
<script>
// Bind click handler to menu items
// so we can get a fancy scroll animation
menuItems.click(function(e){
	var href = $(this).attr("href"),
		offsetTop = href === "#" ? 0 : $(href).offset().top;
	$('html, body').stop().animate({ 
		scrollTop: offsetTop
	}, 300);
	e.preventDefault();
});
// Bind to scroll
$(window).scroll(function(){
	// Get container scroll position
	var fromTop = $(this).scrollTop()+topMenuHeight;
	
	// Get id of current scroll item
	var cur = scrollItems.map(function(){
		if ($(this).offset().top < fromTop)
		return this;
	});
	// Get the id of the current element
	cur = cur[cur.length-1];
	var id = cur && cur.length ? cur[0].id : "";
	
	if (lastId !== id) {
		lastId = id;
		// Set/remove active class
		menuItems
			.parent().removeClass("active")
			.end().filter("[href='#"+id+"']").parent().addClass("active");
	}
});
$(window).load(function () {
	$(window).scroll();
});
</script>
</body>
</html>
