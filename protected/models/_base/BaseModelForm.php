<?php

/**
 * This is the model class for table "model_form".
 *
 * The followings are the available columns in table 'model_form':
 * @property integer $id
 * @property integer $user_id
 * @property integer $choose_metrical
 * @property integer $height
 * @property integer $weight
 * @property integer $chest
 * @property integer $waist
 * @property integer $hips
 * @property integer $dress_jacket
 * @property string $cup
 * @property integer $shoe
 * @property integer $ethnicity
 * @property integer $skin_color
 * @property integer $eye_color
 * @property integer $hair_color
 * @property integer $hair_coloration
 * @property integer $ready_change_color
 * @property integer $hair_lenght
 * @property integer $tattos
 * @property integer $piercing
 * @property integer $travel_ability
 *
 * The followings are the available model relations:
 * @property MediaPositionToForm[] $mediaPositionToForms
 * @property User $id0
 * @property EthnicityList $ethnicity0
 * @property SkinColorList $skinColor
 * @property EyeColorList $eyeColor
 * @property HairColorList $hairColor
 * @property ModelFormToMainReason[] $modelFormToMainReasons
 * @property ModelLanguageToForm[] $modelLanguageToForms
 * @property PiercingToModel[] $piercingToModels
 * @property TattoToModel[] $tattoToModels
 */
class BaseModelForm extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ModelForm the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'model_form';
	}

	/**
	 * @return array validation rules for model attributes.
	 */ 
	 
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, ethnicity, skin_color, eye_color, hair_color, hair_lenght, tattos, piercing, travel_ability', 'required'),
			array('user_id, choose_metrical, height, weight, chest, waist, hips, dress_jacket, shoe, ethnicity, skin_color, eye_color, hair_color, hair_coloration, ready_change_color, hair_lenght, tattos, piercing, travel_ability', 'numerical', 'integerOnly'=>true),
			array('cup', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user_id, choose_metrical, height, weight, chest, waist, hips, dress_jacket, cup, shoe, ethnicity, skin_color, eye_color, hair_color, hair_coloration, ready_change_color, hair_lenght, tattos, piercing, travel_ability', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'mediaPositionToForms' => array(self::HAS_MANY, 'MediaPositionToForm', 'media_form_id'),
			'id0' => array(self::BELONGS_TO, 'User', 'id'),
			'ethnicity0' => array(self::BELONGS_TO, 'EthnicityList', 'ethnicity'),
			'skinColor' => array(self::BELONGS_TO, 'SkinColorList', 'skin_color'),
			'eyeColor' => array(self::BELONGS_TO, 'EyeColorList', 'eye_color'),
			'hairColor' => array(self::BELONGS_TO, 'HairColorList', 'hair_color'),
			'modelFormToMainReasons' => array(self::HAS_MANY, 'ModelFormToMainReason', 'model_form_id'),
			'modelLanguageToForms' => array(self::HAS_MANY, 'ModelLanguageToForm', 'model_form_id'),
			'piercingToModels' => array(self::HAS_MANY, 'PiercingToModel', 'model_form_id'),
			'tattoToModels' => array(self::HAS_MANY, 'TattoToModel', 'model_form_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'choose_metrical' => 'Choose Metrical',
			'height' => 'Height',
			'weight' => 'Weight',
			'chest' => 'Chest',
			'waist' => 'Waist',
			'hips' => 'Hips',
			'dress_jacket' => 'Dress Jacket',
			'cup' => 'Cup',
			'shoe' => 'Shoe',
			'ethnicity' => 'Ethnicity',
			'skin_color' => 'Skin Color',
			'eye_color' => 'Eye Color',
			'hair_color' => 'Hair Color',
			'hair_coloration' => 'Hair Coloration',
			'ready_change_color' => 'Ready Change Color',
			'hair_lenght' => 'Hair Lenght',
			'tattos' => 'Tattos',
			'piercing' => 'Piercing',
			'travel_ability' => 'Travel Ability',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('choose_metrical',$this->choose_metrical);
		$criteria->compare('height',$this->height);
		$criteria->compare('weight',$this->weight);
		$criteria->compare('chest',$this->chest);
		$criteria->compare('waist',$this->waist);
		$criteria->compare('hips',$this->hips);
		$criteria->compare('dress_jacket',$this->dress_jacket);
		$criteria->compare('cup',$this->cup,true);
		$criteria->compare('shoe',$this->shoe);
		$criteria->compare('ethnicity',$this->ethnicity);
		$criteria->compare('skin_color',$this->skin_color);
		$criteria->compare('eye_color',$this->eye_color);
		$criteria->compare('hair_color',$this->hair_color);
		$criteria->compare('hair_coloration',$this->hair_coloration);
		$criteria->compare('ready_change_color',$this->ready_change_color);
		$criteria->compare('hair_lenght',$this->hair_lenght);
		$criteria->compare('tattos',$this->tattos);
		$criteria->compare('piercing',$this->piercing);
		$criteria->compare('travel_ability',$this->travel_ability);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}