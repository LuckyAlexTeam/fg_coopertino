<?php
// id cod cod_down language int_1 int_2 txt_1 txt_2
class Nastroyki extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    public function tableName() {
        return 'nastroyki';
    }
	// id cod cod_down language int_1 int_2 txt_1 txt_2
    public function getAll($cod, $lan='all') {
		$criteria = new CDbCriteria;
		$criteria->condition = "cod = '{$cod}' AND language = '{$lan}' ";
		$criteria->order = " cod_down";
		$list = self::model()->findAll($criteria);
		//var_dump($list);
		$arr = Array();
		foreach ($list as $lab) {
			$id = $lab['id'];
			$arr[$id]['id'] = $lab['id'];
			$arr[$id]['cod'] = $lab['cod'];
			$arr[$id]['cod_down'] = $lab['cod_down'];
			$arr[$id]['language'] = $lab['language'];
			$arr[$id]['int_1'] = $lab['int_1'];
			$arr[$id]['int_2'] = $lab['int_2'];
			$arr[$id]['txt_1'] = $lab['txt_1'];
			$arr[$id]['txt_2'] = $lab['txt_2'];
		};
		//
		return $arr;
	}
	public function getStroka($cod, $cod_down, $lan='all') {
		$criteria = new CDbCriteria;
		$criteria->condition = "cod = '{$cod}' AND cod_down='{$cod_down}' AND language = '{$lan}' ";
		$list = self::model()->findAll($criteria);
		$arr = Array();
		foreach ($list as $lab) {
			$arr['id'] = $lab['id'];
			$arr['cod'] = $lab['cod'];
			$arr['cod_down'] = $lab['cod_down'];
			$arr['language'] = $lab['language'];
			$arr['int_1'] = $lab['int_1'];
			$arr['int_2'] = $lab['int_2'];
			$arr['txt_1'] = $lab['txt_1'];
			$arr['txt_2'] = $lab['txt_2'];
		};
		//
		return $arr;
	}
	public function getPrice() {
		$criteria = new CDbCriteria;
		$criteria->condition = "cod = 'price'";
		$list = self::model()->findAll($criteria);
		$arr = Array();
		foreach ($list as $lab) {
			$arr[ $lab['cod_down'] ] = $lab['int_1'];
		};
		//
		return $arr;
	}
	public function Ajax() {
		var_dump($_POST);
		//return;
		//
		$this->setAttribute( 'cod' , $_POST['cod'] );
		if ( isset($_POST['visitor']) ) {
			$this->setAttribute( 'cod_down' , 'visitor');
			$pole = $_POST['pole'];
			if ($_POST['id_visitor'] != "") {
				$this->setAttribute( $pole , $_POST['visitor']);
				$this->setAttribute( 'id' , $_POST['id_visitor']);
				$this->setPrimaryKey($_POST['id_visitor']);
				$this->isNewRecord=false;
				$tt1 = $this->update();
				//var_dump($tt1);
			} else {
				$this->setAttribute( $pole , $_POST['visitor']);
				$this->setAttribute( 'id' , '');
				$this->isNewRecord=true;
				$tt1 = $this->insert();
				//var_dump($tt1);
				//print "pole=" . $pole;
			};
		};
		if ( isset($_POST['classic']) ) {
			$this->setAttribute( 'cod_down' , 'classic');
			$pole = $_POST['pole'];
			if ($_POST['id_classic'] != "") {
				$this->setAttribute( $pole , $_POST['classic']);
				$this->setAttribute( 'id' , $_POST['id_classic']);
				$this->setPrimaryKey($_POST['id_classic']);
				$this->isNewRecord=false;
				$tt1 = $this->update();
				//var_dump($tt1);
			} else {
				$this->setAttribute( $pole , $_POST['classic']);
				$this->setAttribute( 'id' , '');
				$this->isNewRecord=true;
				$tt1 = $this->insert();
				//var_dump($tt1);
				//print "pole=" . $pole;
			};
		};
		if ( isset($_POST['vip']) ) {
			$this->setAttribute( 'cod_down' , 'vip');
			$pole = $_POST['pole'];
			if ($_POST['id_vip'] != "") {
				$this->setAttribute( $pole , $_POST['vip']);
				$this->setAttribute( 'id' , $_POST['id_vip']);
				$this->setPrimaryKey($_POST['id_vip']);
				$this->isNewRecord=false;
				$tt1 = $this->update();
				//var_dump($tt1);
			} else {
				$this->setAttribute( $pole , $_POST['vip']);
				$this->setAttribute( 'id' , '');
				$this->isNewRecord=true;
				$tt1 = $this->insert();
				//var_dump($tt1);
				//print "pole=" . $pole;
			};
		};
		if ( isset($_POST['corporate']) ) {
			$this->setAttribute( 'cod_down' , 'corporate');
			$pole = $_POST['pole'];
			if ($_POST['id_corporate'] != "") {
				$this->setAttribute( $pole , $_POST['corporate']);
				$this->setAttribute( 'id' , $_POST['id_corporate']);
				$this->setPrimaryKey($_POST['id_corporate']);
				$this->isNewRecord=false;
				$tt1 = $this->update();
				//var_dump($tt1);
			} else {
				$this->setAttribute( $pole , $_POST['corporate']);
				$this->setAttribute( 'id' , '');
				$this->isNewRecord=true;
				$tt1 = $this->insert();
				//var_dump($tt1);
				//print "pole=" . $pole;
			};
		};
		//
		return;
	}
	
}