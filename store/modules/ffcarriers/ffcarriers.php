<?php

if (!defined('_PS_VERSION_'))
    exit;



class Ffcarriers extends CarrierModule
{



    public function __construct()
    {
        $this->name = 'ffcarriers';
        $this->tab = 'advertising_marketing';
        $this->version = '1.0';
        $this->author = 'Sergii Khoroshko khoroshko@gmail.com';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.5', 'max' => _PS_VERSION_);
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('UPS, FedEx, DHL rates and tracking');
        $this->description = $this->l('Show rates for delivery.');
        $this->confirmUninstall = $this->l('Are you shure, do you want to delete module?');


    }


    public function install()
    {
        if ( Configuration::get('SHIPPO_CARRIER_ID') ) {
            $id_carrier = Configuration::get('SHIPPO_CARRIER_ID');
        } else {
            $carrierConfig = array(
                0 => array('name' => 'Shippo',
                    'id_tax_rules_group' => 0, // We do not apply thecarriers tax
                    'active' => true,
                    'is_free' => false,
                    'deleted' => 0,
                    'shipping_handling' => false,
                    'range_behavior' => 0,
                    'delay' => array(
                        'fr' => 'Description Shippo multicarrier',
                        'en' => 'Description Shippo multicarrier',
                        'ru' => 'Описание Shippo multicarrier'),
                    'id_zone' => 1, // Area where the carrier operates
                    'is_module' => true, // We specify that it is a module
                    'shipping_external' => true,
                    'external_module_name' => $this->name, // We specify the name of the module
                    'need_range' => true // We specify that we want the calculations for the ranges
                    // that are configured in the back office
                )
            );

            $id_carrier = $this->installExternalCarrier($carrierConfig[0]);
        }

        copy(dirname(__FILE__) . '/img/shippo_small.jpg', _PS_SHIP_IMG_DIR_ . '/' . $id_carrier . '.jpg');

        if (Shop::isFeatureActive())
            Shop::setContext(Shop::CONTEXT_ALL);

        if (!parent::install() ||
            !$this->installSql() ||
            !$this->registerHook('displayHeader') ||
            !$this->registerHook('displayAdminOrder') ||
            !$this->registerHook('displayCarrierList') ||
            !$this->registerHook('actionCarrierUpdate') ||
            !$this->registerHook('actionCarrierProcess') ||
            !Configuration::updateValue('SHIPPO_CARRIER_ID', $id_carrier)
        )
            return false;



        return true;
    }

    public function uninstall()
    {
        if (!parent::uninstall() ||
            !$this->uninstallSql())
            return false;

//        // Delete External Carrier
//        $carrier = new Carrier((int)(Configuration::get('CARVARI_CARRIER_ID')));
//
//        // If external carrier is default set other one as default
//        if (Configuration::get('PS_CARRIER_DEFAULT') == (int)($carrier->id))
//        {
//            global $cookie;
//            $carriersD = Carrier::getCarriers($cookie->id_lang, true, false, false, NULL, PS_CARRIERS_AND_CARRIER_MODULES_NEED_RANGE);
//            foreach($carriersD as $carrierD)
//                if ($carrierD['active'] AND !$carrierD['deleted'] AND ($carrierD['name'] != $this->_config['name']))
//                    Configuration::updateValue('PS_CARRIER_DEFAULT', $carrierD['id_carrier']);
//        }
//
//        // Then delete Carrier
//        if (!$carrier->delete())
//            return false;

        return true;
    }

    private function installSql(){
//        Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'ffusercards`');

        Db::getInstance()->execute('
		CREATE TABLE IF NOT EXISTS  `'._DB_PREFIX_.'ffshippo` (
			`id_cart` INT NOT NULL,
			`id_manufacturer` INT NOT NULL,
			`provider` varchar(60),
			`servicelevel_token` varchar(60),
			`amount` double(12,2),
			`currency` varchar(60),
			`tracking_number` varchar(60),
		PRIMARY KEY (`id_cart`, `id_manufacturer`),
		INDEX (`id_cart`)
		)  ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;');


        return true;
    }

    private function uninstallSql(){
        Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'ffshippo`');
//        Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'ffusercards_html`');

        return true;
    }

    public function getContent()
    {
//
//// example fromAddress
//        $fromAddress = array(
//            'object_purpose' => 'PURCHASE',
//            'name' => 'Mr Hippo',
//            'company' => 'Shippo',
//            'street1' => '1811 78th Str',
//            'city' => 'Brooklyn',
//            'state' => 'NY',
//            'zip' => '11214',
//            'country' => 'US',
//            'phone' => '+7187585119',
//            'email' => 'mr-hippo@goshipppo.com');
//
//// example fromAddress
//        $toAddress = array(
//            'object_purpose' => 'PURCHASE',
//            'name' => 'Ms Hippo"',
//            'company' => 'San Diego Zoo',
//            'street1' => '5 Lenina',
//            'city' => 'Kiev',
//            'state' => '',
//            'zip' => '71630',
//            'country' => 'UA',
//            'phone' => '+1 555 341 9393',
//            'email' => 'ms-hippo@goshipppo.com');
//
//// example parcel
//        $parcel = array(
//            'length'=> '1',
//            'width'=> '1',
//            'height'=> '1',
//            'distance_unit'=> 'in',
//            'weight'=> '1',
//            'mass_unit'=> 'lb',
//        );
//
//// example Shipment object
//        $shipment = Shippo_Shipment::create(
//            array(
//                'object_purpose'=> 'PURCHASE',
//                'address_from'=> $fromAddress,
//                'address_to'=> $toAddress,
//                'parcel'=> $parcel,
//                'async'=> false
//            ));
//
//// Select the rate you want to purchase.
//// We simply select the first rate in this example.
//
//        d($shipment);
//        $rate = $shipment["rates_list"][0];
//
//// Purchase the desired rate
//        $transaction = Shippo_Transaction::create(array(
//            'rate'=> $rate["object_id"],
//            'async'=> false
//        ));
//
//// label_url and tracking_number
//        if ($transaction["object_status"] == "SUCCESS"){
//            echo($transaction["label_url"]);
//            echo("\n");
//            echo($transaction["tracking_number"]);
//        }else {
//            echo($transaction["messages"]);
//        }
//
//        if(Tools::getValue('test'))
//            $this->test();
//
//        if (Tools::isSubmit('submitOptions'))
//        {
//            Configuration::updateValue('smsinfo_login' , Tools::getValue('smsinfo_login'));
//            if ( Tools::getValue('smsinfo_password') != '' )
//                Configuration::updateValue('smsinfo_password' , Tools::getValue('smsinfo_password'));
//            Configuration::updateValue('smsinfo_sender' , Tools::getValue('smsinfo_sender'));
//            Configuration::updateValue('smsinfo_order_confirmation' , Tools::getValue('smsinfo_order_confirmation'));
//            Configuration::updateValue('smsinfo_order_tpl' , Tools::getValue('smsinfo_order_tpl'));
//        }
//
//        return $this->displayConfigForm();
        return 'new module';
    }


    public function displayConfigForm()
    {


        $this->fields_options = array(
            'general' => array(
                'title' => 'Настройка модуля',
                'fields' => array(

                    'smsinfo_login' => array(
                        'title' => 'TurboSMS Login',
                        'cast' => 'strval',
                        'type' => 'text',
                        'size' => '10'
                    ),
                    'smsinfo_password' => array(
                        'title' => 'TurboSMS Password',
                        'cast' => 'strval',
                        'type' => 'text',
                        'size' => '10'
                    ),
                    'smsinfo_sender' => array(
                        'title' => 'TurboSMS Sender',
                        'cast' => 'strval',
                        'type' => 'text',
                        'size' => '10'
                    ),
                    'smsinfo_order_confirmation' => array(
                        'title' => 'Включить SMS информирование при заказе',
                        'cast' => 'strval',
                        'type' => 'bool',
                        'size' => '10'
                    ),
                    'smsinfo_order_tpl' => array(
                        'title' => 'Шаблон SMS',
                        'desc' => 'Шаблон смс уведомления при заказе. {id} заменяется на номер заказа',
                        'cast' => 'strval',
                        'type' => 'textarea'
                    )

                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                    'class' => 'button',
                    'name' => 'submitOptions'
                )
            )
        );

        $helper = new HelperOptionsCore();
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->module = $this;
        $helper->title = $this->displayName;
        $helper->show_toolbar = true;
        $helper->toolbar_scroll = true;
        $helper->toolbar_btn = array(
            'save' => array(
                'desc' => $this->l('Save'),
                'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
                    '&token='.Tools::getAdminTokenLite('AdminModules'),
            ),
            'back' => array(
                'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('Back to list')
            )
        );
        return $helper->generateOptions($this->fields_options);
    }

    public function hookDisplayHeader($params)
    {
        $this->context->controller->addCSS($this->_path.'css/ffcarriers.css');
    }

    public function hookDisplayAdminOrder($params)
    {
        $order = new Order($params['id_order']);
        if ( Configuration::get('SHIPPO_CARRIER_ID') == $order->id_carrier) {

            $display = $this->display(__FILE__, 'admin_tab.tpl');

            return $display;
        }
    }

    public function hookDisplayCarrierList($params)
    {
        if ($params['cart']->id_carrier != Configuration::get('SHIPPO_CARRIER_ID'))
            return false;


        $cart = $params['cart'];
        $customer_address = new Address($cart->id_address_delivery);
        $products = $cart->getProducts();

        $selected_options = array();
        $results = Db::getInstance()->executeS('SELECT * FROM `'._DB_PREFIX_.'ffshippo` WHERE id_cart='.$params['cart']->id);
        foreach ($results as $result){
            $selected_options[$result['id_manufacturer']][$result['servicelevel_token']] = 1;
        }

        $products_by_manufacturer = array();
        foreach ($products as $product){
            $products_by_manufacturer[$product['id_manufacturer']]['product_list'][] = $product;
        }

        foreach ($products_by_manufacturer as $id_manufacturer=>&$manufacturer) {
            $manufacturer_address = new Address(Manufacturer::getManufacturerIdAddressStatic($id_manufacturer));
            $manufacturer['manufacturer_address'] = $manufacturer_address;
            $manufacturer['rates'] = $this->getRates($manufacturer_address, $customer_address, $manufacturer);
//            d($manufacturer);
        }

//        d($products_by_manufacturer);

        $this->context->smarty->assign('products_by_manufacturer', $products_by_manufacturer);
        $this->context->smarty->assign('selected_options', $selected_options);

        $display = $this->display(__FILE__, 'carrier.tpl');

        return $display;
    }

    public function hookActionCarrierUpdate($params)
    {
        $id_carrier_old = (int)($params['id_carrier']);
        $id_carrier_new = (int)($params['carrier']->id);
        if ($id_carrier_old == (int)(Configuration::get('SHIPPO_CARRIER_ID')))
            Configuration::updateValue('SHIPPO_CARRIER_ID', $id_carrier_new);
    }

    public function hookActionCarrierProcess($params)
    {
        if (Tools::getValue('shippocarrier')) {
            $packages = Tools::getValue('shippo');
//            d($packages);
            Db::getInstance()->delete('ffshippo', 'id_cart='. (int)$params['cart']->id);

            foreach ($packages as $id_manufacturer=>$package) {
                Db::getInstance()->insert('ffshippo',
                    array(
                        'id_cart' => (int)$params['cart']->id,
                        'id_manufacturer' => (int)$id_manufacturer,
                        'provider' => pSQL($package[$package['servicelevel_token']]['provider']),
                        'servicelevel_token' => pSQL($package['servicelevel_token']),
                        'amount' => pSQL($package[$package['servicelevel_token']]['amount']),
                        'currency' => pSQL($package[$package['servicelevel_token']]['currency']),
                    ),
                    false,
                    false,
                    Db::ON_DUPLICATE_KEY);
            }
        }
    }


    protected function getRates($address_from, $address_to, $package)
    {
        require_once(__DIR__.'/libs/shippo/Shippo.php');
        Shippo::setApiKey("fd71be58d64db760b57d039c628f3d138de91357");


        // example fromAddress
        $fromAddress = array(
            'object_purpose' => 'QUOTE',
            'street1' => $address_from->address1,
            'city' => $address_from->city,
//            'state' => StateCore::getNameById($address_from->id_state),
            'zip' => $address_from->postcode,
            'country' => CountryCore::getIsoById($address_from->id_country),
            'phone' => $address_from->phone_mobile ? $address_from->phone_mobile : $address_from->phone,
            );


        // example fromAddress
        $toAddress = array(
            'object_purpose' => 'QUOTE',
            'street1' => $address_to->address1,
            'city' => $address_to->city,
//            'state' => StateCore::getNameById($address_to->id_state),
            'zip' => $address_to->postcode,
            'country' => CountryCore::getIsoById($address_to->id_country),
            'phone' => $address_to->phone_mobile ? $address_to->phone_mobile : $address_to->phone,
            );

//        d($toAddress);
        $max_depth = 0;
        $max_width = 0;
        $max_height = 0;
        $package_weight = 0;
        $products_quantity = 0;
        foreach($package['product_list'] as $product){
            $max_depth = ($product['depth'] > $max_depth) ? $product['depth'] : $max_depth;
            $max_width = ($product['width'] > $max_width) ? $product['width'] : $max_width;
            $max_height = ($product['height'] > $max_height) ? $product['height'] : $max_height;

            $product_weight = $product['weight']*$product['quantity'];

            $package_weight += $product_weight;
            $products_quantity += $product['quantity'];
        }

        // example parcel
        $parcel = array(
            'length'=> $max_depth ? round($max_depth*$products_quantity, 2) : 1,
            'width'=> $max_width ? round($max_width, 2) : 1,
            'height'=> $max_height ? round($max_height, 2) : 1,
            'distance_unit'=> Configuration::get('PS_DIMENSION_UNIT'),
            'weight'=> $package_weight ? round($package_weight, 2) : 1,
            'mass_unit'=> Configuration::get('PS_WEIGHT_UNIT'),
        );

        $cache_key = 'shippo_'.$address_from->id.'_'.$address_to->id.'_'.$products_quantity.'_'.$package_weight;
        $memcache= new CacheMemcacheCore();

        if ($memcache->exists($cache_key))
            return $memcache->get($cache_key);
        try {
            // example Shipment object
            $shipment = Shippo_Shipment::create(
                array(
                    'object_purpose' => 'QUOTE',
                    'address_from' => $fromAddress,
                    'address_to' => $toAddress,
                    'parcel' => $parcel,
                    'async' => false
                ));

        } catch (Exception $e) {
            $this->context->controller->errors[] = $e->getMessage();
            return;
//            echo 'Выброшено исключение: ',  $e->getMessage(), "\n";
        }

        // Select the rate you want to purchase.
        // We simply select the first rate in this example.

        $rates_by_carrier = array();

        foreach($shipment["rates_list"] as $rate){
            $rates_by_carrier[$rate->provider]['provider_image_75'] = $rate->provider_image_75;
            $rates_by_carrier[$rate->provider]['provider_image_200'] = $rate->provider_image_200;
            $rates_by_carrier[$rate->provider]['provider'] = $rate->provider;

            $rate_option = array(
                'attributes' => $rate->attributes,
                'amount' => $rate->amount,
                'currency' => $rate->currency,
                'amount_local' => $rate->amount_local,
                'currency_local' => $rate->currency_local,
                'servicelevel_name' => $rate->servicelevel_name,
                'servicelevel_token' => $rate->servicelevel_token,
                'duration_terms' => $rate->duration_terms,

            );
            $rates_by_carrier[$rate->provider]['options'][] = $rate_option;

        }

        $memcache->set($cache_key, $rates_by_carrier);


        return $rates_by_carrier;
    }

    public static function installExternalCarrier($config)
    {
        $carrier = new Carrier();
        $carrier->name = $config['name'];
        $carrier->id_tax_rules_group = $config['id_tax_rules_group'];
        $carrier->id_zone = $config['id_zone'];
        $carrier->active = $config['active'];
        $carrier->is_free = $config['is_free'];
        $carrier->deleted = $config['deleted'];
        $carrier->delay = $config['delay'];
        $carrier->shipping_handling = $config['shipping_handling'];
        $carrier->range_behavior = $config['range_behavior'];
        $carrier->is_module = $config['is_module'];
        $carrier->shipping_external = $config['shipping_external'];
        $carrier->external_module_name = $config['external_module_name'];
        $carrier->need_range = $config['need_range'];

        $languages = Language::getLanguages(true);
        foreach ($languages as $language)
        {
            if ($language['iso_code'] == 'fr')
                $carrier->delay[(int)$language['id_lang']] = $config['delay'][$language['iso_code']];
            elseif ($language['iso_code'] == 'en')
                $carrier->delay[(int)$language['id_lang']] = $config['delay'][$language['iso_code']];
            elseif ($language['iso_code'] == 'ru')
                $carrier->delay[(int)$language['id_lang']] = $config['delay'][$language['iso_code']];
            else
                $carrier->delay[(int)$language['id_lang']] = $config['delay']['en'];
        }

        if ($carrier->add())
        {
            $groups = Group::getGroups(true);
            foreach ($groups as $group)
                Db::getInstance()->insert('carrier_group', array('id_carrier' => (int)($carrier->id), 'id_group' => (int)($group['id_group'])));

            $rangePrice = new RangePrice();
            $rangePrice->id_carrier = $carrier->id;
            $rangePrice->delimiter1 = '0';
            $rangePrice->delimiter2 = '100000';
            $rangePrice->add();

            $rangeWeight = new RangeWeight();
            $rangeWeight->id_carrier = $carrier->id;
            $rangeWeight->delimiter1 = '0';
            $rangeWeight->delimiter2 = '100000';
            $rangeWeight->add();

            $zones = Zone::getZones(true);
            foreach ($zones as $zone)
            {
                Db::getInstance()->insert('carrier_zone', array('id_carrier' => (int)($carrier->id), 'id_zone' => (int)($zone['id_zone'])));
                Db::getInstance()->insert('delivery', array('id_carrier' => (int)($carrier->id), 'id_range_price' => (int)($rangePrice->id), 'id_range_weight' => NULL, 'id_zone' => (int)($zone['id_zone']), 'price' => '1'), true);
                Db::getInstance()->insert('delivery', array('id_carrier' => (int)($carrier->id), 'id_range_price' => NULL, 'id_range_weight' => (int)($rangeWeight->id), 'id_zone' => (int)($zone['id_zone']), 'price' => '1'), true);
            }

            // Copy Logo
//            if (!copy(dirname(__FILE__).'/carrier.jpg', _PS_SHIP_IMG_DIR_.'/'.(int)$carrier->id.'.jpg'))
//                return false;

            // Return ID Carrier
            return (int)($carrier->id);
        }

        return false;
    }

    public function getPackageShippingCost($cart, $shipping_cost, $products)
    {
//        return 0;

        $shiping_cost = 0;
        $products_by_manufacturer = array();
        $customer_address = new Address($cart->id_address_delivery);


        foreach ($products as $product){
            $products_by_manufacturer[$product['id_manufacturer']][] = $product;
        }

        foreach ($products_by_manufacturer as $id_manufacturer=>$manufacturer_products) {
            $result = Db::getInstance()->getRow('SELECT * FROM `'._DB_PREFIX_.'ffshippo` WHERE id_cart='.$cart->id.' AND id_manufacturer='.$id_manufacturer);

            if ($result)
                $shiping_cost += $result['amount'];
        }
        return $shiping_cost;

    }

    public function getOrderShippingCost($params, $shipping_cost)
    {
        // This example returns shipping cost with overcost set in the back-office, but you can call a webservice or calculate what you want before returning the final value to the Cart
//        if ($this->id_carrier == (int)(Configuration::get('MYCARRIER1_CARRIER_ID')) && Configuration::get('MYCARRIER1_OVERCOST') > 1)
//            return (float)(Configuration::get('MYCARRIER1_OVERCOST'));
//        if ($this->id_carrier == (int)(Configuration::get('MYCARRIER2_CARRIER_ID')) && Configuration::get('MYCARRIER2_OVERCOST') > 1)
//            return (float)(Configuration::get('MYCARRIER2_OVERCOST'));

        // If the carrier is not known, you can return false, the carrier won't appear in the order process
        return;
    }

    public function getOrderShippingCostExternal($params)
    {
        // This example returns the overcost directly, but you can call a webservice or calculate what you want before returning the final value to the Cart
//        if ($this->id_carrier == (int)(Configuration::get('MYCARRIER1_CARRIER_ID')) && Configuration::get('MYCARRIER1_OVERCOST') > 1)
//            return (float)(Configuration::get('MYCARRIER1_OVERCOST'));
//        if ($this->id_carrier == (int)(Configuration::get('MYCARRIER2_CARRIER_ID')) && Configuration::get('MYCARRIER2_OVERCOST') > 1)
//            return (float)(Configuration::get('MYCARRIER2_OVERCOST'));

        // If the carrier is not known, you can return false, the carrier won't appear in the order process
        return;
    }




    protected function getCacheId($name = null)
    {
        $cache_id = parent::getCacheId();

        if ($name !== null)
            $cache_id .= '|'.$name;

        return $cache_id.'|'.Db::getInstance()->getValue('SELECT MAX(id_store) FROM ' . _DB_PREFIX_ . 'store');
    }


}