<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{            
		if (!Yii::app()->user->isGuest){
                    $user = User::model()->findByPk(Yii::app()->user->id);
                    if ($user->account_type == UserAccountType::VISITOR){
                        $this->redirect(Yii::app()->createUrl('site/profile',array('id'=>Yii::app()->user->id)));
                    }
                }
		$this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
                    if (!empty($_POST['LoginForm']['rememberMe']) && $_POST['LoginForm']['rememberMe'] == 'on'){
                        $_POST['LoginForm']['rememberMe'] = 1;
                    }
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
        
        public function actionRegistration(){
        $model=new User();
        $model->scenario = 'registration';
        if (!empty($_POST['User'])){
           $model->attributes = $_POST['User'];
           if ($model->account_type == UserAccountType::VISITOR){
              $auto_password = $this->generate_password(6);
              $model->password = md5($auto_password);
              $model->save();
              if (empty($model->errors)){
                $identity = new UserIdentity($model->email,$auto_password);
                $identity->authenticate();
                Yii::app()->user->login($identity,7*60*60*24);
                $txt = 'Уважаемый пользователь Вы зарегистрировались на сайте '.
                        Yii::app()->createAbsoluteUrl('').
                        ' Ваш логин: '.$model->email.', Ваш пароль: ' .$auto_password;                            
                mail($model->email,"Регистрация на ".Yii::app()->createAbsoluteUrl(''),$txt);                
              } else {
                  echo json_encode($model->errors);
              }
           }           
           Yii::app()->end();
         }
        }
        
        public function actionChangePassword() {
        $id = Yii::app()->user->id;
        $user = User::model()->findByPk($id);
        if (isset($_POST['User'])) {
            $post = $_POST['User'];
            foreach ($post as $value){
                if(empty($value)){
                    $this->renderPartial('application.views.site.bootstrapAlert',array(
                            'type'=>'alert-danger',
                            'message'=>'Ошибка! Одно или  несколько полей не заполнены',
                        ));
                        break;    
                }
            }
            if ($post['newpass1'] === $post['newpass2']) {
                if (md5($post['pass']) === $user->password) {
                    $user->password = md5($post['newpass1']);
                    $user->scenario = 'update_password';
                    if ($user->save(false)) { // 'update_password' - сценарий                        
                        $this->renderPartial('application.views.site.bootstrapAlert',array(
                            'type'=>'alert-success',
                            'message'=>'Данные успешно сохранены',
                        ));                       
                        Yii::app()->end();
                    }
                }else{
                    $this->renderPartial('application.views.site.bootstrapAlert',array(
                            'type'=>'alert-danger',
                            'message'=>'Ошибка! Старый пароль введен неверно!',
                        ));
                   // echo 'Старый пароль введен неверно!';
                   //$user->addError('password', 'Старый пароль введен неверно!'); 
                }
            }else{
                $this->renderPartial('application.views.site.bootstrapAlert',array(
                            'type'=>'alert-danger',
                            'message'=>'Ошибка! Новый пароль не совпадает с подтверждением',
                        ));
               // echo 'Новый пароль не совпадает с подтверждением';
                   //$user->addError('password', 'Новый пароль не совпадает с подтверждением!');
            }
        }
        
    }
        
        
        public function actionLanguage($id){
            Yii::app()->user->setState('lang',$id);
            Yii::app()->language = $id;
            $this->redirect(Yii::app()->createUrl('site/index'));
        }
        
         public function actionProfile($id){
         if (Yii::app()->user->isGuest){
            $this->redirect(Yii::app()->createUrl('site/index'));
        }
            $this->render('profile');
        }
        
        public function actionSettings(){
        if (Yii::app()->user->isGuest){
            $this->redirect(Yii::app()->createUrl('site/index'));
        }
            $this->render('settings');
        }
         public function actionSearch(){
            $this->render('search');
        }
         public function actionJournal(){
            $this->render('journal');
        }
        
         public function actionQuestionary(){
            $this->render('questionary');
        }
}