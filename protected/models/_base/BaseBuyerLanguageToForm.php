<?php

/**
 * This is the model class for table "buyer_language_to_form".
 *
 * The followings are the available columns in table 'buyer_language_to_form':
 * @property integer $id
 * @property integer $language_id
 * @property integer $buyer_form_id
 *
 * The followings are the available model relations:
 * @property BrandForm $buyerForm
 * @property LanguageList $language
 */
class BaseBuyerLanguageToForm extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return BuyerLanguageToForm the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'buyer_language_to_form';
	}

	/**
	 * @return array validation rules for model attributes.
	 */ 
	 
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('language_id, buyer_form_id', 'required'),
			array('language_id, buyer_form_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, language_id, buyer_form_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'buyerForm' => array(self::BELONGS_TO, 'BrandForm', 'buyer_form_id'),
			'language' => array(self::BELONGS_TO, 'LanguageList', 'language_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'language_id' => 'Language',
			'buyer_form_id' => 'Buyer Form',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('language_id',$this->language_id);
		$criteria->compare('buyer_form_id',$this->buyer_form_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}