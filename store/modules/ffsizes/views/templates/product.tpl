<span id="show-sizes-table">
    ({l s='Sizes table' mod='ffsizes'})
</span>

<section id="sizes-table">
    <div class="sizes_table">
        <div class="title">{l s='Sizes table' mod='ffsizes'}</div>
        {$html}
    </div>
</section>