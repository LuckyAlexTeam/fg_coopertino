<?php
class SiteBlock extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    public function tableName() {
        return 'site_block';
    }
	//id code npp model photograph designer agency brand buyer customer company media desinger stylist hair mua 
    public function getAll() {
		$arr = Array();
		if ( isset($_GET['prof']) ) {
			$criteria = new CDbCriteria;
			$criteria->condition = $_GET['prof'] . " > 0";
			$list = self::model()->findAll($criteria);
		} else {
			//$list = self::model()->findAll();
			return $arr;
		};
		
		//$rrr = $this->getAttributes();
		foreach ($list as $lab) {
			$id = $lab['id'];
			$arr[$id]['id'] = $lab['id'];
			$arr[$id]['code'] = $lab['code'];
			$arr[$id]['npp'] = $lab['npp'];
			$arr[$id]['model'] = $lab['model'];
			$arr[$id]['photograph'] = $lab['photograph'];
			$arr[$id]['designer'] = $lab['designer'];
			$arr[$id]['agency'] = $lab['agency'];
			$arr[$id]['brand'] = $lab['brand'];
			$arr[$id]['buyer'] = $lab['buyer'];
			$arr[$id]['customer'] = $lab['customer'];
			$arr[$id]['company'] = $lab['company'];
			$arr[$id]['media'] = $lab['media'];
			$arr[$id]['desinger'] = $lab['desinger'];
			$arr[$id]['stylist'] = $lab['stylist'];
			$arr[$id]['hair'] = $lab['hair'];
			$arr[$id]['mua'] = $lab['mua'];
			$arr[$id]['nnn'] = $lab[$_GET['prof']];
		};
		return $arr;
    }
	//
	public function Add() {
		//
		foreach ($_POST as $key => $val) {
			$this->setAttribute( $key , $val );
		};
		$this->isNewRecord=true;
		$ttt = $this->insert();
	}
	//
	public function Upd() {
		foreach ($_POST as $key => $val) {
			$this->setAttribute( $key , $val );
		};
		$this->setPrimaryKey(Yii::app()->request->getPost('id'));
		$this->isNewRecord=false;
		$ttt = $this->update();
	}
	
	public function Ajax() {
		//
		$this->setAttribute( 'name' , Yii::app()->request->getPost('name') );
		$this->setAttribute( 'npp' , Yii::app()->request->getPost('npp') );
		$this->setAttribute( 'npp_up' , Yii::app()->request->getPost('npp_up') );
		$this->setAttribute( 'other' , Yii::app()->request->getPost('other') );
		$this->setPrimaryKey(Yii::app()->request->getPost('id'));
		$this->isNewRecord=false;
		$tt1 = $this->update();
		//
		if ($_POST['id2'] != 0) {
			$this->setAttribute( 'id', Yii::app()->request->getPost('id2'));
			$this->setAttribute( 'name' , Yii::app()->request->getPost('name2') );
			$this->setAttribute( 'npp' , Yii::app()->request->getPost('npp') );
			$this->setAttribute( 'npp_up' , Yii::app()->request->getPost('npp_up') );
			$this->setAttribute( 'other' , Yii::app()->request->getPost('other') );
			$this->setPrimaryKey(Yii::app()->request->getPost('id2'));
			$this->isNewRecord=false;
			$tt2= $this->update();
		} else {
			$this->setAttribute( 'id' , '' );
			$this->setAttribute( 'language' , 'en' );
			$this->setAttribute( 'name' , $_POST['name2'] );
			$this->setAttribute( 'cod' , $_POST['cod'] );
			$this->setAttribute( 'cod_up' , $_POST['cod_up'] );
			$this->setAttribute( 'npp' , Yii::app()->request->getPost('npp') );
			$this->setAttribute( 'npp_up' , Yii::app()->request->getPost('npp_up') );
			$this->setAttribute( 'other' , Yii::app()->request->getPost('other') );
			$this->setAttribute( 'view' , $_POST['view'] );
			$this->setAttribute( 'img' , 0);
			$this->isNewRecord=true;
			$ttt = $this->insert();
		};
		//
		if ($_POST['id3'] != 0) {
			$this->setAttribute( 'id', Yii::app()->request->getPost('id3'));
			$this->setAttribute( 'name' , Yii::app()->request->getPost('name3') );
			$this->setAttribute( 'npp' , Yii::app()->request->getPost('npp') );
			$this->setAttribute( 'npp_up' , Yii::app()->request->getPost('npp_up') );
			$this->setAttribute( 'other' , Yii::app()->request->getPost('other') );
			$this->setPrimaryKey(Yii::app()->request->getPost('id3'));
			$this->isNewRecord=false;
			$tt3 = $this->update();
		} else {
			$this->setAttribute( 'id' , '' );
			$this->setAttribute( 'language' , 'fr' );
			$this->setAttribute( 'name' , $_POST['name3'] );
			$this->setAttribute( 'cod' , $_POST['cod'] );
			$this->setAttribute( 'cod_up' , $_POST['cod_up'] );
			$this->setAttribute( 'npp' , Yii::app()->request->getPost('npp') );
			$this->setAttribute( 'npp_up' , Yii::app()->request->getPost('npp_up') );
			$this->setAttribute( 'other' , Yii::app()->request->getPost('other') );
			$this->setAttribute( 'view' , $_POST['view'] );
			$this->setAttribute( 'img' , 0);
			$this->isNewRecord=true;
			$ttt = $this->insert();
		};
		//
		if ($_POST['id4'] != 0) {
			$this->setAttribute( 'id', Yii::app()->request->getPost('id4'));
			$this->setAttribute( 'name' , Yii::app()->request->getPost('name4') );
			$this->setAttribute( 'npp' , Yii::app()->request->getPost('npp') );
			$this->setAttribute( 'npp_up' , Yii::app()->request->getPost('npp_up') );
			$this->setAttribute( 'other' , Yii::app()->request->getPost('other') );
			$this->setPrimaryKey(Yii::app()->request->getPost('id4'));
			$this->isNewRecord=false;
			$tt4 = $this->update();
		} else {
			$this->setAttribute( 'id' , '' );
			$this->setAttribute( 'language' , 'ch' );
			$this->setAttribute( 'name' , $_POST['name4'] );
			$this->setAttribute( 'cod' , $_POST['cod'] );
			$this->setAttribute( 'cod_up' , $_POST['cod_up'] );
			$this->setAttribute( 'npp' , Yii::app()->request->getPost('npp') );
			$this->setAttribute( 'npp_up' , Yii::app()->request->getPost('npp_up') );
			$this->setAttribute( 'other' , Yii::app()->request->getPost('other') );
			$this->setAttribute( 'view' , $_POST['view'] );
			$this->setAttribute( 'img' , 0);
			$this->isNewRecord=true;
			$ttt = $this->insert();
		};
		
		print '{"otv":"ok", "id":"'.$_POST['id'].'", "id2":"'.$_POST['id2'].'", "id3":"'.$_POST['id3'].'", "id4":"'.$_POST['id4'].
		'", "tt1":"'.$tt1.'", "tt2":"'.$tt2.'", "tt3":"'.$tt3.'", "tt4":"'.$tt4.
		'"}';
		return;
	}
}