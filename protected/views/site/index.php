<a name="b1" id="b1"></a>
<div class="block b1" id="home">
	<div class="border1">
		<div class="subh">
			САМОЕ БОЛЬШОЕ В МИРЕ СООБЩЕСТВО<!--Самый большой выбор в мире -->
		</div>
		<div class="h">
			МОДЕЛЕЙ, МОДЕЛЬНЫХ АГЕНТСТВ, ФОТОГРАФОВ, ЭКСПЕРТОВ МОДЫ<!-- моделей, модельных агенств, фотографов, экспертов моды -->
		</div>
		<div class="subh">
			И ДРУГИХ ПРОФЕССИОНАЛОВ ИНДУСТРИИ МОДЫ<!-- и прочих fashion профессионалов -->
		</div>
		<div class="menu2">
			<a class="menu2e" id="but_join">РЕГИСТРАЦИЯ</a>
			<a class="menu2e" id="but_signin">АВТОРИЗАЦИЯ</a>
		</div>
	</div>
</div>
<div class="clear"></div>
<a name="b2" id="b2"></a>
<div class="block b2" id="about">
	<div class="about">
		<div class="col-ab">
			<div class="sub-col-ab">
				<div class="col-icon">
					<a href="/about"><i class="fa fa-4x fa-info-circle" aria-hidden="true"></i>
					<br/>о нас</a>
				</div>
			</div>
		</div>
		
		<div class="col-ab">
			<div class="sub-col-ab">
				<div class="col-icon">
					<a href="/services"><i class="fa fa-4x fa-cogs" aria-hidden="true"></i>
					<br/>Наши услуги</a>
				</div>
			</div>
		</div>
		
		<div class="col-ab">
			<div class="sub-col-ab">
				<div class="col-icon">
					<a href="/mentors"><i class="fa fa-4x fa-users" aria-hidden="true"></i>
					<br/>наши наставники</a>
				</div>
			</div>
		</div>
		
		<div class="col-ab">
			<div class="sub-col-ab">
				<div class="col-icon">
					<a href="/faq"><i class="fa fa-4x fa-question-circle" aria-hidden="true"></i>
					<br/>вопросы-ответы</a>
				</div>
			</div>
		</div>
		
		<div class="col-ab">
			<div class="sub-col-ab">
				<div class="col-icon">
					<a href="/contacts"><i class="fa fa-4x fa-envelope-o" aria-hidden="true"></i>
					<br/>контакты</a>
				</div>
			</div>
		</div>
		
		<h1>ДУМАЙТЕ ШИРЕ. ДУМАЙТЕ В МЕЖДУНАРОДНОМ МАСШТАБЕ. МЕЧТАЙТЕ.<!-- Думай шире. Думай глобально. Мечтай. --></h1>
		<div class="ln"></div>
	</div>
</div>

<a name="b3" id="b3"></a> <div class="block b3" id="features">
	<div class="features">
		<div class="wrap-f">
			<div class="f-1-2">
				<div class="carousel-pagination">
					<ul class="list-pagination">
						<li class="c-p current">
							<a class="slid" id="s0" sme="0" href="#s1">МОДЕЛИ<!-- models--></a>
						</li>
						<li class="c-p">
							<a class="slid" id="s-1" sme="-1" href="#s2">фотографы<!-- photographers --></a>
						</li>
						<li class="c-p">
							<a class="slid" id="s-2" sme="-2" href="#s3">Модельные агентства<!-- Model Agencies --></a>
						</li>
						<li class="c-p">
							<a class="slid" id="s-3" sme="-3" href="#s4">Эксперты моды<!-- Fashion Experts --></a>
						</li>
						<li class="c-p">
							<a class="slid" id="s-4" sme="-4" href="#s5">Другие профессионалы<!--Other Professionals--></a>
						</li>
					</ul>
				</div>
				<div class="carousel">
					<div class="car-wrap">
						<div class="f-2-3"> 
							<div id="ulsl" onmousedown="return false" onselectstart="return false">
								<div class="item">
									<div class="i-i-w">
										<div class="item-img">
											<img src="../images/f_s_f1.jpg">
										</div>
										Добро пожаловать на fashiongreatness.com, многофункциональный международный портал для лучших профессионалов индустрии моды. Наше эксклюзивное сообщество предлагает широкий спектр возможностей для любой категории специалистов этой сферы. Таким образом, модели могут использовать такую уникальную услугу, как персональный PR-менеджер, который поможет им продвигать свой профиль на нашем сайте. Кроме того, модели могут находить предложения о работе, искать фотографов, устанавливать новые контакты, участвовать в конкурсах, кастингах, неделях моды, шоу-румах, иметь доступ к VIP-мероприятиям закрытого типа по всему миру, подписываться на/ проводить мастер-классы; отслеживать новости мира моды и узнавать о реалиях модельного бизнеса, загружать фотографии в лучшем качестве и многое другое. И это только малая часть преимуществ, которые можно получить при пользовании нашим порталом. Мы подготовили множество приятных сюрпризов для вас. Приготовьтесь!									</div>
								</div>
								<div class="item">
									<div class="i-i-w">
										<div class="item-img">
											<img src="../images/f_s_f2.jpg">
										</div>
										Добро пожаловать в моде greatness.com, многофункциональной всемирной сети для лучших профессионалов в области индустрии моды. Наше эксклюзивное сообщество предлагает широкий спектр возможностей для любой категории специалистов в этой сфере. Таким образом, фотографы могут использовать такую уникальную услугу в качестве персонального менеджера по связям с общественностью, который поможет им продвигать свой профиль на нашем сайте. Кроме того, фотографы могут продать свои услуги; поиск клиентов; подписаться на / проводить мастер-классы; обмен опытом с другими людьми; знак субподряда; поиск моделей; загрузить фотографии в лучшем качестве; участвовать в конкурсах; имеют доступ к эксклюзивным VIP-мероприятий по всему миру закрытого типа и многие другие. Это лишь малая часть преимуществ, которые вы можете предпринять использования нашей сети. Мы подготовили много приятных сюрпризов для вас. Будь готов!									</div>
								</div>
								<div class="item">
									<div class="i-i-w">
										<div class="item-img">
											<img src="../images/f_s_f3.jpg">
										</div>
										Добро пожаловать на fashiongreatness.com, многофункциональный международный портал для лучших профессионалов индустрии моды. Наше эксклюзивное сообщество предлагает широкий спектр возможностей для любой категории специалистов этой сферы. Таким образом, модельные агентства могут использовать такую уникальную услугу, как персональный PR-менеджер, который поможет им продвигать их моделей на нашем сайте; регистрацию корпоративного аккаунта с определённым количеством профилей для моделей, получение информации о совершённых их моделями сделках на сайте, заполнение календаря занятости. К тому же мы всегда готовы обсудить дополнительные уникальные возможности, предоставляемые вашей компании. Помимо всего прочего, модельные агентства смогут проводить мастер-классы; искать новых моделей; подписывать контракты по субподряду (искать фотографов, визажистов и других профессионалов); использовать огромное количество инструментов по самопродвижению, PR услуг; иметь доступ к VIP-мероприятиям закрытого типа по всему миру; устанавливать новые контакты и многое другое. И это только малая часть преимуществ, которые можно получить при пользовании нашим порталом. Мы подготовили множество приятных сюрпризов для вас. Приготовьтесь!									</div>
								</div>
								<div class="item">
									<div class="i-i-w">
										<div class="item-img">
											<img src="../images/f_s_f4.jpg">
										</div>
										Добро пожаловать на fashiongreatness.com, многофункциональный международный портал для лучших профессионалов индустрии моды. Наше эксклюзивное сообщество предлагает широкий спектр возможностей для любой категории специалистов этой сферы. Таким образом, эксперты моды могут использовать такую уникальную услугу, как персональный PR-менеджер, который поможет им продвигать свой профиль на нашем сайте. Кроме того, эксперты моды могут искать новых клиентов, партнёров; подписывать контракты; использовать огромное количество инструментов по самопродвижению и PR услуг для того, чтобы представлять свои интересы, торговую марку или услуги; сотрудничать с другими профессионалами индустрии моды и участвовать в международных проектах, неделях моды, шоу-румах; иметь доступ к VIP-мероприятиям закрытого типа по всему миру и многое другое. И это только малая часть преимуществ, которые можно получить при пользовании нашим порталом. Мы подготовили множество приятных сюрпризов для вас. Приготовьтесь!									</div>
								</div>
								<div class="item">
									<div class="i-i-w">
										<div class="item-img">
											<img src="../images/f_s_f5.jpg">
										</div>
										Добро пожаловать на fashiongreatness.com, многофункциональный международный портал для лучших профессионалов индустрии моды. Наше эксклюзивное сообщество предлагает широкий спектр возможностей для любой категории специалистов этой сферы. Таким образом, профессионалы индустрии моды могут использовать такую уникальную услугу, как персональный PR-менеджер, который поможет им продвигать свой профиль на нашем сайте. Кроме того, профессионалы индустрии моды могут продавать свои услуги; искать новых клиентов; получать самую свежие новости из мира моды и практическую информацию о развитии карьеры; подписываться на/ проводить мастер-классы; участвовать в международных проектах, неделях моды, шоу-румах; иметь доступ к VIP-мероприятиям закрытого типа по всему миру и многое другое. И это только малая часть преимуществ, которые можно получить при пользовании нашим порталом. Мы подготовили множество приятных сюрпризов для вас. Приготовьтесь!									</div>
								</div>
								<div class="clear"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
				
			<div class="f-2-2">
				<p class="have">У нас есть</p>
				<div class="have-list">
					<p>На сайте уже зарегистрированы</p>
					<p>МОДЕЛИ</p>
					<hr style="width:30%;">
					<p>фотографы</p>
					<hr style="width:50%;">
					<p>Модельные агентства</p>
					<hr style="width:20%;">
					<p>Эксперты моды</p>
					<hr style="width:80%;">
				</div>
			</div>
			
			<div class="clear"></div>
			
			<div class="video-wrapper">
				<div class="video-responsive">
					<iframe src="https://www.youtube.com/embed/iGaF4tKUl0o" frameborder="0" allowfullscreen></iframe>
				</div>
				<div class="video-responsive">
					<iframe src="https://www.youtube.com/embed/sfbfaeG7EJU" frameborder="0" allowfullscreen></iframe>
				</div>
				<div class="clear"></div>
			</div>
			
			<div class="clear"></div>
			
		</div>
	</div>
</div>

<a name="b4" id="b4"></a> <div class="block b4" id="team">
	<div class="border2">
		<div class="yln"></div>
		<img class="t-i" src="img/creative-team.png">
		<p>МЫ — ПРОФЕССИОНАЛЫ СВОЕГО ДЕЛА С МНОГОЛЕТНИМ ОПЫТОМ В СФЕРЕ ВЫСОКОЙ МОДЫ..</p>
		<div class="carousel">
			<div class="arrow l-arrow">
				<img src="../images/prev.png" id="but_team_next">
			</div>
			<div class="car-wrap" id="team_slid">
				
												
				<div class="team-item" id="team_1">
					<p class="team-name">АРТУР ФРАНЧУК ФОН МАНШТЕЙН<br><span>директор, мода величие группа, ООО</span></p>
					<div class="team-inner">
						<div class="team-img">
							<img src="../images/t_f1.jpg" alt="">
						</div>
						<div class="team-description">
							Что делает меня по-настоящему счастливым, так это тот факт, что мы объединяем лучших профессионалов индустрии моды со всех уголков земного шара.						</div>
						<div class="clear"></div>
						<div class="social-networks">
							<a href="mailto:afvm@fashiongreatness.com">afvm@fashiongreatness.com</a>
						</div>
					</div>
				</div>
										
				<div class="team-item" id="team_2">
					<p class="team-name">Алекс<br><span>главный редактор, fashiongreatness.com</span></p>
					<div class="team-inner">
						<div class="team-img">
							<img src="../images/t_f2.jpg" alt="">
						</div>
						<div class="team-description">
							Каждый раз, когда я начинаю писать статью, я спрашиваю себя "эта информация может быть применена на практике?". Если ответ да, я это пишу						</div>
						<div class="clear"></div>
						<div class="social-networks">
							<a href="mailto:Alexandra.S@fashiongreatness.com">Alexandra.S@fashiongreatness.com</a>
						</div>
					</div>
				</div>
										
				<div class="team-item" id="team_3">
					<p class="team-name">Анна<br><span>МЕНЕДЖЕР ПРОЕКТА,FASHIONGREATNESS.COM</span></p>
					<div class="team-inner">
						<div class="team-img">
							<img src="../images/t_f3.jpg" alt="">
						</div>
						<div class="team-description">
							Будь собой, люби то, что делаешь, уважай свой талант и будь успешным!						</div>
						<div class="clear"></div>
						<div class="social-networks">
							<a href="mailto:Anna.May@fashiongreatness.com">Anna.May@fashiongreatness.com</a>
						</div>
					</div>
				</div>
										
				<div class="team-item" id="team_4">
					<p class="team-name">Новый<br><span>Новый член команды</span></p>
					<div class="team-inner">
						<div class="team-img">
							<img src="../images/t_f4.jpg" alt="">
						</div>
						<div class="team-description">
							Делаю						</div>
						<div class="clear"></div>
						<div class="social-networks">
							<a href="mailto:mail@mail.ru">mail@mail.ru</a>
						</div>
					</div>
				</div>
																									
				
			</div>
			<div class="arrow r-arrow">
				<img src="../images/next.png" id="but_team_prev">
			</div>
			<div class="clear"></div>
		</div>
	</div>
</div>

<a name="b5" id="b5"></a>
<div class="block b5" id="prices">
	<div class="price">
		<p class="p-h">ЦЕНОВЫЕ ПАКЕТЫ</p>
		<p class="p-sh">КАЖДЫЙ ЦЕНОВОЙ ПАКЕТ ПРЕДОСТАВЛЯЕТ РАЗНЫЕ ВОЗМОЖНОСТИ. ВЫБЕРИТЕ ТОТ, ЧТО ПОДХОДИТ ИМЕННО ВАМ.</p>
		<div class="price-container">
			<div class="price-item">
				<div class="price-inner">
					<p class="price-title">
						АККАУНТ<!-- Account -->
						<br>
						<span class="price-name">КЛАССИЧЕСКИЙ<!-- Classic --></span>
						<span class="shortline black"></span>
					</p>
					<p class="price-plan"><span>26$</span>&nbsp;/&nbsp;месяц</p>
					<p class="price-description">3 ИЗДАНИЯ ВАКАНСИЙ<br>
20 СОВМЕСТНАЯ ПРЕДЛОЖЕНИЯ<br>
НЕОГРАНИЧЕННАЯ ОТВЕТ НА ВАКАНСИЙ<br>
ОГРАНИЧЕНОЙ в профайле<br>
LIMITED ИЗГОТОВЛЕНИЕ<br></p>
						<a href="#popup-register" data-tarif="5" class="price-btn" id="join1">РЕГИСТРАЦИЯ<!-- JOIN --></a>
						<div class="clear"></div>
				</div>
			</div>
		
			<div class="price-item">
				<div class="price-inner">
					<p class="price-title">
					АККАУНТ<!-- Account -->
					<br>
					<span class="price-name">ВИП<!-- VIP --></span>
					<span class="shortline black"></span>
					</p>
					<p class="price-plan"><span>260$</span>&nbsp;/&nbsp;месяц</p>
					<p class="price-description">ДО 4 ПРЕСС-РЕЛИЗЫ в год<br>
До 4-х очерков В ГОД<br>
3 участников событий различного формата В ГОД<br>
20 ИЗДАНИЯ ВАКАНСИЙ<br>
Неограниченные предложения о сотрудничестве<br>
НЕОГРАНИЧЕННАЯ ОТВЕТ НА ВАКАНСИЙ<br>
ПОЛНАЯ ИНФОРМАЦИЯ В ПРОФИЛЬ<br>
ПОЛНЫЙ возможности настройки<br></p>
					<a href="#popup-register" data-tarif="6" class="price-btn" id="join2">РЕГИСТРАЦИЯ<!-- JOIN --></a>
					<div class="clear"></div>
				</div>
			</div>
		
			<div class="price-item">
				<div class="price-inner">
					<p class="price-title">
					АККАУНТ<!-- Account -->
					<br>
					<span class="price-name">КОРПОРАТИВНЫЙ<!-- Corporate --></span>
					<span class="shortline black"></span>
					</p>
					<p class="price-plan"><span>360$</span>&nbsp;/&nbsp;месяц</p>
					<p class="price-description">ДО 4 ПРЕСС-РЕЛИЗЫ в год<br>
До 4-х очерков В ГОД<br>
3 участников событий различного формата В ГОД<br>
20 ИЗДАНИЯ ВАКАНСИЙ<br>
Неограниченные предложения о сотрудничестве<br>
НЕОГРАНИЧЕННАЯ ОТВЕТ НА ВАКАНСИЙ<br>
ПОЛНАЯ ИНФОРМАЦИЯ В ПРОФИЛЬ<br>
ПОЛНЫЙ возможности настройки<br>
20 ДОПОЛНИТЕЛЬНЫЕ CLASSIC ПРОФИЛИ для сотрудников Вашей компании<br></p>
					<a href="#popup-register" data-tarif="7" class="price-btn" id="join3">РЕГИСТРАЦИЯ<!-- JOIN --></a>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>
</div>

<a name="b6" id="b6"></a>
<div class="block b6" id="contactus">
	<img class="c-img" src="img/contactus.png" alt="Обратная связь" />
	<div class="contact">
		<div class="c-f">
				<div class="c-f-i">
					<input placeholder="ИМЯ *" type="text" class="form-control" id="b6_1" value="" size="0">
				</div>
				<div class="c-f-i">
					<input placeholder="E-MAIL *" type="text" class="form-control" id="b6_2" value="" size="0">
				</div>
				<div class="c-f-i">
					<select class="form-control f-c-s" id="b6_3"><option value='sel_1#1'>Общие вопросы</option><option value='sel_1#2'>Техническая поддержка</option><option value='sel_1#3'>Партнерство / Сотрудничество</option><option value='sel_1#4'>Мероприятия</option><option value='sel_1#5'>Претензии и предложения</option><option value='sel_1#6'>Пресс-службы</option><option value='sel_1#7'>Другие</option></select>
				</div>
				<div class="c-f-i">
					<textarea placeholder="СООБЩЕНИЕ *" id="b6_4" cols="40" rows="5" class="form-control"></textarea>
				</div>
				<div class="c-f-i tc">
					<i class="fa fa2 fa-envelope-o"></i><input class="button-submit" type="submit" id="b6_but" name="web_form_submit" value="отправить">
				</div>
				<div class="c-f-i" style="display:none;" id="b6_soo">
					Thank you for your message. <br>We will answer you as soon as possible.
				</div>
		</div>
	</div>
</div>

<div id="lay-hid_join" class="lay-hid">
	<div class="lay-hid-wrap">
		<form id="form_reg" action="/registration" method="post">
		<div id="lay-hid_join_close" class="lay-hid_close">Х</div>
		<div id="lay-hid_join_forma" class="join-form">
			<h1>РЕГИСТРАЦИЯ</h1>
			<p class="c-p">
ВЫБЕРИТЕ Стоимость пакета</p>
                        <label class="custom-radio"><input type="radio" name="User[account_type]" id="reg_pa1" value="<?php echo UserAccountType::VISITOR;?>" checked></input><span><div></div></span><p>ПОСЕТИТЕЛЬ</p></label>
				<label class="custom-radio"><input type="radio" name="User[account_type]" id="reg_pa2" value="<?php echo UserAccountType::CLASSIC;?>"></input><span><div></div></span><p> КЛАССИЧЕСКИЙ</p></label>
				<label class="custom-radio"><input type="radio" name="User[account_type]" id="reg_pa3" value="<?php echo UserAccountType::VIP;?>"></input><span><div></div></span><p> ВИП</p></label>
				<label class="custom-radio"><input type="radio" name="User[account_type]" id="reg_pa4" value="<?php echo UserAccountType::CORPORATE;?>"></input><span><div></div></span><p> КОРПОРАТИВНЫЙ</p></label>
				<input class="form-field" type="text" name="User[name]" id="reg_nam" placeholder="ИМЯ*">
				<input class="form-field" type="text" name="User[last_name]" id="reg_lna" placeholder="ФАМИЛИЯ*">
				<input class="form-field" type="text" name="User[email]" id="reg_mai" placeholder="E-MAIL*">
				<input class="form-field" type="text" name="User[phone]" id="reg_pho" placeholder="НОМЕР ТЕЛЕФОНА*">
				<select class="form-sel" name="User[language]" id="reg_lan">
					<option value="">ЯЗЫК*</option>					
                                        <?php $htmlOptions = array(); 
                                        echo CHtml::listOptions(null,CHtml::listData(UserLanguage::model()->findAll(),'id','name'),$htmlOptions);
                                        ?>
				</select>
				<select class="form-sel" name="User[country]" id="reg_loc">
					<option value="">СТРАНА*</option>
                                        <?php $htmlOptions = array(); 
                                        echo CHtml::listOptions(null,CHtml::listData(UserCountry::model()->findAll(),'id','name'),$htmlOptions);
                                        ?>
					</select>
			<div id="reg_gr1" style="display:none">
					<select class="form-sel" name="User[reg_kpr]" id="reg_kpr">
						<option>КОЛИЧЕСТВО СПЕЦИАЛЬНОСТЕЙ*</option>
						<option>1</option>
						<option>2</option>
						<option>3</option>
						<option>4</option>
					</select>
					<select class="form-sel" name="User[reg_ypr]" id="reg_ypr">
						<option>ВАША ПРОФЕССИЯ*</option>
						<option value='sel_prof#1'>Модель</option><option value='sel_prof#2'>Фотограф</option><option value='sel_prof#3'>Дизайнер</option><option value='sel_prof#4'>Агентство</option><option value='sel_prof#5'>Эксперт</option>					</select>
				<div class="form-pay">
					<p>Назовите нескольких самых успешных событий / проекты с вашим участием (для каждого ПРОФЕССИИ) И Даешь ИСТОЧНИКОВ, ссылки, где мы можем прочитать о них*</p>
					<textarea name="User[reg_sum]" id="reg_sum"></textarea>
				</div>
			</div>
			<font id="error"></font>
			<div>
				<input type="submit" id="lay-hid_join_but" class="form-btn" value="РЕГИСТРАЦИЯ">
			</div>
		</form>
		</div>
	</div>
    
<script>
	$('#form_reg').submit( function() {
		var soo = "";
                    $.ajax({
                        method: "POST",
                        url: "<?php echo Yii::app()->createUrl('site/registration');?>",
                        data: $('#form_reg').serializeArray()
                      })
                        .done(function( msg ) {
                          if (msg !=''){
                            var dat = $.parseJSON(msg);
                            $.each(dat,function(k,v){
                              soo += "<br>"+v[0];  
                            });
                          } else {
                             window.location.href = "<?php echo Yii::app()->createUrl('site/index')?>"; 
                          }
                          $('#error').html(soo);
                        });
                  // }
		return false;
	});
</script>