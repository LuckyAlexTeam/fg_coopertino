<?php

Yii::import('application.models._base.BaseLanguageList');

class LanguageList extends BaseLanguageList
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
}