<?php

class Manufacturer extends ManufacturerCore {

    public static function getManufacturerIdAddressStatic($id_address)
    {
        if (!(int)$id_address) {
            return false;
        }

        return Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('SELECT `id_address` FROM '._DB_PREFIX_.'address WHERE deleted=0 AND `id_manufacturer` = '.(int)$id_address);
    }
}