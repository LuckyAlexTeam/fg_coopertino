<?php

Yii::import('application.models._base.BaseUserAccountType');

class UserAccountType extends BaseUserAccountType
{
    
    const VISITOR = 1;
    const VIP = 3;
    const CLASSIC = 2;
    const CORPORATE = 4;
    
    
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
}