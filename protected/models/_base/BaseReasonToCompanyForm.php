<?php

/**
 * This is the model class for table "reason_to_company_form".
 *
 * The followings are the available columns in table 'reason_to_company_form':
 * @property integer $id
 * @property integer $reason_id
 * @property integer $company_form_id
 *
 * The followings are the available model relations:
 * @property CompanyForm $companyForm
 * @property MainReasonList $reason
 */
class BaseReasonToCompanyForm extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ReasonToCompanyForm the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'reason_to_company_form';
	}

	/**
	 * @return array validation rules for model attributes.
	 */ 
	 
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('reason_id, company_form_id', 'required'),
			array('reason_id, company_form_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, reason_id, company_form_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'companyForm' => array(self::BELONGS_TO, 'CompanyForm', 'company_form_id'),
			'reason' => array(self::BELONGS_TO, 'MainReasonList', 'reason_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'reason_id' => 'Reason',
			'company_form_id' => 'Company Form',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('reason_id',$this->reason_id);
		$criteria->compare('company_form_id',$this->company_form_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}