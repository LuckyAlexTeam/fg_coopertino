<?php

class SiteController extends CController {
	public $layout='/layouts/main';
	public $arr_menu;
	public $arr_lang;
	public $arr_site;
	public $arr_header;
	public $arr_cont;
	public $arr_block;
	public $arr_maket;
	public $tlanguage;
	
	public $arh = Array(); // Таблица th
	public $ard = Array(); // Таблица соответствия полей формы (rx)(ajax) и имен БД
	public $arp = Array(); // Поля формы
	public $ars = Array(); // собственно данные из БД (array[array])
	public $arv = Array(); // собственно данные из БД (array[array]) - VIP
	public $paginator;
	public $limit = 5;
	public $offset = 0;
	
	public $breadcrumbs;
	/**
	 * Declares class-based actions.
	 */
	public function actions() {
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}
	/*
	public function filters() {
        return array(
            'accessControl',
        );
    }
 */
    public function accessRules() {
        return array(
            // если используется проверка прав, не забывайте разрешить доступ к
            // действию, отвечающему за генерацию изображения
            array('allow',
                'actions'=>array('captcha'),
                'users'=>array('*'),
            ),
            array('deny',
                'users'=>array('*'),
            ),
        );
    }
	//
	public function actionIndex() {
		$model=new LoginForm;
		//var_dump($_POST);
		if(isset($_POST['LoginForm'])) {
			//print "<br>LoginForm";
			$model->attributes=$_POST['LoginForm'];
			//$arr = $_POST['LoginForm'];
			//var_dump($arr);
			//$uuu = $arr['username'];
			//$model->username = "admin"; // $arr['username'];
			//$model->password = "admin";
			//print "<br>model->validate()";
			//var_dump($model->validate());
			//print "<br>model->login()";
			//var_dump($model->login());
			if($model->validate() && $model->login()) {
				//print "<br>Index";
				$this->render('index');
				//$this->actionSite();
			} else {
				//print "<br>Login";
				$this->actionLogin();
			};
		} else {
			//print "SiteController = Index = login";
			$this->render('login'); // ,array('model'=>$model));
		};
	}
	//
	public function actionSite() {
		$model=new Site;
		if (isset($_POST['add']   )) { $model->Add(); };
		if (isset($_POST['update'])) { $model->Upd();};
		$this->arr_site = $model->getAll($_GET['lan'], $_GET['view']);
		$this->render('site'); 
	}
	//
	public function actionBlock() {
		$model=new SiteBlock;
		if (isset($_GET['add'])) { $model->Add(); };
		if (isset($_GET['upd'])) { $model->Upd(); };
		$this->arh = $model->getAttributes();
		$this->arr_site = $model->getAll();
		$this->render('siteblock'); 
	}
	//
	public function actionMaket() {
		$model=new SiteMaket;
		if (isset($_GET['add'])) { $model->Add(); };
		if (isset($_GET['upd'])) { $model->Upd(); };
		$this->arh = $model->getAttributes();
		$this->arr_site = $model->getAll();
		$this->render('sitemaket'); 
	}
	// Новый конструктор
	public function actionKonst() {
		$model=new SiteBlock;
		if (isset($_GET['add'])) { $model->Add(); };
		if (isset($_GET['upd'])) { $model->Upd(); };
		//$this->arh = $model->getAttributes();
		$this->arr_block = $model->getAll();
		
		$model=new SiteMaket;
		if (isset($_REQUEST['ope'])) { 
			if ($_REQUEST['ope'] == "ДП") {$model->Add_DP(); };
		};
		$this->arr_maket = $model->getAll();
		$this->render('konstructor'); 
	}
	//
	public function actionAjax() {
		$model=new Site;
		$model->Ajax();
		//$this->render('site'); 
	}
	public function actionTeam() {
		$model=new Site;
		if (isset($_POST['add']   )) { $model->Add(); };
		if (isset($_POST['update'])) { $model->Upd();};
		$this->arr_site = $model->getAllTeam($_GET['lan']);
		$this->render('team'); 
	}
	public function actionUsers() {
		$model=new Users;
		//if (isset($_POST['add']   )) { $model->Add(); };
		//if (isset($_POST['update'])) { $model->Upd();};
		$this->ars = $model->getAll(); 
		$this->arh = $model->attributeLabels();
		$this->render('users'); 
	}
	// Прайс услуг
	public function actionPrice() {
		$model=new Nastroyki;
		if(Yii::app()->request->isAjaxRequest) {
			print '"otv":"ok"';
			return;
		};
		$this->ars = $model->getAll('price');
		$this->render('price'); 
	}
	// E-Mail на которые рассылать регистрации
	public function actionMail() {
		$model=new Nastroyki;
		if(Yii::app()->request->isAjaxRequest) {
			$model->Ajax(); 
			print '"otv":"ok"';
			return;
		};
		$this->ars = $model->getAll('mail');
		$this->render('mail'); 
	}
	// E-Mail на которые рассылать регистрации
	public function actionMailOs() {
		$model=new Nastroyki;
		if(Yii::app()->request->isAjaxRequest) {
			$model->Ajax_2(); 
			print '"otv":"ok"';
			return;
		};
		$this->ars = $model->getAll('mailos');
		$this->render('mailos'); 
	}
	// Название месяцев
	public function actionMonth() {
		$model=new Nastroyki;
		if(Yii::app()->request->isAjaxRequest) {
			$model->Ajax_2(); 
			print '"otv":"ok"';
			return;
		};
		$this->ars = $model->getAll('month');
		$this->render('month'); 
	}
	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError() {
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin() {
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form 
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout() {
		if (TRASSA == "Y") {print "/protected/controllers/SiteController.php - function actionLogout() <br>";};
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
	
	public function actionMenu() {
		$model=new Menu_Role;
		$this->arr_menu = $model->getAll();
	}
	public function site_stranica() {
		$model=new Language();
		$this->tlanguage = $model->getLanguage();
		$this->arr_lang = $model->getAll();
		$modea=new Site();
		$this->arr_site = $modea->getAll($this->tlanguage, 'main');
		$modeb=new SiteHeader();
		$this->arr_header = $modeb->getAll($this->tlanguage, 'index');
	}
}