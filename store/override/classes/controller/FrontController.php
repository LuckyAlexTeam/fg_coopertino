<?php
class FrontController extends FrontControllerCore
{
	/**
	 * Displays maintenance page if shop is closed.
	 */
	protected function displayMaintenancePage()
	{
		$cookie = new Cookie('psAdmin');

		if ($this->maintenance == true || !(int)Configuration::get('PS_SHOP_ENABLE')) {
			$this->maintenance = true;
			if (!in_array(Tools::getRemoteAddr(), explode(',', Configuration::get('PS_MAINTENANCE_IP'))) && !$cookie->id_employee) {
				header('HTTP/1.1 503 temporarily overloaded');

				$this->context->smarty->assign($this->initLogoAndFavicon());
				$this->context->smarty->assign(array(
					'HOOK_MAINTENANCE' => Hook::exec('displayMaintenance', array()),
				));

				if (Tools::isSubmit('loginForm')) {
					$this->context->smarty->assign(array(
						'wrong' => 1,
					));;
				}

				// If the controller is a module, then getTemplatePath will try to find the template in the modules, so we need to instanciate a real frontcontroller
				$front_controller = preg_match('/ModuleFrontController$/', get_class($this)) ? new FrontController() : $this;
				$this->smartyOutputContent($front_controller->getTemplatePath($this->getThemeDir().'maintenance.tpl'));
				exit;
			}
		}
	}

	/**
	 * Sets controller CSS and JS files.
	 *
	 * @return bool
	 */
	public function setMedia()
	{
		/**
		 * If website is accessed by mobile device
		 * @see FrontControllerCore::setMobileMedia()
		 */
		if ($this->useMobileTheme())
		{
			$this->setMobileMedia();
			return true;
		}

		$this->addCSS(_THEME_CSS_DIR_.'grid_prestashop.css', 'all');  // retro compat themes 1.5.0.1
		$this->addCSS(_THEME_CSS_DIR_.'global.css', 'all');
		$this->addCSS(_THEME_CSS_DIR_.'shop.css', 'all');
		$this->addJquery();
		$this->addJqueryPlugin('easing');
		$this->addJqueryPlugin('formstyler');
		$this->addJqueryPlugin('validate');
		$this->addJS(_PS_JS_DIR_.'jquery/plugins/validate/localization/messages_'.$this->context->language->iso_code.'.js');
		$this->addJS(_PS_JS_DIR_.'tools.js');
		$this->addJS(_THEME_JS_DIR_.'global.js');
		$this->addJS(_THEME_JS_DIR_.'shop.js');

		// Automatically add js files from js/autoload directory in the template
		if (@filemtime($this->getThemeDir().'js/autoload/'))
			foreach (scandir($this->getThemeDir().'js/autoload/', 0) as $file)
				if (preg_match('/^[^.].*\.js$/', $file))
					$this->addJS($this->getThemeDir().'js/autoload/'.$file);
		// Automatically add css files from css/autoload directory in the template
		if (@filemtime($this->getThemeDir().'css/autoload/'))
			foreach (scandir($this->getThemeDir().'css/autoload', 0) as $file)
				if (preg_match('/^[^.].*\.css$/', $file))
					$this->addCSS($this->getThemeDir().'css/autoload/'.$file);

		if (Tools::isSubmit('live_edit') && Tools::getValue('ad') && Tools::getAdminToken('AdminModulesPositions'.(int)Tab::getIdFromClassName('AdminModulesPositions').(int)Tools::getValue('id_employee')))
		{
			$this->addJqueryUI('ui.sortable');
			$this->addjqueryPlugin('fancybox');
			$this->addJS(_PS_JS_DIR_.'hookLiveEdit.js');
		}

		if (Configuration::get('PS_QUICK_VIEW'))
			$this->addjqueryPlugin('fancybox');

		if (Configuration::get('PS_COMPARATOR_MAX_ITEM') > 0)
			$this->addJS(_THEME_JS_DIR_.'products-comparison.js');

		// Execute Hook FrontController SetMedia
		Hook::exec('actionFrontControllerSetMedia', array());

		return true;
	}

	/**
	 * Compiles and outputs page header section (including HTML <head>)
	 *
	 * @param bool $display If true, renders visual page header section
	 * @deprecated 1.5.0.1
	 */
	public function displayHeader($display = true)
	{
		Tools::displayAsDeprecated();

		$this->initHeader();
		$hook_header = Hook::exec('displayHeader');
		if ((Configuration::get('PS_CSS_THEME_CACHE') || Configuration::get('PS_JS_THEME_CACHE')) && is_writable(_PS_THEME_DIR_.'cache')) {
			// CSS compressor management
			if (Configuration::get('PS_CSS_THEME_CACHE')) {
				$this->css_files = Media::cccCss($this->css_files);
			}

			//JS compressor management
			if (Configuration::get('PS_JS_THEME_CACHE')) {
				$this->js_files = Media::cccJs($this->js_files);
			}
		}

		// Call hook before assign of css_files and js_files in order to include correctly all css and javascript files
		$this->context->smarty->assign(array(
			'HOOK_HEADER'       => $hook_header,
			'HOOK_TOP'          => Hook::exec('displayTop'),
			'HOOK_LEFT_COLUMN'  => ($this->display_column_left  ? Hook::exec('displayLeftColumn') : ''),
			'HOOK_RIGHT_COLUMN' => ($this->display_column_right ? Hook::exec('displayRightColumn', array('cart' => $this->context->cart)) : ''),
			'HOOK_FOOTER'       => Hook::exec('displayFooter')
		));

		$this->context->smarty->assign(array(
			'css_files' => $this->css_files,
			'js_files'  => ($this->getLayout() && (bool)Configuration::get('PS_JS_DEFER')) ? array() : $this->js_files
		));

		$this->display_header = $display;
		$this->smartyOutputContent(_PS_THEME_DIR_.'header.tpl');
	}

}
