{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<ul	class="shop_menu">
	{foreach from=$cms_menu_new.children item=menu}
		{if $cms_category->id_cms_category == $menu.id}
			<li><span>{$menu.name|escape:'html':'UTF-8'}</span></li>
		{else}
			<li><a href="{$menu.link}">{$menu.name|escape:'html':'UTF-8'}</a></li>
		{/if}
	{/foreach}
	{*{foreach from=$cms_menu item=menu}*}
		{*{if isset($cms) && ($cms->id == $menu.id_cms)}*}
			{*<li><span>{$menu.meta_title|escape:'html':'UTF-8'}</span></li>*}
		{*{else}*}
			{*<li><a href="{$link->getCMSLink($menu.id_cms, $menu.link_rewrite)|escape:'html':'UTF-8'}">{$menu.meta_title|escape:'html':'UTF-8'}</a></li>*}
		{*{/if}*}
	{*{/foreach}*}
</ul>

{*{if isset($cms) && !isset($cms_category)}*}
	{*{if !$cms->active}*}
		{*<br />*}
		{*<div id="admin-action-cms">*}
			{*<p>*}
				{*<span>{l s='This CMS page is not visible to your customers.'}</span>*}
				{*<input type="hidden" id="admin-action-cms-id" value="{$cms->id}" />*}
				{*<input type="submit" value="{l s='Publish'}" name="publish_button" class="button btn btn-default"/>*}
				{*<input type="submit" value="{l s='Back'}" name="lnk_view" class="button btn btn-default"/>*}
			{*</p>*}
			{*<div class="clear" ></div>*}
			{*<p id="admin-action-result"></p>*}
		{*</div>*}
	{*{/if}*}
	{*<div class="rte{if $content_only} content_only{/if}">*}
		{*{$cms->content}*}
	{*</div>*}
{*{elseif isset($cms_category)}*}
{if isset($cms_category)}
	{*<div class="block-cms">*}
		<div class="care_title">{$cms_category->name|escape:'html':'UTF-8'}</div>
		{*{if $cms_category->description}*}
			{*<p>{$cms_category->description|escape:'html':'UTF-8'}</p>*}
		{*{/if}*}
		{*{if isset($sub_category) && !empty($sub_category)}*}
			{*<p class="title_block">{l s='List of sub categories in %s:' sprintf=$cms_category->name}</p>*}
			{*<ul class="bullet list-group">*}
				{*{foreach from=$sub_category item=subcategory}*}
					{*<li>*}
						{*<a class="list-group-item" href="{$link->getCMSCategoryLink($subcategory.id_cms_category, $subcategory.link_rewrite)|escape:'html':'UTF-8'}">{$subcategory.name|escape:'html':'UTF-8'}</a>*}
					{*</li>*}
				{*{/foreach}*}
			{*</ul>*}
		{*{/if}*}
		{*{if isset($cms_pages) && !empty($cms_pages)}*}
		{*<p class="title_block">{l s='List of pages in %s:' sprintf=$cms_category->name}</p>*}
			{*<ul class="bullet list-group">*}
				{*{foreach from=$cms_pages item=cmspages}*}
					{*<li>*}
						{*<a class="list-group-item" href="{$link->getCMSLink($cmspages.id_cms, $cmspages.link_rewrite)|escape:'html':'UTF-8'}">{$cmspages.meta_title|escape:'html':'UTF-8'}</a>*}
					{*</li>*}
				{*{/foreach}*}
			{*</ul>*}
		{*{/if}*}
	{*</div>*}

	<div>

		<!-- Nav tabs -->
		{*<ul class="nav nav-tabs" role="tablist">*}
		<ul class="nav_help">
			{foreach from=$cms_pages item=cmspages name=cmsMenu}
				<li role="presentation" {if $smarty.foreach.cmsMenu.iteration == 1} class="active"{/if}><a href="#tab-{$cmspages.id_cms}" aria-controls="tab-{$cmspages.id_cms}" role="tab" data-toggle="tab">{$cmspages.meta_title|escape:'html':'UTF-8'}</a></li>
			{/foreach}
			{*<li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Profile</a></li>*}
			{*<li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Messages</a></li>*}
			{*<li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Settings</a></li>*}
		</ul>

		<!-- Tab panes -->
		<div class="tab-content">
			{foreach from=$cms_pages item=cmspages name=cmsContent}
				<div role="tabpanel" class="tab-pane fade in {if $smarty.foreach.cmsContent.iteration == 1} active{/if}" id="tab-{$cmspages.id_cms}">{$cmspages.content}</div>
			{/foreach}
			{*<div role="tabpanel" class="tab-pane fade " id="profile">..fdgdfg.</div>*}
			{*<div role="tabpanel" class="tab-pane fade " id="messages">.dfgdfg..</div>*}
			{*<div role="tabpanel" class="tab-pane fade " id="settings">dfggf</div>*}
		</div>

	</div>
	{else}
	<div class="alert alert-danger">
		{l s='This page does not exist.'}
	</div>
{/if}
<br />
{strip}
{if isset($smarty.get.ad) && $smarty.get.ad}
{addJsDefL name=ad}{$base_dir|cat:$smarty.get.ad|escape:'html':'UTF-8'}{/addJsDefL}
{/if}
{if isset($smarty.get.adtoken) && $smarty.get.adtoken}
{addJsDefL name=adtoken}{$smarty.get.adtoken|escape:'html':'UTF-8'}{/addJsDefL}
{/if}
{/strip}
