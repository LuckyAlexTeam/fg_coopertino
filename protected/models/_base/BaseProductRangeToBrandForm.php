<?php

/**
 * This is the model class for table "product_range_to_brand_form".
 *
 * The followings are the available columns in table 'product_range_to_brand_form':
 * @property integer $id
 * @property integer $product_range_id
 * @property integer $brand_form_id
 *
 * The followings are the available model relations:
 * @property BrandForm $brandForm
 * @property ProductRangeList $productRange
 */
class BaseProductRangeToBrandForm extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ProductRangeToBrandForm the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'product_range_to_brand_form';
	}

	/**
	 * @return array validation rules for model attributes.
	 */ 
	 
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('product_range_id', 'required'),
			array('product_range_id, brand_form_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, product_range_id, brand_form_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'brandForm' => array(self::BELONGS_TO, 'BrandForm', 'brand_form_id'),
			'productRange' => array(self::BELONGS_TO, 'ProductRangeList', 'product_range_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'product_range_id' => 'Product Range',
			'brand_form_id' => 'Brand Form',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('product_range_id',$this->product_range_id);
		$criteria->compare('brand_form_id',$this->brand_form_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}