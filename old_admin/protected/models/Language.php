<?php
class Language extends CActiveRecord {
	public $tlanguage = "";
	public $lang = "";

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    public function tableName() {
        return 'language';
    }
	// cod Код языка ico Путь к иконке name Наименование языка на языке def
    public function getAll() {
		$criteria = new CDbCriteria;
		$criteria->select = '*'; // 't.cod AS cod1, t.name AS name1, t.def AS def1';
		$this->lang = self::model()->findAll($criteria);
		return $this->lang;
    }
	//
	//
	public function getLanguage() {
		if ( isset($_COOKIE['language'])) { $this->tlanguage = $_COOKIE['language']; return $this->tlanguage; };
		$aaa = $this->getAll();
		foreach ($aaa as $key => $val) {
			if ($val['def'] == "Y") {$this->tlanguage = $val['cod']; return $this->tlanguage; };
		};
		return "ch";
    }
	//
}