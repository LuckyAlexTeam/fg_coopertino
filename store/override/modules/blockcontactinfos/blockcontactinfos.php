<?php

	
class BlockcontactinfosOverride extends Blockcontactinfos
{

	public function install()
	{
		return (parent::install() && $this->registerHook('showContactInfos'));
	}

	public function hookShowContactInfos($params)
	{
		return $this->hookFooter($params);
	}

}