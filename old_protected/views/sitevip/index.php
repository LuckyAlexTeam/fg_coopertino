<!--setevip/index.php + mainvip.php -->
<style type="text/css">
	.model {
		display: none;
	}
	.photographer {
		display: inline-block;
	}
</style>
<!-- Анкета Модель -->
<!--
<div class="person-wrapper model photographer">
	<h2>Подробно заполните Вашу анкету</h2>
	<form class="acc-gen">
		<h2>General information</h2>
		<span class="hr"></span>
		<div class="d-wrap model photographer">
			<div class="details-item">
				*First Name:
			</div>
			<div class="details-data general-input">
				<input type="text" class="f-n" placeholder="First Name *"></input>
			</div>
		</div>
		<div class="d-wrap model photographer">
			<div class="details-item">
				*Lust Name:
			</div>
			<div class="details-data general-input">
				<input type="text" class="l-n" placeholder="Last Name *"></input>
			</div>
		</div>
		<div class="d-wrap model photographer">
			<div class="details-item">
				*Phone number:
			</div>
			<div class="details-data general-input">
				<input type="phone" class="ph-n" placeholder="Phone Number *"></input>
			</div>
		</div>
		<div class="d-wrap model photographer">
			<div class="details-item">
				*E-mail:
			</div>
			<div class="details-data general-input">
				<input type="text" class="em" placeholder="E-mail *"></input>
			</div>
		</div>
		<div class="d-wrap model photographer">
			<div class="details-item">
				*Skype:
			</div>
			<div class="details-data general-input">
				<input type="text" class="sk" placeholder="Skype *"></input>
			</div>
		</div>
		<div class="d-wrap model">
			<div class="details-item">
				Agency Info:
			</div>
			<div class="details-data general-input">
				<input type="text" class="a-i" placeholder="Agency Info:"></input>
			</div>
		</div>
		<div class="d-wrap model photographer">
			<div class="details-item">
				Web-site:
			</div>
			<div class="details-data general-input">
				<input type="text" class="w-s" placeholder="Web-site:"></input>
			</div>
		</div>
		<div class="d-wrap model photographer">
			<div class="details-item model photographer">
				*Gender:
			</div>
			<div class="details-data general-input">
				<select class="ge">
					<option>Male</option>
					<option>Female</option>
				</select>
			</div>
		</div>
		<div class="d-wrap model photographer">
			<div class="details-item">
				*Date of Birth:
			</div>
			<div class="details-data general-input">
				<input type="text" class="d-b"></input>
			</div>
		</div>
		<div class="d-wrap model">
			<div class="details-item">
				*Do you have your Legal Guardian’s agree [required for models under 18]:
			</div>
			<div class="details-data general-input">
				<select class="guardians">
					<option>Yes</option>
					<option>No</option>
				</select>
			</div>
		</div>
		<div class="d-wrap model photographer">
			<div class="details-item">
				*Location:
			</div>
			<div class="details-data general-input">
				<select>
					<option class="loc loc-m">China:</option>
					<option class="loc loc-ch">Beijing</option>
					<option class="loc loc-ch">Hong Gong</option>
					<option class="loc loc-ch">Shanghai</option>
					<option class="loc loc-ch">Other</option>
					<option class="loc loc-m">England:</option>
					<option class="loc loc-en">London</option>
					<option class="loc loc-en">London area</option>
					<option class="loc loc-en">Other</option>
					<option class="loc loc-m">France:</option>
					<option class="loc loc-fr">Paris</option>
					<option class="loc loc-fr">Paris area</option>
					<option class="loc loc-fr">Other</option>
					<option class="loc loc-m">Germany:</option>
					<option class="loc loc-ge">Berlin</option>
					<option class="loc loc-ge">Berlin area</option>
					<option class="loc loc-ge">Other</option>
					<option class="loc loc-m">Italy:</option>
					<option class="loc loc-it">Milan</option>
					<option class="loc loc-it">Milan area</option>
					<option class="loc loc-it">Rome</option>
					<option class="loc loc-it">Rome area</option>
					<option class="loc loc-it">Other</option>
					<option class="loc loc-m">USA:</option>
					<option class="loc loc-usa">Los Angeles</option>
					<option class="loc loc-usa">Los Angeles area</option>
					<option class="loc loc-usa">New York</option>
					<option class="loc loc-usa">New York area</option>
					<option class="loc loc-usa">Other</option>
					<option class="loc loc-m">Ukraine</option>
					<option class="loc loc-m">Russia</option>
					<option class="loc loc-m">Other</option>
				</select>
			</div>
		</div>
	</form>
	<form class="acc-phys model">
		<h2>Physical Details</h2>
		<span class="hr"></span>
		<p class="ch-m-s">Choose you metrical system: 
			<input type="radio" value="cm" checked>cm</input> 
			<input type="radio" value="in">in</input>
		</p>
		<div class="d-wrap">
			<div class="details-item">
				*Height:
			</div>
			<div class="details-data general-input">
				<input type="number" min="61" max="229" class="he" placeholder="165 cm"></input>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Weight:
			</div>
			<div class="details-data general-input">
				<input type="number" min="30" max="170" class="we" placeholder="65 kg"></input>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Chest:
			</div>
			<div class="details-data general-input">
				<input type="number" min="54" max="230" class="che" placeholder="85 cm"></input>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Waist:
			</div>
			<div class="details-data general-input">
				<input type="number" min="48" max="134" class="wa" placeholder="60 cm"></input>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Hips:
			</div>
			<div class="details-data general-input">
				<input type="number" min="48" max="134" class="hips" placeholder="95 cm"></input>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Dress/Jacket, EU:
			</div>
			<div class="details-data general-input">
				<input type="number" min="32" max="60" class="dre" placeholder="38 EU"></input>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Cup:
			</div>
			<div class="details-data general-input">
				<input type="text" class="cup" placeholder="C"></input>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Shoe, EU:
			</div>
			<div class="details-data general-input">
				<input type="number" min="34" max="47" class="shoe" placeholder="37 EU"></input>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Ethnicity:
			</div>
			<div class="details-data general-input">
				<select class="ethn">
					<option>African</option>
					<option>Asian</option>
					<option>European</option>
					<option>Hispanic</option>
					<option>Indian</option>
					<option>Latin American</option>
					<option>Mediterranean</option>
					<option>Near East</option>
					<option>Scandinavian</option>
					<option>Mixed</option>
					<option>Other тот пункт поле для ввода текста</option>Э
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Skin color:
			</div>
			<div class="details-data general-input">
				<select class="sc-c">
					<option>Pale white</option>
					<option>White</option>
					<option>Olive</option>
					<option>Light brown</option>
					<option>Brown</option>
					<option>Dark brown</option>
					<option>Black</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Eye color:
			</div>
			<div class="details-data general-input">
				<select class="ey-c">
					<option>Black</option>
					<option>Blue</option>
					<option>Green</option>
					<option>Hazel</option>
					<option>Other</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Natural hair color:
			</div>
			<div class="details-data general-input">
				<select class="nh-c">
					<option>Blonde</option>
					<option>Strawberry Blonde</option>
					<option>Auburn</option>
					<option>Ginger</option>
					<option>Light Brown</option>
					<option>Brown</option>
					<option>Dark Brown</option>
					<option>Salt & Pepper</option>
					<option>Black</option>
					<option>Other</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Hair coloration:
			</div>
			<div class="details-data general-input">
				<select class="h-col">
					<option class="h-col-m">Not colored</option>
					<option class="h-col-m">Colored</option>
					<option class="h-col-h">Blonde</option>
					<option class="h-col-h">Strawbery Blonde</option>
					<option class="h-col-h">Auburn</option>
					<option class="h-col-h">Ginger</option>
					<option class="h-col-h">Red</option>
					<option class="h-col-h">Light Brown</option>
					<option class="h-col-h">Brown</option>
					<option class="h-col-h">Dark Brown</option>
					<option class="h-col-h">Salt & Pepper</option>
					<option class="h-col-h">Black</option>
					<option class="h-col-h">Black Blue</option>
					<option class="h-col-h">Other</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Readness to change color:
			</div>
			<div class="details-data general-input">
				<select>
					<option>Ready to change hair color</option>
					<option>not ready to change hair color</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Hair lenght:
			</div>
			<div class="details-data general-input">
				<select>
					<option>Bald</option>
					<option>Short</option>
					<option>Medium</option>
					<option>Shoulder length</option>
					<option>Very long</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Tattoos:
			</div>
			<div class="details-data general-input">
				<select class="tat">
					<option class="tat-m">No tattoos</option>
					<option class="tat-m">Has tattoos</option>
					<option class="tat-h">Neck</option>
					<option class="tat-h">Shoulder</option>
					<option class="tat-h">Biceps</option>
					<option class="tat-h">Wrist</option>
					<option class="tat-h">Chest</option>
					<option class="tat-h">Belly</option>
					<option class="tat-h">Back</option>
					<option class="tat-h">Loin</option>
					<option class="tat-h">Hip</option>
					<option class="tat-h">Shin</option>
					<option class="tat-h">Ankle</option>
					<option class="tat-h">Foot</option>
					<option class="tat-h">Others</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Piercing:
			</div>
			<div class="details-data general-input">
				<select>
					<option class="pier-m">No piercing (earlobes don't count)</option>
					<option class="pier-m">Has other piercing</option>
					<option class="pier-h">Ear tunnel</option>
					<option class="pier-h">Eyebrows</option>
					<option class="pier-h">Tongue</option>
					<option class="pier-h">Nose</option>
					<option class="pier-h">Nipple</option>
					<option class="pier-h">Navel</option>
					<option class="pier-h">Body implants</option>
					<option class="pier-h">Other</option>
				</select>
			</div>
		</div>
	</form>
	<form class="acc-prof">
		<h2>Professional Details</h2>
		<span class="hr"></span>
		<div class="d-wrap model">
			<div class="details-item">
				*Modeling categories/Genre:
			</div>
			<div class="details-data general-input">
				<select class="mc-g">
					<option class="mc-g-m">Alternative model</option>
					<option class="mc-g-m">Body Paint</option>
					<option class="mc-g-m">Body Parts</option>
					<option class="mc-g-h">Ears</option>
					<option class="mc-g-h">Eyes</option>
					<option class="mc-g-h">Fingers</option>
					<option class="mc-g-h">Foot</option>
					<option class="mc-g-h">Hair</option>
					<option class="mc-g-h">Hands</option>
					<option class="mc-g-h">Lips</option>
					<option class="mc-g-h">Neck</option>
					<option class="mc-g-m">Print photo model</option>
					<option class="mc-g-h">Editorial</option>
					<option class="mc-g-h">Print ads</option>
					<option class="mc-g-m">Hight fashion model</option>
					<option class="mc-g-m">Runway model</option>
					<option class="mc-g-m">Glamour model</option>
					<option class="mc-g-h">Lingerie/Body model</option>
					<option class="mc-g-h">Swimwear model</option>
					<option class="mc-g-m">Fitness model</option>
					<option class="mc-g-m">Beauty model (hair/makeup)</option>
					<option class="mc-g-m">Nude</option>
					<option class="mc-g-m">Promotional model</option>
					<option class="mc-g-m">Plus-sized model</option>
					<option class="mc-g-m">Street Fashion</option>
					<option class="mc-g-m">Sport model</option>
					<option class="mc-g-m">Senior model (40+)</option>
					<option class="mc-g-m">Video</option>
				</select>
			</div>
		</div>
		<div class="d-wrap model photographer">
			<div class="details-item">
				*Professional Experience:
			</div>
			<div class="details-data general-input">
				<select>
					<option>Up to 1 year</option>
					<option>1-5 years</option>
					<option>More that 5 years</option>
				</select>
			</div>
		</div>
		<div class="d-wrap photographer">
			<div class="details-item">
				*Work areas:
			</div>
			<div class="details-data general-input">
				<select>
					<option class="w-a-m">Advertising photographers</option>
					<option class="w-a-m">Beauty & Hair photographers</option>
					<option class="w-a-m">Black & Wite photographers</option>
					<option class="w-a-m">Children photographers</option>
					<option class="w-a-m">Celebrity/Entertainment</option>
					<option class="w-a-m">Fashion/Editorials photographers</option>
					<option class="w-a-m">Fitness/body photographers</option>
					<option class="w-a-m">Nude</option>
					<option class="w-a-m">People & lifestyle photographers</option>
					<option class="w-a-m">Portrait photographers</option>
					<option class="w-a-m">Sports/Action photographers</option>
					<option class="w-a-m">Reportage photographers</option>
					<option class="w-a-m">Runway/Fashion Event photographers</option>
					<option class="w-a-m">Swimwear & lingerie photographers</option>
					<option class="w-a-m">Travel photographers</option>
					<option class="w-a-m">Underwater photographers</option>
					<option class="w-a-m">Photo post production (retouching)</option>
					<option class="w-a-m">Film/Video production</option>
					<option class="w-a-m">Commercial production</option>
					<option class="w-a-h">TV/Video Production</option>
					<option class="w-a-h">Film/Video Post Production & CGI</option>
					<option class="w-a-m">CGI & 3D (CGI & 3D)</option>
					<option class="w-a-h">Motion graphics</option>
					<option class="w-a-h">Sound design/scoring</option>
					<option class="w-a-m">Rental studios</option>
				</select>
			</div>
		</div>
		<div class="d-wrap photographer">
			<div class="details-item">
				*Work type:
			</div>
			<div class="details-data general-input">
				<select>
					<option>Editorials</option>
					<option>Runway</option>
					<option>Beauty</option>
					<option>Portraits/Celebrity</option>
					<option>Glamour/Nude</option>
					<option>Commercial/Advertising</option>
					<option>Street Fashion</option>
					<option>Creative Projects</option>
					<option>Test Shoot</option>
					<option>Other</option>
				</select>
			</div>
		</div>
		<div class="d-wrap photographer">
			<div class="details-item">
				Type of camera equipment:
			</div>
			<div class="details-data general-input">
				<input type="text" name="cam-enquip" placeholder="Type of camera equipment">
			</div>
		</div>
		<div class="d-wrap photographer">
			<div class="details-item">
				*Studio:
			</div>
			<div class="details-data general-input">
				<select>
					<option>Provided</option>
					<option>Not provided</option>
				</select>
			</div>
		</div>
		<div class="d-wrap photographer">
			<div class="details-item">
				*Macke-Up Artist:
			</div>
			<div class="details-data general-input">
				<select>
					<option>Provided</option>
					<option>Not provided</option>
				</select>
			</div>
		</div>
		<div class="d-wrap photographer">
			<div class="details-item">
				*Hair Stylist:
			</div>
			<div class="details-data general-input">
				<select>
					<option>Provided</option>
					<option>Not provided</option>
				</select>
			</div>
		</div>
		<div class="d-wrap model">
			<div class="details-item">
				Additional Skills:
			</div>
			<div class="details-data general-input">
				<select>
					<option>Actor or Actress</option>
					<option>Film Acting</option>
					<option>TV Acting</option>
					<option>Dance</option>
					<option>Music</option>
					<option>Extreme Sports</option>
					<option>Stage Agting</option>
					<option>Commercial Acting</option>
					<option>Singer</option>
					<option>Performance Artist</option>
					<option>Voice Over Talent</option>
				</select>
			</div>
		</div>
		<div class="d-wrap model">
			<div class="details-item">
				*Languages:
			</div>
			<div class="details-data general-input">
				<select>
					<option>Chinese</option>
					<option>English</option>
					<option>French</option>
					<option>German</option>
					<option>Italian</option>
					<option>Russian</option>
					<option>Spanish</option>
					<option>Ukrainian</option>
				</select>
			</div>
		</div>
		<div class="d-wrap model photographer">
			<div class="details-item">
				Compensation:
			</div>
			<div class="details-data general-input">
				<select>
					<option>Payment Only</option>
					<option>Photo/Time For Print</option>
					<option>Test Shoot</option>
					<option>Negotiable</option>
				</select>
			</div>
		</div>
		<div class="d-wrap model photographer">
			<div class="details-item">
				Rates for photoshoot:
			</div>
			<div class="details-data general-input">
				<input type="text" class="r-f-ph" placeholder="per hour"></input>
				<input type="text" class="r-f-ph" placeholder="half day/4 hours"></input>
				<input type="text" class="r-f-ph" placeholder="full day/8 hours"></input>
			</div>
		</div>
		<div class="d-wrap model">
			<div class="details-item">
				Rates for Runway:
			</div>
			<div class="details-data general-input">
				<input type="text" class="r-f-r" placeholder="rates for runway"></input>
			</div>
		</div>
		<div class="d-wrap model">
			<div class="details-item">
				*Contacted or not:
			</div>
			<div class="details-data general-input">
				<select>
					<option>Contacted with the agency</option>
					<option>Not contacted</option>
				</select>
			</div>
		</div>
		<div class="d-wrap model photographer">
			<div class="details-item">
				*Travel Ability:
			</div>
			<div class="details-data general-input">
				<select>
					<option>Ready to travel</option>
					<option>Work within location</option>
				</select>
			</div>
		</div>
		<div class="d-wrap model">
			<div class="details-item">
				Main reason to be on site:
			</div>
			<div class="details-data general-input">
				<select>
					<option>To search for work offers</option>
					<option>To sign up for master classes</option>
					<option>To carry out master classes</option>
					<option>To search for photographers</option>
					<option>To participate in competition and castings</option>
					<option>Fashion Weeks, ShowRooms</option>
					<option>To monitor the fashion news</option>
					<option>To establish new contacts</option>
				</select>
			</div>
		</div>
		<div class="d-wrap photographer">
			<div class="details-item">
				Main reason to be on site:
			</div>
			<div class="details-data general-input">
				<select>
					<option>To sell services</option>
					<option>To serch for clients</option>
					<option>Subcontacts</option>
					<option>To sign up for master classes</option>
					<option>To carry out master classes</option>
					<option>To serch for models</option>
					<option>To participate in competitions</option>
					<option>Experience exchange</option>
					<option>To establish new contacts</option>
				</select>
			</div>
		</div>
	</form>
</div>
-->
<div class="swich">
	<a href="">model</a> | <a href="">photograph</a> | <a href="">agency</a>
</div>
<div class="person-wrapper agency">
	<h2>Подробно заполните анкету (агентство)</h2>
	<form class="acc-gen">
		<h2>General information</h2>
		<span class="hr"></span>
		<div class="d-wrap">
			<div class="details-item">
				*Name:
			</div>
			<div class="details-data general-input">
				<input type="text" class="a-n" placeholder=" Name *"></input>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Location:
			</div>
			<div class="details-data general-input">
				<select>
					<option class="loc loc-m">China:</option>
					<option class="loc loc-ch">Beijing</option>
					<option class="loc loc-ch">Hong Gong</option>
					<option class="loc loc-ch">Shanghai</option>
					<option class="loc loc-ch">Other</option>
					<option class="loc loc-m">England:</option>
					<option class="loc loc-en">London</option>
					<option class="loc loc-en">London area</option>
					<option class="loc loc-en">Other</option>
					<option class="loc loc-m">France:</option>
					<option class="loc loc-fr">Paris</option>
					<option class="loc loc-fr">Paris area</option>
					<option class="loc loc-fr">Other</option>
					<option class="loc loc-m">Germany:</option>
					<option class="loc loc-ge">Berlin</option>
					<option class="loc loc-ge">Berlin area</option>
					<option class="loc loc-ge">Other</option>
					<option class="loc loc-m">Italy:</option>
					<option class="loc loc-it">Milan</option>
					<option class="loc loc-it">Milan area</option>
					<option class="loc loc-it">Rome</option>
					<option class="loc loc-it">Rome area</option>
					<option class="loc loc-it">Other</option>
					<option class="loc loc-m">USA:</option>
					<option class="loc loc-usa">Los Angeles</option>
					<option class="loc loc-usa">Los Angeles area</option>
					<option class="loc loc-usa">New York</option>
					<option class="loc loc-usa">New York area</option>
					<option class="loc loc-usa">Other</option>
					<option class="loc loc-m">Ukraine</option>
					<option class="loc loc-m">Russia</option>
					<option class="loc loc-m">Other</option>
				</select>
			</div>
		</div>
		<div class="d-wrap model photographer">
			<div class="details-item">
				*Address:
			</div>
			<div class="details-data general-input">
				<input type="text" class="a-a" placeholder="Address *"></input>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Phone number:
			</div>
			<div class="details-data general-input">
				<input type="phone" class="ph-n" placeholder="Phone Number *"></input>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*E-mail:
			</div>
			<div class="details-data general-input">
				<input type="text" class="em" placeholder="E-mail *"></input>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Skype:
			</div>
			<div class="details-data general-input">
				<input type="text" class="sk" placeholder="Skype *"></input>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Web-site:
			</div>
			<div class="details-data general-input">
				<input type="text" class="w-s" placeholder="Web-site *"></input>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				Official social media:
			</div>
			<div class="details-data general-input">
				<input type="text" class="a-osm" placeholder="Official social media:"></input>
			</div>
		</div>
	</form>
	<form class="acc-req">
		<h2>Model Requirements</h2>
		<span class="hr"></span>
		<div class="d-wrap">
			<div class="details-item">
				*Gender:
			</div>
			<div class="details-data general-input">
				<select>
					<option>Male</option>
					<option>Female</option>
					<option>Male and female</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Age:
			</div>
			<div class="details-data general-input">
				<select>
					<option>Children</option>
					<option>Teens</option>
					<option>Adults</option>
					<option>Senior (40+)</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				*Model category:
			</div>
			<div class="details-data general-input">
				<select>
					<option class="mc-m">Alternative model</option>
					<option class="mc-m">Body Paint</option>
					<option class="mc-m">Body Parts</option>
					<option class="mc-h">Ears</option>
					<option class="mc-h">Eyes</option>
					<option class="mc-h">Fingers</option>
					<option class="mc-h">Foot</option>
					<option class="mc-h">Hair</option>
					<option class="mc-h">Hands</option>
					<option class="mc-h">Lips</option>
					<option class="mc-h">Neck</option>
					<option class="mc-m">Print photo model</option>
					<option class="mc-h">Editorial</option>
					<option class="mc-h">Print ads</option>
					<option class="mc-m">Hight fashion model</option>
					<option class="mc-m">Runway model</option>
					<option class="mc-m">Glamour model</option>
					<option class="mc-h">Lingery/Body model</option>
					<option class="mc-h">Swimear model</option>
					<option class="mc-m">Fitness model</option>
					<option class="mc-m">Beauty model (hair/makeup)</option>
					<option class="mc-m">Nude</option>
					<option class="mc-m">Promotional model</option>
					<option class="mc-m">Plus-sized model</option>
					<option class="mc-m">Street Fashion</option>
					<option class="mc-m">Sport model</option>
					<option class="mc-m">Senior model (40+)</option>
					<option class="mc-m">Video</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				Other requirements:
			</div>
			<div class="details-data general-input">
				<textarea placeholder="Other requirements"></textarea>
			</div>
		</div>
	</form>
	<form class="acc-prof agency">
		<h2>Professional Details</h2>
		<span class="hr"></span>
		<div class="d-wrap">
			<div class="details-item">
				*Professional experience:
			</div>
			<div class="details-data general-input">
				<select>
					<option>Up to 10 years</option>
					<option>10-20 years</option>
					<option>More that 20 years</option>
				</select>
			</div>
		</div>
		<div class="d-wrap model photographer">
			<div class="details-item">
				Total models:
			</div>
			<div class="details-data general-input">
				<input type="number" name="tot-models">
			</div>
		</div>
		<div class="d-wrap photographer">
			<div class="details-item">
				*Type/Specialization:
			</div>
			<div class="details-data general-input">
				<select>
					<option>Agency</option>
					<option>Publisher</option>
					<option>Model Site</option>
					<option>Model Scout</option>
					<option>Event Manager/Staffing company</option>
					<option>Casting Director</option>
					<option>Advertiser</option>
					<option>Manager</option>
					<option>Stilist+MUA</option>
					<option>Other (поле для ввода текста)</option>
				</select>
			</div>
		</div>
		<div class="d-wrap">
			<div class="details-item">
				Mine reason to be on site:
			</div>
			<div class="details-data general-input">
				<select>
					<option>World visibility</option>
					<option>Model promotion</option>
					<option>PR&Representation</option>
					<option>To carry out master classes</option>
					<option>Subcontracts</option>
					<option>To establish new contacts</option>
					<option>To serch for models</option>
					<option>To promote services</option>
				</select>
			</div>
		</div>
	</form>
</div>
<!-- Анкета Модель Конец -->
<!-- Pay Form
<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">

<input type="hidden" name="cmd" value="_s-xclick">

<input type="hidden" name="hosted_button_id" value="2QPMN4EWCXFNN">

<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_cart_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">

<img alt="" border="0" src="https://www.paypalobjects.com/ru_RU/i/scr/pixel.gif" width="1" height="1">

</form>





<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">

<input type="hidden" name="cmd" value="_s-xclick">

<input type="hidden" name="encrypted" value="-----BEGIN PKCS7-----MIIIGQYJKoZIhvcNAQcEoIIICjCCCAYCAQExggEwMIIBLAIBADCBlDCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20CAQAwDQYJKoZIhvcNAQEBBQAEgYCV2+Uv/9PIsa7mMzLVZeWB/2gDndWBP+oPtkQJwzViiTRWVl7ont3ziudxrwn4K24PqQYtTiM8G5ESi8JnK9Acz5WIrUxzF6Zu4WncUzL8Ux944faGWIpVsiTEe08YdzYXyVux1yuMfKr+oXw8VsRAqtjWQqpHKciGud+lrlwNJzELMAkGBSsOAwIaBQAwggGVBgkqhkiG9w0BBwEwFAYIKoZIhvcNAwcECLIs3P6n6FCsgIIBcEKza+ANWhs4vA+tO73m2OXmpDRbzKnuVkq4eU85AnMdkRg/TRVJ3huDqs6wbcqtxrjZNFmnFhKCFANVvTIWobsBM2glmuT0ZpIOY2dqWgvtw09dEsL8N0A0ik6KAVwkqaq1SLRUTrHXsoyEC8pqgIPXBM9O02wAiRZjg7y8DisFRBEuHYaqaYIUC/Hs23KQW+Hu+PgCEqAf28wdddUna6Y9IS7VQ0u6rsgKckJuEh9qhrMFlE6aRBeflOqTKShtop9HM4tQMTFBC6VtjxxJAlFlYnjjBvlp/yCBU4Nf9Cyqxc09zHPEK/yYNAfEYV7ogjKha8RaMKFB4OpCAOc0uypLRtgfYGdEqssi2iIOWXMpA4cZVpZecZuB27qcLkSbeG5nLamxeLCoq9XpX7uyG+DA+pIrR3BXEwNiqA7rrY+kog0F+KVFe6Dxh3Vzj11bUHHtceNbCVIPdj9VCLKULQTZH7R+XxtqvMzPD24DviPOoIIDhzCCA4MwggLsoAMCAQICAQAwDQYJKoZIhvcNAQEFBQAwgY4xCzAJBgNVBAYTAlVTMQswCQYDVQQIEwJDQTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEUMBIGA1UEChMLUGF5UGFsIEluYy4xEzARBgNVBAsUCmxpdmVfY2VydHMxETAPBgNVBAMUCGxpdmVfYXBpMRwwGgYJKoZIhvcNAQkBFg1yZUBwYXlwYWwuY29tMB4XDTA0MDIxMzEwMTMxNVoXDTM1MDIxMzEwMTMxNVowgY4xCzAJBgNVBAYTAlVTMQswCQYDVQQIEwJDQTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEUMBIGA1UEChMLUGF5UGFsIEluYy4xEzARBgNVBAsUCmxpdmVfY2VydHMxETAPBgNVBAMUCGxpdmVfYXBpMRwwGgYJKoZIhvcNAQkBFg1yZUBwYXlwYWwuY29tMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDBR07d/ETMS1ycjtkpkvjXZe9k+6CieLuLsPumsJ7QC1odNz3sJiCbs2wC0nLE0uLGaEtXynIgRqIddYCHx88pb5HTXv4SZeuv0Rqq4+axW9PLAAATU8w04qqjaSXgbGLP3NmohqM6bV9kZZwZLR/klDaQGo1u9uDb9lr4Yn+rBQIDAQABo4HuMIHrMB0GA1UdDgQWBBSWn3y7xm8XvVk/UtcKG+wQ1mSUazCBuwYDVR0jBIGzMIGwgBSWn3y7xm8XvVk/UtcKG+wQ1mSUa6GBlKSBkTCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb22CAQAwDAYDVR0TBAUwAwEB/zANBgkqhkiG9w0BAQUFAAOBgQCBXzpWmoBa5e9fo6ujionW1hUhPkOBakTr3YCDjbYfvJEiv/2P+IobhOGJr85+XHhN0v4gUkEDI8r2/rNk1m0GA8HKddvTjyGw/XqXa+LSTlDYkqI8OwR8GEYj4efEtcRpRYBxV8KxAW93YDWzFGvruKnnLbDAF6VR5w/cCMn5hzGCAZowggGWAgEBMIGUMIGOMQswCQYDVQQGEwJVUzELMAkGA1UECBMCQ0ExFjAUBgNVBAcTDU1vdW50YWluIFZpZXcxFDASBgNVBAoTC1BheVBhbCBJbmMuMRMwEQYDVQQLFApsaXZlX2NlcnRzMREwDwYDVQQDFAhsaXZlX2FwaTEcMBoGCSqGSIb3DQEJARYNcmVAcGF5cGFsLmNvbQIBADAJBgUrDgMCGgUAoF0wGAYJKoZIhvcNAQkDMQsGCSqGSIb3DQEHATAcBgkqhkiG9w0BCQUxDxcNMTYwNTAyMTcwMTQyWjAjBgkqhkiG9w0BCQQxFgQUzOhWoTCCaMGhhgcPP4+NUxn538gwDQYJKoZIhvcNAQEBBQAEgYAuCjdrSvOTJU0CFr7SVk6YgTaG04gIIO1r4GxVy/pktbM1xhh6K90EmtFhLJ+Xlv9LRoSDl4tD84KKAlifkpy6akUVSDBz/8KXrePJYmY0RVtSzk2L47oxapB1ilJjh66PTcNCmhwd7Xjw1kB6qFu2DR9iAVqyti0TiDjwHr6nSQ==-----END PKCS7-----

">

<input type="image" src="http://fashingreatness.ru/img/PayPal.png" border="0" name="submit" alt="PayPal — более безопасный и легкий способ оплаты через Интернет!">

<img alt="" border="0" src="https://www.paypalobjects.com/ru_RU/i/scr/pixel.gif" width="1" height="1">

</form>

-->
<div>
	footer
</div>