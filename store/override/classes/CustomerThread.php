<?php

class CustomerThread extends CustomerThreadCore
{


    public $id_country;
    public $lastname;
    public $firstname;
    public $phone;

    public static $definition = array(
        'table' => 'customer_thread',
        'primary' => 'id_customer_thread',
        'fields' => array(
            'id_lang' =>    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'id_contact' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'id_shop' =>    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'id_customer' =>array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'id_order' =>    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'id_product' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'id_country' =>        array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'email' =>        array('type' => self::TYPE_STRING, 'validate' => 'isEmail', 'size' => 128),
            'lastname' =>   array('type' => self::TYPE_STRING, 'validate' => 'isName', 'size' => 32),
            'firstname' =>  array('type' => self::TYPE_STRING, 'validate' => 'isName', 'size' => 32),
            'phone' =>        array('type' => self::TYPE_STRING, 'validate' => 'isPhoneNumber', 'size' => 32),
            'token' =>        array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => true),
            'status' =>    array('type' => self::TYPE_STRING),
            'date_add' =>    array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
            'date_upd' =>    array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
        ),
    );

    public static function updateCustomerThreadTable()
    {
        try {
            Db::getInstance()->execute('ALTER TABLE `' . _DB_PREFIX_ . 'customer_thread` ADD COLUMN `id_country` int(11) NULL AFTER `id_product`');
            Db::getInstance()->execute('ALTER TABLE `' . _DB_PREFIX_ . 'customer_thread` ADD COLUMN `phone` varchar(32) NULL AFTER `email`');
            Db::getInstance()->execute('ALTER TABLE `' . _DB_PREFIX_ . 'customer_thread` ADD COLUMN `firstname` varchar(32) NULL AFTER `email`');
            Db::getInstance()->execute('ALTER TABLE `' . _DB_PREFIX_ . 'customer_thread` ADD COLUMN `lastname` varchar(32) NULL AFTER `email`');
        }

        catch(Exception $e) {
//            echo 'Message: ' .$e->getMessage();
        }


        return true;
    }
}
