-- phpMyAdmin SQL Dump
-- version 4.4.15.7
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Авг 22 2016 г., 16:02
-- Версия сервера: 5.5.50
-- Версия PHP: 5.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `luckyale_fash`
--

-- --------------------------------------------------------

--
-- Структура таблицы `agency_age_type`
--

CREATE TABLE IF NOT EXISTS `agency_age_type` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_num` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Структура таблицы `agency_form`
--

CREATE TABLE IF NOT EXISTS `agency_form` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `gender` int(11) NOT NULL,
  `age_type` int(11) NOT NULL,
  `other_requirements` text,
  `professional_experience` int(11) NOT NULL,
  `total_models` int(11) NOT NULL,
  `type_specialization` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Структура таблицы `agency_model_category`
--

CREATE TABLE IF NOT EXISTS `agency_model_category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `parent` int(11) NOT NULL,
  `sort_num` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Структура таблицы `agency_professional_expiriens`
--

CREATE TABLE IF NOT EXISTS `agency_professional_expiriens` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_num` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Структура таблицы `agency_site_reason`
--

CREATE TABLE IF NOT EXISTS `agency_site_reason` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_num` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Структура таблицы `agency_specialization_to_form`
--

CREATE TABLE IF NOT EXISTS `agency_specialization_to_form` (
  `id` int(11) NOT NULL,
  `agency_specialization_id` int(11) NOT NULL,
  `agency_form_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Структура таблицы `agency_type_specialization`
--

CREATE TABLE IF NOT EXISTS `agency_type_specialization` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_num` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Структура таблицы `all_profexpiriens_list`
--

CREATE TABLE IF NOT EXISTS `all_profexpiriens_list` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_num` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Структура таблицы `brand_form`
--

CREATE TABLE IF NOT EXISTS `brand_form` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `professional_expiriens` int(11) NOT NULL,
  `product_range` varchar(255) DEFAULT NULL,
  `trading` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Структура таблицы `brand_location`
--

CREATE TABLE IF NOT EXISTS `brand_location` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `parent` int(11) DEFAULT NULL,
  `sort_num` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 PACK_KEYS=0;

--
-- Дамп данных таблицы `brand_location`
--

INSERT INTO `brand_location` (`id`, `name`, `parent`, `sort_num`) VALUES
(1, 'China:', NULL, 0),
(2, 'Beijing', 1, 0),
(3, 'Hong Kong', 1, 0),
(4, 'Shanghai', 1, 0),
(5, 'Other', 1, 0),
(6, 'England:', NULL, 0),
(7, 'London', 6, 0),
(8, 'London area', 6, 0),
(9, 'Other', 6, 0),
(10, 'France:', NULL, 0),
(11, 'Paris', 10, 0),
(12, 'Paris area', 10, 0),
(13, 'Other', 10, 0),
(14, 'Germany:', NULL, 0),
(15, 'Berlin', 14, 0),
(16, 'Berlin area', 14, 0),
(17, 'Other', 14, 0),
(18, 'Italy:', NULL, 0),
(19, 'Milan', 18, 0),
(20, 'Milan area', 18, 0),
(21, 'Rome', 18, 0),
(22, 'Rome area', 18, 0),
(23, 'Other', 18, 0),
(24, 'USA:', NULL, 0),
(25, 'Los Angeles', 24, 0),
(26, 'Los Angeles area', 24, 0),
(27, 'New York', 24, 0),
(28, 'New York area', 24, 0),
(29, 'Other', 24, 0),
(30, 'Ukraine', NULL, 0),
(31, 'Russia', NULL, 0),
(32, 'Other', NULL, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `brand_location_to_user`
--

CREATE TABLE IF NOT EXISTS `brand_location_to_user` (
  `id` int(11) NOT NULL,
  `brand_location_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Структура таблицы `buyer_form`
--

CREATE TABLE IF NOT EXISTS `buyer_form` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `buyer_location` int(11) NOT NULL,
  `product_range` varchar(255) DEFAULT NULL,
  `trading` int(11) NOT NULL,
  `professional_expiriens` int(11) NOT NULL,
  `travel_ability` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Структура таблицы `buyer_language_to_form`
--

CREATE TABLE IF NOT EXISTS `buyer_language_to_form` (
  `id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `buyer_form_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Структура таблицы `company_form`
--

CREATE TABLE IF NOT EXISTS `company_form` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  `scope_activity` varchar(255) DEFAULT NULL,
  `workplace` varchar(255) NOT NULL,
  `professional_experience` int(11) NOT NULL,
  `travel_ability` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Структура таблицы `company_language_to_form`
--

CREATE TABLE IF NOT EXISTS `company_language_to_form` (
  `id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `company_form_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Структура таблицы `ethnicity_list`
--

CREATE TABLE IF NOT EXISTS `ethnicity_list` (
  `id` int(11) NOT NULL,
  `name` int(11) NOT NULL,
  `sort_num` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Структура таблицы `eye_color_list`
--

CREATE TABLE IF NOT EXISTS `eye_color_list` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_num` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Структура таблицы `hair_color_list`
--

CREATE TABLE IF NOT EXISTS `hair_color_list` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_num` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Структура таблицы `hair_lenght_list`
--

CREATE TABLE IF NOT EXISTS `hair_lenght_list` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_num` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Структура таблицы `hair_oloration_list`
--

CREATE TABLE IF NOT EXISTS `hair_oloration_list` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_num` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Структура таблицы `language_list`
--

CREATE TABLE IF NOT EXISTS `language_list` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_num` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Структура таблицы `main_reason_list`
--

CREATE TABLE IF NOT EXISTS `main_reason_list` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_num` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Структура таблицы `media_form`
--

CREATE TABLE IF NOT EXISTS `media_form` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name_magazine_blog` varchar(255) NOT NULL,
  `travel_ability` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Структура таблицы `media_language_to_form`
--

CREATE TABLE IF NOT EXISTS `media_language_to_form` (
  `id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `media_form_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Структура таблицы `media_position_list`
--

CREATE TABLE IF NOT EXISTS `media_position_list` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_num` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Структура таблицы `media_position_to_form`
--

CREATE TABLE IF NOT EXISTS `media_position_to_form` (
  `id` int(11) NOT NULL,
  `media_position_id` int(11) NOT NULL,
  `media_form_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Структура таблицы `model_form`
--

CREATE TABLE IF NOT EXISTS `model_form` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `choose_metrical` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `chest` int(11) DEFAULT NULL,
  `waist` int(11) DEFAULT NULL,
  `hips` int(11) DEFAULT NULL,
  `dress_jacket` int(11) DEFAULT NULL,
  `cup` varchar(20) DEFAULT NULL,
  `shoe` int(11) DEFAULT NULL,
  `ethnicity` int(11) NOT NULL,
  `skin_color` int(11) NOT NULL,
  `eye_color` int(11) NOT NULL,
  `hair_color` int(11) NOT NULL,
  `hair_coloration` int(11) DEFAULT NULL,
  `ready_change_color` int(11) DEFAULT NULL,
  `hair_lenght` int(11) NOT NULL,
  `tattos` int(11) NOT NULL,
  `piercing` int(11) NOT NULL,
  `travel_ability` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Структура таблицы `model_form_to_main_reason`
--

CREATE TABLE IF NOT EXISTS `model_form_to_main_reason` (
  `field1` int(11) NOT NULL,
  `main_reason_id` int(11) NOT NULL,
  `model_form_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Структура таблицы `model_language_to_form`
--

CREATE TABLE IF NOT EXISTS `model_language_to_form` (
  `id` int(11) NOT NULL,
  `model_language_id` int(11) NOT NULL,
  `model_form_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Структура таблицы `piercing_has_list`
--

CREATE TABLE IF NOT EXISTS `piercing_has_list` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_num` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Структура таблицы `piercing_to_model`
--

CREATE TABLE IF NOT EXISTS `piercing_to_model` (
  `id` int(11) NOT NULL,
  `piercing_has_id` int(11) NOT NULL,
  `model_form_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Структура таблицы `position_list`
--

CREATE TABLE IF NOT EXISTS `position_list` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_num` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Структура таблицы `product_range_list`
--

CREATE TABLE IF NOT EXISTS `product_range_list` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_num` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Структура таблицы `product_range_to_brand_form`
--

CREATE TABLE IF NOT EXISTS `product_range_to_brand_form` (
  `id` int(11) NOT NULL,
  `product_range_id` int(11) NOT NULL,
  `brand_form_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Структура таблицы `product_range_to_buyer_form`
--

CREATE TABLE IF NOT EXISTS `product_range_to_buyer_form` (
  `id` int(11) NOT NULL,
  `product_range_id` int(11) NOT NULL,
  `buyer_form_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Структура таблицы `reason_to_agency_form`
--

CREATE TABLE IF NOT EXISTS `reason_to_agency_form` (
  `id` int(11) NOT NULL,
  `agency_reason_id` int(11) NOT NULL,
  `agency_form_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Структура таблицы `reason_to_brand_form`
--

CREATE TABLE IF NOT EXISTS `reason_to_brand_form` (
  `id` int(11) NOT NULL,
  `reason_id` int(11) NOT NULL,
  `brand_form_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Структура таблицы `reason_to_buyer_form`
--

CREATE TABLE IF NOT EXISTS `reason_to_buyer_form` (
  `id` int(11) NOT NULL,
  `reason_id` int(11) NOT NULL,
  `buyer_form_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Структура таблицы `reason_to_company_form`
--

CREATE TABLE IF NOT EXISTS `reason_to_company_form` (
  `id` int(11) NOT NULL,
  `reason_id` int(11) NOT NULL,
  `company_form_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Структура таблицы `reason_to_media_form`
--

CREATE TABLE IF NOT EXISTS `reason_to_media_form` (
  `id` int(11) NOT NULL,
  `reason_id` int(11) NOT NULL,
  `media_form_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Структура таблицы `skin_color_list`
--

CREATE TABLE IF NOT EXISTS `skin_color_list` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_num` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Структура таблицы `specialization_to_user`
--

CREATE TABLE IF NOT EXISTS `specialization_to_user` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_specialization_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Структура таблицы `tatoo_place_list`
--

CREATE TABLE IF NOT EXISTS `tatoo_place_list` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_num` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Структура таблицы `tatto_to_model`
--

CREATE TABLE IF NOT EXISTS `tatto_to_model` (
  `id` int(11) NOT NULL,
  `totoo_plase_id` int(11) NOT NULL,
  `model_form_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Структура таблицы `trading_list`
--

CREATE TABLE IF NOT EXISTS `trading_list` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_num` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `account_type` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(60) NOT NULL,
  `password` varchar(100) NOT NULL,
  `login` varchar(100) NOT NULL,
  `profession` int(11) DEFAULT NULL,
  `gender` int(11) DEFAULT NULL COMMENT '0-женщина, 1-мужчина',
  `skype` varchar(100) NOT NULL,
  `website` varchar(255) NOT NULL,
  `date_of_birth` date NOT NULL COMMENT 'Дата рождения',
  `birth_show` int(11) DEFAULT NULL,
  `country` int(11) NOT NULL,
  `language` int(11) DEFAULT NULL,
  `avatar` varchar(255) NOT NULL DEFAULT 'no_avatar.png' COMMENT 'Аватар - имя файла',
  `company` varchar(255) DEFAULT NULL,
  `agency_info` varchar(20) DEFAULT NULL,
  `legal_guardian` int(11) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `social_media` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `account_type`, `name`, `first_name`, `last_name`, `email`, `phone`, `password`, `login`, `profession`, `gender`, `skype`, `website`, `date_of_birth`, `birth_show`, `country`, `language`, `avatar`, `company`, `agency_info`, `legal_guardian`, `address`, `social_media`) VALUES
(22, 1, 'Сергей', NULL, 'Шамаев', 'vis1@mail.ru', '111 222 33 44', 'c01c821005b4448df8aa2da2629a397a', 'vis1', NULL, NULL, '', '', '0000-00-00', NULL, 1, NULL, 'no_avatar.png', NULL, NULL, NULL, NULL, NULL),
(25, 3, 'Витя', NULL, 'Викторов', 'cla1@mail.ru', '222 222 22 22', '9906ee9c955fa5555a54caef099a3cdd', 'cla1', NULL, NULL, '', '', '0000-00-00', NULL, 1, NULL, 'no_avatar.png', NULL, NULL, NULL, NULL, NULL),
(26, 4, 'Анна', NULL, 'Анина', 'cor1@mail.ru', '', '9ef0681978ad75d3db7b3bce3e01ac77', 'cor1', NULL, NULL, '', '', '0000-00-00', NULL, 1, NULL, 'no_avatar.png', NULL, NULL, NULL, NULL, NULL),
(48, 1, 'yiidev', NULL, 'free', 'sergey-free@ya.ru', '79640000000', '92c7de348d1096ec549bb1428badf522', '', NULL, NULL, '', '', '0000-00-00', NULL, 1, NULL, 'no_avatar.png', NULL, NULL, NULL, NULL, NULL),
(49, 1, 'serg', NULL, 'free', 'sergey29262@mail.ru', '79654123548', '082ee097e5f364ea320820a53eba4307', '', NULL, NULL, '', '', '0000-00-00', NULL, 1, NULL, 'no_avatar.png', NULL, NULL, NULL, NULL, NULL),
(59, 1, 'тест', NULL, 'тест', 'a35b62@mail.ru', '12345', 'd9d1b168eac8f197e0576b56cfc23ece', '', NULL, NULL, '', '', '0000-00-00', NULL, 1, NULL, 'no_avatar.png', NULL, NULL, NULL, NULL, NULL),
(62, 1, 'jeka', NULL, 'nearbird', 'phpuser802@mail.ru', '357740', '2dd59b9f373310543c5f2e3197f98d9c', '', NULL, NULL, '', '', '0000-00-00', NULL, 1, 1, 'no_avatar.png', NULL, NULL, NULL, NULL, NULL),
(63, 1, 'ertwertwetr', NULL, 'ertertre', 'SLIM@ya.ru', '+79640000000', '2222d86d6356481468cb53f1970d8825', '', NULL, NULL, '', '', '0000-00-00', NULL, 1, 1, 'no_avatar.png', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `user_account_type`
--

CREATE TABLE IF NOT EXISTS `user_account_type` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `short_name` varchar(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 PACK_KEYS=0;

--
-- Дамп данных таблицы `user_account_type`
--

INSERT INTO `user_account_type` (`id`, `name`, `short_name`) VALUES
(1, 'Visitor', 'vis'),
(2, 'Classic', 'cla'),
(3, 'Vip', 'vip'),
(4, 'Corporation', 'cor');

-- --------------------------------------------------------

--
-- Структура таблицы `user_country`
--

CREATE TABLE IF NOT EXISTS `user_country` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `sort_num` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 PACK_KEYS=0;

--
-- Дамп данных таблицы `user_country`
--

INSERT INTO `user_country` (`id`, `name`, `sort_num`) VALUES
(1, 'Англия', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `user_json`
--

CREATE TABLE IF NOT EXISTS `user_json` (
  `id` int(11) NOT NULL,
  `router` int(11) NOT NULL,
  `json` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user_json`
--

INSERT INTO `user_json` (`id`, `router`, `json`) VALUES
(22, 0, '{"router":"actionAppendMyDann","User":{"first_name":"","name":"","last_name":"","company":"","phone":"","email":"","skype":"","agency_info":"","website":"","gender":"1","date_of_birth":"","birth_show":"1","legal_guardian":"Yes"},"Users":{"AgeAddress":"","Officialsocialmedia":"","Phymetric":"cm","PhyHeight":"","PhyWeight":"","PhyChest":"","PhyWaist":"","PhyHips":"","PhyDressJacket":"","PhyCup":"","PhyShoe":"","Ethnicity":"African","Skincolor":"Pale","Eyecolor":"Black","Naturalhaircolor":"Blonde","Readnesstochangecolor":"Ready to change hair color","Hairlenght":"Bald","ProfessionalExperience":"Up to 1 year","Ratesperhour":"","Rateshalfday":"","Ratesfullday":"","ratesforrunway":"","Contactedornot":"Contacted","TravelAbility":"Ready to travel","FoProfessionalExperience":"Up to 1 year","cameracam-enquip":"","Studiocam-enquip":"Provided","StudioMacke-Up":"Provided","StudiStylist":"Provided","photoshootperhour":"","photoshoothalfdayhours":"","photoshootfulldayhours":"","Professionalexperience":"Up to 10 years","ProfessionalWorkplace":"","EducationProfessional":"","StyContacted":"Contacted","StyWorkplace":"","StyEducationProfessional":"","StyProfessionalexperience":"Up to 10 years","ArtContacted":"Contacted","ArtWorkplace":"","ArtcoursesMaster":"","ArtCosmeticbrands":"","ArtProfessionalexperience":"Up to 10 years","MuaContacted":"Contacted","MuaWorkplace":"","MuaEducation":"","MuaCosmetic":"","Muaexperience":"Up to 10 years","MuaTravelAbility":"Ready to travel","AgeGender":"Male","Agetextarea":"","Ageexperience":"Up to 10 years","Agetot-models":"","AgeSpec-Other":"","BraProfessionalexperience":"Up to 1 year","BraSpec-Other":"","BraTrading":"Domestic market","BuyClients":"","BuyBrands":"","BuyrangeSpec-Other":"","BuyTrading":"Domestic market","Buyexperience":"Up to 1 year","BuyTravelAbility":"Ready to travel","CoProfessional":"Manager","Coactivity":"","CoWorkplace":"","Coexperience":"Up to 1 year","CoTravelAbility":"Ready to travel","MeName":"","MeOther":"","MeProfessionalexperience":"Up to 1 year","MeTravelAbility":"Ready to travel"}}');

-- --------------------------------------------------------

--
-- Структура таблицы `user_language`
--

CREATE TABLE IF NOT EXISTS `user_language` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `code` varchar(20) NOT NULL,
  `sort_num` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 PACK_KEYS=0;

--
-- Дамп данных таблицы `user_language`
--

INSERT INTO `user_language` (`id`, `name`, `code`, `sort_num`) VALUES
(1, 'Английский', 'en', 0),
(2, 'Русский', 'ru', 0),
(3, 'Французский', 'fr', 0),
(4, 'Китайский', 'ch', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `user_payments`
--

CREATE TABLE IF NOT EXISTS `user_payments` (
  `id` int(11) NOT NULL,
  `login` varchar(100) NOT NULL,
  `cod` varchar(20) NOT NULL COMMENT 'код операции : Приход / Расход',
  `valuta` varchar(10) NOT NULL COMMENT 'Валюта',
  `payment` int(10) NOT NULL,
  `data_payment` date NOT NULL,
  `txt` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user_payments`
--

INSERT INTO `user_payments` (`id`, `login`, `cod`, `valuta`, `payment`, `data_payment`, `txt`) VALUES
(1, '6', 'При', '', 250, '2016-05-02', '');

-- --------------------------------------------------------

--
-- Структура таблицы `user_profession`
--

CREATE TABLE IF NOT EXISTS `user_profession` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `sort_num` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 PACK_KEYS=0;

--
-- Дамп данных таблицы `user_profession`
--

INSERT INTO `user_profession` (`id`, `name`, `sort_num`) VALUES
(1, 'model', 1),
(2, 'photographer', 2),
(3, 'designer', 3),
(4, 'agency', 4),
(5, 'Fashion Expert', 5);

-- --------------------------------------------------------

--
-- Структура таблицы `user_specialization`
--

CREATE TABLE IF NOT EXISTS `user_specialization` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `sort_num` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 PACK_KEYS=0;

--
-- Дамп данных таблицы `user_specialization`
--

INSERT INTO `user_specialization` (`id`, `name`, `sort_num`) VALUES
(1, 'Brand', 0),
(2, 'Buyer', 0),
(3, 'Customer', 0),
(4, 'Company Representative', 0),
(5, 'Media', 0),
(6, 'Stylist', 0),
(7, 'Hair Stylist/Artist', 0),
(8, 'MUA', 0);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `agency_age_type`
--
ALTER TABLE `agency_age_type`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `agency_form`
--
ALTER TABLE `agency_form`
  ADD PRIMARY KEY (`id`),
  ADD KEY `age_type` (`age_type`);

--
-- Индексы таблицы `agency_model_category`
--
ALTER TABLE `agency_model_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent` (`parent`);

--
-- Индексы таблицы `agency_professional_expiriens`
--
ALTER TABLE `agency_professional_expiriens`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `agency_site_reason`
--
ALTER TABLE `agency_site_reason`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `agency_specialization_to_form`
--
ALTER TABLE `agency_specialization_to_form`
  ADD PRIMARY KEY (`id`),
  ADD KEY `agency_specialization_id` (`agency_specialization_id`),
  ADD KEY `agency_form_id` (`agency_form_id`);

--
-- Индексы таблицы `agency_type_specialization`
--
ALTER TABLE `agency_type_specialization`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `all_profexpiriens_list`
--
ALTER TABLE `all_profexpiriens_list`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `brand_form`
--
ALTER TABLE `brand_form`
  ADD PRIMARY KEY (`id`),
  ADD KEY `professional_expiriens` (`professional_expiriens`),
  ADD KEY `trading` (`trading`);

--
-- Индексы таблицы `brand_location`
--
ALTER TABLE `brand_location`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent` (`parent`);

--
-- Индексы таблицы `brand_location_to_user`
--
ALTER TABLE `brand_location_to_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `brand_location_id` (`brand_location_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `buyer_form`
--
ALTER TABLE `buyer_form`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `trading` (`trading`),
  ADD KEY `professional_expiriens` (`professional_expiriens`);

--
-- Индексы таблицы `buyer_language_to_form`
--
ALTER TABLE `buyer_language_to_form`
  ADD PRIMARY KEY (`id`),
  ADD KEY `language_id` (`language_id`),
  ADD KEY `buyer_form_id` (`buyer_form_id`);

--
-- Индексы таблицы `company_form`
--
ALTER TABLE `company_form`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `position` (`position`),
  ADD KEY `professional_experience` (`professional_experience`);

--
-- Индексы таблицы `company_language_to_form`
--
ALTER TABLE `company_language_to_form`
  ADD PRIMARY KEY (`id`),
  ADD KEY `language_id` (`language_id`),
  ADD KEY `company_form_id` (`company_form_id`);

--
-- Индексы таблицы `ethnicity_list`
--
ALTER TABLE `ethnicity_list`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eye_color_list`
--
ALTER TABLE `eye_color_list`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `hair_color_list`
--
ALTER TABLE `hair_color_list`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `hair_lenght_list`
--
ALTER TABLE `hair_lenght_list`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `hair_oloration_list`
--
ALTER TABLE `hair_oloration_list`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `language_list`
--
ALTER TABLE `language_list`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `main_reason_list`
--
ALTER TABLE `main_reason_list`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `media_form`
--
ALTER TABLE `media_form`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `media_language_to_form`
--
ALTER TABLE `media_language_to_form`
  ADD PRIMARY KEY (`id`),
  ADD KEY `language_id` (`language_id`),
  ADD KEY `media_form_id` (`media_form_id`);

--
-- Индексы таблицы `media_position_list`
--
ALTER TABLE `media_position_list`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `media_position_to_form`
--
ALTER TABLE `media_position_to_form`
  ADD PRIMARY KEY (`id`),
  ADD KEY `media_position_id` (`media_position_id`),
  ADD KEY `media_form_id` (`media_form_id`);

--
-- Индексы таблицы `model_form`
--
ALTER TABLE `model_form`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ethnicity` (`ethnicity`),
  ADD KEY `skin_color` (`skin_color`),
  ADD KEY `eye_color` (`eye_color`),
  ADD KEY `hair_color` (`hair_color`);

--
-- Индексы таблицы `model_form_to_main_reason`
--
ALTER TABLE `model_form_to_main_reason`
  ADD PRIMARY KEY (`field1`),
  ADD KEY `main_reason_id` (`main_reason_id`),
  ADD KEY `model_form_id` (`model_form_id`);

--
-- Индексы таблицы `model_language_to_form`
--
ALTER TABLE `model_language_to_form`
  ADD PRIMARY KEY (`id`),
  ADD KEY `model_form_id` (`model_form_id`),
  ADD KEY `model_language_id` (`model_language_id`);

--
-- Индексы таблицы `piercing_has_list`
--
ALTER TABLE `piercing_has_list`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `piercing_to_model`
--
ALTER TABLE `piercing_to_model`
  ADD PRIMARY KEY (`id`),
  ADD KEY `piercing_has_id` (`piercing_has_id`),
  ADD KEY `model_form_id` (`model_form_id`);

--
-- Индексы таблицы `position_list`
--
ALTER TABLE `position_list`
  ADD KEY `id` (`id`);

--
-- Индексы таблицы `product_range_list`
--
ALTER TABLE `product_range_list`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `product_range_to_brand_form`
--
ALTER TABLE `product_range_to_brand_form`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_range_id` (`product_range_id`),
  ADD KEY `brand_form_id` (`brand_form_id`);

--
-- Индексы таблицы `product_range_to_buyer_form`
--
ALTER TABLE `product_range_to_buyer_form`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_range_id` (`product_range_id`),
  ADD KEY `buyer_form_id` (`buyer_form_id`);

--
-- Индексы таблицы `reason_to_agency_form`
--
ALTER TABLE `reason_to_agency_form`
  ADD PRIMARY KEY (`id`),
  ADD KEY `agency_reason_id` (`agency_reason_id`),
  ADD KEY `agency_form_id` (`agency_form_id`);

--
-- Индексы таблицы `reason_to_brand_form`
--
ALTER TABLE `reason_to_brand_form`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reason_id` (`reason_id`),
  ADD KEY `brand_form_id` (`brand_form_id`);

--
-- Индексы таблицы `reason_to_buyer_form`
--
ALTER TABLE `reason_to_buyer_form`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reason_id` (`reason_id`),
  ADD KEY `buyer_form_id` (`buyer_form_id`);

--
-- Индексы таблицы `reason_to_company_form`
--
ALTER TABLE `reason_to_company_form`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reason_id` (`reason_id`),
  ADD KEY `company_form_id` (`company_form_id`);

--
-- Индексы таблицы `reason_to_media_form`
--
ALTER TABLE `reason_to_media_form`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reason_id` (`reason_id`),
  ADD KEY `media_form_id` (`media_form_id`);

--
-- Индексы таблицы `skin_color_list`
--
ALTER TABLE `skin_color_list`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `specialization_to_user`
--
ALTER TABLE `specialization_to_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `user_specialization_id` (`user_specialization_id`);

--
-- Индексы таблицы `tatoo_place_list`
--
ALTER TABLE `tatoo_place_list`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tatto_to_model`
--
ALTER TABLE `tatto_to_model`
  ADD PRIMARY KEY (`id`),
  ADD KEY `totoo_plase_id` (`totoo_plase_id`),
  ADD KEY `model_form_id` (`model_form_id`);

--
-- Индексы таблицы `trading_list`
--
ALTER TABLE `trading_list`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_type` (`account_type`),
  ADD KEY `country` (`country`),
  ADD KEY `profession` (`profession`);

--
-- Индексы таблицы `user_account_type`
--
ALTER TABLE `user_account_type`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user_country`
--
ALTER TABLE `user_country`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user_json`
--
ALTER TABLE `user_json`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user_language`
--
ALTER TABLE `user_language`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user_payments`
--
ALTER TABLE `user_payments`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user_profession`
--
ALTER TABLE `user_profession`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user_specialization`
--
ALTER TABLE `user_specialization`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `agency_age_type`
--
ALTER TABLE `agency_age_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `agency_form`
--
ALTER TABLE `agency_form`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `agency_model_category`
--
ALTER TABLE `agency_model_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `agency_professional_expiriens`
--
ALTER TABLE `agency_professional_expiriens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `agency_site_reason`
--
ALTER TABLE `agency_site_reason`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `agency_specialization_to_form`
--
ALTER TABLE `agency_specialization_to_form`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `agency_type_specialization`
--
ALTER TABLE `agency_type_specialization`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `all_profexpiriens_list`
--
ALTER TABLE `all_profexpiriens_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `brand_form`
--
ALTER TABLE `brand_form`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `brand_location`
--
ALTER TABLE `brand_location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT для таблицы `brand_location_to_user`
--
ALTER TABLE `brand_location_to_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `buyer_form`
--
ALTER TABLE `buyer_form`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `buyer_language_to_form`
--
ALTER TABLE `buyer_language_to_form`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `company_form`
--
ALTER TABLE `company_form`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `company_language_to_form`
--
ALTER TABLE `company_language_to_form`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `ethnicity_list`
--
ALTER TABLE `ethnicity_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `eye_color_list`
--
ALTER TABLE `eye_color_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `hair_color_list`
--
ALTER TABLE `hair_color_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `hair_lenght_list`
--
ALTER TABLE `hair_lenght_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `hair_oloration_list`
--
ALTER TABLE `hair_oloration_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `language_list`
--
ALTER TABLE `language_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `main_reason_list`
--
ALTER TABLE `main_reason_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `media_form`
--
ALTER TABLE `media_form`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `media_language_to_form`
--
ALTER TABLE `media_language_to_form`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `media_position_list`
--
ALTER TABLE `media_position_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `media_position_to_form`
--
ALTER TABLE `media_position_to_form`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `model_form`
--
ALTER TABLE `model_form`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `model_form_to_main_reason`
--
ALTER TABLE `model_form_to_main_reason`
  MODIFY `field1` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `model_language_to_form`
--
ALTER TABLE `model_language_to_form`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `piercing_has_list`
--
ALTER TABLE `piercing_has_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `piercing_to_model`
--
ALTER TABLE `piercing_to_model`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `product_range_list`
--
ALTER TABLE `product_range_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `product_range_to_brand_form`
--
ALTER TABLE `product_range_to_brand_form`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `product_range_to_buyer_form`
--
ALTER TABLE `product_range_to_buyer_form`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `reason_to_agency_form`
--
ALTER TABLE `reason_to_agency_form`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `reason_to_brand_form`
--
ALTER TABLE `reason_to_brand_form`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `reason_to_buyer_form`
--
ALTER TABLE `reason_to_buyer_form`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `reason_to_company_form`
--
ALTER TABLE `reason_to_company_form`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `reason_to_media_form`
--
ALTER TABLE `reason_to_media_form`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `skin_color_list`
--
ALTER TABLE `skin_color_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `specialization_to_user`
--
ALTER TABLE `specialization_to_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `tatoo_place_list`
--
ALTER TABLE `tatoo_place_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `tatto_to_model`
--
ALTER TABLE `tatto_to_model`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `trading_list`
--
ALTER TABLE `trading_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=64;
--
-- AUTO_INCREMENT для таблицы `user_account_type`
--
ALTER TABLE `user_account_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `user_country`
--
ALTER TABLE `user_country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `user_json`
--
ALTER TABLE `user_json`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT для таблицы `user_language`
--
ALTER TABLE `user_language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `user_payments`
--
ALTER TABLE `user_payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `user_profession`
--
ALTER TABLE `user_profession`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `user_specialization`
--
ALTER TABLE `user_specialization`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `agency_form`
--
ALTER TABLE `agency_form`
  ADD CONSTRAINT `agency_form_fk1` FOREIGN KEY (`age_type`) REFERENCES `agency_age_type` (`id`);

--
-- Ограничения внешнего ключа таблицы `agency_model_category`
--
ALTER TABLE `agency_model_category`
  ADD CONSTRAINT `agency_model_category_fk1` FOREIGN KEY (`parent`) REFERENCES `agency_model_category` (`id`);

--
-- Ограничения внешнего ключа таблицы `agency_specialization_to_form`
--
ALTER TABLE `agency_specialization_to_form`
  ADD CONSTRAINT `agency_specialization_to_form_fk2` FOREIGN KEY (`agency_form_id`) REFERENCES `agency_form` (`id`),
  ADD CONSTRAINT `agency_specialization_to_form_fk1` FOREIGN KEY (`agency_specialization_id`) REFERENCES `agency_type_specialization` (`id`);

--
-- Ограничения внешнего ключа таблицы `brand_form`
--
ALTER TABLE `brand_form`
  ADD CONSTRAINT `brand_form_fk2` FOREIGN KEY (`trading`) REFERENCES `trading_list` (`id`),
  ADD CONSTRAINT `brand_form_fk1` FOREIGN KEY (`professional_expiriens`) REFERENCES `all_profexpiriens_list` (`id`);

--
-- Ограничения внешнего ключа таблицы `brand_location`
--
ALTER TABLE `brand_location`
  ADD CONSTRAINT `brand_location_fk1` FOREIGN KEY (`parent`) REFERENCES `brand_location` (`id`);

--
-- Ограничения внешнего ключа таблицы `brand_location_to_user`
--
ALTER TABLE `brand_location_to_user`
  ADD CONSTRAINT `brand_location_to_user_fk2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `brand_location_to_user_fk1` FOREIGN KEY (`brand_location_id`) REFERENCES `brand_location` (`id`);

--
-- Ограничения внешнего ключа таблицы `buyer_form`
--
ALTER TABLE `buyer_form`
  ADD CONSTRAINT `buyer_form_fk1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `buyer_form_fk2` FOREIGN KEY (`trading`) REFERENCES `trading_list` (`id`),
  ADD CONSTRAINT `buyer_form_fk3` FOREIGN KEY (`professional_expiriens`) REFERENCES `all_profexpiriens_list` (`id`);

--
-- Ограничения внешнего ключа таблицы `buyer_language_to_form`
--
ALTER TABLE `buyer_language_to_form`
  ADD CONSTRAINT `buyer_language_to_form_fk2` FOREIGN KEY (`buyer_form_id`) REFERENCES `brand_form` (`id`),
  ADD CONSTRAINT `buyer_language_to_form_fk1` FOREIGN KEY (`language_id`) REFERENCES `language_list` (`id`);

--
-- Ограничения внешнего ключа таблицы `company_form`
--
ALTER TABLE `company_form`
  ADD CONSTRAINT `company_form_fk1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `company_form_fk2` FOREIGN KEY (`position`) REFERENCES `position_list` (`id`),
  ADD CONSTRAINT `company_form_fk3` FOREIGN KEY (`professional_experience`) REFERENCES `all_profexpiriens_list` (`id`);

--
-- Ограничения внешнего ключа таблицы `company_language_to_form`
--
ALTER TABLE `company_language_to_form`
  ADD CONSTRAINT `company_language_to_form_fk2` FOREIGN KEY (`company_form_id`) REFERENCES `company_form` (`id`),
  ADD CONSTRAINT `company_language_to_form_fk1` FOREIGN KEY (`language_id`) REFERENCES `language_list` (`id`);

--
-- Ограничения внешнего ключа таблицы `media_form`
--
ALTER TABLE `media_form`
  ADD CONSTRAINT `media_form_fk1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Ограничения внешнего ключа таблицы `media_language_to_form`
--
ALTER TABLE `media_language_to_form`
  ADD CONSTRAINT `media_language_to_form_fk2` FOREIGN KEY (`media_form_id`) REFERENCES `media_form` (`id`),
  ADD CONSTRAINT `media_language_to_form_fk1` FOREIGN KEY (`language_id`) REFERENCES `language_list` (`id`);

--
-- Ограничения внешнего ключа таблицы `media_position_to_form`
--
ALTER TABLE `media_position_to_form`
  ADD CONSTRAINT `media_position_to_form_fk2` FOREIGN KEY (`media_form_id`) REFERENCES `model_form` (`id`),
  ADD CONSTRAINT `media_position_to_form_fk1` FOREIGN KEY (`media_position_id`) REFERENCES `media_position_list` (`id`);

--
-- Ограничения внешнего ключа таблицы `model_form`
--
ALTER TABLE `model_form`
  ADD CONSTRAINT `model_form_fk1` FOREIGN KEY (`id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `model_form_fk2` FOREIGN KEY (`ethnicity`) REFERENCES `ethnicity_list` (`id`),
  ADD CONSTRAINT `model_form_fk3` FOREIGN KEY (`skin_color`) REFERENCES `skin_color_list` (`id`),
  ADD CONSTRAINT `model_form_fk4` FOREIGN KEY (`eye_color`) REFERENCES `eye_color_list` (`id`),
  ADD CONSTRAINT `model_form_fk5` FOREIGN KEY (`hair_color`) REFERENCES `hair_color_list` (`id`);

--
-- Ограничения внешнего ключа таблицы `model_form_to_main_reason`
--
ALTER TABLE `model_form_to_main_reason`
  ADD CONSTRAINT `model_form_to_main_reason_fk2` FOREIGN KEY (`model_form_id`) REFERENCES `model_form` (`id`),
  ADD CONSTRAINT `model_form_to_main_reason_fk1` FOREIGN KEY (`main_reason_id`) REFERENCES `main_reason_list` (`id`);

--
-- Ограничения внешнего ключа таблицы `model_language_to_form`
--
ALTER TABLE `model_language_to_form`
  ADD CONSTRAINT `model_language_to_form_fk1` FOREIGN KEY (`model_language_id`) REFERENCES `language_list` (`id`),
  ADD CONSTRAINT `model_language_to_form_fk2` FOREIGN KEY (`model_form_id`) REFERENCES `model_form` (`id`);

--
-- Ограничения внешнего ключа таблицы `piercing_to_model`
--
ALTER TABLE `piercing_to_model`
  ADD CONSTRAINT `piercing_to_model_fk2` FOREIGN KEY (`model_form_id`) REFERENCES `model_form` (`id`),
  ADD CONSTRAINT `piercing_to_model_fk1` FOREIGN KEY (`piercing_has_id`) REFERENCES `piercing_has_list` (`id`);

--
-- Ограничения внешнего ключа таблицы `product_range_to_brand_form`
--
ALTER TABLE `product_range_to_brand_form`
  ADD CONSTRAINT `product_range_to_brand_form_fk2` FOREIGN KEY (`brand_form_id`) REFERENCES `brand_form` (`id`),
  ADD CONSTRAINT `product_range_to_brand_form_fk1` FOREIGN KEY (`product_range_id`) REFERENCES `product_range_list` (`id`);

--
-- Ограничения внешнего ключа таблицы `product_range_to_buyer_form`
--
ALTER TABLE `product_range_to_buyer_form`
  ADD CONSTRAINT `product_range_to_buyer_form_fk2` FOREIGN KEY (`buyer_form_id`) REFERENCES `buyer_form` (`id`),
  ADD CONSTRAINT `product_range_to_buyer_form_fk1` FOREIGN KEY (`product_range_id`) REFERENCES `product_range_list` (`id`);

--
-- Ограничения внешнего ключа таблицы `reason_to_agency_form`
--
ALTER TABLE `reason_to_agency_form`
  ADD CONSTRAINT `reason_to_agency_form_fk2` FOREIGN KEY (`agency_form_id`) REFERENCES `agency_form` (`id`),
  ADD CONSTRAINT `reason_to_agency_form_fk1` FOREIGN KEY (`agency_reason_id`) REFERENCES `agency_site_reason` (`id`);

--
-- Ограничения внешнего ключа таблицы `reason_to_brand_form`
--
ALTER TABLE `reason_to_brand_form`
  ADD CONSTRAINT `reason_to_brand_form_fk2` FOREIGN KEY (`brand_form_id`) REFERENCES `brand_form` (`id`),
  ADD CONSTRAINT `reason_to_brand_form_fk1` FOREIGN KEY (`reason_id`) REFERENCES `main_reason_list` (`id`);

--
-- Ограничения внешнего ключа таблицы `reason_to_buyer_form`
--
ALTER TABLE `reason_to_buyer_form`
  ADD CONSTRAINT `reason_to_buyer_form_fk2` FOREIGN KEY (`buyer_form_id`) REFERENCES `brand_form` (`id`),
  ADD CONSTRAINT `reason_to_buyer_form_fk1` FOREIGN KEY (`reason_id`) REFERENCES `main_reason_list` (`id`);

--
-- Ограничения внешнего ключа таблицы `reason_to_company_form`
--
ALTER TABLE `reason_to_company_form`
  ADD CONSTRAINT `reason_to_company_form_fk2` FOREIGN KEY (`company_form_id`) REFERENCES `company_form` (`id`),
  ADD CONSTRAINT `reason_to_company_form_fk1` FOREIGN KEY (`reason_id`) REFERENCES `main_reason_list` (`id`);

--
-- Ограничения внешнего ключа таблицы `reason_to_media_form`
--
ALTER TABLE `reason_to_media_form`
  ADD CONSTRAINT `reason_to_media_form_fk2` FOREIGN KEY (`media_form_id`) REFERENCES `media_form` (`id`),
  ADD CONSTRAINT `reason_to_media_form_fk1` FOREIGN KEY (`reason_id`) REFERENCES `main_reason_list` (`id`);

--
-- Ограничения внешнего ключа таблицы `specialization_to_user`
--
ALTER TABLE `specialization_to_user`
  ADD CONSTRAINT `specialization_to_user_fk2` FOREIGN KEY (`user_specialization_id`) REFERENCES `user_specialization` (`id`),
  ADD CONSTRAINT `specialization_to_user_fk1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Ограничения внешнего ключа таблицы `tatto_to_model`
--
ALTER TABLE `tatto_to_model`
  ADD CONSTRAINT `tatto_to_model_fk2` FOREIGN KEY (`model_form_id`) REFERENCES `model_form` (`id`),
  ADD CONSTRAINT `tatto_to_model_fk1` FOREIGN KEY (`totoo_plase_id`) REFERENCES `tatoo_place_list` (`id`);

--
-- Ограничения внешнего ключа таблицы `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_fk3` FOREIGN KEY (`profession`) REFERENCES `user_profession` (`id`),
  ADD CONSTRAINT `user_fk1` FOREIGN KEY (`account_type`) REFERENCES `user_account_type` (`id`),
  ADD CONSTRAINT `user_fk2` FOREIGN KEY (`country`) REFERENCES `user_country` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
