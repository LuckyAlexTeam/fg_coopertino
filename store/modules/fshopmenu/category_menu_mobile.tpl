<span class="js_menu_item menu_burger">
    <div></div>
    <div></div>
    <div></div>
    <ul style="display: none;">
        <li><a href="/my-account">{l s='My profile' mod='ffcarvarimenu'}</a></li>
        <li><a href="/content/pro-magazin-6">{l s='About shop' mod='ffcarvarimenu'}</a></li>
        <li><a href="/content/delivery-1">{l s='Delivery' mod='ffcarvarimenu'}</a></li>
        <li><a href="/content/oplata-7">{l s='Payment' mod='ffcarvarimenu'}</a></li>
        <li><a href="/content/garantiya-8">{l s='Guaranty' mod='ffcarvarimenu'}</a></li>
        <li><a href="/content/obmin-ta-povernennya-9">{l s='Exchange and return' mod='ffcarvarimenu'}</a></li>
    </ul>
</span>


<div class="menu">
    {foreach from=$menuCategTree.children item=menuCategory}

        <div class="menu_item js_menu_item">
            {$menuCategory.name}
            <ul style="display: none">
                {foreach from=$menuCategory.children item=menuSubcategory name=menuSubForeach}
                    <li><a href="{$menuSubcategory.link}">{$menuSubcategory.name}</a></li>
                 {/foreach}
            </ul>
        </div>

    {/foreach}
</div>



