<?php
class SiteBlock extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    public function tableName() {
        return 'site_block';
    }
	//id code npp model photograph designer agency brand buyer customer company media desinger stylist hair mua 
    public function getAll() {
		$list = self::model()->findAll();
		$arr = Array();
		//$rrr = $this->getAttributes();
		foreach ($list as $lab) {
			$id = $lab['id'];
			$arr[$id]['id'] = $lab['id'];
			$arr[$id]['code'] = $lab['code'];
			$arr[$id]['npp'] = $lab['npp'];
			$arr[$id]['model'] = $lab['model'];
			$arr[$id]['photograph'] = $lab['photograph'];
			$arr[$id]['designer'] = $lab['designer'];
			$arr[$id]['agency'] = $lab['agency'];
			$arr[$id]['brand'] = $lab['brand'];
			$arr[$id]['buyer'] = $lab['buyer'];
			$arr[$id]['customer'] = $lab['customer'];
			$arr[$id]['company'] = $lab['company'];
			$arr[$id]['media'] = $lab['media'];
			$arr[$id]['desinger'] = $lab['desinger'];
			$arr[$id]['stylist'] = $lab['stylist'];
			$arr[$id]['hair'] = $lab['hair'];
			$arr[$id]['mua'] = $lab['mua'];
		};
		//var_dump($arr);
		return $arr;
    }
	
	
}