<?php

/**
 * This is the model class for table "piercing_to_model".
 *
 * The followings are the available columns in table 'piercing_to_model':
 * @property integer $id
 * @property integer $piercing_has_id
 * @property integer $model_form_id
 *
 * The followings are the available model relations:
 * @property ModelForm $modelForm
 * @property PiercingHasList $piercingHas
 */
class BasePiercingToModel extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PiercingToModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'piercing_to_model';
	}

	/**
	 * @return array validation rules for model attributes.
	 */ 
	 
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('piercing_has_id, model_form_id', 'required'),
			array('piercing_has_id, model_form_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, piercing_has_id, model_form_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'modelForm' => array(self::BELONGS_TO, 'ModelForm', 'model_form_id'),
			'piercingHas' => array(self::BELONGS_TO, 'PiercingHasList', 'piercing_has_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'piercing_has_id' => 'Piercing Has',
			'model_form_id' => 'Model Form',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('piercing_has_id',$this->piercing_has_id);
		$criteria->compare('model_form_id',$this->model_form_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}