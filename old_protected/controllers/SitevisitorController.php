<?php

class SitevisitorController extends CController {
	public $layout='/layouts/mainvisitor';
	//
	public $arr_menu;
	public $arr_lang;
	public $arr_site;
	public $arr_header;
	public $arr_cont;
	public $tlanguage;
	public $tuser;
	
	public $breadcrumbs;
	//
	public function actions() {
		return array();
	}

	public function actionIndex() {
		$this->site_stranica();
		$this->render('index');
	}
	public function actionSetting() {
		$this->site_stranica();
		$this->render('setting');
	}
	
	public function site_stranica() {
		// Контроль входа 
		//print "<!-- isGuest = "; var_dump( Yii::app()->user->isGuest ); /* boolean */ print "-->";
		if (Yii::app()->user->isGuest) {$this->redirect('index.php?r=site/index');};
		
		$pac = Yii::app()->user->getState('package');
		$lan = Yii::app()->user->getState('language');
		$use = Yii::app()->user->getState('id_user');
		//if ( isset($_REQUEST['id']) ) { $vus = $_REQUEST['id']; } else { $vus = ''; }; // id usera которого я смотрю
		if ( isset($_REQUEST['id_viewuser']) ) { $vus = $_REQUEST['id_viewuser']; } else { $vus = ''; }; // id usera которого я смотрю
		Yii::app()->user->setState('id_viewuser', $vus);
		//print "<!-- *** {$pac} = {$lan} = user={$use} , viewuser={$vus}, guest=".Yii::app()->user->isGuest." *** -->";
			
		$modvh = new Vhod;
		$otv = $modvh->getUser();
		
		
		$model=new Language();
		$this->tlanguage = $model->getLanguage();
		$this->arr_lang = $model->getAll();
		$modea=new Site();
		//$this->arr_site  = $modea->getAll($this->tlanguage, 'visitor');
		$this->arr_site  = $modea->getAll($lan, 'visitor');
		$modeb=new SiteHeader();
		$this->arr_header = $modeb->getAll($this->tlanguage, 'index');
		$modec=new Users();
		$this->tuser = $modec->getKart($use);
	}
}
?>