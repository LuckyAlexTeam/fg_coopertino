<?php

class WidProfessional  extends CWidget {
	
	public $items=array();
	public $arm = array();
	public $arh = array();
	public $ars = array();
	public $edit = "Y";
	// Макет
	private $vis = array('genre', 'experience', 'additional_skills', 'languages', 'compensation', 'rate_per_hour', 'rate_half_day', 'rate_full_day', 'rates_for_unway', 'contracted', 'travel_ability', 'reasons'); 
	public function run() {
		$pac = Yii::app()->user->getState('package');
		$lan = Yii::app()->user->getState('language');
		$uid = Yii::app()->user->getState('id_user');
		$vid = Yii::app()->user->getState('id_viewuser');
		print "<!-- ***wid*** {$pac} = {$lan} = ***user= {$uid} view={$vid}= -->";
		
		$model=new Site;
		$this->arh = $model->getAll($lan, 'professional'); // Названия всех полей для данного виджета на нужном языке
		if ($this->items['cod'] == "visitor") { $this->arm = $this->vis; }; // Макет - перечень полей из БД и их последовательность
		$modus=new Users;
		if ($vid != '') { $this->edit="N"; $uid=$vid;};
		$this->ars = $modus->getKart($uid);
		$this->render('professional');
	}
}
?>
