-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: serv50.coopertino.ru    Database: luckyale_fash
-- ------------------------------------------------------
-- Server version	5.6.30-cll-lve

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `calendar`
--

DROP TABLE IF EXISTS `calendar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calendar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_typ` tinyint(1) NOT NULL DEFAULT '0',
  `data_s` datetime NOT NULL,
  `data_p` datetime NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `location` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Календарь';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calendar`
--

LOCK TABLES `calendar` WRITE;
/*!40000 ALTER TABLE `calendar` DISABLE KEYS */;
INSERT INTO `calendar` VALUES (1,3,0,'2016-05-15 12:00:00','2016-05-25 22:00:00','Фотосессия','','Фотосессия для девушек чернокожих.');
/*!40000 ALTER TABLE `calendar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calendar_location`
--

DROP TABLE IF EXISTS `calendar_location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calendar_location` (
  `id` int(11) NOT NULL,
  `id_calendar` int(11) NOT NULL,
  `id_country` int(11) NOT NULL,
  `id_city` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calendar_location`
--

LOCK TABLES `calendar_location` WRITE;
/*!40000 ALTER TABLE `calendar_location` DISABLE KEYS */;
/*!40000 ALTER TABLE `calendar_location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event`
--

DROP TABLE IF EXISTS `event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_typ` smallint(1) NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event`
--

LOCK TABLES `event` WRITE;
/*!40000 ALTER TABLE `event` DISABLE KEYS */;
/*!40000 ALTER TABLE `event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `language`
--

DROP TABLE IF EXISTS `language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `language` (
  `cod` varchar(10) NOT NULL COMMENT 'Код языка',
  `ico` varchar(255) NOT NULL COMMENT 'Путь к иконке',
  `name` varchar(50) NOT NULL COMMENT 'Наименование языка на языке',
  `def` varchar(10) NOT NULL COMMENT 'Язык по умолчанию',
  PRIMARY KEY (`cod`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `language`
--

LOCK TABLES `language` WRITE;
/*!40000 ALTER TABLE `language` DISABLE KEYS */;
INSERT INTO `language` VALUES ('en','','English','N'),('ru','','Русский','Y');
/*!40000 ALTER TABLE `language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nastroyki`
--

DROP TABLE IF EXISTS `nastroyki`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nastroyki` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cod` varchar(100) NOT NULL DEFAULT '',
  `cod_down` varchar(100) NOT NULL DEFAULT '',
  `language` varchar(10) NOT NULL DEFAULT 'all',
  `int_1` int(11) NOT NULL DEFAULT '0',
  `int_2` int(11) NOT NULL DEFAULT '0',
  `txt_1` varchar(200) NOT NULL DEFAULT '',
  `txt_2` varchar(200) NOT NULL DEFAULT '',
  `txt_3` varchar(255) NOT NULL DEFAULT '',
  `txt_4` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nastroyki`
--

LOCK TABLES `nastroyki` WRITE;
/*!40000 ALTER TABLE `nastroyki` DISABLE KEYS */;
INSERT INTO `nastroyki` VALUES (3,'price','classic','all',26,0,'','','',''),(4,'price','vip','all',260,0,'','','',''),(5,'price','corporate','all',360,0,'','','',''),(6,'mail','visitor','all',0,0,'visiter@fashingreatness.ru','','',''),(11,'mail','vip','all',0,0,'vip@fashingreatness.ru','','',''),(12,'mail','corporate','all',0,0,'corporate@fashingreatness.ru','','',''),(13,'mail','classic','all',0,0,'classic@fashingreatness.ru','','',''),(21,'mailos','sel_1#1','all',0,0,'info@fashingreatness.ru','General questions','',''),(22,'mailos','sel_1#2','all',0,0,'support@fashingreatness.ru','Technical support','',''),(23,'mailos','sel_1#3','all',0,0,'contacts@fashingreatness.ru','Partnership/Cooperation','',''),(24,'mailos','sel_1#4','all',0,0,'contacts@fashingreatness.ru','Events','',''),(25,'mailos','sel_1#5','all',0,0,'support@fashingreatness.ru','Claims and proposais','',''),(26,'mailos','sel_1#6','all',0,0,'contacts@fashingreatness.ru','Press Services','',''),(27,'mailos','sel_1#7','all',0,0,'info@fashingreatness.ru','Other','',''),(28,'mailos','sel_1#8','all',0,0,'Anna.May@fashingreatness.ru','Копия','',''),(29,'mailos','sel_1#9','all',0,0,'Alexandra.S@fashingreatness.ru','Копия','','');
/*!40000 ALTER TABLE `nastroyki` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site`
--

DROP TABLE IF EXISTS `site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `view` varchar(255) NOT NULL COMMENT 'Имя view',
  `cod` varchar(50) NOT NULL COMMENT 'Код поля',
  `cod_up` varchar(50) NOT NULL COMMENT 'Код поля верхнего уровня',
  `other` tinyint(1) NOT NULL COMMENT 'флаг 0=не other, 1=other (other условие - да показать input)',
  `npp_up` int(11) NOT NULL COMMENT '№ пп внутри code_up ',
  `npp` int(11) NOT NULL COMMENT '№ пп в форме (для генератора)',
  `img` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Флаг картинки',
  `language` varchar(10) NOT NULL COMMENT 'Код языка',
  `name` text NOT NULL COMMENT 'Значение поля',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cod_lang` (`cod`,`language`)
) ENGINE=InnoDB AUTO_INCREMENT=961 DEFAULT CHARSET=utf8 COMMENT='Поля форм на разных языках';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site`
--

LOCK TABLES `site` WRITE;
/*!40000 ALTER TABLE `site` DISABLE KEYS */;
INSERT INTO `site` VALUES (1,'main','1','',0,0,0,0,'en','THE LARGEST IN THE WORLD COMMUNITY'),(2,'main','1','',0,0,0,0,'ru','САМОЕ БОЛЬШОЕ В МИРЕ СООБЩЕСТВО'),(3,'main','2','',0,0,0,0,'ru','МОДЕЛЕЙ, МОДЕЛЬНЫХ АГЕНТСТВ, ФОТОГРАФОВ, ЭКСПЕРТОВ МОДЫ'),(4,'main','2','',0,0,0,0,'en','MODELS, MODEL AGENCIES, PHOTOGRAPHERS, EXPERTS OF FASHION'),(5,'main','3','',0,0,0,0,'ru','И ДРУГИХ ПРОФЕССИОНАЛОВ ИНДУСТРИИ МОДЫ'),(6,'main','3','',0,0,0,0,'en','AND OTHER PROFESSIONALS OF FASHION'),(7,'main','m1','',0,0,0,0,'ru','ДОМОЙ'),(8,'main','m1','',0,0,0,0,'en','HOME'),(9,'main','m2','',0,0,0,0,'ru','О НАС'),(10,'main','m2','',0,0,0,0,'en','ABOUT'),(11,'main','m3','',0,0,0,0,'ru','ПРЕИМУЩЕСТВА'),(12,'main','m3','',0,0,0,0,'en','FEATURES'),(13,'main','m4','',0,0,0,0,'ru','КОМАНДА'),(14,'main','m4','',0,0,0,0,'en','TEAM'),(15,'main','m5','',0,0,0,0,'ru','ЦЕНОВЫЕ ПАКЕТЫ'),(16,'main','m5','',0,0,0,0,'en','PRICES'),(17,'main','m6','',0,0,0,0,'ru','СВЯЖИТЕСЬ С НАМИ'),(18,'main','m6','',0,0,0,0,'en','CONTACT US'),(19,'main','r1','',0,0,0,0,'ru','РЕГИСТРАЦИЯ'),(20,'main','r1','',0,0,0,0,'en','JOIN'),(21,'main','r2','',0,0,0,0,'ru','АВТОРИЗАЦИЯ'),(22,'main','r2','',0,0,0,0,'en','SIGN IN'),(23,'main','slogan','',0,0,0,0,'ru','эксклюзивная презентация в сети'),(24,'main','slogan','',0,0,0,0,'en','exclusive professional network'),(28,'main','cod','',0,0,0,0,'ru','name'),(29,'main','cod1','',0,0,0,0,'ru','name1'),(30,'main','cod22','',0,0,0,0,'ru','name22'),(31,'main','cod22','',0,0,0,0,'en','name22'),(32,'about','1_1','',0,0,0,0,'ru','Глобальная видимость'),(33,'about','1_1','',0,0,0,0,'en','Global visibility'),(34,'main','a_1_1','',0,0,0,0,'ru','МИРОВАЯ ВИДИМОСТЬ'),(35,'main','a_1_1','',0,0,0,0,'en','Global visibility'),(36,'main','a_2_1','',0,0,0,0,'ru','НОВЫЕ ПРОЕКТЫ'),(37,'main','a_2_1','',0,0,0,0,'en','NEW PROJECTS'),(38,'main','a_3_1','',0,0,0,0,'ru','ОБМЕН ОПЫТОМ'),(39,'main','a_3_1','',0,0,0,0,'en','Experience exchange'),(40,'main','a_4_1','',0,0,0,0,'ru','ИВЭНТ МЕНЕДЖМЕНТ'),(41,'main','a_4_1','',0,0,0,0,'en','Event management'),(42,'main','a_1_2','',0,0,0,0,'ru','Обширный набор услуг PR'),(43,'main','a_1_2','',0,0,0,0,'en','Extensive PR'),(44,'main','a_1_3','',0,0,0,0,'ru','Доступ к огромной мировой базе профессионалов'),(45,'main','a_1_3','',0,0,0,0,'en','Access to huge worldwide database of professionals'),(46,'main','a_1_4','',0,0,0,0,'ru','Объявите себя к целому миру'),(47,'main','a_1_4','',0,0,0,0,'en','Declare yourself to the whole world'),(48,'main','a_2_2','',0,0,0,0,'ru','Продавайте свои услуги'),(49,'main','a_2_2','',0,0,0,0,'en','Sell your services'),(50,'main','a_2_3','',0,0,0,0,'ru','Получайте новые предложения о работе'),(51,'main','a_2_3','',0,0,0,0,'en','Get new job offers'),(52,'main','a_2_4','',0,0,0,0,'ru','Ищите новых клиентов и/или сотрудников'),(53,'main','a_2_4','',0,0,0,0,'en','Search for new clients and/or employers'),(54,'main','a_2_5','',0,0,0,0,'ru','Подписывайте новые контракты'),(55,'main','a_2_5','',0,0,0,0,'en','Sign new contracts'),(56,'main','a_3_2','',0,0,0,0,'ru','Будьте лектором или слушателем на мастер классах'),(57,'main','a_3_2','',0,0,0,0,'en','Be a lecturer or listener on master classes'),(58,'main','a_3_3','',0,0,0,0,'ru','Получайте практическую информацию о развитии карьеры'),(59,'main','a_3_3','',0,0,0,0,'en','Get practical information about career development'),(60,'main','a_3_4','',0,0,0,0,'ru','Консультируйтесь с Гуру Моды'),(61,'main','a_3_4','',0,0,0,0,'en','Consult with fashion Gurus'),(62,'main','a_3_5','',0,0,0,0,'ru','Отслеживайте самую свежую информацию о моде'),(63,'main','a_3_5','',0,0,0,0,'en','Read latest fashion news'),(64,'main','a_4_2','',0,0,0,0,'ru','Недели моды/Шоу-румы'),(65,'main','a_4_2','',0,0,0,0,'en','Fashion weeks/Show Rooms'),(66,'main','a_4_3','',0,0,0,0,'ru','Эксклюзивные VIP мероприятия'),(67,'main','a_4_3','',0,0,0,0,'en','Exclusive VIP events'),(68,'main','a_4_4','',0,0,0,0,'ru','Ивэнт консалтинг'),(69,'main','a_4_4','',0,0,0,0,'en','Event consulting'),(70,'main','a_4_5','',0,0,0,0,'ru','Международные конкурсы'),(71,'main','a_4_5','',0,0,0,0,'en','International Competitions'),(72,'main','a_5','',0,0,0,0,'ru','ДУМАЙТЕ ШИРЕ. ДУМАЙТЕ В МЕЖДУНАРОДНОМ МАСШТАБЕ. МЕЧТАЙТЕ.'),(73,'main','a_5','',0,0,0,0,'en','THINK WIDER. THINK GLOBALLY. DREAM.'),(74,'main','f_m_1','',0,0,0,0,'ru','МОДЕЛИ'),(75,'main','f_m_1','',0,0,0,0,'en','MODELS'),(76,'main','f_m_2','',0,0,0,0,'ru','фотографы'),(77,'main','f_m_2','',0,0,0,0,'en','photographers'),(80,'main','f_m_3','',0,0,0,0,'ru','Модельные агентства'),(81,'main','f_m_3','',0,0,0,0,'en','Model Agencies'),(82,'main','f_m_4','',0,0,0,0,'ru','Эксперты моды'),(83,'main','f_m_4','',0,0,0,0,'en','Fashion Experts'),(84,'main','f_m_5','',0,0,0,0,'ru','Другие профессионалы'),(85,'main','f_m_5','',0,0,0,0,'en','Other Professionals'),(86,'main','f_m_6','',0,0,0,0,'ru','На сайте уже зарегистрированы'),(87,'main','f_m_6','',0,0,0,0,'en','On our site are already registered'),(88,'main','f_s_1','',0,0,0,0,'ru','Добро пожаловать на fashiongreatness.com, многофункциональный международный портал для лучших профессионалов индустрии моды. Наше эксклюзивное сообщество предлагает широкий спектр возможностей для любой категории специалистов этой сферы. Таким образом, модели могут использовать такую уникальную услугу, как персональный PR-менеджер, который поможет им продвигать свой профиль на нашем сайте. Кроме того, модели могут находить предложения о работе, искать фотографов, устанавливать новые контакты, участвовать в конкурсах, кастингах, неделях моды, шоу-румах, иметь доступ к VIP-мероприятиям закрытого типа по всему миру, подписываться на/ проводить мастер-классы; отслеживать новости мира моды и узнавать о реалиях модельного бизнеса, загружать фотографии в лучшем качестве и многое другое. И это только малая часть преимуществ, которые можно получить при пользовании нашим порталом. Мы подготовили множество приятных сюрпризов для вас. Приготовьтесь!'),(89,'main','f_s_1','',0,0,0,0,'en','Welcome to fashiongreatness.com, the multifunctional worldwide network for the best professionals in Fashion Industry. Our exclusive community offers a wide range of opportunities for any category of specialists in this sphere. Thus, models can use such a unique service as a personal PR manager, who will help them to promote their profile on our website. Besides, models are able to search for work offers, photographers, establish new contacts, participate in competitions, castings, Fashion Weeks, Show Rooms; have an access to worldwide closed-type VIP events, sign up for/carry out master classes; monitor fashion news and know about the realias of fashion world, upload photos in the best quality and many others. It’s only a small part of advantages you can take of using our network. We’ve prepared a lot of pleasant surprises for you. Be ready!    '),(90,'main','f_s_2','',0,0,0,0,'ru','Добро пожаловать в моде greatness.com, многофункциональной всемирной сети для лучших профессионалов в области индустрии моды. Наше эксклюзивное сообщество предлагает широкий спектр возможностей для любой категории специалистов в этой сфере. Таким образом, фотографы могут использовать такую уникальную услугу в качестве персонального менеджера по связям с общественностью, который поможет им продвигать свой профиль на нашем сайте. Кроме того, фотографы могут продать свои услуги; поиск клиентов; подписаться на / проводить мастер-классы; обмен опытом с другими людьми; знак субподряда; поиск моделей; загрузить фотографии в лучшем качестве; участвовать в конкурсах; имеют доступ к эксклюзивным VIP-мероприятий по всему миру закрытого типа и многие другие. Это лишь малая часть преимуществ, которые вы можете предпринять использования нашей сети. Мы подготовили много приятных сюрпризов для вас. Будь готов!'),(91,'main','f_s_2','',0,0,0,0,'en','Welcome to fashiongreatness.com, the multifunctional worldwide network for the best professionals in Fashion Industry. Our exclusive community offers a wide range of opportunities for any category of specialists in this sphere. Thus, photographers can use such a unique service as a personal PR manager, who will help them to promote their profile on our website. Besides, photographers are able to sell their services; search for clients; sign up for/ carry out master classes; exchange experience with others; sign subcontracts; search for models; upload photos in the best quality; participate in competitions; have an access to exclusive worldwide closed-type VIP events and many others. It’s only a small part of advantages you can take of using our network. We’ve prepared a lot of pleasant surprises for you. Be ready!'),(92,'main','f_s_3','',0,0,0,0,'ru','Добро пожаловать на fashiongreatness.com, многофункциональный международный портал для лучших профессионалов индустрии моды. Наше эксклюзивное сообщество предлагает широкий спектр возможностей для любой категории специалистов этой сферы. Таким образом, модельные агентства могут использовать такую уникальную услугу, как персональный PR-менеджер, который поможет им продвигать их моделей на нашем сайте; регистрацию корпоративного аккаунта с определённым количеством профилей для моделей, получение информации о совершённых их моделями сделках на сайте, заполнение календаря занятости. К тому же мы всегда готовы обсудить дополнительные уникальные возможности, предоставляемые вашей компании. Помимо всего прочего, модельные агентства смогут проводить мастер-классы; искать новых моделей; подписывать контракты по субподряду (искать фотографов, визажистов и других профессионалов); использовать огромное количество инструментов по самопродвижению, PR услуг; иметь доступ к VIP-мероприятиям закрытого типа по всему миру; устанавливать новые контакты и многое другое. И это только малая часть преимуществ, которые можно получить при пользовании нашим порталом. Мы подготовили множество приятных сюрпризов для вас. Приготовьтесь!'),(93,'main','f_s_3','',0,0,0,0,'en','Welcome to fashiongreatness.com, the multifunctional worldwide network for the best professionals in Fashion Industry. Our exclusive community offers a wide range of opportunities for any category of specialists in this sphere. Thus, model agencies can use such a unique service as a personal PR manager, who will help them to promote their models on our website, corporate registration with certain number of profiles for  models, worldwide visibility and new contracts for them, controlling their activity, receiving information about transactions which are made by models on our network, marking the workload in a calendar. And we are always ready to discuss extra unique advantages for your company. Besides, model agencies are able to carry out master classes, search for new models, sign subcontracts (search for photographers, MUA and other professionals); use a great number of self-promotion tools and PR-services; have an access to exclusive worldwide closed-type VIP events; establish new contacts and many others. It’s only a small part of advantages you can take of using our network. We’ve prepared a lot of pleasant surprises for you. Be ready!'),(94,'main','f_s_4','',0,0,0,0,'ru','Добро пожаловать на fashiongreatness.com, многофункциональный международный портал для лучших профессионалов индустрии моды. Наше эксклюзивное сообщество предлагает широкий спектр возможностей для любой категории специалистов этой сферы. Таким образом, эксперты моды могут использовать такую уникальную услугу, как персональный PR-менеджер, который поможет им продвигать свой профиль на нашем сайте. Кроме того, эксперты моды могут искать новых клиентов, партнёров; подписывать контракты; использовать огромное количество инструментов по самопродвижению и PR услуг для того, чтобы представлять свои интересы, торговую марку или услуги; сотрудничать с другими профессионалами индустрии моды и участвовать в международных проектах, неделях моды, шоу-румах; иметь доступ к VIP-мероприятиям закрытого типа по всему миру и многое другое. И это только малая часть преимуществ, которые можно получить при пользовании нашим порталом. Мы подготовили множество приятных сюрпризов для вас. Приготовьтесь!'),(95,'main','f_s_4','',0,0,0,0,'en','Welcome to fashiongreatness.com, the multifunctional worldwide network for the best professionals in Fashion Industry. Our exclusive community offers a wide range of opportunities for any category of specialists in this sphere. Thus, fashion experts can use such a unique service as a personal PR manager, who will help them to promote their profile on our website. Besides, fashion experts are able to search for new clients, partners; sign new contracts; use a great number of self-promotion and PR tools to promote their interests, their trademark or services, cooperate with other fashion professionals and participate in international projects, Fashion Weeks, Show Rooms; have an access to exclusive worldwide closed-type VIP events, and many others. It’s only a small part of advantages you can take of using our network. We’ve prepared a lot of pleasant surprises for you. Be ready!'),(96,'main','f_s_5','',0,0,0,0,'ru','Добро пожаловать на fashiongreatness.com, многофункциональный международный портал для лучших профессионалов индустрии моды. Наше эксклюзивное сообщество предлагает широкий спектр возможностей для любой категории специалистов этой сферы. Таким образом, профессионалы индустрии моды могут использовать такую уникальную услугу, как персональный PR-менеджер, который поможет им продвигать свой профиль на нашем сайте. Кроме того, профессионалы индустрии моды могут продавать свои услуги; искать новых клиентов; получать самую свежие новости из мира моды и практическую информацию о развитии карьеры; подписываться на/ проводить мастер-классы; участвовать в международных проектах, неделях моды, шоу-румах; иметь доступ к VIP-мероприятиям закрытого типа по всему миру и многое другое. И это только малая часть преимуществ, которые можно получить при пользовании нашим порталом. Мы подготовили множество приятных сюрпризов для вас. Приготовьтесь!'),(97,'main','f_s_5','',0,0,0,0,'en','Welcome to fashiongreatness.com, the multifunctional worldwide network for the best professionals in Fashion Industry. Our exclusive community offers a wide range of opportunities for any category of specialists in this sphere. Thus, fashion professionals can use such a unique service as a personal PR manager, who will help them to promote their profile on our website. Besides, fashion professionals are able to sell their services; search for new clients; get the latest news of fashion world and practical information about career development; sign up for/ carry out master classes; participate in international projects/events, Fashion Weeks, Show Rooms; have an access to exclusive worldwide closed-type VIP events, and many others. It’s only a small part of advantages you can take of using our network. We’ve prepared a lot of pleasant surprises for you. Be ready!'),(98,'main','t_zag','',0,0,0,0,'ru','МЫ — ПРОФЕССИОНАЛЫ СВОЕГО ДЕЛА С МНОГОЛЕТНИМ ОПЫТОМ В СФЕРЕ ВЫСОКОЙ МОДЫ.'),(99,'main','t_zag','',0,0,0,0,'en','WE HAVE A LONG-TERM EXPERIENCE OF WORK IN FASHION INDUSTRY AND KNOW OUR BUSINESS WELL.'),(100,'main','t_1_1','',0,0,0,0,'ru','АРТУР ФРАНЧУК ФОН МАНШТЕЙН'),(101,'main','t_1_1','',0,0,0,0,'en','Arthur Frantchouk Von Manstein'),(102,'main','t_1_2','',0,0,0,0,'ru','директор, мода величие группа, ООО'),(103,'main','t_1_2','',0,0,0,0,'en','director, fashion greatness group, ltd'),(104,'main','t_1_3','',0,0,0,0,'ru','Что делает меня по-настоящему счастливым, так это тот факт, что мы объединяем лучших профессионалов индустрии моды со всех уголков земного шара.'),(105,'main','t_1_3','',0,0,0,0,'en','What makes me feel happy is the fact that we join together the best professionals in Fashion Industry from every corner of the globe'),(106,'main','t_1_4','',0,0,0,0,'ru','afvm@fashiongreatness.com'),(107,'main','t_1_4','',0,0,0,0,'en','afvm@fashiongreatness.com'),(108,'main','t_2_1','',0,0,0,0,'ru','Алекс'),(109,'main','t_2_1','',0,0,0,0,'en','Alex'),(110,'main','t_2_2','',0,0,0,0,'ru','главный редактор, fashiongreatness.com'),(111,'main','t_2_2','',0,0,0,0,'en','chief editor, fashiongreatness.com'),(112,'main','t_2_3','',0,0,0,0,'ru','Каждый раз, когда я начинаю писать статью, я спрашиваю себя \"эта информация может быть применена на практике?\". Если ответ да, я это пишу'),(113,'main','t_2_3','',0,0,0,0,'en','Every time I begin to write an article, I ask myself “can this information be applied in practice?”. If the answer is yes, I write it'),(114,'main','t_2_4','',0,0,0,0,'ru','Alexandra.S@fashiongreatness.com'),(115,'main','t_2_4','',0,0,0,0,'en','Alexandra.S@fashiongreatness.com'),(116,'main','t_3_1','',0,0,0,0,'ru','Анна'),(117,'main','t_3_1','',0,0,0,0,'en','Anna'),(118,'main','t_3_2','',0,0,0,0,'ru','МЕНЕДЖЕР ПРОЕКТА,FASHIONGREATNESS.COM'),(119,'main','t_3_2','',0,0,0,0,'en','Project Manager, Fashion Greatness.com'),(120,'main','t_3_3','',0,0,0,0,'ru','Будь собой, люби то, что делаешь, уважай свой талант и будь успешным!'),(121,'main','t_3_3','',0,0,0,0,'en','Be yourself, love what you do, respect your talent and be successful!'),(122,'main','t_3_4','',0,0,0,0,'ru','Anna.May@fashiongreatness.com'),(123,'main','t_3_4','',0,0,0,0,'en','Anna.May@fashiongreatness.com'),(124,'main','p_1','',0,0,0,0,'ru','ЦЕНОВЫЕ ПАКЕТЫ'),(125,'main','p_1','',0,0,0,0,'en','PRICE PAKAGES'),(126,'main','p_2','',0,0,0,0,'ru','КАЖДЫЙ ЦЕНОВОЙ ПАКЕТ ПРЕДОСТАВЛЯЕТ РАЗНЫЕ ВОЗМОЖНОСТИ. ВЫБЕРИТЕ ТОТ, ЧТО ПОДХОДИТ ИМЕННО ВАМ.'),(127,'main','p_2','',0,0,0,0,'en','DIFFERENT PACKAGES WILL GIVE YOU DIFFERENT OPPORTUNITIES. CHOOSE YOUR ONE.'),(128,'main','p_3','',0,0,0,0,'ru','АККАУНТ'),(129,'main','p_3','',0,0,0,0,'en','ACCOUNT'),(130,'main','p_4','',0,0,0,0,'ru','КЛАССИЧЕСКИЙ'),(131,'main','p_4','',0,0,0,0,'en','CLASSIC'),(132,'main','p_5','',0,0,0,0,'ru','ВИП'),(133,'main','p_5','',0,0,0,0,'en','VIP'),(134,'main','p_6','',0,0,0,0,'ru','КОРПОРАТИВНЫЙ'),(135,'main','p_6','',0,0,0,0,'en','CORPORATE'),(136,'main','p_7','',0,0,0,0,'ru','месяц'),(137,'main','p_7','',0,0,0,0,'en','month'),(150,'main','pr_2_1','',0,0,0,0,'ru','ДО 4 ПРЕСС-РЕЛИЗЫ в год<br>\r\nДо 4-х очерков В ГОД<br>\r\n3 участников событий различного формата В ГОД<br>\r\n20 ИЗДАНИЯ ВАКАНСИЙ<br>\r\nНеограниченные предложения о сотрудничестве<br>\r\nНЕОГРАНИЧЕННАЯ ОТВЕТ НА ВАКАНСИЙ<br>\r\nПОЛНАЯ ИНФОРМАЦИЯ В ПРОФИЛЬ<br>\r\nПОЛНЫЙ возможности настройки<br>'),(151,'main','pr_2_1','',0,0,0,0,'en','UP TO 4  PRESS RELEASES PER YEAR<br>\r\nUP TO 4 FEATURE ARTICLES PER YEAR<br>\r\n3 PARTICIPATIONS IN THE EVENTS OF <br>\r\nDIFFERENT FORMAT PER YEAR<br>\r\n20 PUBLICATIONS OF VACANCIES<br>\r\nUNLIMITED COLLABORATION PROPOSALS<br>\r\nUNLIMITED ANSWER FOR VACANCIES<br>\r\nFULL INFORMATION IN PROFILE<br>\r\nFULL CUSTOMIZATION POSSIBILITIES<br>'),(152,'main','pr_3_1','',0,0,0,0,'ru','ДО 4 ПРЕСС-РЕЛИЗЫ в год<br>\r\nДо 4-х очерков В ГОД<br>\r\n3 участников событий различного формата В ГОД<br>\r\n20 ИЗДАНИЯ ВАКАНСИЙ<br>\r\nНеограниченные предложения о сотрудничестве<br>\r\nНЕОГРАНИЧЕННАЯ ОТВЕТ НА ВАКАНСИЙ<br>\r\nПОЛНАЯ ИНФОРМАЦИЯ В ПРОФИЛЬ<br>\r\nПОЛНЫЙ возможности настройки<br>\r\n20 ДОПОЛНИТЕЛЬНЫЕ CLASSIC ПРОФИЛИ для сотрудников Вашей компании<br>'),(153,'main','pr_3_1','',0,0,0,0,'en','UP TO 4  PRESS RELEASES PER YEAR<br>\r\nUP TO 4 FEATURE ARTICLES PER YEAR<br>\r\n3 PARTICIPATIONS IN THE EVENTS OF DIFFERENT FORMAT PER YEAR<br>\r\n20 PUBLICATIONS OF VACANCIES<br>\r\nUNLIMITED COLLABORATION PROPOSALS<br>\r\nUNLIMITED ANSWER FOR VACANCIES<br>\r\nFULL INFORMATION IN PROFILE<br>\r\nFULL CUSTOMIZATION POSSIBILITIES<br>\r\n20 ADDITIONAL CLASSIC PROFILES FOR YOUR EMPLOYEES<br>'),(154,'main','pr_1_1','',0,0,0,0,'ru','3 ИЗДАНИЯ ВАКАНСИЙ<br>\r\n20 СОВМЕСТНАЯ ПРЕДЛОЖЕНИЯ<br>\r\nНЕОГРАНИЧЕННАЯ ОТВЕТ НА ВАКАНСИЙ<br>\r\nОГРАНИЧЕНОЙ в профайле<br>\r\nLIMITED ИЗГОТОВЛЕНИЕ<br>'),(155,'main','pr_1_1','',0,0,0,0,'en','3 PUBLICATIONS OF VACANCIES<br>\r\n20 COLLABORATION PROPOSALS<br>\r\nUNLIMITED ANSWER FOR VACANCIES<br>\r\nLIMITED INFORMATION IN PROFILE<br>\r\nLIMITED CUSTOMIZATION<br>'),(156,'main','reg_1','',0,0,0,0,'ru','РЕГИСТРАЦИЯ'),(157,'main','reg_1','',0,0,0,0,'en','REGISTRATION'),(158,'main','reg_2','',0,0,0,0,'ru','\r\nВЫБЕРИТЕ Стоимость пакета'),(159,'main','reg_2','',0,0,0,0,'en','CHOOSE YOUR PRICE PACKAGE'),(160,'main','reg_3','',0,0,0,0,'ru','ПОСЕТИТЕЛЬ'),(161,'main','reg_3','',0,0,0,0,'en','VISITOR'),(162,'main','reg_4','',0,0,0,0,'ru','ЯЗЫК'),(163,'main','reg_4','',0,0,0,0,'en','LANGUAGE'),(164,'main','reg_5','',0,0,0,0,'ru','СТРАНА'),(165,'main','reg_5','',0,0,0,0,'en','LOCATION'),(166,'main','reg_6','',0,0,0,0,'ru','ИМЯ'),(167,'main','reg_6','',0,0,0,0,'en','NAME'),(168,'main','reg_7','',0,0,0,0,'ru','ФАМИЛИЯ'),(169,'main','reg_7','',0,0,0,0,'en','LAST NAME'),(170,'main','reg_8','',0,0,0,0,'ru','E-MAIL'),(171,'main','reg_8','',0,0,0,0,'en','E-MAIL'),(172,'main','reg_9','',0,0,0,0,'ru','НОМЕР ТЕЛЕФОНА'),(173,'main','reg_9','',0,0,0,0,'en','PHONE NUMBER'),(174,'main','reg_10','',0,0,0,0,'ru','РЕГИСТРАЦИЯ'),(175,'main','reg_10','',0,0,0,0,'en','REGISTER'),(176,'main','reg_11','',0,0,0,0,'ru','ПАРОЛЬ'),(177,'main','reg_11','',0,0,0,0,'en','PASSWORD'),(178,'main','reg_12','',0,0,0,0,'ru','ЗАПОМНИТЬ МЕНЯ'),(179,'main','reg_12','',0,0,0,0,'en','REMEMBER ME'),(180,'main','reg_13','',0,0,0,0,'ru','ЗАБЫЛИ ПАРОЛЬ'),(181,'main','reg_13','',0,0,0,0,'en','FORGOT YOUR PASSWORD'),(182,'main','reg_14','',0,0,0,0,'ru','ВОЙТИ'),(183,'main','reg_14','',0,0,0,0,'en','SIGN IN'),(184,'main','reg_15','',0,0,0,0,'ru','АВТОРИЗОВАТЬСЯ'),(185,'main','reg_15','',0,0,0,0,'en','LOG IN'),(186,'main','reg_16','',0,0,0,0,'ru','ЗАБЫЛИ ПАРОЛЬ'),(187,'main','reg_16','',0,0,0,0,'en','FORGOT PASSWORD'),(188,'main','reg_17','',0,0,0,0,'ru','ЕСЛИ ВЫ ЗАБЫЛИ ПАРОЛЬ, ВВЕДИТЕ E-MAIL. ВАША ИНФОРМАЦИЯ БУДЕТ <br> ОПРАВЛЕНА ВАМ ПО ЭЛЕКТРОННОЙ ПОЧТЕ'),(189,'main','reg_17','',0,0,0,0,'en','IF YOU FORGOT PASSWORD, ENTER E-MAIL. YOUR ACCOUNT INFO WILL BE <br> SENT TO YOU BY E_MAIL'),(190,'main','reg_18','',0,0,0,0,'ru','ОТПРАВИТЬ'),(191,'main','reg_18','',0,0,0,0,'en','SEND'),(192,'main','reg_19','',0,0,0,0,'ru','ТЕМА'),(193,'main','reg_19','',0,0,0,0,'en','SUBJECT'),(194,'main','reg_20','',0,0,0,0,'ru','СООБЩЕНИЕ'),(195,'main','reg_20','',0,0,0,0,'en','MESSAGE'),(196,'main','reg_21','',0,0,0,0,'ru','отправить'),(197,'main','reg_21','',0,0,0,0,'en','SEND'),(198,'all','slogan1','',0,0,0,0,'ru','эксклюзивная презентация в сети'),(199,'all','slogan1','',0,0,0,0,'en','exclusive professional network'),(200,'visitor','m100','',0,0,0,0,'ru','Настройки'),(201,'visitor','m100','',0,0,0,0,'en','Setting'),(202,'visitor','m2v','',0,0,0,0,'ru','Поиск'),(203,'visitor','m2v','',0,0,0,0,'en','Search'),(204,'visitor','m3v','',0,0,0,0,'ru','Журнал'),(205,'visitor','m3v','',0,0,0,0,'en','Journal'),(218,'main','reg_22','',0,0,0,0,'ru','КОЛИЧЕСТВО СПЕЦИАЛЬНОСТЕЙ'),(219,'main','reg_22','',0,0,0,0,'en','NUMBER OF PROFESSIONS'),(220,'main','reg_23','',0,0,0,0,'ru','ВАША ПРОФЕССИЯ'),(221,'main','reg_23','',0,0,0,0,'en','YOUR PROFESSION'),(222,'main','reg_24','',0,0,0,0,'ru','Назовите нескольких самых успешных событий / проекты с вашим участием (для каждого ПРОФЕССИИ) И Даешь ИСТОЧНИКОВ, ссылки, где мы можем прочитать о них'),(223,'main','reg_24','',0,0,0,0,'en','NAME SEVERAL MOST SUCCESSFUL EVENTS/PROJECTS WITH YOUR PARTICPATIONS (FOR EACH OF PROFESSION) AND GIVE US THE SOURCES, LINKS, WHERE WE CAN READ ABOUT THEM'),(224,'main','t_4_1','',0,0,0,0,'ru','Новый'),(225,'main','t_4_1','',0,0,0,0,'en','New'),(226,'main','t_4_2','',0,0,0,0,'ru','Новый член команды'),(227,'main','t_4_2','',0,0,0,0,'en','New command member'),(228,'main','t_4_3','',0,0,0,0,'ru','Делаю'),(229,'main','t_4_3','',0,0,0,0,'en','Doing'),(230,'main','t_4_4','',0,0,0,0,'ru','mail@mail.ru'),(231,'main','t_4_4','',0,0,0,0,'en','mail@mail.ru'),(240,'main','t_4_5','',0,0,0,1,'ru','t_f4.jpg'),(241,'main','t_4_5','',0,0,0,1,'en','t_f4.jpg'),(244,'main','t_3_5','',0,0,0,1,'ru','t_f3.jpg'),(245,'main','t_3_5','',0,0,0,1,'en','t_f3.jpg'),(246,'main','t_2_5','',0,0,0,1,'ru','t_f2.jpg'),(247,'main','t_2_5','',0,0,0,1,'en','t_f2.jpg'),(248,'main','t_1_5','',0,0,0,1,'ru','t_f1.jpg'),(249,'main','t_1_5','',0,0,0,1,'en','t_f1.jpg'),(262,'main','reg','',0,0,0,0,'fr',''),(263,'main','reg','',0,0,0,0,'ch',''),(264,'main','1','',0,0,0,0,'fr','undefined'),(265,'main','1','',0,0,0,0,'ch','undefined'),(266,'visitor','m2v','',0,0,0,0,'fr','sdsdsdsd'),(267,'visitor','m2v','',0,0,0,0,'ch','qwqwqw'),(268,'visitor','m3v','',0,0,0,0,'fr','sdsdsdsd'),(269,'visitor','m3v','',0,0,0,0,'ch','qwqwqw'),(270,'all','slogan1','',0,0,0,0,'fr','sdsdsdsd'),(271,'all','slogan1','',0,0,0,0,'ch','qwqwqw'),(273,'general','name','',0,0,100,0,'ru','Имя'),(274,'general','name','',0,0,100,0,'en','First Name'),(275,'general','name','',0,0,100,0,'fr','Prénom'),(276,'general','name','',0,0,100,0,'ch','名字'),(277,'general','last_name','',0,0,200,0,'ru','Фамилия'),(278,'general','last_name','',0,0,200,0,'en','Last Name'),(279,'general','last_name','',0,0,200,0,'fr','Nom de famille'),(280,'general','last_name','',0,0,200,0,'ch','姓'),(285,'general','phone','',0,0,300,0,'ru','Номер телефона'),(286,'general','phone','',0,0,300,0,'en','Phone number'),(287,'general','phone','',0,0,300,0,'fr','Numéro de téléphone'),(288,'general','phone','',0,0,300,0,'ch','電話號碼'),(289,'general','email','',0,0,400,0,'ru','Эл. адрес'),(290,'general','email','',0,0,400,0,'en','Email'),(291,'general','email','',0,0,400,0,'fr','Email'),(292,'general','email','',0,0,400,0,'ch','電子郵件'),(293,'general','skype','',0,0,500,0,'ru','Skype'),(294,'general','skype','',0,0,500,0,'en','Skype'),(295,'general','skype','',0,0,500,0,'fr','Skype'),(296,'general','skype','',0,0,500,0,'ch','Skype的'),(297,'general','agency','',0,0,600,0,'ru','Информация об Агентстве'),(298,'general','agency','',0,0,600,0,'en','Agency info'),(299,'general','agency','',0,0,600,0,'fr','information Agency'),(300,'general','agency','',0,0,600,0,'ch','代理信息'),(301,'general','website','',0,0,700,0,'ru','Веб-сайт'),(302,'general','website','',0,0,700,0,'en','Website'),(303,'general','website','',0,0,700,0,'fr','Site Internet'),(304,'general','website','',0,0,700,0,'ch','網站'),(305,'general','gender','',0,0,800,0,'ru','Пол'),(306,'general','gender','',0,0,800,0,'en','Gender'),(307,'general','gender','',0,0,800,0,'fr','Le genre'),(308,'general','gender','',0,0,800,0,'ch','性別'),(309,'general','guardians','',0,0,1100,0,'ru','У вас есть согласие вашего законного опекуна (требуется для моделей младше 18 лет)'),(310,'general','guardians','',0,0,1100,0,'en','Do you have your Legal Guardian’s agree (required for models under 18)'),(311,'general','guardians','',0,0,1100,0,'fr','Avez-vous de votre tuteur légal d\'accord (obligatoire pour les modèles de moins de 18)'),(312,'general','guardians','',0,0,1100,0,'ch','你有你的法定監護人的同意（要求18歲以下的機型）'),(313,'general','general','',0,0,0,0,'ru','ОБЩЕЕ'),(314,'general','general','',0,0,0,0,'en','GENERAL'),(315,'general','general','',0,0,0,0,'fr','GÉNÉRAL'),(316,'general','general','',0,0,0,0,'ch','一般'),(317,'all','edit','',0,0,0,0,'ru','Редактировать'),(318,'all','edit','',0,0,0,0,'en','Edit'),(319,'all','edit','',0,0,0,0,'fr','Modifier'),(320,'all','edit','',0,0,0,0,'ch','编辑'),(321,'all','update','',0,0,0,0,'ru','Обновить'),(322,'all','update','',0,0,0,0,'en','Update'),(323,'all','update','',0,0,0,0,'fr','Mettre à jour'),(324,'all','update','',0,0,0,0,'ch','更新'),(325,'general','locationall','',0,0,1200,0,'ru','Место нахождения'),(326,'general','locationall','',0,0,1200,0,'en','Location'),(327,'general','locationall','',0,0,1200,0,'fr','Emplacement'),(328,'general','locationall','',0,0,1200,0,'ch','位置'),(329,'physical','general_ph','',0,0,0,0,'ru','ФИЗИЧЕСКИЕ ДАННЫЕ'),(330,'physical','general_ph','',0,0,0,0,'en','PHYSICAL DETAILS'),(331,'physical','general_ph','',0,0,0,0,'fr','Caractéristiques physiques'),(332,'physical','general_ph','',0,0,0,0,'ch','物理细节'),(337,'physical','height','',0,0,100,0,'ru','Рост'),(338,'physical','height','',0,0,100,0,'en','Height'),(339,'physical','height','',0,0,100,0,'fr','La taille'),(340,'physical','height','',0,0,100,0,'ch','高度'),(341,'physical','weight','',0,0,200,0,'ru','Вес'),(342,'physical','weight','',0,0,200,0,'en','Weight'),(343,'physical','weight','',0,0,200,0,'fr','Poids'),(344,'physical','weight','',0,0,200,0,'ch','重量'),(345,'physical','chest','',0,0,300,0,'ru','Грудь'),(346,'physical','chest','',0,0,300,0,'en','Chest'),(347,'physical','chest','',0,0,300,0,'fr','Poitrine'),(348,'physical','chest','',0,0,300,0,'ch','胸部'),(349,'physical','hips','',0,0,500,0,'ru','Бедра'),(350,'physical','hips','',0,0,500,0,'en','Hips'),(351,'physical','hips','',0,0,500,0,'fr','Les hanches'),(352,'physical','hips','',0,0,500,0,'ch','臀部'),(353,'physical','waist','',0,0,400,0,'ru','Талия'),(354,'physical','waist','',0,0,400,0,'en','Waist'),(355,'physical','waist','',0,0,400,0,'fr','Taille'),(356,'physical','waist','',0,0,400,0,'ch','腰部'),(357,'physical','dress','',0,0,600,0,'ru','Платье / куртка'),(358,'physical','dress','',0,0,600,0,'en','Dress/Jacket'),(359,'physical','dress','',0,0,600,0,'fr','Robe / Veste'),(360,'physical','dress','',0,0,600,0,'ch','连衣裙/外套'),(361,'physical','cup','',0,0,700,0,'ru','Кружка'),(362,'physical','cup','',0,0,700,0,'en','Cup'),(363,'physical','cup','',0,0,700,0,'fr','Coupe'),(364,'physical','cup','',0,0,700,0,'ch','杯子'),(365,'physical','shoe','',0,0,800,0,'ru','Обувь'),(366,'physical','shoe','',0,0,800,0,'en','Shoe'),(367,'physical','shoe','',0,0,800,0,'fr','Chaussure'),(368,'physical','shoe','',0,0,800,0,'ch','鞋'),(369,'physical','ethnicity','',0,0,900,0,'ru','Этнос'),(370,'physical','ethnicity','',0,0,900,0,'en','Ethnicity'),(371,'physical','ethnicity','',0,0,900,0,'fr','Origine ethnique'),(372,'physical','ethnicity','',0,0,900,0,'ch','种族'),(373,'physical','skin_color','',0,0,1000,0,'ru','Цвет кожи'),(374,'physical','skin_color','',0,0,1000,0,'en','Skin color'),(375,'physical','skin_color','',0,0,1000,0,'fr','Couleur de peau'),(376,'physical','skin_color','',0,0,1000,0,'ch','肤色'),(377,'physical','eye_color','',0,0,1100,0,'ru','Цвет глаз'),(378,'physical','eye_color','',0,0,1100,0,'en','Eye color'),(379,'physical','eye_color','',0,0,1100,0,'fr','Couleur des yeux'),(380,'physical','eye_color','',0,0,1100,0,'ch','眼睛的颜色'),(381,'physical','natural_hair_color','',0,0,1200,0,'ru','Натуральный цвет волос'),(382,'physical','natural_hair_color','',0,0,1200,0,'en','Natural hair color'),(383,'physical','natural_hair_color','',0,0,1200,0,'fr','La couleur des cheveux naturels'),(384,'physical','natural_hair_color','',0,0,1200,0,'ch','自然发色'),(385,'physical','hair_coloration','',0,0,1300,0,'ru','Цвет окрашенных волос'),(386,'physical','hair_coloration','',0,0,1300,0,'en','Hair coloration'),(387,'physical','hair_coloration','',0,0,1300,0,'fr','Coloration des cheveux'),(388,'physical','hair_coloration','',0,0,1300,0,'ch','染发'),(389,'physical','readiness_to_change_color','',0,0,1400,0,'ru','Готовность изменить цвет'),(390,'physical','readiness_to_change_color','',0,0,1400,0,'en','Readiness to change color'),(391,'physical','readiness_to_change_color','',0,0,1400,0,'fr','Prêt à changer de couleur'),(392,'physical','readiness_to_change_color','',0,0,1400,0,'ch','准备改变颜色'),(393,'physical','hair_length','',0,0,1500,0,'ru','Длина волос'),(394,'physical','hair_length','',0,0,1500,0,'en','Hair length'),(395,'physical','hair_length','',0,0,1500,0,'fr','Longueur des cheveux'),(396,'physical','hair_length','',0,0,1500,0,'ch','头发长度'),(397,'physical','tattoos','',0,0,1600,0,'ru','Татуировки'),(398,'physical','tattoos','',0,0,1600,0,'en','Tattoos'),(399,'physical','tattoos','',0,0,1600,0,'fr','Tatouages'),(400,'physical','tattoos','',0,0,1600,0,'ch','纹身'),(401,'physical','piercing','',0,0,1700,0,'ru','Пронизывающий'),(402,'physical','piercing','',0,0,1700,0,'en','Piercing'),(403,'physical','piercing','',0,0,1700,0,'fr','Perçant'),(404,'physical','piercing','',0,0,1700,0,'ch','冲孔'),(405,'professional','general_pr','',0,0,0,0,'ru','Профессиональные детали'),(406,'professional','general_pr','',0,0,0,0,'en','Professional detais'),(407,'professional','general_pr','',0,0,0,0,'fr','Détails professionnels'),(408,'professional','general_pr','',0,0,0,0,'ch','专业细节'),(409,'professional','genre','',0,0,0,0,'ru','Моделирование категории / Жанр'),(410,'professional','genre','',0,0,0,0,'en','Modeling categories/Genre'),(411,'professional','genre','',0,0,0,0,'fr','Modélisation de catégories / Genre'),(412,'professional','genre','',0,0,0,0,'ch','造型类/音乐风格'),(413,'professional','experience','',0,0,0,0,'ru','Профессиональный опыт'),(414,'professional','experience','',0,0,0,0,'en','Professional Experience'),(415,'professional','experience','',0,0,0,0,'fr','Expérience professionnelle'),(416,'professional','experience','',0,0,0,0,'ch','专业经验'),(417,'professional','additional_skills','',0,0,0,0,'ru','Дополнительные навыки'),(418,'professional','additional_skills','',0,0,0,0,'en','Additional Skills'),(419,'professional','additional_skills','',0,0,0,0,'fr','Des compétences supplementaires'),(420,'professional','additional_skills','',0,0,0,0,'ch','附加技能'),(421,'professional','languages','',0,0,0,0,'ru','Языки'),(422,'professional','languages','',0,0,0,0,'en','Languages'),(423,'professional','languages','',0,0,0,0,'fr','Langues'),(424,'professional','languages','',0,0,0,0,'ch','语言'),(425,'professional','compensation','',0,0,0,0,'ru','Компенсация'),(426,'professional','compensation','',0,0,0,0,'en','Compensation'),(427,'professional','compensation','',0,0,0,0,'fr','Compensation'),(428,'professional','compensation','',0,0,0,0,'ch','赔偿金'),(429,'professional','rate_per_hour','',0,0,0,0,'ru','Стоимость за час'),(430,'professional','rate_per_hour','',0,0,0,0,'en','Rate per hour'),(431,'professional','rate_per_hour','',0,0,0,0,'fr','Tarif par heure'),(432,'professional','rate_per_hour','',0,0,0,0,'ch','速度在每小时'),(433,'professional','rate_half_day','',0,0,0,0,'ru','Оценить полдня / 4 часа'),(434,'professional','rate_half_day','',0,0,0,0,'en','Rate half day/4 hours'),(435,'professional','rate_half_day','',0,0,0,0,'fr','Taux de demi-journée / 4 heures'),(436,'professional','rate_half_day','',0,0,0,0,'ch','率半天/4小时'),(437,'professional','rate_full_day','',0,0,0,0,'ru','Оценить полный день / 8 часов'),(438,'professional','rate_full_day','',0,0,0,0,'en','Rate full day/8 hours'),(439,'professional','rate_full_day','',0,0,0,0,'fr','Tarif journée complète / 8 heures'),(440,'professional','rate_full_day','',0,0,0,0,'ch','率整天/8小时'),(441,'professional','rates_for_unway','',0,0,0,0,'ru','Цены на взлетно-посадочной полосы'),(442,'professional','rates_for_unway','',0,0,0,0,'en','Rates for Runway'),(443,'professional','rates_for_unway','',0,0,0,0,'fr','Tarifs pour la piste'),(444,'professional','rates_for_unway','',0,0,0,0,'ch','价格的跑道'),(445,'professional','contracted','',0,0,0,0,'ru','Контрактная или нет'),(446,'professional','contracted','',0,0,0,0,'en','Contracted or not'),(447,'professional','contracted','',0,0,0,0,'fr','Contracté ou non'),(448,'professional','contracted','',0,0,0,0,'ch','签约与否'),(449,'professional','travel_ability','',0,0,0,0,'ru','Возможность путешествия'),(450,'professional','travel_ability','',0,0,0,0,'en','Travel Ability'),(451,'professional','travel_ability','',0,0,0,0,'fr','Capacité Voyage'),(452,'professional','travel_ability','',0,0,0,0,'ch','旅游能力'),(453,'professional','reasons','',0,0,0,0,'ru','Основные причины, чтобы быть на сайте'),(454,'professional','reasons','',0,0,0,0,'en','Main reasons to be on site'),(455,'professional','reasons','',0,0,0,0,'fr','Principales raisons d\'être sur place'),(456,'professional','reasons','',0,0,0,0,'ch','主要的原因是在现场'),(457,'main','sel_1#1','',0,0,0,0,'ru','Общие вопросы'),(458,'main','sel_1#1','',0,0,0,0,'en','General questions'),(459,'main','sel_1#1','',0,0,0,0,'fr','1f'),(460,'main','sel_1#1','',0,0,0,0,'ch','1c'),(461,'main','sel_1#2','',0,0,0,0,'ru','Техническая поддержка'),(462,'main','sel_1#2','',0,0,0,0,'en','Technical support'),(463,'main','sel_1#2','',0,0,0,0,'fr','2'),(464,'main','sel_1#2','',0,0,0,0,'ch','2'),(465,'main','sel_1#3','',0,0,0,0,'ru','Партнерство / Сотрудничество'),(466,'main','sel_1#3','',0,0,0,0,'en','Partnership/Cooperation'),(467,'main','sel_1#3','',0,0,0,0,'fr','3'),(468,'main','sel_1#3','',0,0,0,0,'ch','3'),(469,'main','sel_1#4','',0,0,0,0,'ru','Мероприятия'),(470,'main','sel_1#4','',0,0,0,0,'en','Events'),(471,'main','sel_1#4','',0,0,0,0,'fr','4'),(472,'main','sel_1#4','',0,0,0,0,'ch','4'),(473,'main','sel_1#5','',0,0,0,0,'ru','Претензии и предложения'),(474,'main','sel_1#5','',0,0,0,0,'en','Claims and proposais'),(475,'main','sel_1#5','',0,0,0,0,'fr','5'),(476,'main','sel_1#5','',0,0,0,0,'ch','5'),(477,'main','sel_1#6','',0,0,0,0,'ru','Пресс-службы'),(478,'main','sel_1#6','',0,0,0,0,'en','Press Services'),(479,'main','sel_1#6','',0,0,0,0,'fr','6'),(480,'main','sel_1#6','',0,0,0,0,'ch','6'),(481,'main','sel_1#7','',0,0,0,0,'ru','Другие'),(482,'main','sel_1#7','',0,0,0,0,'en','Other'),(483,'main','sel_1#7','',0,0,0,0,'fr','7'),(484,'main','sel_1#7','',0,0,0,0,'ch','7'),(493,'select','sel_country#1','',0,0,0,0,'ru','Китай'),(494,'select','sel_country#1','',0,0,0,0,'en','China'),(495,'select','sel_country#1','',0,0,0,0,'fr','China ак'),(496,'select','sel_country#1','',0,0,0,0,'ch','China ср'),(497,'select','sel_country#2','',0,0,0,0,'ru','Англия'),(498,'select','sel_country#2','',0,0,0,0,'en','England'),(499,'select','sel_country#2','',0,0,0,0,'fr','England fr'),(500,'select','sel_country#2','',0,0,0,0,'ch','England ch'),(501,'select','sel_country#3','',0,0,0,0,'ru','Франция'),(502,'select','sel_country#3','',0,0,0,0,'en','France'),(503,'select','sel_country#3','',0,0,0,0,'fr','France fr'),(504,'select','sel_country#3','',0,0,0,0,'ch','France ch'),(505,'select','sel_country#4','',0,0,0,0,'ru','Германия'),(506,'select','sel_country#4','',0,0,0,0,'en','Germani'),(507,'select','sel_country#4','',0,0,0,0,'fr','Germani fr'),(508,'select','sel_country#4','',0,0,0,0,'ch','Germani ch'),(509,'select','sel_country#5','',0,0,0,0,'ru','Италия'),(510,'select','sel_country#5','',0,0,0,0,'en','Italy'),(511,'select','sel_country#5','',0,0,0,0,'fr','Italy fr'),(512,'select','sel_country#5','',0,0,0,0,'ch','Italy ch'),(513,'select','sel_country#6','',0,0,0,0,'ru','США'),(514,'select','sel_country#6','',0,0,0,0,'en','USA'),(515,'select','sel_country#6','',0,0,0,0,'fr','USA fr'),(516,'select','sel_country#6','',0,0,0,0,'ch','USA ch'),(517,'select','sel_country#7','',0,0,0,0,'ru','Украина'),(518,'select','sel_country#7','',0,0,0,0,'en','Ukraine'),(519,'select','sel_country#7','',0,0,0,0,'fr','Ukraine fr'),(520,'select','sel_country#7','',0,0,0,0,'ch','Ukraine ch'),(521,'select','sel_country#8','',0,0,0,0,'ru','Россия'),(522,'select','sel_country#8','',0,0,0,0,'en','Russia'),(523,'select','sel_country#8','',0,0,0,0,'fr','Russia fr'),(524,'select','sel_country#8','',0,0,0,0,'ch','Russia ch'),(525,'select','sel_country#9','',1,0,0,0,'ru','Прочие'),(526,'select','sel_country#9','',1,0,0,0,'en','Other'),(527,'select','sel_country#9','',1,0,0,0,'fr','Other fr'),(528,'select','sel_country#9','',1,0,0,0,'ch','Other ch'),(533,'select','sel_city#1','sel_country#1',0,0,0,0,'ru','Пекин'),(534,'select','sel_city#1','sel_country#1',0,0,0,0,'en','Beijing'),(535,'select','sel_city#1','sel_country#1',0,0,0,0,'fr','Beijing fr'),(536,'select','sel_city#1','sel_country#1',0,0,0,0,'ch','Beijing ch'),(537,'select','sel_city#2','sel_country#1',0,0,0,0,'ru','Гонк Конг'),(538,'select','sel_city#2','sel_country#1',0,0,0,0,'en','Hong Kong'),(539,'select','sel_city#2','sel_country#1',0,0,0,0,'fr','Hong Kong fr'),(540,'select','sel_city#2','sel_country#1',0,0,0,0,'ch','Hong Kong ch'),(541,'select','sel_city#3','sel_country#1',0,0,0,0,'ru','Шанхай'),(542,'select','sel_city#3','sel_country#1',0,0,0,0,'en','Shanghai'),(543,'select','sel_city#3','sel_country#1',0,0,0,0,'fr','Shanghai fr'),(544,'select','sel_city#3','sel_country#1',0,0,0,0,'ch','Shanghai ch'),(545,'select','sel_city#4','sel_country#1',1,0,0,0,'ru','Прочие'),(546,'select','sel_city#4','sel_country#1',1,0,0,0,'en','Other'),(547,'select','sel_city#4','sel_country#1',1,0,0,0,'fr','Other fr'),(548,'select','sel_city#4','sel_country#1',1,0,0,0,'ch','Other ch'),(549,'select','sel_city#101','sel_country#2',0,0,0,0,'ru','Лондон'),(550,'select','sel_city#101','sel_country#2',0,0,0,0,'en','London'),(551,'select','sel_city#101','sel_country#2',0,0,0,0,'fr','London fr'),(552,'select','sel_city#101','sel_country#2',0,0,0,0,'ch','London ch'),(553,'select','sel_city#102','sel_country#2',0,0,0,0,'ru','Пригороды Лондона'),(554,'select','sel_city#102','sel_country#2',0,0,0,0,'en','London area'),(555,'select','sel_city#102','sel_country#2',0,0,0,0,'fr','London area fr'),(556,'select','sel_city#102','sel_country#2',0,0,0,0,'ch','London area ch'),(561,'all','setting','',0,0,0,0,'ru','Настройки'),(562,'all','setting','',0,0,0,0,'en','Setting'),(563,'all','setting','',0,0,0,0,'fr','Setting fr'),(564,'all','setting','',0,0,0,0,'ch','Setting ch'),(565,'all','logout','',0,0,0,0,'ru','Выход'),(566,'all','logout','',0,0,0,0,'en','Log out'),(567,'all','logout','',0,0,0,0,'fr','Log out fr'),(568,'all','logout','',0,0,0,0,'ch','Log out ch'),(569,'all','changebackground','',0,0,0,0,'ru','Сменить фон'),(570,'all','changebackground','',0,0,0,0,'en','Change background'),(571,'all','changebackground','',0,0,0,0,'fr','change background fr'),(572,'all','changebackground','',0,0,0,0,'ch','change background ch'),(573,'all','ratingp','',0,0,0,0,'ru','Рейтинг в %'),(574,'all','ratingp','',0,0,0,0,'en','Rating in %'),(575,'all','ratingp','',0,0,0,0,'fr','Rating in %'),(576,'all','ratingp','',0,0,0,0,'ch','Rating in %'),(577,'all','ratingn','',0,0,0,0,'ru','Позиция в рейтинге'),(578,'all','ratingn','',0,0,0,0,'en','Position in rating'),(579,'all','ratingn','',0,0,0,0,'fr','Position in rating'),(580,'all','ratingn','',0,0,0,0,'ch','Position in rating'),(581,'all','account_type','',0,0,0,0,'ru','Тип аккаунта'),(582,'all','account_type','',0,0,0,0,'en','Account type'),(583,'all','account_type','',0,0,0,0,'fr','Account type'),(584,'all','account_type','',0,0,0,0,'ch','Account type'),(585,'all','remove','',0,0,0,0,'ru','Удалить'),(586,'all','remove','',0,0,0,0,'en','Remove'),(587,'all','remove','',0,0,0,0,'fr','Remove'),(588,'all','remove','',0,0,0,0,'ch','Remove'),(589,'all','calendar','',0,0,0,0,'ru','Календарь'),(590,'all','calendar','',0,0,0,0,'en','Calendar'),(591,'all','calendar','',0,0,0,0,'fr','Calendar'),(592,'all','calendar','',0,0,0,0,'ch','Calendar'),(593,'all','journal','',0,0,0,0,'ru','Журнал'),(594,'all','journal','',0,0,0,0,'en','Journal\r\n'),(595,'all','journal','',0,0,0,0,'fr','Journal\r\n'),(596,'all','journal','',0,0,0,0,'ch','Journal\r\n'),(597,'all','event','',0,0,0,0,'ru','События'),(598,'all','event','',0,0,0,0,'en','Event'),(599,'all','event','',0,0,0,0,'fr','Event'),(600,'all','event','',0,0,0,0,'ch','Event'),(601,'all','search','',0,0,0,0,'ru','Поиск'),(602,'all','search','',0,0,0,0,'en','Search'),(603,'all','search','',0,0,0,0,'fr','Search'),(604,'all','search','',0,0,0,0,'ch','Search'),(605,'all','people','',0,0,0,0,'ru','Люди'),(606,'all','people','',0,0,0,0,'en','People'),(607,'all','people','',0,0,0,0,'fr','People'),(608,'all','people','',0,0,0,0,'ch','People'),(609,'all','job','',0,0,0,0,'ru','Работа'),(610,'all','job','',0,0,0,0,'en','Job'),(611,'all','job','',0,0,0,0,'fr','Job'),(612,'all','job','',0,0,0,0,'ch','Job'),(613,'all','subscription','',0,0,0,0,'ru','Подписки'),(614,'all','subscription','',0,0,0,0,'en','Subscription'),(615,'all','subscription','',0,0,0,0,'fr','Subscription'),(616,'all','subscription','',0,0,0,0,'ch','Subscription'),(617,'all','recommendations','',0,0,0,0,'ru','Рекомендации'),(618,'all','recommendations','',0,0,0,0,'en','Recommendations'),(619,'all','recommendations','',0,0,0,0,'fr','Recommendations'),(620,'all','recommendations','',0,0,0,0,'ch','Recommendations'),(621,'all','vacancies','',0,0,0,0,'ru','Вакансии'),(622,'all','vacancies','',0,0,0,0,'en','Vacancies'),(623,'all','vacancies','',0,0,0,0,'fr','Vacancies'),(624,'all','vacancies','',0,0,0,0,'ch','Vacancies'),(625,'select','sel_prof#1','',0,0,0,0,'ru','Модель'),(626,'select','sel_prof#1','',0,0,0,0,'en','Model'),(627,'select','sel_prof#1','',0,0,0,0,'fr','Model fr'),(628,'select','sel_prof#1','',0,0,0,0,'ch','Model ch'),(629,'select','sel_prof#2','',0,0,0,0,'ru','Фотограф'),(630,'select','sel_prof#2','',0,0,0,0,'en','Photographer'),(631,'select','sel_prof#2','',0,0,0,0,'fr','Photographer fr'),(632,'select','sel_prof#2','',0,0,0,0,'ch','Photographer ch'),(633,'select','sel_prof#3','',0,0,0,0,'ru','Дизайнер'),(634,'select','sel_prof#3','',0,0,0,0,'en','Designer'),(635,'select','sel_prof#3','',0,0,0,0,'fr','Designer fr'),(636,'select','sel_prof#3','',0,0,0,0,'ch','Designer ch'),(637,'select','sel_prof#4','',0,0,0,0,'ru','Агентство'),(638,'select','sel_prof#4','',0,0,0,0,'en','Agency'),(639,'select','sel_prof#4','',0,0,0,0,'fr','Agency fr'),(640,'select','sel_prof#4','',0,0,0,0,'ch','Agency ch'),(641,'main','a_2_6','',0,0,0,0,'ru','о нас'),(642,'main','a_2_6','',0,0,0,0,'en','about us'),(643,'main','a_2_6','',0,0,0,0,'fr',''),(644,'main','a_2_6','',0,0,0,0,'ch',''),(645,'main','a_2_7','',0,0,0,0,'ru','Наши услуги'),(646,'main','a_2_7','',0,0,0,0,'en','our services'),(647,'main','a_2_7','',0,0,0,0,'fr',''),(648,'main','a_2_7','',0,0,0,0,'ch',''),(649,'main','a_2_8','',0,0,0,0,'ru','наши наставники'),(650,'main','a_2_8','',0,0,0,0,'en','our mentors'),(651,'main','a_2_8','',0,0,0,0,'fr',''),(652,'main','a_2_8','',0,0,0,0,'ch',''),(653,'main','a_2_9','',0,0,0,0,'ru','вопросы-ответы'),(654,'main','a_2_9','',0,0,0,0,'en','faq'),(655,'main','a_2_9','',0,0,0,0,'fr',''),(656,'main','a_2_9','',0,0,0,0,'ch',''),(657,'main','a_2_10','',0,0,0,0,'ru','контакты'),(658,'main','a_2_10','',0,0,0,0,'en','contacts'),(659,'main','a_2_10','',0,0,0,0,'fr',''),(660,'main','a_2_10','',0,0,0,0,'ch',''),(661,'main','a_3_6','',0,0,0,0,'ru','У нас есть'),(662,'main','a_3_6','',0,0,0,0,'en','We have'),(663,'main','a_3_6','',0,0,0,0,'fr',''),(664,'main','a_3_6','',0,0,0,0,'ch',''),(665,'general','sel_gender#1','',0,1,801,0,'ru','Мужской'),(666,'general','sel_gender#1','',0,1,801,0,'en','Male'),(667,'general','sel_gender#1','',0,1,801,0,'fr','Male fr'),(668,'general','sel_gender#1','',0,1,801,0,'ch','Male ch'),(669,'general','sel_gender#2','',0,2,802,0,'ru','Женский'),(670,'general','sel_gender#2','',0,2,802,0,'en','Female'),(671,'general','sel_gender#2','',0,2,802,0,'fr','Female fr'),(672,'general','sel_gender#2','',0,2,802,0,'ch','Female ch'),(673,'general','date_of_birth','',0,0,900,0,'ru','Дата рождения'),(674,'general','date_of_birth','',0,0,900,0,'en','Date of Birth'),(675,'general','date_of_birth','',0,0,900,0,'fr','Date of Birth fr'),(676,'general','date_of_birth','',0,0,900,0,'ch','Date of Birth ch'),(677,'general','sel_guardians#1','',0,0,1105,0,'ru','Да'),(678,'general','sel_guardians#1','',0,0,1105,0,'en','Yes'),(679,'general','sel_guardians#1','',0,0,1105,0,'fr','Y fr'),(680,'general','sel_guardians#1','',0,0,1105,0,'ch','Y ch'),(681,'general','sel_guardians#2','',0,0,1110,0,'ru','Нет'),(682,'general','sel_guardians#2','',0,0,1110,0,'en','No'),(683,'general','sel_guardians#2','',0,0,1110,0,'fr','No fr'),(684,'general','sel_guardians#2','',0,0,1110,0,'ch','No ch'),(685,'select','sel_ethnicity#1','',0,0,100,0,'ru','Африка'),(686,'select','sel_ethnicity#1','',0,0,100,0,'en','African'),(687,'select','sel_ethnicity#1','',0,0,100,0,'fr','African fr'),(688,'select','sel_ethnicity#1','',0,0,100,0,'ch','African ch'),(689,'select','sel_ethnicity#2','',0,0,200,0,'ru','Азия'),(690,'select','sel_ethnicity#2','',0,0,200,0,'en','Asian'),(691,'select','sel_ethnicity#2','',0,0,200,0,'fr','Asian fr'),(692,'select','sel_ethnicity#2','',0,0,200,0,'ch','Asian ch'),(693,'select','sel_ethnicity#3','',0,0,300,0,'ru','Европа'),(694,'select','sel_ethnicity#3','',0,0,300,0,'en','European'),(695,'select','sel_ethnicity#3','',0,0,300,0,'fr','European fr'),(696,'select','sel_ethnicity#3','',0,0,300,0,'ch','European ch'),(697,'select','sel_ethnicity#4','',0,0,400,0,'ru','Испания'),(698,'select','sel_ethnicity#4','',0,0,400,0,'en','Hispanic'),(699,'select','sel_ethnicity#4','',0,0,400,0,'fr','Hispanic fr'),(700,'select','sel_ethnicity#4','',0,0,400,0,'ch','Hispanic ch'),(701,'select','sel_ethnicity#5','',0,0,500,0,'ru','Индия'),(702,'select','sel_ethnicity#5','',0,0,500,0,'en','Indian'),(703,'select','sel_ethnicity#5','',0,0,500,0,'fr','Indian fr'),(704,'select','sel_ethnicity#5','',0,0,500,0,'ch','Indian ch'),(705,'select','sel_ethnicity#6','',0,0,600,0,'ru','Латинская Америка'),(706,'select','sel_ethnicity#6','',0,0,600,0,'en','Latin American'),(707,'select','sel_ethnicity#6','',0,0,600,0,'fr','Latin American fr'),(708,'select','sel_ethnicity#6','',0,0,600,0,'ch','Latin American ch'),(709,'select','sel_ethnicity#7','',0,0,700,0,'ru','Средиземное море'),(710,'select','sel_ethnicity#7','',0,0,700,0,'en','Mediterranean'),(711,'select','sel_ethnicity#7','',0,0,700,0,'fr','Mediterranean fr'),(712,'select','sel_ethnicity#7','',0,0,700,0,'ch','Mediterranean ch'),(713,'select','sel_ethnicity#8','',0,0,800,0,'ru','Ближний Восток'),(714,'select','sel_ethnicity#8','',0,0,800,0,'en','Near East'),(715,'select','sel_ethnicity#8','',0,0,800,0,'fr','Near East fr'),(716,'select','sel_ethnicity#8','',0,0,800,0,'ch','Near East ch'),(717,'select','sel_ethnicity#9','',0,0,900,0,'ru','Скандинавия'),(718,'select','sel_ethnicity#9','',0,0,900,0,'en','Scandinavian'),(719,'select','sel_ethnicity#9','',0,0,900,0,'fr','Scandinavian fr'),(720,'select','sel_ethnicity#9','',0,0,900,0,'ch','Scandinavian ch'),(721,'select','sel_ethnicity#a','',0,0,1000,0,'ru','Смешанный'),(722,'select','sel_ethnicity#a','',0,0,1000,0,'en','Mixed'),(723,'select','sel_ethnicity#a','',0,0,1000,0,'fr','Mixed fr'),(724,'select','sel_ethnicity#a','',0,0,1000,0,'ch','Mixed ch'),(725,'select','sel_ethnicity#b','',1,0,1100,0,'ru','Прочее'),(726,'select','sel_ethnicity#b','',1,0,1100,0,'en','Other'),(727,'select','sel_ethnicity#b','',1,0,1100,0,'fr','Other fr'),(728,'select','sel_ethnicity#b','',1,0,1100,0,'ch','Other ch'),(729,'select','sel_city#103','sel_country#2',1,0,0,0,'ru','Прочие'),(730,'select','sel_city#103','sel_country#2',1,0,0,0,'en','Other'),(731,'select','sel_city#103','sel_country#2',1,0,0,0,'fr','Other fr'),(732,'select','sel_city#103','sel_country#2',1,0,0,0,'ch','Other ch'),(733,'select','sel_city#301','sel_country#3',0,0,0,0,'ru','Париж'),(734,'select','sel_city#301','sel_country#3',0,0,0,0,'en','Paris'),(735,'select','sel_city#301','sel_country#3',0,0,0,0,'fr','Paris fr'),(736,'select','sel_city#301','sel_country#3',0,0,0,0,'ch','Paris ch'),(737,'select','sel_city#302','sel_country#3',0,0,0,0,'ru','Пригород Парижа'),(738,'select','sel_city#302','sel_country#3',0,0,0,0,'en','Paris area'),(739,'select','sel_city#302','sel_country#3',0,0,0,0,'fr','Paris area fr'),(740,'select','sel_city#302','sel_country#3',0,0,0,0,'ch','Paris area ch'),(741,'select','sel_city#303','sel_country#3',1,0,0,0,'ru','Прочее'),(742,'select','sel_city#303','sel_country#3',1,0,0,0,'en','Other'),(743,'select','sel_city#303','sel_country#3',1,0,0,0,'fr','Other fr'),(744,'select','sel_city#303','sel_country#3',1,0,0,0,'ch','Other ch'),(745,'select','sel_city#401','sel_country#4',0,0,0,0,'ru','Берлин'),(746,'select','sel_city#401','sel_country#4',0,0,0,0,'en','Berlin'),(747,'select','sel_city#401','sel_country#4',0,0,0,0,'fr','Berlin fr'),(748,'select','sel_city#401','sel_country#4',0,0,0,0,'ch','Berlin fr'),(749,'select','sel_city#402','sel_country#4',0,0,0,0,'ru','Пригород Берлина'),(750,'select','sel_city#402','sel_country#4',0,0,0,0,'en','Berlin area'),(751,'select','sel_city#402','sel_country#4',0,0,0,0,'fr','Berlin area fr'),(752,'select','sel_city#402','sel_country#4',0,0,0,0,'ch','Berlin area ch'),(753,'select','sel_city#403','sel_country#4',1,0,0,0,'ru','Прочее'),(754,'select','sel_city#403','sel_country#4',1,0,0,0,'en','Other'),(755,'select','sel_city#403','sel_country#4',1,0,0,0,'fr','Other fr'),(756,'select','sel_city#403','sel_country#4',1,0,0,0,'ch','Other ch'),(757,'select','sel_city#501','sel_country#5',0,0,0,0,'ru','Милан'),(758,'select','sel_city#501','sel_country#5',0,0,0,0,'en','Milan'),(759,'select','sel_city#501','sel_country#5',0,0,0,0,'fr','Milan fr'),(760,'select','sel_city#501','sel_country#5',0,0,0,0,'ch','Milan ch'),(761,'select','sel_city#502','sel_country#5',0,0,0,0,'ru','Пригород Милана'),(762,'select','sel_city#502','sel_country#5',0,0,0,0,'en','Milan area'),(763,'select','sel_city#502','sel_country#5',0,0,0,0,'fr','Milan area fr'),(764,'select','sel_city#502','sel_country#5',0,0,0,0,'ch','Milan area ch'),(765,'select','sel_city#503','sel_country#5',0,0,0,0,'ru','Рим'),(766,'select','sel_city#503','sel_country#5',0,0,0,0,'en','Rome'),(767,'select','sel_city#503','sel_country#5',0,0,0,0,'fr','Rome fr'),(768,'select','sel_city#503','sel_country#5',0,0,0,0,'ch','Rome ch'),(769,'select','sel_city#504','sel_country#5',0,0,0,0,'ru','Пригород Рима'),(770,'select','sel_city#504','sel_country#5',0,0,0,0,'en','Rome area'),(771,'select','sel_city#504','sel_country#5',0,0,0,0,'fr','Rome area fr'),(772,'select','sel_city#504','sel_country#5',0,0,0,0,'ch','Rome area ch'),(773,'select','sel_city#505','sel_country#5',1,0,0,0,'ru','Прочее'),(774,'select','sel_city#505','sel_country#5',1,0,0,0,'en','Other'),(775,'select','sel_city#505','sel_country#5',1,0,0,0,'fr','Other fr'),(776,'select','sel_city#505','sel_country#5',1,0,0,0,'ch','Other ch'),(777,'select','sel_city#601','sel_country#6',0,0,0,0,'ru','Лос-Анджелес'),(778,'select','sel_city#601','sel_country#6',0,0,0,0,'en','Los Angeles'),(779,'select','sel_city#601','sel_country#6',0,0,0,0,'fr','Los Angeles fr'),(780,'select','sel_city#601','sel_country#6',0,0,0,0,'ch','Los Angeles ch'),(781,'select','sel_city#602','sel_country#6',0,0,0,0,'ru','Пригород Лос-Анджелеса'),(782,'select','sel_city#602','sel_country#6',0,0,0,0,'en','Los Angeles area'),(783,'select','sel_city#602','sel_country#6',0,0,0,0,'fr','Los Angeles area fr'),(784,'select','sel_city#602','sel_country#6',0,0,0,0,'ch','Los Angeles area ch'),(785,'select','sel_city#603','sel_country#6',0,0,0,0,'ru','Нью-Йорк'),(786,'select','sel_city#603','sel_country#6',0,0,0,0,'en','New York'),(787,'select','sel_city#603','sel_country#6',0,0,0,0,'fr','New York fr'),(788,'select','sel_city#603','sel_country#6',0,0,0,0,'ch','New York ch'),(789,'select','sel_city#604','sel_country#6',0,0,0,0,'ru','Пригород Нью-Йорка'),(790,'select','sel_city#604','sel_country#6',0,0,0,0,'en','New York area'),(791,'select','sel_city#604','sel_country#6',0,0,0,0,'fr','New York area fr'),(792,'select','sel_city#604','sel_country#6',0,0,0,0,'ch','New York area ch'),(793,'select','sel_city#605','sel_country#6',1,0,0,0,'ru','Прочее'),(794,'select','sel_city#605','sel_country#6',1,0,0,0,'en','Other'),(795,'select','sel_city#605','sel_country#6',1,0,0,0,'fr','Other fr'),(796,'select','sel_city#605','sel_country#6',1,0,0,0,'ch','Other ch'),(797,'select','sel_skin_color#1','',0,0,0,0,'ru','Бледно-белый'),(798,'select','sel_skin_color#1','',0,0,0,0,'en','Pale white'),(799,'select','sel_skin_color#1','',0,0,0,0,'fr','Pale white fr'),(800,'select','sel_skin_color#1','',0,0,0,0,'ch','Pale white ch'),(801,'select','sel_skin_color#2','',0,0,0,0,'ru','Белый'),(802,'select','sel_skin_color#2','',0,0,0,0,'en','White'),(803,'select','sel_skin_color#2','',0,0,0,0,'fr','White fr'),(804,'select','sel_skin_color#2','',0,0,0,0,'ch','White ch'),(805,'select','sel_skin_color#3','',0,0,0,0,'ru','Оливковый'),(806,'select','sel_skin_color#3','',0,0,0,0,'en','Olive'),(807,'select','sel_skin_color#3','',0,0,0,0,'fr','Olive fr'),(808,'select','sel_skin_color#3','',0,0,0,0,'ch','Olive ch'),(809,'select','sel_skin_color#4','',0,0,0,0,'ru','Светло-коричневый'),(810,'select','sel_skin_color#4','',0,0,0,0,'en','Light brown'),(811,'select','sel_skin_color#4','',0,0,0,0,'fr','Light brown fr'),(812,'select','sel_skin_color#4','',0,0,0,0,'ch','Light brown ch'),(813,'select','sel_skin_color#5','',0,0,0,0,'ru','Коричневый'),(814,'select','sel_skin_color#5','',0,0,0,0,'en','Brown'),(815,'select','sel_skin_color#5','',0,0,0,0,'fr','Brown fr'),(816,'select','sel_skin_color#5','',0,0,0,0,'ch','Brown ch'),(817,'select','sel_skin_color#6','',0,0,0,0,'ru','Темно коричневый'),(818,'select','sel_skin_color#6','',0,0,0,0,'en','Dark brown'),(819,'select','sel_skin_color#6','',0,0,0,0,'fr','Dark brown fr'),(820,'select','sel_skin_color#6','',0,0,0,0,'ch','Dark brown ch'),(821,'select','sel_skin_color#7','',0,0,0,0,'ru','Черный'),(822,'select','sel_skin_color#7','',0,0,0,0,'en','Black'),(823,'select','sel_skin_color#7','',0,0,0,0,'fr','Black fr'),(824,'select','sel_skin_color#7','',0,0,0,0,'ch','Black ch'),(825,'select','sel_eye_color#1','',0,0,0,0,'ru','Черный'),(826,'select','sel_eye_color#1','',0,0,0,0,'en','Black'),(827,'select','sel_eye_color#1','',0,0,0,0,'fr','Black fr'),(828,'select','sel_eye_color#1','',0,0,0,0,'ch','Black ch'),(829,'select','sel_eye_color#2','',0,0,0,0,'ru','Синий'),(830,'select','sel_eye_color#2','',0,0,0,0,'en','Blue'),(831,'select','sel_eye_color#2','',0,0,0,0,'fr','Blue fr'),(832,'select','sel_eye_color#2','',0,0,0,0,'ch','Blue ch'),(833,'select','sel_eye_color#3','',0,0,0,0,'ru','Зеленый'),(834,'select','sel_eye_color#3','',0,0,0,0,'en','Green'),(835,'select','sel_eye_color#3','',0,0,0,0,'fr','Green fr'),(836,'select','sel_eye_color#3','',0,0,0,0,'ch','Green ch'),(837,'select','sel_eye_color#4','',0,0,0,0,'ru','Орешник'),(838,'select','sel_eye_color#4','',0,0,0,0,'en','Hazel'),(839,'select','sel_eye_color#4','',0,0,0,0,'fr','Hazel fr'),(840,'select','sel_eye_color#4','',0,0,0,0,'ch','Hazel ch'),(841,'select','sel_eye_color#5','',1,0,0,0,'ru','Прочее'),(842,'select','sel_eye_color#5','',1,0,0,0,'en','Other'),(843,'select','sel_eye_color#5','',1,0,0,0,'fr','Other fr'),(844,'select','sel_eye_color#5','',1,0,0,0,'ch','Other ch'),(845,'select','sel_natural_hair_color#1','',0,0,0,0,'ru','Блондинка'),(846,'select','sel_natural_hair_color#1','',0,0,0,0,'en','Blonde'),(847,'select','sel_natural_hair_color#1','',0,0,0,0,'fr','Blonde fr'),(848,'select','sel_natural_hair_color#1','',0,0,0,0,'ch','Blonde ch'),(849,'select','sel_natural_hair_color#2','',0,0,0,0,'ru','Клубничная блондинка'),(850,'select','sel_natural_hair_color#2','',0,0,0,0,'en','Strawberry Blonde'),(851,'select','sel_natural_hair_color#2','',0,0,0,0,'fr','Strawberry Blonde fr'),(852,'select','sel_natural_hair_color#2','',0,0,0,0,'ch','Strawberry Blonde ch'),(853,'select','sel_natural_hair_color#3','',0,0,0,0,'ru','Рыжеватый'),(854,'select','sel_natural_hair_color#3','',0,0,0,0,'en','Auburn'),(855,'select','sel_natural_hair_color#3','',0,0,0,0,'fr','Auburn fr'),(856,'select','sel_natural_hair_color#3','',0,0,0,0,'ch','Auburn ch'),(857,'select','sel_natural_hair_color#4','',0,0,0,0,'ru','Имбирь'),(858,'select','sel_natural_hair_color#4','',0,0,0,0,'en','Ginger'),(859,'select','sel_natural_hair_color#4','',0,0,0,0,'fr','Ginger fr'),(860,'select','sel_natural_hair_color#4','',0,0,0,0,'ch','Ginger ch'),(861,'select','sel_natural_hair_color#5','',0,0,0,0,'ru','Светло-коричневый'),(862,'select','sel_natural_hair_color#5','',0,0,0,0,'en','Light Brown'),(863,'select','sel_natural_hair_color#5','',0,0,0,0,'fr','Light Brown fr'),(864,'select','sel_natural_hair_color#5','',0,0,0,0,'ch','Light Brown ch'),(865,'select','sel_natural_hair_color#6','',0,0,0,0,'ru','Коричневый'),(866,'select','sel_natural_hair_color#6','',0,0,0,0,'en','Brown'),(867,'select','sel_natural_hair_color#6','',0,0,0,0,'fr','Brown fr'),(868,'select','sel_natural_hair_color#6','',0,0,0,0,'ch','Brown ch'),(869,'select','sel_natural_hair_color#7','',0,0,0,0,'ru','Темно коричневый'),(870,'select','sel_natural_hair_color#7','',0,0,0,0,'en','Dark Brown'),(871,'select','sel_natural_hair_color#7','',0,0,0,0,'fr','Dark Brown fr'),(872,'select','sel_natural_hair_color#7','',0,0,0,0,'ch','Dark Brown ch'),(873,'select','sel_natural_hair_color#8','',0,0,0,0,'ru','Соль и перец'),(874,'select','sel_natural_hair_color#8','',0,0,0,0,'en','Salt & Pepper'),(875,'select','sel_natural_hair_color#8','',0,0,0,0,'fr','Salt & Pepper fr'),(876,'select','sel_natural_hair_color#8','',0,0,0,0,'ch','Salt & Pepper ch'),(877,'select','sel_natural_hair_color#9','',0,0,0,0,'ru','Черный'),(878,'select','sel_natural_hair_color#9','',0,0,0,0,'en','Black'),(879,'select','sel_natural_hair_color#9','',0,0,0,0,'fr','Black fr'),(880,'select','sel_natural_hair_color#9','',0,0,0,0,'ch','Black ch'),(881,'select','sel_natural_hair_color#a','',1,0,0,0,'ru','Прочее'),(882,'select','sel_natural_hair_color#a','',1,0,0,0,'en','Other'),(883,'select','sel_natural_hair_color#a','',1,0,0,0,'fr','Other fr'),(884,'select','sel_natural_hair_color#a','',1,0,0,0,'ch','Other ch'),(885,'select','sel_hair_length#1','',0,0,0,0,'ru','Не окрашенный'),(886,'select','sel_hair_length#1','',0,0,0,0,'en','Not colored'),(887,'select','sel_hair_length#1','',0,0,0,0,'fr','Not colored fr'),(888,'select','sel_hair_length#1','',0,0,0,0,'ch','Not colored ch'),(889,'select','sel_hair_length#2','',0,0,0,0,'ru','Окрашенный'),(890,'select','sel_hair_length#2','',0,0,0,0,'en','Colored'),(891,'select','sel_hair_length#2','',0,0,0,0,'fr','Colored fr'),(892,'select','sel_hair_length#2','',0,0,0,0,'ch','Colored ch'),(893,'select','sel_hair_length#210','sel_hair_length#2',0,0,0,0,'ru','Блондинка'),(894,'select','sel_hair_length#210','sel_hair_length#2',0,0,0,0,'en','Blonde'),(895,'select','sel_hair_length#210','sel_hair_length#2',0,0,0,0,'fr','Blonde fr'),(896,'select','sel_hair_length#210','sel_hair_length#2',0,0,0,0,'ch','Blonde fr'),(897,'select','sel_hair_length#220','sel_hair_length#2',0,0,0,0,'ru','Клубничная блондинка'),(898,'select','sel_hair_length#220','sel_hair_length#2',0,0,0,0,'en','Strawberry Blonde'),(899,'select','sel_hair_length#220','sel_hair_length#2',0,0,0,0,'fr','Strawberry Blonde fr'),(900,'select','sel_hair_length#220','sel_hair_length#2',0,0,0,0,'ch','Strawberry Blonde ch'),(901,'select','sel_hair_length#230','sel_hair_length#2',0,0,0,0,'ru','Рыжеватый'),(902,'select','sel_hair_length#230','sel_hair_length#2',0,0,0,0,'en','Auburn'),(903,'select','sel_hair_length#230','sel_hair_length#2',0,0,0,0,'fr','Auburn fr'),(904,'select','sel_hair_length#230','sel_hair_length#2',0,0,0,0,'ch','Auburn ch'),(905,'select','sel_hair_length#240','sel_hair_length#2',0,0,0,0,'ru','Имбирь'),(906,'select','sel_hair_length#240','sel_hair_length#2',0,0,0,0,'en','Ginger'),(907,'select','sel_hair_length#240','sel_hair_length#2',0,0,0,0,'fr','Ginger fr'),(908,'select','sel_hair_length#240','sel_hair_length#2',0,0,0,0,'ch','Ginger ch'),(909,'select','sel_hair_length#250','sel_hair_length#2',0,0,0,0,'ru','Красный'),(910,'select','sel_hair_length#250','sel_hair_length#2',0,0,0,0,'en','Red'),(911,'select','sel_hair_length#250','sel_hair_length#2',0,0,0,0,'fr','Red fr'),(912,'select','sel_hair_length#250','sel_hair_length#2',0,0,0,0,'ch','Red ch'),(913,'select','sel_hair_length#260','sel_hair_length#2',0,0,0,0,'ru','Светло-коричневый'),(914,'select','sel_hair_length#260','sel_hair_length#2',0,0,0,0,'en','Light Brown'),(915,'select','sel_hair_length#260','sel_hair_length#2',0,0,0,0,'fr','Light Brown fr'),(916,'select','sel_hair_length#260','sel_hair_length#2',0,0,0,0,'ch','Light Brown ch'),(917,'select','sel_hair_length#270','sel_hair_length#2',0,0,0,0,'ru','Коричневый'),(918,'select','sel_hair_length#270','sel_hair_length#2',0,0,0,0,'en','Brown'),(919,'select','sel_hair_length#270','sel_hair_length#2',0,0,0,0,'fr','Brown fr'),(920,'select','sel_hair_length#270','sel_hair_length#2',0,0,0,0,'ch','Brown ch'),(921,'select','sel_hair_length#280','sel_hair_length#2',0,0,0,0,'ru','Темно коричневый'),(922,'select','sel_hair_length#280','sel_hair_length#2',0,0,0,0,'en','Dark Brown'),(923,'select','sel_hair_length#280','sel_hair_length#2',0,0,0,0,'fr','Dark Brown fr'),(924,'select','sel_hair_length#280','sel_hair_length#2',0,0,0,0,'ch','Dark Brown ch'),(925,'select','sel_hair_length#290','sel_hair_length#2',0,0,0,0,'ru','Соль и перец'),(926,'select','sel_hair_length#290','sel_hair_length#2',0,0,0,0,'en','Salt & Pepper'),(927,'select','sel_hair_length#290','sel_hair_length#2',0,0,0,0,'fr','Salt & Pepper fr'),(928,'select','sel_hair_length#290','sel_hair_length#2',0,0,0,0,'ch','Salt & Pepper ch'),(929,'select','sel_hair_length#2a0','sel_hair_length#2',0,0,0,0,'ru','Черный'),(930,'select','sel_hair_length#2a0','sel_hair_length#2',0,0,0,0,'en','Black'),(931,'select','sel_hair_length#2a0','sel_hair_length#2',0,0,0,0,'fr','Black fr'),(932,'select','sel_hair_length#2a0','sel_hair_length#2',0,0,0,0,'ch','Black ch'),(933,'select','sel_hair_length#2b0','sel_hair_length#2',0,0,0,0,'ru','Черный Синий'),(934,'select','sel_hair_length#2b0','sel_hair_length#2',0,0,0,0,'en','Black Blue'),(935,'select','sel_hair_length#2b0','sel_hair_length#2',0,0,0,0,'fr','Black Blue fr'),(936,'select','sel_hair_length#2b0','sel_hair_length#2',0,0,0,0,'ch','Black Blue ch'),(937,'select','sel_hair_length#2c0','sel_hair_length#2',1,0,0,0,'ru','Прочее'),(938,'select','sel_hair_length#2c0','sel_hair_length#2',1,0,0,0,'en','Other'),(939,'select','sel_hair_length#2c0','sel_hair_length#2',1,0,0,0,'fr','Other fr'),(940,'select','sel_hair_length#2c0','sel_hair_length#2',1,0,0,0,'ch','Other ch'),(941,'select','sel_tattoos#1','',0,0,0,0,'ru','Нет татуировок'),(942,'select','sel_tattoos#1','',0,0,0,0,'en','No tattoos'),(943,'select','sel_tattoos#1','',0,0,0,0,'fr','No tattoos fr'),(944,'select','sel_tattoos#1','',0,0,0,0,'ch','No tattoos ch'),(945,'select','sel_piercing#1','',0,0,0,0,'ru','Нет пирсинга'),(946,'select','sel_piercing#1','',0,0,0,0,'en','No piercing'),(947,'select','sel_piercing#1','',0,0,0,0,'fr','No piercing fr'),(948,'select','sel_piercing#1','',0,0,0,0,'ch','No piercing ch'),(949,'select','sel_prof#5','',0,0,0,0,'ru','Эксперт'),(950,'select','sel_prof#5','',0,0,0,0,'en','Expert'),(951,'select','sel_prof#5','',0,0,0,0,'fr','Expert fr'),(952,'select','sel_prof#5','',0,0,0,0,'ch','Expert ch'),(957,'block','block','',0,0,0,0,'ru','Общее'),(958,'block','block','',0,0,0,0,'en','General'),(959,'block','block','',0,0,0,0,'fr','Gener fr'),(960,'block','block','',0,0,0,0,'ch','Gener ch');
/*!40000 ALTER TABLE `site` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_block`
--

DROP TABLE IF EXISTS `site_block`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_block` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(100) NOT NULL,
  `npp` int(11) NOT NULL,
  `model` int(11) NOT NULL,
  `photograph` int(11) NOT NULL,
  `designer` int(11) NOT NULL,
  `agency` int(11) NOT NULL,
  `brand` int(11) NOT NULL,
  `buyer` int(11) NOT NULL,
  `customer` int(11) NOT NULL,
  `company` int(11) NOT NULL,
  `media` int(11) NOT NULL,
  `desinger` int(11) NOT NULL,
  `stylist` int(11) NOT NULL,
  `hair` int(11) NOT NULL,
  `mua` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_block`
--

LOCK TABLES `site_block` WRITE;
/*!40000 ALTER TABLE `site_block` DISABLE KEYS */;
INSERT INTO `site_block` VALUES (1,'general',100,100,1,1,1,0,0,0,0,0,0,0,0,0),(2,'xxx',300,0,0,0,0,0,0,0,0,0,0,0,0,0),(3,'yyy',200,0,0,0,0,0,0,0,0,0,0,0,0,0),(4,'zzz',400,200,0,0,0,0,0,0,0,0,0,0,0,0);
/*!40000 ALTER TABLE `site_block` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_content`
--

DROP TABLE IF EXISTS `site_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site_str` varchar(255) NOT NULL,
  `language` varchar(10) NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_content`
--

LOCK TABLES `site_content` WRITE;
/*!40000 ALTER TABLE `site_content` DISABLE KEYS */;
INSERT INTO `site_content` VALUES (1,'about','ru','О нас ф м м ум йу мйу акмсйуакмсйуамйуа мйу м йуам йуа мйамй умс йуам '),(2,'about','en','About sa dqwx w ev r ve ve c wcxex qwex  xwex x ex wecxw cw cw cw c ecae xcxce xca ecxx w ecw ew');
/*!40000 ALTER TABLE `site_content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_header`
--

DROP TABLE IF EXISTS `site_header`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_header` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site_str` varchar(255) NOT NULL,
  `language` varchar(10) NOT NULL,
  `title` varchar(255) NOT NULL,
  `key` varchar(255) NOT NULL,
  `des` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_header`
--

LOCK TABLES `site_header` WRITE;
/*!40000 ALTER TABLE `site_header` DISABLE KEYS */;
INSERT INTO `site_header` VALUES (1,'index','ru','Мода','key index','des index'),(2,'home','ru','11','111','1111'),(3,'home','en','22','222','2222'),(4,'index','en','Fashing','key en index','des en index');
/*!40000 ALTER TABLE `site_header` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_maket`
--

DROP TABLE IF EXISTS `site_maket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_maket` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cod_block` varchar(100) NOT NULL,
  `cod_site` varchar(100) NOT NULL,
  `npp` int(10) NOT NULL,
  `zag` tinyint(1) NOT NULL,
  `typ_pole` char(2) NOT NULL,
  `uslovie` char(2) NOT NULL,
  `cod_site_uslovie` varchar(255) NOT NULL,
  `znach` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_maket`
--

LOCK TABLES `site_maket` WRITE;
/*!40000 ALTER TABLE `site_maket` DISABLE KEYS */;
INSERT INTO `site_maket` VALUES (1,'zzz','general',0,1,'tx','','',''),(2,'zzz','name',100,0,'in','','',''),(3,'zzz','sel_city',300,0,'se','','',''),(4,'zzz','phone',400,0,'in','=','sel_city','111');
/*!40000 ALTER TABLE `site_maket` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `guardians` varchar(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(60) NOT NULL,
  `skype` varchar(100) NOT NULL,
  `agency` varchar(255) NOT NULL,
  `website` varchar(255) NOT NULL,
  `date_of_birth` date NOT NULL COMMENT 'Дата рождения',
  `kol_prof` int(5) NOT NULL,
  `prof` varchar(100) NOT NULL COMMENT 'Профессия',
  `prof1` varchar(100) NOT NULL,
  `prof2` varchar(100) NOT NULL,
  `prof3` varchar(100) NOT NULL,
  `prof4` varchar(100) NOT NULL,
  `prof5` varchar(100) NOT NULL,
  `summary` text NOT NULL,
  `height` int(10) NOT NULL COMMENT 'рост',
  `weight` int(10) NOT NULL COMMENT 'вес',
  `chest` int(10) NOT NULL COMMENT 'Грудь',
  `waist` int(11) NOT NULL COMMENT 'Талия',
  `hips` int(11) NOT NULL COMMENT 'Бёдра',
  `dress` int(11) NOT NULL COMMENT 'Платье',
  `cup` varchar(10) NOT NULL COMMENT 'Кружка',
  `shoe` int(11) NOT NULL COMMENT 'Обувь',
  `ethnicity` varchar(100) NOT NULL COMMENT 'Этнос',
  `skin_color` varchar(100) NOT NULL COMMENT 'Цвет кожи',
  `eye_color` varchar(100) NOT NULL COMMENT 'Цвет глаз',
  `natural_hair_color` varchar(100) NOT NULL COMMENT 'Натуральный цвет волос',
  `hair_coloration` varchar(100) NOT NULL COMMENT 'Цвет окрашенных волос',
  `readiness_to_change_color` varchar(200) NOT NULL COMMENT 'Готовность изменить цвет',
  `hair_length` int(10) NOT NULL COMMENT 'Длина волос',
  `tattoos` varchar(255) NOT NULL COMMENT 'Татуировки',
  `piercing` varchar(255) NOT NULL COMMENT 'Пронизывающий',
  `genre` varchar(255) NOT NULL,
  `experience` varchar(255) NOT NULL,
  `additional_skills` varchar(255) NOT NULL,
  `languages` varchar(255) NOT NULL,
  `compensation` varchar(255) NOT NULL,
  `rate_per_hour` varchar(255) NOT NULL,
  `rate_half_day` varchar(255) NOT NULL,
  `rate_full_day` varchar(255) NOT NULL,
  `rates_for_unway` varchar(255) NOT NULL,
  `contracted` varchar(255) NOT NULL,
  `travel_ability` varchar(255) NOT NULL,
  `reasons` varchar(255) NOT NULL,
  `language` varchar(10) NOT NULL,
  `location` varchar(50) NOT NULL,
  `locationall` varchar(1024) NOT NULL DEFAULT '',
  `package` varchar(10) NOT NULL,
  `reyting` int(11) NOT NULL COMMENT 'Рейтинг в %',
  `reyting_nom` int(11) NOT NULL COMMENT 'Рейтинг №',
  `avatar` varchar(255) NOT NULL DEFAULT 'no_avatar.png' COMMENT 'Аватар - имя файла',
  `phpsessid` varchar(30) NOT NULL,
  `anketa` int(1) NOT NULL,
  `rol` varchar(20) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (5,'cla','cla','111cla','222cla','','','cla','444 444 44 44','','','','0000-00-00',0,'','','','','','','',0,0,0,0,0,0,'',0,'','','','','','',0,'','','','','','','','','','','','','','','ru','ro','','cla',0,0,'no_avatar.png','',0,'',0),(7,'vip','vip','111vip','222vip','','','vip','444vip','','','','0000-00-00',0,'','','','','','','',0,0,0,0,0,0,'',0,'','','0','','','',0,'','','','','','','','','','','','','','','ru','ro','','vip',0,0,'no_avatar.png','',0,'',0),(8,'cor','cor','111cor','222cor','','','cor','444cor','','','','0000-00-00',0,'','','','','','','',0,0,0,0,0,0,'',0,'','','0','','','',0,'','','','','','','','','','','','','','','ru','ro','','cor',0,0,'no_avatar.png','',0,'',0),(9,'12','','12','12','','','12','12','','','','0000-00-00',0,'','','','','','','',0,0,0,0,0,0,'',0,'','','0','','','',0,'','','','','','','','','','','','','','','ru','ro','','cor',0,0,'no_avatar.png','',0,'',0),(10,'admin','admin','Sergey','','','','123@mail.ru','123 456 78 90','','','','0000-00-00',0,'','','','','','','',0,0,0,0,0,0,'',0,'','','0','','','',0,'','','','','','','','','','','','','','','ru','ro','','cor',0,0,'no_avatar.png','',0,'admin',1),(11,'c1','c1','c1','c1','','','c1','c1','','','','0000-00-00',3,'model','','','','','','model all',0,0,0,0,0,0,'',0,'','','0','','','',0,'','','','','','','','','','','','','','','ru','ro','','otladka',0,0,'no_avatar.png','',0,'',0),(12,'222','','222','222','','','222','222','','','','0000-00-00',2,'wwww','','','','','','wwwww',0,0,0,0,0,0,'',0,'','','0','','','',0,'','','','','','','','','','','','','','','ru','ro','','vip',0,0,'no_avatar.png','',0,'',0),(13,'vis','vis','Сергей','Шамаев','','','vis','+7 902 007 99 88','','','','0000-00-00',0,'','','','','','','',0,0,0,0,0,0,'',0,'','','','','','',0,'','','','','','','','','','','','','','','ru','ro','','vis',0,0,'no_avatar.png','',0,'',0),(14,'visch','visch','chch','chch','','','visch','111','','','','0000-00-00',0,'ВАША ПРОФЕССИЯ*','','','','','','',0,0,0,0,0,0,'',0,'','','0','','','',0,'','','','','','','','','','','','','','','ch','ch','','vis',0,0,'no_avatar.png','',0,'',0),(15,'visen','visen','Smit','Vesson','','','visen','+1 111 222 33 44','','','','0000-00-00',0,'','','','','','','',0,0,0,0,0,0,'',0,'','','0','','','',0,'','','','','','','','','','','','','','','en','en','','vis',0,0,'no_avatar.png','',0,'',0),(16,'visfr','visfr','visfr','visfr','','','visfr','111','','','','0000-00-00',0,'ВАША ПРОФЕССИЯ*','','','','','','',0,0,0,0,0,0,'',0,'','','0','','','',0,'','','','','','','','','','','','','','','fr','ru','','vis',0,0,'no_avatar.png','',0,'',0),(17,'frantchouk.von.manst','vlf9','Arthur','Frantchouk','','','frantchouk.von.manstein@gmail.com','0679154743','','','','0000-00-00',0,'ВАША ПРОФЕССИЯ*','','','','','','',0,0,0,0,0,0,'',0,'','','0','','','',0,'','','','','','','','','','','','','','','ru','uk','','vis',0,0,'no_avatar.png','',0,'',0),(18,'test-class@fashingre','9ibd','dcfvtbynuji','crftvgyhj','','','test-class@fashingreatness.ru','12345678','','','','0000-00-00',3,'Дизайнер','','','','','','скмеин6тг7ш',0,0,0,0,0,0,'',0,'','','','','','',0,'','','','','','','','','','','','','','','en','fr','','cla',0,0,'no_avatar.png','',0,'',0),(19,'чистмпьирб','fpgp','праоплро','враоплрдожл','','','чистмпьирб','апро','','','','0000-00-00',1,'Модель','','','','','','апролд\r\n',0,0,0,0,0,0,'',0,'','','','','','',0,'','','','','','','','','','','','','','','ru','ch','','cor',0,0,'no_avatar.png','',0,'',0),(20,'САМЕПНОГЛ','8759','кумеинтогАПРОГЛШ','АПРН','','','САМЕПНОГЛ','ПРНО','','','','0000-00-00',0,'ВАША ПРОФЕССИЯ*','','','','','','',0,0,0,0,0,0,'',0,'','','','','','',0,'','','','','','','','','','','','','','','ru','ch','','vis',0,0,'no_avatar.png','',0,'',0),(21,'вскамеинтг','i3jb','чскме','ывсакмепнр','','','вскамеинтг','вскамеинтг','','','','0000-00-00',1,'Модель','','','','','','чувскапеингь',0,0,0,0,0,0,'',0,'','','','','','',0,'','','','','','','','','','','','','','','ru','ch','','cla',0,0,'no_avatar.png','',0,'',0),(22,'vis1','vis1','Сергей','Шамаев','','','vis1','111 222 33 44','','','','0000-00-00',0,'','','','','','','',0,0,0,0,0,0,'',0,'','','','','','',0,'','','','','','','','','','','','','','','ru','ru','','vis',0,0,'no_avatar.png','',1,'',0),(23,'vip1','vip1','Паша','Павлов','sel_gender#1','','ser51@rambler.ru','111 222 11 22','','','','0000-00-00',0,'sel_prof#1','','','','','','',0,0,0,0,0,0,'',0,'','','','','','',0,'','','','','','','','','','','','','','','en','ro','','vip',0,0,'my_avatar/23_659123.jpg','',1,'',0),(25,'cla1','cla1','Витя','Викторов','','','','222 222 22 22','','','','0000-00-00',0,'','','','','','','',0,0,0,0,0,0,'',0,'','','','','','',0,'','','','','','','','','','','','','','','ru','ro','','cla',0,0,'no_avatar.png','',1,'',0),(26,'cor1','cor1','Анна','Анина','','','','','','','','0000-00-00',0,'','','','','','','',0,0,0,0,0,0,'',0,'','','','','','',0,'','','','','','','','','','','','','','','ru','ro','','cor',0,0,'no_avatar.png','',1,'',0),(27,'cla2','cla2','sss','ssF','','','cla2','11','','','','0000-00-00',2,'sel_prof#1','','','','','','asasas',0,0,0,0,0,0,'',0,'','','','','','',0,'','','','','','','','','','','','','','','ru','en','','cla',0,0,'no_avatar.png','',0,'',0),(30,'ser51@ser51.ru','111','Сергей','Шамаев','','','ser51@ser51.ru','111 222 33 44','','','','0000-00-00',0,'sel_prof#4','','','','','','',0,0,0,0,0,0,'',0,'','','','','','',0,'','','','','','','','','','','','','','','ru','sel_country#8','','vis',0,0,'no_avatar.png','',1,'',0),(31,'shurik-malish@yandex.ru','9ua6','агентство','классик','','','shurik-malish@yandex.ru','123456789','','','','0000-00-00',0,'sel_prof#4','','','','','','проверка регистрации',0,0,0,0,0,0,'',0,'','','','','','',0,'','','','','','','','','','','','','','','ru','sel_country#3','','cla',0,0,'no_avatar.png','',0,'',0),(32,'winbear@yandex.ru','sk0n','Игорь','Зубков','','','winbear@yandex.ru','+79039382696','','','','0000-00-00',0,'ВАША ПРОФЕССИЯ*','','','','','','',0,0,0,0,0,0,'',0,'','','','','','',0,'','','','','','','','','','','','','','','ru','sel_country#8','','vis',0,0,'no_avatar.png','',0,'',0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_location`
--

DROP TABLE IF EXISTS `users_location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(100) NOT NULL,
  `country` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `other` tinyint(1) NOT NULL,
  `nm_other` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_location`
--

LOCK TABLES `users_location` WRITE;
/*!40000 ALTER TABLE `users_location` DISABLE KEYS */;
INSERT INTO `users_location` VALUES (1,'vip1','sel_country#7','',0,''),(2,'vip1','sel_country#8','',0,''),(3,'vip1','sel_country#1','sel_city#1',1,'');
/*!40000 ALTER TABLE `users_location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_many`
--

DROP TABLE IF EXISTS `users_many`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_many` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(100) NOT NULL,
  `pokaz` varchar(100) NOT NULL,
  `cod` varchar(100) NOT NULL,
  `cod_up` varchar(100) NOT NULL,
  `other` int(10) NOT NULL,
  `nm_other` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Users = 1-многим';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_many`
--

LOCK TABLES `users_many` WRITE;
/*!40000 ALTER TABLE `users_many` DISABLE KEYS */;
INSERT INTO `users_many` VALUES (1,'vip1','','','',0,'');
/*!40000 ALTER TABLE `users_many` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_payments`
--

DROP TABLE IF EXISTS `users_payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(100) NOT NULL,
  `cod` varchar(20) NOT NULL COMMENT 'код операции : Приход / Расход',
  `valuta` varchar(10) NOT NULL COMMENT 'Валюта',
  `payment` int(10) NOT NULL,
  `data_payment` date NOT NULL,
  `txt` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_payments`
--

LOCK TABLES `users_payments` WRITE;
/*!40000 ALTER TABLE `users_payments` DISABLE KEYS */;
INSERT INTO `users_payments` VALUES (1,'6','При','',250,'2016-05-02','');
/*!40000 ALTER TABLE `users_payments` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-07-17  4:12:28
