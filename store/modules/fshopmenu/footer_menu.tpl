<ul>
    <li><a href="/content/pro-magazin-6">{l s='About shop' mod='ffcarvarimenu'}</a></li>
    <li><a href="/content/delivery-1">{l s='Delivery' mod='ffcarvarimenu'}</a></li>
    <li><a href="/content/oplata-7">{l s='Payment' mod='ffcarvarimenu'}</a></li>
    <li><a href="/content/garantiya-8">{l s='Guaranty' mod='ffcarvarimenu'}</a></li>
</ul>
<ul>
    <li><a href="/content/obmin-ta-povernennya-9">{l s='Exchange and return' mod='ffcarvarimenu'}</a></li>
    <li><a href="/stores">{l s='Contacts' mod='ffcarvarimenu'}</a></li>
    <li><a href="/trends">{l s='Trends' mod='ffcarvarimenu'}</a></li>
    {if $logged}
        <li><a href="/carvaricard">{l s='Carvari card' mod='ffcarvarimenu'}</a></li>
    {/if}
</ul>
