<? /* widget - Ввод событий  */ ?>
<div class="cal-modal">
	<div class="c-m-wrap">
		<div class="c-m-form">
			<div class="c-m-close">
				X
			</div>
			<h1>Месяц число год</h1>
			<form>
				<h3>Date</h3>
				<p>From:</p>
				<p>To:</p>
				<input class="date-from" value="from" type="date">
				<input class="date-to" value="to" type="date">
				<h3>Hours</h3>
				<select>
					<option>00</option>
					<option>01</option>
					<option>02</option>
					<option>03</option>
					<option>04</option>
					<option>05</option>
					<option>06</option>
					<option>07</option>
					<option>08</option>
					<option>09</option>
					<option>10</option>
					<option>11</option>
					<option>12</option>
					<option>13</option>
					<option>14</option>
					<option>15</option>
					<option>16</option>
					<option>17</option>
					<option>18</option>
					<option>19</option>
					<option>20</option>
					<option>21</option>
					<option>22</option>
					<option>23</option>
				</select>
				<select>
					<option>00</option>
					<option>01</option>
					<option>02</option>
					<option>03</option>
					<option>04</option>
					<option>05</option>
					<option>06</option>
					<option>07</option>
					<option>08</option>
					<option>09</option>
					<option>10</option>
					<option>11</option>
					<option>12</option>
					<option>13</option>
					<option>14</option>
					<option>15</option>
					<option>16</option>
					<option>17</option>
					<option>18</option>
					<option>19</option>
					<option>20</option>
					<option>21</option>
					<option>22</option>
					<option>23</option>
				</select>
				<input type="text" name="date" class="tcal" value="" />
				<input></input>
				<select style="width: 93%;">
					<option class="loc loc-m">China:</option>
					<option class="loc loc-ch">Beijing</option>
					<option class="loc loc-ch">Hong Gong</option>
					<option class="loc loc-ch">Shanghai</option>
					<option class="loc loc-ch">Other</option>
					<option class="loc loc-m">England:</option>
					<option class="loc loc-en">London</option>
					<option class="loc loc-en">London area</option>
					<option class="loc loc-en">Other</option>
					<option class="loc loc-m">France:</option>
					<option class="loc loc-fr">Paris</option>
					<option class="loc loc-fr">Paris area</option>
					<option class="loc loc-fr">Other</option>
					<option class="loc loc-m">Germany:</option>
					<option class="loc loc-ge">Berlin</option>
					<option class="loc loc-ge">Berlin area</option>
					<option class="loc loc-ge">Other</option>
					<option class="loc loc-m">Italy:</option>
					<option class="loc loc-it">Milan</option>
					<option class="loc loc-it">Milan area</option>
					<option class="loc loc-it">Rome</option>
					<option class="loc loc-it">Rome area</option>
					<option class="loc loc-it">Other</option>
					<option class="loc loc-m">USA:</option>
					<option class="loc loc-usa">Los Angeles</option>
					<option class="loc loc-usa">Los Angeles area</option>
					<option class="loc loc-usa">New York</option>
					<option class="loc loc-usa">New York area</option>
					<option class="loc loc-usa">Other</option>
					<option class="loc loc-m">Ukraine</option>
					<option class="loc loc-m">Russia</option>
					<option class="loc loc-m">Other</option>
				</select>
				<input type="submit"></input>
			</form>
		</div>
	</div>
</div>

<? /*
???? Вакансия - это предложение работы 
------------------------------------------------------
Календарь 
Отображение календаря занятости пользователя.
Возможность добавлять в календарь события с несколькими параметрами
Возможность подтягивать в календарь даты некоторых из событий моих подписок, из вкладки Events и свои вакансии/сделки.
--
создание записи о событии –  нужны следующие параметры : title, кол-во дней, часы/минуты, location, description 
***** ????? Заголовок, Дата_начала+время(час,мин), Дата_окончания+время(час,мин), Перечень стран/городов, описание
и возможность просматривать и редактировать запись по клику ***** ????? на правой части
--
отображение событий в самом календаре справа -  просто одна строка – title и location, ***** ????? отображаются по клику или при наведении мышкой (получение фокуса)
***** ????? если в эту дату несколько событий то справа отражать все события с разделительной чертой и вертикальной прокруткой (например 50 событий)
***** ????? события в этот день разные поэтому в начале надо выводить чьё это событие
просто как пример -  Photoshooting,  New York или Fashion Week, Paris
***** пример
My event (опубликовано) - фотосессия. Россия Москва
My event (черновик) - фотосессия. Россия Питер
---------------------------------------
FG event - фотосессия, Германия Берлин, Китай Пекин, Франция Париж
...
...
...
---------------------------------------
Subscribtions - Фотосессия , Бали
...
...
...
---------------------------------------
***** конец примера

--
функционал календаря
создать согласно макету дизайн галочки/выключатели и возможность выбирать один пункт или несколько.
 
My events - включает в себя события.  заполненные от руки пользователем, сделки,  в которых принимает участие пользователь 
My vacancies - вакансии самого пользователя .
FG events – включает в себя даты всех ивентов, отображаемых в разделе Events – закладке FG events
Subscribtions events  - включает в себя события, публикуемые пользователями, входящими в список Subscribtions.
--
Models ***** ????? нет на рисунке
эта вкладка есть только у корпоративного профиля (модельное агентство). Детально описана ниже, в разделе “Профиль модельного агентства”
--
My vacancies 
список размещенных пользователем вакансий (весь функционал по вакансиям описан ниже в разделе Вакансии)

3 подвкладки
Первая (Published Vacancies) -  список вакансий, опубликованных пользователем.
(в течение первого часа после публикации вакансии – возможность ее редактировать 
(редактировать данные, внесенные в форму вакансии, уточняем, так как в зависимости от вида вакансии формы отличаются))

Вторая (My Drafts)  – список черновиков вакансий пользователя с возможностью редактирования и публикации
(Пояснение: Для классик профилей есть лимит на публикацию вакансий. При публикации можно будет Опубликовать или Сохранить как черновик. 
В счетчике опубликованных вакансий черновики не учитываются)

Третья (Closed Vacancies)– Корзина. Список закрытых вакансий. 
Возможность Republish (c внесением изменений) – в счетчике опубликованных вакансий это будет считаться как новая. 
Возможность удалить 1 вакансию и очистить всю корзину.)
--

Примерная структура

1. event
id, id_user, title, datatime_s, datatime_po, description
2. event_location (1=M)
id, id_event, country, city


*/ ?>