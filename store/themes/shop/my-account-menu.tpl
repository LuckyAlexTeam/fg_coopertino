{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{capture name=path}{l s='My account'}{/capture}

<ul	class="shop_menu">
    <li>
        {if $page_name == 'identity'}
            <span>{l s='My personal information'}</span>
        {else}
            <a href="{$link->getPageLink('identity', true)|escape:'html':'UTF-8'}" title="{l s='Information'}">{l s='My personal information'}</a>
        {/if}
    </li>

    <li>
        {if $page_name == 'addresses' || $page_name == 'address'}
            <span>{l s='My addresses'}</span>
        {else}
            <a href="{$link->getPageLink('addresses', true)|escape:'html':'UTF-8'}" title="{l s='Addresses'}">{l s='My addresses'}</a>
        {/if}
    </li>
    <li>
        {if $page_name == 'history'}
            <span>{l s='Order history and details'}</span>
        {else}
            <a href="{$link->getPageLink('history', true)|escape:'html':'UTF-8'}" title="{l s='Orders'}">{l s='Order history and details'}</a>
        {/if}
    </li>
    {if $returnAllowed}
        <li>
            {if $page_name == 'order-follow'}
                <span>{l s='My merchandise returns'}</span>
            {else}
                <a href="{$link->getPageLink('order-follow', true)|escape:'html':'UTF-8'}" title="{l s='Merchandise returns'}">{l s='My merchandise returns'}</a>
            {/if}
        </li>
    {/if}
    {*<li><a href="{$link->getPageLink('order-slip', true)|escape:'html':'UTF-8'}" title="{l s='Credit slips'}">{l s='My credit slips'}</a></li>*}


    {*{if $voucherAllowed}*}
        {*<li><a href="{$link->getPageLink('discount', true)|escape:'html':'UTF-8'}" title="{l s='Vouchers'}"><span>{l s='My vouchers'}</span></a></li>*}
    {*{/if}*}
    {hook h='displayCustomerAccount'}

</ul>