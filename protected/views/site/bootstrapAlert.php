<?php 
/* type list
 * alert-success - зелененькая
 * alert-warning - оранж
 * alert-danger - красная
 * alert-info - голубая
 */  
?>

<div class="alert alert-dismissible <?php echo $type;?>">
    <button onclick="$(this).parent().remove();" type="button" class="close" data-dismiss="alert">×</button>
    <?php echo $message;?>
</div>

