<?php
class SiteHeader extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    public function tableName() {
        return 'site_header';
    }
	// id site_str language title key des
    public function getAll($lan, $str) {
		$criteria = new CDbCriteria;
		$criteria->condition = "site_str = '{$str}' AND language = '{$lan}' ";
		$list = self::model()->findAll($criteria);
		$arr = Array();
		foreach ($list as $lab) {
			$arr['tit'] = $lab['title'];
			$arr['key'] = $lab['key'];
			$arr['des'] = $lab['des'];
		};
		return $arr;
    }
}