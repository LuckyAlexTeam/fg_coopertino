<style type="text/css">
	.head {
		position: relative;
		display: inline-block;
	}
</style>
<div class="static-wrapper">
	<a href="/" class="link-back">Fashion Greatness</a>  >> AboutFG
	<h2>About Fashion Greatness</h2>
	<span class="hr"></span>
	<nav class="static-nav">
		<ul>
			<li class="current">
				<a href="/about">About FG</a>
			</li>
			<li>
				<a href="/services">Our services</a>
			</li>
			<li>
				<a href="/mentors">Our mentors</a>
			</li>
			<li>
				<a href="/faq">FAQ</a>
			</li>
			<li>
				<a href="/contacts">Contacts</a>
			</li>
		</ul>
	</nav>
</div>

