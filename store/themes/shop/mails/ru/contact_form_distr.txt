
[{shop_url}] 

Message from a {shop_name} customer (Distributor request)

CUSTOMER E-MAIL ADDRESS: {email}

Customer phone: {phone}
Customer name: {firstname} {lastname}

Brief description: {description}
Regions of work: {regions}
Customer clients: {clients}
Interested regions: {regions_interested}
Interested products: {products_interested}

Other suggestions:{suggestions}

ATTACHED FILE: {attached_file} 		 

{shop_name} [{shop_url}] powered by
PrestaShop(tm) [http://www.prestashop.com/] 

