<? /* widget - календарь */ ?>
<? $weekday=array('Пон','Вто','Сре','Чет','Пят','Суб','Вос'); ?>
<? $day_cal = 7*6; ?>
<h2>Calendar</h2>

<span class="hr"></span>
<div class="cal-wrap">
	<div class="c-c">
		<div class="cal-p">
			&lt;
		</div>
		<div class="cal-title">
			<font id="cal-month">Month</font> <font id="cal-year">2016</font>
		</div>
		<div class="cal-n">
			&gt;
		</div>
		<!-- Дни недели -->
		<? for ($i=0; $i<7; ++$i) { ?>
			<div class="c-w-d">
			<?=$weekday[$i]?>
			</div>
		<? }; ?>
		<!-- Дни недели конец -->

		<!-- Числа -->
		<? for ($i=1; $i<=$day_cal; ++$i) { ?>
		<div class="c-d-d" id="cal-day_<?=$i?>">
			-
		</div>
		<? }; ?>
		<!-- Числа конец -->
	</div>
	<div class="c-e">
		<div class="c-e-t">
			<div class="c-e-1">все</div>
			<div class="c-e-2">мои</div>
			<div class="c-e-3">занят</div>
		</div>
		<div class="c-e-b">
			lined events
		</div>
	</div>
</div>


<script>
var cal_year = 2016; // календарь будет только на 2016 год? каждый год переписываться будет?
var cal_tab_month = ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'];
var cal_tab_month_it = 4;
var cal_day_0 = 24;
var cal_mon_0 = 3; //счет от 0
var cal_yea_0 = 2016;
var cal_date = new Date();
cal_date.setFullYear(cal_year, cal_tab_month_it, cal_day_0, 1, 1);
var we_day = 0;
var we_mon = 0;
var we_yea = 0;
$(document).ready(function() {
	// следующий месяц
	$('.cal-n').click( function() {
		++cal_tab_month_it;
		if (cal_tab_month_it > 11) {cal_tab_month_it = 0; ++cal_year;};
		cal_cal();
		//cal_yea_0 = we_yea;
		//cal_mon_0 = we_mon;
		//cal_day_0 = we_day;
		//aaa = ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"][cal_date.getDay()];
		//$('#otl').text(cal_yea_0 +" = "+ cal_mon_0 +" = "+ cal_day_0 + " === "+aaa);
	});
	// предыдущий месяц
	$('.cal-p').click( function() {
		--cal_tab_month_it;
		if (cal_tab_month_it < 1) {cal_tab_month_it = 11; --cal_year;};
		cal_cal();
	});
	
});
//alert(["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"][cal_date.getDay()])

function cal_cal() {
	$('#cal-month').text(cal_tab_month[cal_tab_month_it]);
	$('#cal-year').text(cal_year);
	//
	$(".c-d-d").removeClass("prew-next-m");
	cal_date.setFullYear(cal_yea_0, cal_tab_month_it, 1, 1, 1); // 1-e
	//aaa = ["1вс", "2по", "3вт", "4ср", "5че", "6пя", "7су"][cal_date.getDay()];
	bbb = ["7", "1", "2", "3", "4", "5", "6"][cal_date.getDay()];
	//$('#otl').text(aaa+"="+bbb);
	cal_day_0 = 0;
	j = bbb;
	$(".c-d-d").text('');
	for (i=1; i<=42; ++i) {
		++cal_day_0;
		cal_date.setFullYear(cal_yea_0, cal_tab_month_it, cal_day_0, 1, 1);
		rm = cal_date.getMonth();
		if (rm != cal_tab_month_it) {
			break;
		};
		$('#cal-day_'+j).text(i);
		++j;
	};
	
};
function cal_calzz() {
	$('#cal-month').text(cal_tab_month[cal_tab_month_it]);
	$('#cal-year').text(cal_year);
	//
	$(".c-d-d").removeClass("prew-next-m");
	we = 0;
	we_day = 0;
	we_mon = 0;
	we_yea = 0;
	for (i=1; i<=42; ++i) {
		++cal_day_0;
		cal_date.setFullYear(cal_yea_0, cal_mon_0, cal_day_0, 1, 1);
		day = cal_date.getDate();
		//mmm = cal_date.getMonth();
		rm = cal_date.getMonth();
		$('#cal-day_'+i).text(day); // +"-"+mmm+"-"+rm
		if (rm != cal_tab_month_it) {
			$('#cal-day_'+i).addClass("prew-next-m");
		};
		if (day == 1) {cal_mon_0 = cal_date.getMonth(); cal_day_0 = 1;
		aaa = ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"][cal_date.getDay()];
		$('#otl').text(cal_yea_0 +" = "+ cal_mon_0 +" = "+ cal_day_0 + " === "+aaa);};
		++we;
		if (we == 7) {
			we = 0;
			if (rm == cal_tab_month_it) {
				we_day = cal_date.getDate();
				we_mon = cal_date.getMonth();
				we_yea = cal_date.getFullYear();;
			};
			
		};
	};
	
};
cal_cal();

</script>

<? /*
???? Вакансия - это предложение работы 
------------------------------------------------------
Календарь 
Отображение календаря занятости пользователя.
Возможность добавлять в календарь события с несколькими параметрами
Возможность подтягивать в календарь даты некоторых из событий моих подписок, из вкладки Events и свои вакансии/сделки.
--
создание записи о событии –  нужны следующие параметры : title, кол-во дней, часы/минуты, location, description 
***** ????? Заголовок, Дата_начала+время(час,мин), Дата_окончания+время(час,мин), Перечень стран/городов, описание
и возможность просматривать и редактировать запись по клику ***** ????? на правой части
--
отображение событий в самом календаре справа -  просто одна строка – title и location, ***** ????? отображаются по клику или при наведении мышкой (получение фокуса)
***** ????? если в эту дату несколько событий то справа отражать все события с разделительной чертой и вертикальной прокруткой (например 50 событий)
***** ????? события в этот день разные поэтому в начале надо выводить чьё это событие
просто как пример -  Photoshooting,  New York или Fashion Week, Paris
***** пример
My event (опубликовано) - фотосессия. Россия Москва
My event (черновик) - фотосессия. Россия Питер
---------------------------------------
FG event - фотосессия, Германия Берлин, Китай Пекин, Франция Париж
...
...
...
---------------------------------------
Subscribtions - Фотосессия , Бали
...
...
...
---------------------------------------
***** конец примера

--
функционал календаря
создать согласно макету дизайн галочки/выключатели и возможность выбирать один пункт или несколько.
 
My events - включает в себя события.  заполненные от руки пользователем, сделки,  в которых принимает участие пользователь 
My vacancies - вакансии самого пользователя .
FG events – включает в себя даты всех ивентов, отображаемых в разделе Events – закладке FG events
Subscribtions events  - включает в себя события, публикуемые пользователями, входящими в список Subscribtions.
--
Models ***** ????? нет на рисунке
эта вкладка есть только у корпоративного профиля (модельное агентство). Детально описана ниже, в разделе “Профиль модельного агентства”
--
My vacancies 
список размещенных пользователем вакансий (весь функционал по вакансиям описан ниже в разделе Вакансии)

3 подвкладки
Первая (Published Vacancies) -  список вакансий, опубликованных пользователем.
(в течение первого часа после публикации вакансии – возможность ее редактировать 
(редактировать данные, внесенные в форму вакансии, уточняем, так как в зависимости от вида вакансии формы отличаются))

Вторая (My Drafts)  – список черновиков вакансий пользователя с возможностью редактирования и публикации
(Пояснение: Для классик профилей есть лимит на публикацию вакансий. При публикации можно будет Опубликовать или Сохранить как черновик. 
В счетчике опубликованных вакансий черновики не учитываются)

Третья (Closed Vacancies)– Корзина. Список закрытых вакансий. 
Возможность Republish (c внесением изменений) – в счетчике опубликованных вакансий это будет считаться как новая. 
Возможность удалить 1 вакансию и очистить всю корзину.)
--

Примерная структура

1. event
id, id_user, title, datatime_s, datatime_po, description
2. event_location (1=M)
id, id_event, country, city


*/ ?>