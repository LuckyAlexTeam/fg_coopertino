<?php
class Select {

	public function Sel_vajno($s=0) {
		$r = '<select id="sel_vajno" name="sel_vajno">';
		if ($s == 0) {$r .= '<option value="0" selected>Укажите важность запроса</option>';} else {$r .= '<option value="0">Укажите важность запроса</option>';};
		if ($s == 1) {$r .= '<option value="1" selected>Обычный</option>';} else {$r .= '<option value="1">Обычный</option>';};
		if ($s == 2) {$r .= '<option value="2" selected>Срочный</option>';} else {$r .= '<option value="2">Срочный</option>';};
		$r .= '</select>';
		return $r;
	}
	
	public function Arr_vajno() {
		return Array("0"=>"Укажите важность запроса", "1"=>"Обычный", "2"=>"Срочный");
	}
	
	public function Sel_nds00($s=1, $n='sel_nds00') {
		$r = "<select id='{$n}' name='{$n}'>";
		if ($s == 1) {$r .= "<option value='1' selected>С НДС</option>";   } else {$r .= "<option value='1'>С НДС</option>";   };
		if ($s == 2) {$r .= "<option value='2' selected>Без НДС</option>"; } else {$r .= "<option value='2'>Без НДС</option>"; };
		$r .= "</select>";
		return $r;
	}
	public function Sel_Opt_nds($s=1) {
		$r = "";
		if ($s == 1) {$r .= "<option value='1' selected>С НДС</option>";   } else {$r .= "<option value='1'>С НДС</option>";   };
		if ($s == 2) {$r .= "<option value='2' selected>Без НДС</option>"; } else {$r .= "<option value='2'>Без НДС</option>"; };
		$r .= "</select>";
		return $r;
	}
	public function Arr_nds00() {
		return Array("1"=>"С НДС", "2"=>"Без НДС");
	}
	public function Sel_jf($s=0) {
		$r = '<select id="sel_jf" name="sel_jf">';
		if ($s == 0) {$r .= '<option value="0" selected>Укажите юр/физ лицо</option>';} else {$r .= '<option value="0">Укажите юр/физ лицо</option>';};
		if ($s == 'Ю') {$r .= '<option value="Ю" selected>Юрлицо</option>';} else {$r .= '<option value="Ю">Юрлицо</option>';};
		if ($s == 'Ф') {$r .= '<option value="Ф" selected>Физлицо</option>';} else {$r .= '<option value="Ф">Физлицо</option>';};
		$r .= '</select>';
		return $r;
	}
	public function Sel_yn($s="Y", $n='sel_yn') {
		$r = "<select id='{$n}' name='{$n}'>";
		if ($s == 'Y') {$r .= '<option value="Y" selected>Y</option>';} else {$r .= '<option value="Y">Y</option>';};
		if ($s == 'N') {$r .= '<option value="N" selected>N</option>';} else {$r .= '<option value="N">N</option>';};
		$r .= '</select>';
		return $r;
	}
	public function Sel_rol($s="ecko", $n='sel_rol') {
		$r = "<select id='{$n}' name='{$n}'>";
		if ($s == 'admin') {$r .= '<option value="admin" selected>Админ</option>';} else {$r .= '<option value="admin">Админ</option>';};
		if ($s == 'ecko' ) {$r .= '<option value="ecko"  selected>Экономист</option>';} else {$r .= '<option value="ecko">Экономист</option>';};
		if ($s == 'man'  ) {$r .= '<option value="man"   selected>Менеджер</option>';} else {$r .= '<option value="man">Менеджер</option>';};
		if ($s == 'law'  ) {$r .= '<option value="law"   selected>Юрист</option>';} else {$r .= '<option value="law">Юрист</option>';};
		$r .= '</select>';
		return $r;
	}
	public function Sel_ei_ves($s="кг", $n='sel_ei_ves') {
		$r = "<select id='{$n}' name='{$n}'>";
		if ($s == 'кг') {$r .= '<option value="кг" selected>кг</option>';} else {$r .= '<option value="кг">кг</option>';};
		if ($s == 'т' ) {$r .= '<option value="т"  selected>т</option>';} else {$r .= '<option value="т">т</option>';};
		$r .= '</select>';
		return $r;
	}
	public function Sel_ei_obe($s="м.куб.", $n='sel_ei_obe') {
		$r = "<select id='{$n}' name='{$n}'>";
		if ($s == 'м.куб.' ) {$r .= '<option value="м.куб."  selected>м.куб.</option>'; } else {$r .= '<option value="м.куб." >м.куб.</option>';};
		if ($s == 'см.куб.') {$r .= '<option value="см.куб." selected>см.куб.</option>';} else {$r .= '<option value="см.куб.">см.куб.</option>';};
		$r .= '</select>';
		return $r;
	}
	public function Sel_ei_raz($s="м.", $n='sel_ei_raz') {
		$r = "<select id='{$n}' name='{$n}'>";
		if ($s == 'м.' ) {$r .= '<option value="м."  selected>м.</option>'; } else {$r .= '<option value="м." >м.</option>';};
		if ($s == 'см.') {$r .= '<option value="см." selected>см.</option>';} else {$r .= '<option value="см.">см.</option>';};
		$r .= '</select>';
		return $r;
	}
}
