<?php

Yii::import('application.models._base.BaseModelForm');

class ModelForm extends BaseModelForm
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
}