<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockcontactinfos}shop>blockcontactinfos_fc02586aa99596947b184a01f43fb4ae'] = 'Блок контактной информации ';
$_MODULE['<{blockcontactinfos}shop>blockcontactinfos_86458ae1631be34a6fcbf1a4584f5abe'] = 'Настраиваемый блок, отображающий контактную информацию вашего магазина.';
$_MODULE['<{blockcontactinfos}shop>blockcontactinfos_20015706a8cbd457cbb6ea3e7d5dc9b3'] = 'Конфигурация обновлена';
$_MODULE['<{blockcontactinfos}shop>blockcontactinfos_f4f70727dc34561dfde1a3c529b6205c'] = 'Настройки';
$_MODULE['<{blockcontactinfos}shop>blockcontactinfos_c281f92b77ba329f692077d23636f5c9'] = 'Наименование организации';
$_MODULE['<{blockcontactinfos}shop>blockcontactinfos_dd7bf230fde8d4836917806aff6a6b27'] = 'Адрес';
$_MODULE['<{blockcontactinfos}shop>blockcontactinfos_1f8261d17452a959e013666c5df45e07'] = 'Номер телефона';
$_MODULE['<{blockcontactinfos}shop>blockcontactinfos_ce8ae9da5b7cd6c3df2929543a9af92d'] = 'E-mail';
$_MODULE['<{blockcontactinfos}shop>blockcontactinfos_c9cc8cce247e49bae79f15173ce97354'] = 'Сохранить';
$_MODULE['<{blockcontactinfos}shop>blockcontactinfos_02d4482d332e1aef3437cd61c9bcc624'] = 'Свяжитесь с нами';
$_MODULE['<{blockcontactinfos}shop>blockcontactinfos_2e006b735fbd916d8ab26978ae6714d4'] = 'Телефон:';
$_MODULE['<{blockcontactinfos}shop>blockcontactinfos_6a1e265f92087bb6dd18194833fe946b'] = 'E-mail:';
$_MODULE['<{blockcontactinfos}shop>blockcontactinfos_0b0b7d012b5a628495e0a34536ca2a1a'] = 'О FG Store';
$_MODULE['<{blockcontactinfos}shop>blockcontactinfos_4b7960b9f0125c8b3b8746d82dc53ed9'] = 'Главный портал:';
$_MODULE['<{blockcontactinfos}shop>blockcontactinfos_320abee94a07e976991e4df0d4afb319'] = 'Звоните нам:';
$_MODULE['<{blockcontactinfos}shop>blockcontactinfos_b689ff1685e45b92f4cbff6570d96647'] = 'Рабочее время:';
$_MODULE['<{blockcontactinfos}shop>blockcontactinfos_827c908049a28c0892e0fdd5a6833764'] = 'с 10 до 19 (Франция).';
