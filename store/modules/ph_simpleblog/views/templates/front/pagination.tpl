<!-- Pagination -->
<div class="bottom-pagination-content ">
	<div id="pagination_bottom" class="pagination clearfix">
	{if $start!=$stop}
		<ul class="pagination">
			{if $p != 1}
				{assign var='p_previous' value=$p-1}
				<li id="pagination_previous{if isset($paginationId)}_{$paginationId}{/if}" class="pagination_previous">
					<a href="{$link->goPage($requestPage, $p_previous)}" rel="prev">
						<i class="icon-chevron-left"></i>
						{*<b>{l s='Previous'}</b>*}
					</a>
				</li>
			{else}
				<li id="pagination_previous{if isset($paginationId)}_{$paginationId}{/if}" class="disabled pagination_previous">
						<span>
							<i class="icon-chevron-left"></i>
							{*<b>{l s='Previous'}</b>*}
						</span>
				</li>
			{/if}
			{if $start>3}
				<li><a href="{SimpleBlogPost::getPageLink(1, $type, $rewrite)}">1</a></li>
				<li class="truncate">...</li>
			{/if}
			{section name=pagination start=$start loop=$stop+1 step=1}
				{if $p == $smarty.section.pagination.index}
					<li class="active current"><span><span>{$p|escape:'htmlall':'UTF-8'}</span></span></li>
				{else}
					<li><a href="{SimpleBlogPost::getPageLink($smarty.section.pagination.index, $type, $rewrite)}"><span>{$smarty.section.pagination.index|escape:'htmlall':'UTF-8'}</span></a></li>
				{/if}
			{/section}
			{if $pages_nb>$stop+2}
				<li class="truncate">...</li>
				<li><a href="{SimpleBlogPost::getPageLink($pages_nb, $type, $rewrite)}"><span>{$pages_nb|intval}</span></a></li>
			{/if}
			{if $pages_nb > 1 AND $p != $pages_nb}
				{assign var='p_next' value=$p+1}
				<li id="pagination_next{if isset($paginationId)}_{$paginationId}{/if}" class="pagination_next">
					<a href="{$link->goPage($requestPage, $p_next)}" rel="next">
						{*<b>{l s='Next'}</b> *}
						<i class="icon-chevron-right"></i>
					</a>
				</li>
			{else}
				<li id="pagination_next{if isset($paginationId)}_{$paginationId}{/if}" class="disabled pagination_next">
						<span>
							{*<b>{l s='Next'}</b> *}
							<i class="icon-chevron-right"></i>
						</span>
				</li>
			{/if}
		</ul>
	{/if}
</div>
</div>
<!-- /Pagination -->		