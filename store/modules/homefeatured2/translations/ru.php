<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{homefeatured2}prestashop>homefeatured2_5d17bf499a1b9b2e816c99eebf0153a9'] = 'Эксклюзивные товары на главной';
$_MODULE['<{homefeatured2}prestashop>homefeatured2_6d37ec35b5b6820f90394e5ee49e8cec'] = 'Отображает эксклюзивные товары в центральной колонке главной страницы.';
$_MODULE['<{homefeatured2}prestashop>homefeatured2_fddb8a1881e39ad11bfe0d0aca5becc3'] = 'Количество товаров неверно. Должно быть положительное число.';
$_MODULE['<{homefeatured2}prestashop>homefeatured2_c284a59996a4e984b30319999a7feb1d'] = 'Неправильный ID категории. Пожалуйста, выберите ID существующей категории.';
$_MODULE['<{homefeatured2}prestashop>homefeatured2_fd2608d329d90e9a49731393427d0a5a'] = 'Неверное значение для флага "случайно".';
$_MODULE['<{homefeatured2}prestashop>homefeatured2_6af91e35dff67a43ace060d1d57d5d1a'] = 'Настройки обновлены';
$_MODULE['<{homefeatured2}prestashop>homefeatured2_f4f70727dc34561dfde1a3c529b6205c'] = 'Настройки';
$_MODULE['<{homefeatured2}prestashop>homefeatured2_abc877135a96e04fc076becb9ce6fdfa'] = 'Чтобы добавить товары на главную страницу, добавьте их в соответствующую категорию (по умолчанию: "Главная").';
$_MODULE['<{homefeatured2}prestashop>homefeatured2_d44168e17d91bac89aab3f38d8a4da8e'] = 'Число отображаемых товаров';
$_MODULE['<{homefeatured2}prestashop>homefeatured2_1b73f6b70a0fcd38bbc6a6e4b67e3010'] = 'Задать число товаров, отображаемых на главной странице (стандартно: 8).';
$_MODULE['<{homefeatured2}prestashop>homefeatured2_b773a38d8c456f7b24506c0e3cd67889'] = 'Отображать товары из этой категории';
$_MODULE['<{homefeatured2}prestashop>homefeatured2_0db2d53545e2ee088cfb3f45e618ba68'] = 'Выберите ID категории товаров, которые вы хотите отобразить на главной странице (по умолчанию: 2 для "Главной").';
$_MODULE['<{homefeatured2}prestashop>homefeatured2_49417670345173e7b95018b7bf976fc7'] = 'Отображать в случайном порядке';
$_MODULE['<{homefeatured2}prestashop>homefeatured2_3c12c1068fb0e02fe65a6c4fc40bc29a'] = 'Включите, если хотите отображать товары в случайном порядке (по умолчанию: нет).';
$_MODULE['<{homefeatured2}prestashop>homefeatured2_93cba07454f06a4a960172bbd6e2a435'] = 'Да';
$_MODULE['<{homefeatured2}prestashop>homefeatured2_bafd7322c6e97d25b6299b5d6fe8920b'] = 'Нет';
$_MODULE['<{homefeatured2}prestashop>homefeatured2_c9cc8cce247e49bae79f15173ce97354'] = 'Сохранить';
$_MODULE['<{homefeatured2}prestashop>homefeatured2_ca7d973c26c57b69e0857e7a0332d545'] = 'Эксклюзивные товары';
$_MODULE['<{homefeatured2}prestashop>homefeatured2_03c2e7e41ffc181a4e84080b4710e81e'] = 'Новое';
$_MODULE['<{homefeatured2}prestashop>homefeatured2_d3da97e2d9aee5c8fbe03156ad051c99'] = 'Еще';
$_MODULE['<{homefeatured2}prestashop>homefeatured2_4351cfebe4b61d8aa5efa1d020710005'] = 'См.';
$_MODULE['<{homefeatured2}prestashop>homefeatured2_2d0f6b8300be19cf35e89e66f0677f95'] = 'В корзину';
$_MODULE['<{homefeatured2}prestashop>homefeatured2_e0e572ae0d8489f8bf969e93d469e89c'] = 'Пусто';
$_MODULE['<{homefeatured2}prestashop>tab_2cc1943d4c0b46bfcf503a75c44f988b'] = 'Эксклюзивное';
$_MODULE['<{homefeatured2}prestashop>homefeatured2_d505d41279039b9a68b0427af27705c6'] = 'В данный момент нет эксклюзивных товаров.';


return $_MODULE;
