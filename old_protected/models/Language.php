<?php
class Language extends CActiveRecord {
	public $tlanguage = "";
	//public $lang = "";

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    public function tableName() {
        return 'language';
    }
	// cod Код языка ico Путь к иконке name Наименование языка на языке def
    public function getAll() {
		$list = self::model()->findAll();
		$arr = Array();
		foreach ($list as $lab) {
			$arr[$lab['cod']]['cod']  = $lab['cod'];
			$arr[$lab['cod']]['ico']  = $lab['ico'];
			$arr[$lab['cod']]['name'] = $lab['name'];
			$arr[$lab['cod']]['def']  = $lab['def'];
		};
		return $arr;
    }
	//
	//
	public function getLanguage() {
		if ( isset($_COOKIE['language'])) { $this->tlanguage = $_COOKIE['language']; return $this->tlanguage; };
		$aaa = $this->getAll();
		foreach ($aaa as $key => $val) {
			if ($val['def'] == "Y") {$this->tlanguage = $val['cod']; return $this->tlanguage; };
		};
		return "ru";
    }
	//
}