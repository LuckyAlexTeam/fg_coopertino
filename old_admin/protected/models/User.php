<?php

/**
 * This is the model class for table "{{users}}".
 *
 * The followings are the available columns in table '{{users}}':
 * @property string $id
 * @property string $login
 * @property string $password
 * @property string $email
 * @property integer $status
 * @property integer $subscribe
 * @property string $created
 * @property string $activationKey
 *
 * The followings are the available model relations:
 * @property Messages[] $messages
 */
class User extends CActiveRecord {

    public $rememberMe;
    public $key;
    public $captcha;
    public $password2;
    private $_identity;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Users the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
	 
    public function tableName() {
        return 'users';
    }
	
    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
		// Вы должны только определить правила для тех атрибутов, которые будут получать пользовательский ввод.
        return array(
		/*
            // rememberMe needs to be a boolean
            array('rememberMe', 'boolean'),
            // password needs to be authenticated
            array('password', 'authenticate', 'on' => 'login'),
            array('login, password, email, phone', 'required'),
//            array('status, subscribe', 'numerical', 'integerOnly' => true, 'except' => array('activation')),
            array('login, password, email, password2', 'length', 'max' => 255),
            array('phone', 'length', 'max' => 15),
            array('created', 'length', 'max' => 11),
            array('email', 'email'),
            // 'on'=>'reg'
            array('password2', 'compare', 'compareAttribute' => 'password', 'on' => 'registration, reset'), //
            array('captcha', 'captcha', 'allowEmpty' => !extension_loaded('gd'), 'on' => 'registration, reset'), //
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, login, password, email, phone, status, subscribe, created, changed', 'safe', 'on' => 'search, registration, reset'),
			array('id, login, password, fio, phone, rol, status'),
			array('login, password, fio'),
		*/
			//array('login, password')
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'messages' => array(self::HAS_MANY, 'Messages', 'id_user'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
	public function attributeLabels() {
		return array(
			'id' => 'id',
			'login' => 'Логин',
			'password' => 'Пароль',
			'fio' => 'Ф.И.О.',
			'phone' => 'Телефон',
			'rol' => 'Роль',
			'status' => 'Статус',
		);
	}

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('login', $this->login, true);
        $criteria->compare('password', $this->password, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('phone', $this->phone, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('subscribe', $this->subscribe);
        $date_search = $this->date_search($this->created);
        $criteria->addBetweenCondition('created', $date_search['from'], $date_search['untill']); 
        $date_search = $this->date_search($this->changed);
        $criteria->addBetweenCondition('changed', $date_search['from'], $date_search['untill']); 

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function getAll() {//$add_blank = FALSE
        $list = self::model()->findAll();
		//return $list;
		//
        $array = array();
        foreach ($list as $value) { // $key =>
			//print "<br>key=" . $key . " , val=" . $value; 
			//print "<br>+++++***<br>";
			//var_dump($value);
			//print "<br>***+++++<br>";
			$rab = array ("id"=>$value->id, "login"=>$value->login, "password"=>$value->password, "name"=>$value->name, "rol"=>$value->rol, "status"=>$value->status);
            //$array[$value->id] = $value->login . $value->fio;
			$array[$value->id] = $rab;
        }
		//print "<br>=== array<br>";
		//var_dump($array);
		//print "<br>===<br>";
        return $array;
		//
    }
	/*
	public function insert() { // $arr
		//$ins = new CDbCommand;
		return "11";
		
	}
	*/
   public static function getLogPas($log, $pas) {//$add_blank = FALSE
		$criteria = new CDbCriteria;
		$criteria->condition = " login = '{$log}' AND password = '{$pas}' ";
		$list = self::model()->findAll($criteria);
		$rab = NULL;
        foreach ($list as $value) { // $key =>
			$rab = array ($value->login => $value->password );
        };
        return $rab;
    }
	
	public static function getLog($log) {
		$criteria = new CDbCriteria;
		$criteria->condition = " login = '{$log}' ";
		$list = self::model()->findAll($criteria);
        $rab = array();
        foreach ($list as $value) {
			$rab = array ("id"=>$value->id, "login"=>$value->login, "password"=>$value->password, "name"=>$value->name, "rol"=>$value->rol, "status"=>$value->status);
        };
        return $rab;
    }
	
    public static function getIdByEmail($email) {
        $model = self::model()->findByAttributes(array('email' => $email)); ;  
        if ($model)
            return $model->id;
        return '';
    }

    public static function getEmail($id) {
        $model = self::model()->findByPk($id);  
        if ($model)
            return $model->email;
        return '';
    }

    public static function getStatus($id) {
        $model = self::model()->findByPk($id);  
        if ($model)
            return $model->status;
        return '';
    }

    protected function beforeSave() {
        if ($this->isNewRecord) {
            // Хешировать пароль
           //$this->password = $this->hashPassword($this->password);
		   $this->password = $this->password;
        } 
        return parent::beforeSave();
    }

    public function hashPassword($password) {
		return $password;
        //return md5($password);
    }

    /**
     * Собственное правило для проверки
     * Данный метод является связующем звеном с UserIdentity
     *
     * @param $attribute
     * @param $params
     */
    public function authenticate($attribute, $params) {
        // Проверяем были ли ошибки в других правилах валидации.
        // если были - нет смысла выполнять проверку
        if (!$this->hasErrors()) {
            // Создаем экземпляр класса UserIdentity
            // и передаем в его конструктор введенный пользователем логин и пароль (с формы)
            $identity = new UserIdentity($this->login, $this->password);
            // Выполняем метод authenticate (о котором мы с вами говорили пару абзацев назад)
            // Он у нас проверяет существует ли такой пользователь и возвращает ошибку (если она есть)
            // в $identity->errorCode
            $identity->authenticate();

            // Теперь мы проверяем есть ли ошибка..    
            switch ($identity->errorCode) {
                // Если ошибки нету...
                case UserIdentity::ERROR_NONE: {
                        // Данная строчка говорит что надо выдать пользователю
                        // соответствующие куки о том что он зарегистрирован, срок действий
                        // у которых указан вторым параметром. 
                        Yii::app()->user->login($identity, 0);
                        break;
                    }
                case UserIdentity::ERROR_USERNAME_INVALID: {
                        // Если логин был указан наверно - создаем ошибку
                        $this->addError('login', 'Пользователь не существует!');
                        break;
                    }
                case UserIdentity::ERROR_PASSWORD_INVALID: {
                        // Если пароль был указан наверно - создаем ошибку
                        $this->addError('password', 'Вы указали неверный пароль!');
                        break;
                    }
            }
        }
    }

    /**
     * Logs in the user using the given username and password in the model.
     * @return boolean whether login is successful
     */
    public function login() {
        if ($this->_identity === null) {
            $this->_identity = new UserIdentity($this->login, $this->password);
            $this->_identity->authenticate();
        }
        if ($this->_identity->errorCode === UserIdentity::ERROR_NONE) {
            $duration = $this->rememberMe ? 3600 * 24 * 30 : 0; // 30 days
            Yii::app()->user->login($this->_identity, $duration);
            return true;
        }
        else
            return false;
    }

}