<?php

/**
 * This is the model class for table "brand_location".
 *
 * The followings are the available columns in table 'brand_location':
 * @property integer $id
 * @property string $name
 * @property integer $parent
 * @property integer $sort_num
 *
 * The followings are the available model relations:
 * @property BrandLocation $parent0
 * @property BrandLocation[] $brandLocations
 * @property BrandLocationToUser[] $brandLocationToUsers
 */
class BaseBrandLocation extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return BrandLocation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'brand_location';
	}

	/**
	 * @return array validation rules for model attributes.
	 */ 
	 
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('parent, sort_num', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, parent, sort_num', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'parent0' => array(self::BELONGS_TO, 'BrandLocation', 'parent'),
			'brandLocations' => array(self::HAS_MANY, 'BrandLocation', 'parent'),
			'brandLocationToUsers' => array(self::HAS_MANY, 'BrandLocationToUser', 'brand_location_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'parent' => 'Parent',
			'sort_num' => 'Sort Num',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('parent',$this->parent);
		$criteria->compare('sort_num',$this->sort_num);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}