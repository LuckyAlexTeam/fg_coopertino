<?php

/**
 * This is the model class for table "agency_specialization_to_form".
 *
 * The followings are the available columns in table 'agency_specialization_to_form':
 * @property integer $id
 * @property integer $agency_specialization_id
 * @property integer $agency_form_id
 *
 * The followings are the available model relations:
 * @property AgencyForm $agencyForm
 * @property AgencyTypeSpecialization $agencySpecialization
 */
class BaseAgencySpecializationToForm extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AgencySpecializationToForm the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'agency_specialization_to_form';
	}

	/**
	 * @return array validation rules for model attributes.
	 */ 
	 
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('agency_specialization_id, agency_form_id', 'required'),
			array('agency_specialization_id, agency_form_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, agency_specialization_id, agency_form_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'agencyForm' => array(self::BELONGS_TO, 'AgencyForm', 'agency_form_id'),
			'agencySpecialization' => array(self::BELONGS_TO, 'AgencyTypeSpecialization', 'agency_specialization_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'agency_specialization_id' => 'Agency Specialization',
			'agency_form_id' => 'Agency Form',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('agency_specialization_id',$this->agency_specialization_id);
		$criteria->compare('agency_form_id',$this->agency_form_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}