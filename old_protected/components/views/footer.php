<a href="/write">
	<i class="fa fa-4x fa-pencil-square-o" aria-hidden="true"></i>
	<br/>write to us
</a>
<a href="/about">
	<i class="fa fa-4x fa-info-circle" aria-hidden="true"></i>
	<br/>about us
</a>
<a href="/services">
	<i class="fa fa-4x fa-cogs" aria-hidden="true"></i>
	<br/>our services
</a>
<a href="/mentors">
	<i class="fa fa-4x fa-users" aria-hidden="true"></i>
	<br/>our mentors
</a>
<a href="/faq">
	<i class="fa fa-4x fa-question-circle" aria-hidden="true"></i>
	<br/>faq
</a>
<a href="/contacts">
	<i class="fa fa-4x fa-envelope-o" aria-hidden="true"></i>
	<br/>contacts
</a>